unit Unit_tool;

interface
uses
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart,FFTs,Complexs;

type
  Tfbytes = array[0..3] of byte;//字符串数组

function BytesToSingle(fdata : Pointer; mode : Integer):Single;
function SingleToBytes(f:Single):Tfbytes;
procedure dbg_msg(msg:string);
function GetCurTime(dis_time_formate : Integer):string;







implementation

procedure dbg_msg(msg:string);
begin
{$DEFINE CONSOLE}
{$IFDEF CONSOLE}
//    writeln(msg);
{$ENDIF}
end;

function BytesToSingle(fdata : Pointer; mode : Integer):Single;
var
    bSingle:array[0..3] of byte;
    pdata : PByte;
    pdata1 : PChar;
    s : string;
    temp:Single;
begin
    pdata := pbyte(fdata);
    pdata1 := PChar(fdata);
    Result := 0;
    if mode <> 0 then begin
        //s := format('%x', [pdata1[0]]) + format('%x', [pdata1[1]]) + format('%x', [pdata1[2]]) + format('%x', [pdata1[3]]);
        {s:= IntToHex(Byte(pdata1[0]), 2)+IntToHex(Byte(pdata1[1]), 2)+IntToHex(Byte(pdata1[2]), 2)+IntToHex(Byte(pdata1[3]), 2);
        ShowMessage('1 s='+s);
        if (TryStrToFloat(s, temp) = True) then begin
            Result := temp;
        end; }
        bSingle[3]   :=   pdata^;inc(pbyte(pdata));
        bSingle[2]   :=   pdata^;inc(pbyte(pdata));
        bSingle[1]   :=   pdata^;inc(pbyte(pdata));
        bSingle[0]   :=   pdata^;inc(pbyte(pdata));
    end
    else begin
        //s := format('%x', [pdata1[3]]) + format('%x', [pdata1[2]]) + format('%x', [pdata1[1]]) + format('%x', [pdata1[0]]);
        {s := IntToHex(Byte(pdata1[3]), 2)+IntToHex(Byte(pdata1[2]), 2)+IntToHex(Byte(pdata1[1]), 2)+IntToHex(Byte(pdata1[0]), 2);
        ShowMessage('0 s='+s);
        if (TryStrToFloat(s, temp) = True) then begin
            Result := temp;
        end; }
        bSingle[0]   :=   pdata^;inc(pbyte(pdata));
        bSingle[1]   :=   pdata^;inc(pbyte(pdata));
        bSingle[2]   :=   pdata^;inc(pbyte(pdata));
        bSingle[3]   :=   pdata^;inc(pbyte(pdata));
    end;
    Result := PSingle(@bSingle[0])^;
end;

//2、Single转为字节数组
function SingleToBytes(f:Single):Tfbytes;
var
  bSingle:array[0..3] of byte;
begin
  Move(f,bSingle,sizeof(f));
  //Result := bSingle;
  Result[0]   :=   bSingle[0];
  Result[1]   :=   bSingle[1];
  Result[2]   :=   bSingle[2];
  Result[3]   :=   bSingle[3];
end;

function GetCurTime(dis_time_formate : Integer):string;
begin
   if dis_time_formate = 0 then
      Result := '[' + formatdatetime('mm/dd hh:mm:ss:zzz',now) + '] '
   else if dis_time_formate = 1 then
      Result := '[' + formatdatetime('hh:mm:ss:zzz',now) + '] '
   else if dis_time_formate = 2 then
      Result := '[' + formatdatetime('hh:mm:ss', now) + '] '
   else if dis_time_formate = 10 then
      Result := formatdatetime('mm/dd hh:mm:ss:zzz',now)
   else if dis_time_formate = 11 then
      Result := formatdatetime('hh:mm:ss:zzz',now)
   else if dis_time_formate = 12 then
      Result := formatdatetime('hh:mm:ss', now);
end;

end.
