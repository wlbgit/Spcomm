object Form1: TForm1
  Left = 203
  Top = 28
  Width = 695
  Height = 745
  HorzScrollBar.Range = 633
  VertScrollBar.Range = 414
  AutoScroll = False
  Caption = 'Spcom V7.3'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Courier New'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 14
  object Splitter1: TSplitter
    Left = 686
    Top = 0
    Width = 1
    Height = 545
    Align = alRight
    OnPaint = Splitter1Paint
  end
  object Splitter2: TSplitter
    Left = 0
    Top = 545
    Width = 687
    Height = 2
    Cursor = crVSplit
    Align = alBottom
    OnPaint = Splitter2Paint
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 547
    Width = 687
    Height = 144
    Align = alBottom
    Constraints.MinHeight = 70
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -1
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object GroupBox5: TGroupBox
      Left = 2
      Top = 125
      Width = 683
      Height = 17
      Align = alBottom
      TabOrder = 0
      object Label1: TLabel
        Left = 1
        Top = 3
        Width = 0
        Height = 14
        AutoSize = False
        Color = clBtnFace
        ParentColor = False
      end
      object StatusBar1: TStatusBar
        Left = 2
        Top = -3
        Width = 679
        Height = 18
        Panels = <
          item
            Text = 'S:0'
            Width = 100
          end
          item
            Text = 'R:0'
            Width = 100
          end
          item
            Text = 'COM '#26410#25171#24320
            Width = 200
          end
          item
            BiDiMode = bdLeftToRight
            ParentBiDiMode = False
            Text = 'https://gitee.com/wlbgit/Spcomm'
            Width = 100
          end>
        SimpleText = 'werrqwer'
        OnMouseMove = StatusBar1MouseMove
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 5
      Width = 229
      Height = 120
      Align = alLeft
      Caption = 'Panel1'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object shape1: TShape
        Left = 145
        Top = 14
        Width = 20
        Height = 22
        Hint = #21047#26032#20018#21475#21015#34920
        Brush.Color = clGray
        ParentShowHint = False
        Shape = stCircle
        ShowHint = True
        OnMouseDown = shape1MouseDown
      end
      object Label4: TLabel
        Left = 126
        Top = 75
        Width = 35
        Height = 14
        Caption = 'ms/'#27425
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
      end
      object Edit2: TEdit
        Left = 85
        Top = 70
        Width = 39
        Height = 22
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Text = '1000'
        OnKeyPress = Edit2KeyPress
      end
      object ComboBox4: TComboBox
        Left = 69
        Top = 46
        Width = 75
        Height = 22
        Style = csDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 14
        ItemIndex = 0
        ParentFont = False
        TabOrder = 1
        Text = 'None'
        OnChange = ComboBox4Change
        Items.Strings = (
          'None'
          'Odd'
          'Even'
          'Mark'
          'Space')
      end
      object ComboBox3: TComboBox
        Left = 7
        Top = 46
        Width = 60
        Height = 22
        Style = csDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 14
        ItemIndex = 0
        ParentFont = False
        TabOrder = 2
        Text = '8'
        OnChange = ComboBox3Change
        Items.Strings = (
          '8'
          '7'
          '6'
          '5')
      end
      object ComboBox2: TComboBox
        Left = 69
        Top = 14
        Width = 75
        Height = 22
        Hint = '1'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 14
        ItemIndex = 0
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Text = '9600'
        OnChange = ComboBox2Change
        OnClick = ComboBox2Click
        Items.Strings = (
          '9600'
          '14400'
          '19200'
          '28800'
          '38400'
          '56000'
          '57600'
          '115200'
          '230400'
          '460800'
          '921600'
          '1000000'
          '1500000'
          '2000000'
          '3000000'
          'Custom')
      end
      object ComboBox1: TComboBox
        Left = 7
        Top = 14
        Width = 60
        Height = 22
        Style = csDropDownList
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 14
        ItemIndex = 0
        ParentFont = False
        TabOrder = 4
        Text = 'COM1'
        OnChange = ComboBox1Change
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4'
          'COM5'
          'COM6'
          'COM7'
          'COM8'
          'COM9')
      end
      object CheckBox7: TCheckBox
        Left = 125
        Top = 96
        Width = 39
        Height = 17
        Caption = #39'\n'#39
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnClick = CheckBox7Click
      end
      object CheckBox6: TCheckBox
        Left = 85
        Top = 96
        Width = 37
        Height = 17
        Caption = #39'\r'#39
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        OnClick = CheckBox6Click
      end
      object CheckBox4: TCheckBox
        Left = 6
        Top = 72
        Width = 77
        Height = 22
        Caption = #23450#26102#21457#36865
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        OnClick = CheckBox4Click
      end
      object CheckBox2: TCheckBox
        Left = 6
        Top = 96
        Width = 73
        Height = 17
        Caption = 'HEX'#21457#36865
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        OnClick = CheckBox2Click
      end
      object Button2: TButton
        Left = 165
        Top = 75
        Width = 60
        Height = 30
        Caption = #21457' '#36865
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clTeal
        Font.Height = -15
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 9
        OnClick = Button2Click
      end
      object Button1: TButton
        Left = 167
        Top = 16
        Width = 60
        Height = 20
        Caption = #25171#24320#20018#21475
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        OnClick = Button1Click
      end
    end
    object Panel2: TPanel
      Left = 231
      Top = 5
      Width = 454
      Height = 120
      Align = alClient
      Caption = 'Panel2'
      TabOrder = 2
      object Label33: TLabel
        Left = 38
        Top = 27
        Width = 84
        Height = 14
        Caption = #33267#26411#23614#21152#26657#39564
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
      end
      object Label32: TLabel
        Left = 5
        Top = 27
        Width = 14
        Height = 14
        Caption = #31532
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
      end
      object check_result: TLabel
        Left = 279
        Top = 27
        Width = 7
        Height = 14
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
      end
      object Edit4: TEdit
        Left = 19
        Top = 24
        Width = 19
        Height = 22
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 2
        ParentFont = False
        TabOrder = 0
        Text = '1'
        OnChange = Edit4Change
        OnKeyPress = Edit4KeyPress
      end
      object ComboBox5: TComboBox
        Left = 123
        Top = 24
        Width = 85
        Height = 22
        Hint = 'None'
        Style = csDropDownList
        DropDownCount = 16
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 14
        ItemIndex = 0
        MaxLength = 32
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = 'None'
        OnChange = ComboBox5Change
        Items.Strings = (
          'None'
          'Xor'
          'Add'
          'CRC16'
          'CRC16(8005)'
          'CRC16(Modbus)'
          'CRC-CCITT(Sick)'
          'CRC-CCITT(XModem)'
          'CRC-CCITT(0xFFFF)'
          'CRC-CCITT(0x1D0F)'
          'CRC-CCITT(Kermit)'
          'CRC-DNP'
          'CRC-32')
      end
      object CheckBox62: TCheckBox
        Left = 139
        Top = 7
        Width = 79
        Height = 17
        Caption = 'UTF8'#26174#31034
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = CheckBox62Click
      end
      object CheckBox60: TCheckBox
        Left = 215
        Top = 27
        Width = 64
        Height = 17
        Caption = #22823#23567#31471
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        Visible = False
        OnClick = CheckBox60Click
      end
      object CheckBox53: TCheckBox
        Left = 293
        Top = 7
        Width = 53
        Height = 19
        Caption = #32622#39030
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = CheckBox53Click
      end
      object CheckBox5: TCheckBox
        Left = 218
        Top = 7
        Width = 75
        Height = 17
        Caption = #26242#20572#25509#25910
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        OnClick = CheckBox5Click
      end
      object CheckBox3: TCheckBox
        Left = 9
        Top = 7
        Width = 60
        Height = 17
        Caption = #26102#38388#25139
        Checked = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 6
      end
      object CheckBox1: TCheckBox
        Left = 69
        Top = 7
        Width = 70
        Height = 17
        Caption = 'HEX'#26174#31034
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        OnClick = CheckBox1Click
      end
      object Button7: TButton
        Left = 356
        Top = 7
        Width = 45
        Height = 18
        Caption = #20445#23384
        Font.Charset = GB2312_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        OnClick = Button7Click
      end
      object Button66: TButton
        Left = 356
        Top = 26
        Width = 45
        Height = 18
        Caption = #25240#21472#8595
        Font.Charset = GB2312_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        OnClick = Button66Click
      end
      object Button13: TButton
        Left = 399
        Top = 26
        Width = 45
        Height = 18
        Caption = #25193#23637#8594
        Font.Charset = GB2312_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        OnClick = Button13Click
      end
      object BitBtn1: TBitBtn
        Left = 399
        Top = 7
        Width = 45
        Height = 18
        Caption = #28165#38500
        Font.Charset = GB2312_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
        OnClick = BitBtn1Click
      end
      object GroupBoxinput: TGroupBox
        Left = 1
        Top = 52
        Width = 452
        Height = 67
        Align = alBottom
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 12
        object Memo2: TMemo
          Left = 2
          Top = 16
          Width = 448
          Height = 49
          Hint = #21452#20987#21487#21457#36865#25991#20214
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          PopupMenu = pm1
          ShowHint = True
          TabOrder = 0
          OnChange = Memo2Change
          OnDblClick = Memo2DblClick
          OnKeyPress = Memo2KeyPress
        end
      end
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 441
    Height = 545
    Align = alClient
    ParentShowHint = False
    PopupMenu = PopupMenu1
    ReadOnly = True
    ScrollBars = ssVertical
    ShowHint = False
    TabOrder = 1
    WordWrap = False
  end
  object PageControl2: TPageControl
    Left = 441
    Top = 0
    Width = 245
    Height = 545
    ActivePage = TabSheet1
    Align = alRight
    Constraints.MinWidth = 245
    ParentShowHint = False
    ShowHint = False
    TabOrder = 2
    Visible = False
    object TabSheet1: TTabSheet
      Caption = 'String'
      object GroupBox8: TGroupBox
        Left = 0
        Top = 0
        Width = 237
        Height = 28
        Align = alTop
        TabOrder = 0
        object Label9: TLabel
          Left = 136
          Top = 8
          Width = 14
          Height = 14
          Caption = 'ms'
        end
        object CheckBox8: TCheckBox
          Left = 4
          Top = 8
          Width = 83
          Height = 15
          Caption = #24490#29615#21457#36865
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -8
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = CheckBox8Click
        end
        object Edit3: TEdit
          Left = 95
          Top = 6
          Width = 40
          Height = 22
          TabOrder = 1
          Text = '1000'
          OnKeyPress = Edit3KeyPress
        end
        object BitBtn2: TBitBtn
          Left = 167
          Top = 9
          Width = 58
          Height = 15
          Caption = #20840#28165
          TabOrder = 2
          OnClick = BitBtn2Click
        end
      end
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 28
        Width = 237
        Height = 488
        VertScrollBar.Range = 2200
        VertScrollBar.Smooth = True
        VertScrollBar.Tracking = True
        Align = alClient
        AutoScroll = False
        TabOrder = 1
        OnMouseWheel = ScrollBox1MouseWheel
        object GroupBox9: TGroupBox
          Left = 0
          Top = 0
          Width = 216
          Height = 2200
          Align = alClient
          TabOrder = 0
          object lbl1: TLabel
            Left = 2
            Top = 7
            Width = 22
            Height = 13
            Caption = 'HEX'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -7
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label10: TLabel
            Left = 80
            Top = 8
            Width = 70
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = #23383#31526#20018
          end
          object Label11: TLabel
            Left = 184
            Top = 8
            Width = 33
            Height = 17
            AutoSize = False
            Caption = #21457#36865
          end
          object chk1: TCheckBox
            Left = 4
            Top = 25
            Width = 17
            Height = 17
            TabOrder = 0
          end
          object TESEND01: TEdit
            Left = 21
            Top = 22
            Width = 173
            Height = 22
            TabOrder = 1
          end
          object TESEND02: TEdit
            Left = 21
            Top = 44
            Width = 173
            Height = 22
            TabOrder = 2
          end
          object TESEND03: TEdit
            Left = 21
            Top = 66
            Width = 173
            Height = 22
            TabOrder = 3
          end
          object TESEND04: TEdit
            Left = 21
            Top = 88
            Width = 173
            Height = 22
            TabOrder = 4
          end
          object TESEND05: TEdit
            Left = 21
            Top = 110
            Width = 173
            Height = 22
            TabOrder = 5
          end
          object TESEND06: TEdit
            Left = 21
            Top = 132
            Width = 173
            Height = 22
            TabOrder = 6
          end
          object TESEND07: TEdit
            Left = 21
            Top = 154
            Width = 173
            Height = 22
            TabOrder = 7
          end
          object TESEND08: TEdit
            Left = 21
            Top = 176
            Width = 173
            Height = 22
            TabOrder = 8
          end
          object TESEND09: TEdit
            Left = 21
            Top = 198
            Width = 173
            Height = 22
            TabOrder = 9
          end
          object TESEND10: TEdit
            Left = 21
            Top = 220
            Width = 173
            Height = 22
            TabOrder = 10
          end
          object TESEND11: TEdit
            Left = 21
            Top = 242
            Width = 173
            Height = 22
            TabOrder = 11
          end
          object TESEND13: TEdit
            Left = 21
            Top = 286
            Width = 173
            Height = 22
            TabOrder = 12
          end
          object CheckBox9: TCheckBox
            Left = 4
            Top = 47
            Width = 17
            Height = 17
            TabOrder = 13
          end
          object CheckBox10: TCheckBox
            Left = 4
            Top = 69
            Width = 17
            Height = 17
            TabOrder = 14
          end
          object CheckBox11: TCheckBox
            Left = 4
            Top = 91
            Width = 17
            Height = 17
            TabOrder = 15
          end
          object CheckBox12: TCheckBox
            Left = 4
            Top = 113
            Width = 17
            Height = 17
            TabOrder = 16
          end
          object CheckBox13: TCheckBox
            Left = 4
            Top = 135
            Width = 17
            Height = 17
            TabOrder = 17
          end
          object CheckBox14: TCheckBox
            Left = 4
            Top = 157
            Width = 17
            Height = 17
            TabOrder = 18
          end
          object CheckBox15: TCheckBox
            Left = 4
            Top = 179
            Width = 17
            Height = 17
            TabOrder = 19
          end
          object CheckBox16: TCheckBox
            Left = 4
            Top = 201
            Width = 17
            Height = 17
            TabOrder = 20
          end
          object CheckBox17: TCheckBox
            Left = 4
            Top = 223
            Width = 17
            Height = 17
            TabOrder = 21
          end
          object CheckBox18: TCheckBox
            Left = 4
            Top = 245
            Width = 17
            Height = 17
            TabOrder = 22
          end
          object CheckBox19: TCheckBox
            Left = 4
            Top = 267
            Width = 17
            Height = 17
            TabOrder = 23
          end
          object CheckBox20: TCheckBox
            Left = 4
            Top = 289
            Width = 17
            Height = 17
            TabOrder = 24
          end
          object CheckBox21: TCheckBox
            Left = 4
            Top = 311
            Width = 17
            Height = 17
            TabOrder = 25
          end
          object CheckBox22: TCheckBox
            Left = 4
            Top = 333
            Width = 17
            Height = 17
            TabOrder = 26
          end
          object CheckBox23: TCheckBox
            Left = 4
            Top = 355
            Width = 17
            Height = 17
            TabOrder = 27
          end
          object TESEND12: TEdit
            Left = 21
            Top = 264
            Width = 173
            Height = 22
            TabOrder = 28
          end
          object TESEND14: TEdit
            Left = 21
            Top = 308
            Width = 173
            Height = 22
            TabOrder = 29
          end
          object TESEND15: TEdit
            Left = 21
            Top = 330
            Width = 173
            Height = 22
            TabOrder = 30
          end
          object TESEND16: TEdit
            Left = 21
            Top = 352
            Width = 173
            Height = 22
            TabOrder = 31
          end
          object BTSEND01: TButton
            Left = 192
            Top = 22
            Width = 22
            Height = 22
            Caption = '1'
            TabOrder = 32
            OnClick = BTSEND01Click
          end
          object BTSEND02: TButton
            Left = 192
            Top = 44
            Width = 22
            Height = 22
            Caption = '2'
            TabOrder = 33
            OnClick = BTSEND02Click
          end
          object BTSEND03: TButton
            Left = 192
            Top = 66
            Width = 22
            Height = 22
            Caption = '3'
            TabOrder = 34
            OnClick = BTSEND03Click
          end
          object BTSEND04: TButton
            Left = 192
            Top = 88
            Width = 22
            Height = 22
            Caption = '4'
            TabOrder = 35
            OnClick = BTSEND04Click
          end
          object BTSEND05: TButton
            Left = 192
            Top = 110
            Width = 22
            Height = 22
            Caption = '5'
            TabOrder = 36
            OnClick = BTSEND05Click
          end
          object BTSEND06: TButton
            Left = 192
            Top = 132
            Width = 22
            Height = 22
            Caption = '6'
            TabOrder = 37
            OnClick = BTSEND06Click
          end
          object BTSEND07: TButton
            Left = 192
            Top = 154
            Width = 22
            Height = 22
            Caption = '7'
            TabOrder = 38
            OnClick = BTSEND07Click
          end
          object BTSEND08: TButton
            Left = 192
            Top = 176
            Width = 22
            Height = 22
            Caption = '8'
            TabOrder = 39
            OnClick = BTSEND08Click
          end
          object BTSEND09: TButton
            Left = 192
            Top = 198
            Width = 22
            Height = 22
            Caption = '9'
            TabOrder = 40
            OnClick = BTSEND09Click
          end
          object BTSEND10: TButton
            Left = 192
            Top = 220
            Width = 22
            Height = 22
            Caption = '10'
            TabOrder = 41
            OnClick = BTSEND10Click
          end
          object BTSEND11: TButton
            Left = 192
            Top = 242
            Width = 22
            Height = 22
            Caption = '11'
            TabOrder = 42
            OnClick = BTSEND11Click
          end
          object BTSEND12: TButton
            Left = 192
            Top = 264
            Width = 22
            Height = 22
            Caption = '12'
            TabOrder = 43
            OnClick = BTSEND12Click
          end
          object BTSEND13: TButton
            Left = 192
            Top = 286
            Width = 22
            Height = 22
            Caption = '13'
            TabOrder = 44
            OnClick = BTSEND13Click
          end
          object BTSEND14: TButton
            Left = 192
            Top = 308
            Width = 22
            Height = 22
            Caption = '14'
            TabOrder = 45
            OnClick = BTSEND14Click
          end
          object BTSEND15: TButton
            Left = 192
            Top = 330
            Width = 22
            Height = 22
            Caption = '15'
            TabOrder = 46
            OnClick = BTSEND15Click
          end
          object BTSEND16: TButton
            Left = 192
            Top = 352
            Width = 22
            Height = 22
            Caption = '16'
            TabOrder = 47
            OnClick = BTSEND16Click
          end
          object TESEND17: TEdit
            Left = 21
            Top = 374
            Width = 173
            Height = 22
            TabOrder = 48
          end
          object TESEND18: TEdit
            Left = 21
            Top = 396
            Width = 173
            Height = 22
            TabOrder = 49
          end
          object TESEND19: TEdit
            Left = 21
            Top = 418
            Width = 173
            Height = 22
            TabOrder = 50
          end
          object TESEND20: TEdit
            Left = 21
            Top = 440
            Width = 173
            Height = 22
            TabOrder = 51
          end
          object TESEND21: TEdit
            Left = 21
            Top = 462
            Width = 173
            Height = 22
            TabOrder = 52
          end
          object TESEND22: TEdit
            Left = 21
            Top = 484
            Width = 173
            Height = 22
            TabOrder = 53
          end
          object TESEND23: TEdit
            Left = 21
            Top = 506
            Width = 173
            Height = 22
            TabOrder = 54
          end
          object TESEND24: TEdit
            Left = 21
            Top = 528
            Width = 173
            Height = 22
            TabOrder = 55
          end
          object TESEND25: TEdit
            Left = 21
            Top = 550
            Width = 173
            Height = 22
            TabOrder = 56
          end
          object TESEND26: TEdit
            Left = 21
            Top = 572
            Width = 173
            Height = 22
            TabOrder = 57
          end
          object TESEND27: TEdit
            Left = 21
            Top = 594
            Width = 173
            Height = 22
            TabOrder = 58
          end
          object BTSEND17: TButton
            Left = 192
            Top = 374
            Width = 22
            Height = 22
            Caption = '17'
            TabOrder = 59
            OnClick = BTSEND17Click
          end
          object BTSEND18: TButton
            Left = 192
            Top = 396
            Width = 22
            Height = 22
            Caption = '18'
            TabOrder = 60
            OnClick = BTSEND18Click
          end
          object BTSEND19: TButton
            Left = 192
            Top = 418
            Width = 22
            Height = 22
            Caption = '19'
            TabOrder = 61
            OnClick = BTSEND19Click
          end
          object BTSEND20: TButton
            Left = 192
            Top = 440
            Width = 22
            Height = 22
            Caption = '20'
            TabOrder = 62
            OnClick = BTSEND20Click
          end
          object BTSEND21: TButton
            Left = 193
            Top = 462
            Width = 22
            Height = 22
            Caption = '21'
            TabOrder = 63
            OnClick = BTSEND21Click
          end
          object BTSEND22: TButton
            Left = 192
            Top = 484
            Width = 22
            Height = 22
            Caption = '22'
            TabOrder = 64
            OnClick = BTSEND22Click
          end
          object BTSEND23: TButton
            Left = 192
            Top = 506
            Width = 22
            Height = 22
            Caption = '23'
            TabOrder = 65
            OnClick = BTSEND23Click
          end
          object BTSEND24: TButton
            Left = 192
            Top = 528
            Width = 22
            Height = 22
            Caption = '24'
            TabOrder = 66
            OnClick = BTSEND24Click
          end
          object BTSEND25: TButton
            Left = 192
            Top = 550
            Width = 22
            Height = 22
            Caption = '25'
            TabOrder = 67
            OnClick = BTSEND25Click
          end
          object BTSEND26: TButton
            Left = 192
            Top = 572
            Width = 22
            Height = 22
            Caption = '26'
            TabOrder = 68
            OnClick = BTSEND26Click
          end
          object BTSEND27: TButton
            Left = 192
            Top = 594
            Width = 22
            Height = 22
            Caption = '27'
            TabOrder = 69
            OnClick = BTSEND27Click
          end
          object BTSEND28: TButton
            Left = 192
            Top = 616
            Width = 22
            Height = 22
            Caption = '28'
            TabOrder = 70
            OnClick = BTSEND28Click
          end
          object CheckBox28: TCheckBox
            Left = 4
            Top = 377
            Width = 17
            Height = 17
            TabOrder = 71
          end
          object CheckBox29: TCheckBox
            Left = 4
            Top = 399
            Width = 17
            Height = 17
            TabOrder = 72
          end
          object CheckBox30: TCheckBox
            Left = 4
            Top = 421
            Width = 17
            Height = 17
            TabOrder = 73
          end
          object CheckBox31: TCheckBox
            Left = 4
            Top = 443
            Width = 17
            Height = 17
            TabOrder = 74
          end
          object CheckBox32: TCheckBox
            Left = 4
            Top = 465
            Width = 17
            Height = 17
            TabOrder = 75
          end
          object CheckBox33: TCheckBox
            Left = 4
            Top = 487
            Width = 17
            Height = 17
            TabOrder = 76
          end
          object CheckBox34: TCheckBox
            Left = 4
            Top = 509
            Width = 17
            Height = 17
            TabOrder = 77
          end
          object CheckBox35: TCheckBox
            Left = 4
            Top = 531
            Width = 17
            Height = 17
            TabOrder = 78
          end
          object CheckBox36: TCheckBox
            Left = 4
            Top = 553
            Width = 17
            Height = 17
            TabOrder = 79
          end
          object CheckBox37: TCheckBox
            Left = 4
            Top = 575
            Width = 17
            Height = 17
            TabOrder = 80
          end
          object CheckBox38: TCheckBox
            Left = 4
            Top = 597
            Width = 17
            Height = 17
            TabOrder = 81
          end
          object TESEND28: TEdit
            Left = 21
            Top = 616
            Width = 173
            Height = 22
            TabOrder = 82
          end
          object TESEND29: TEdit
            Left = 21
            Top = 638
            Width = 173
            Height = 22
            TabOrder = 83
          end
          object TESEND30: TEdit
            Left = 21
            Top = 660
            Width = 173
            Height = 22
            TabOrder = 84
          end
          object TESEND31: TEdit
            Left = 21
            Top = 682
            Width = 173
            Height = 22
            TabOrder = 85
          end
          object TESEND32: TEdit
            Left = 21
            Top = 704
            Width = 173
            Height = 22
            TabOrder = 86
          end
          object TESEND33: TEdit
            Left = 21
            Top = 726
            Width = 173
            Height = 22
            TabOrder = 87
          end
          object TESEND34: TEdit
            Left = 21
            Top = 748
            Width = 173
            Height = 22
            TabOrder = 88
          end
          object TESEND35: TEdit
            Left = 21
            Top = 770
            Width = 173
            Height = 22
            TabOrder = 89
          end
          object TESEND36: TEdit
            Left = 21
            Top = 792
            Width = 173
            Height = 22
            TabOrder = 90
          end
          object TESEND37: TEdit
            Left = 21
            Top = 814
            Width = 173
            Height = 22
            TabOrder = 91
          end
          object TESEND38: TEdit
            Left = 21
            Top = 836
            Width = 173
            Height = 22
            TabOrder = 92
          end
          object TESEND39: TEdit
            Left = 21
            Top = 858
            Width = 173
            Height = 22
            TabOrder = 93
          end
          object TESEND40: TEdit
            Left = 21
            Top = 880
            Width = 173
            Height = 22
            TabOrder = 94
          end
          object TESEND41: TEdit
            Left = 21
            Top = 902
            Width = 173
            Height = 22
            TabOrder = 95
          end
          object BTSEND29: TButton
            Left = 192
            Top = 638
            Width = 22
            Height = 22
            Caption = '29'
            TabOrder = 96
            OnClick = BTSEND29Click
          end
          object BTSEND30: TButton
            Left = 192
            Top = 660
            Width = 22
            Height = 22
            Caption = '30'
            TabOrder = 97
            OnClick = BTSEND30Click
          end
          object BTSEND31: TButton
            Left = 192
            Top = 682
            Width = 22
            Height = 22
            Caption = '31'
            TabOrder = 98
            OnClick = BTSEND31Click
          end
          object BTSEND32: TButton
            Left = 192
            Top = 704
            Width = 22
            Height = 22
            Caption = '32'
            TabOrder = 99
            OnClick = BTSEND32Click
          end
          object BTSEND33: TButton
            Left = 192
            Top = 726
            Width = 22
            Height = 22
            Caption = '33'
            TabOrder = 100
            OnClick = BTSEND33Click
          end
          object BTSEND34: TButton
            Left = 192
            Top = 748
            Width = 22
            Height = 22
            Caption = '34'
            TabOrder = 101
            OnClick = BTSEND34Click
          end
          object BTSEND35: TButton
            Left = 192
            Top = 770
            Width = 22
            Height = 22
            Caption = '35'
            TabOrder = 102
            OnClick = BTSEND35Click
          end
          object BTSEND36: TButton
            Left = 192
            Top = 792
            Width = 22
            Height = 22
            Caption = '36'
            TabOrder = 103
            OnClick = BTSEND36Click
          end
          object BTSEND37: TButton
            Left = 192
            Top = 814
            Width = 22
            Height = 22
            Caption = '37'
            TabOrder = 104
            OnClick = BTSEND37Click
          end
          object BTSEND38: TButton
            Left = 192
            Top = 836
            Width = 22
            Height = 22
            Caption = '38'
            TabOrder = 105
            OnClick = BTSEND38Click
          end
          object BTSEND39: TButton
            Left = 192
            Top = 858
            Width = 22
            Height = 22
            Caption = '39'
            TabOrder = 106
            OnClick = BTSEND39Click
          end
          object BTSEND40: TButton
            Left = 193
            Top = 880
            Width = 22
            Height = 22
            Caption = '40'
            TabOrder = 107
            OnClick = BTSEND40Click
          end
          object BTSEND41: TButton
            Left = 192
            Top = 902
            Width = 22
            Height = 22
            Caption = '41'
            TabOrder = 108
            OnClick = BTSEND41Click
          end
          object CheckBox39: TCheckBox
            Left = 4
            Top = 619
            Width = 17
            Height = 17
            TabOrder = 109
          end
          object CheckBox40: TCheckBox
            Left = 4
            Top = 641
            Width = 17
            Height = 17
            TabOrder = 110
          end
          object CheckBox41: TCheckBox
            Left = 4
            Top = 663
            Width = 17
            Height = 17
            TabOrder = 111
          end
          object CheckBox42: TCheckBox
            Left = 4
            Top = 685
            Width = 17
            Height = 17
            TabOrder = 112
          end
          object CheckBox43: TCheckBox
            Left = 4
            Top = 707
            Width = 17
            Height = 17
            TabOrder = 113
          end
          object CheckBox44: TCheckBox
            Left = 4
            Top = 729
            Width = 17
            Height = 17
            TabOrder = 114
          end
          object CheckBox45: TCheckBox
            Left = 4
            Top = 751
            Width = 17
            Height = 17
            TabOrder = 115
          end
          object CheckBox46: TCheckBox
            Left = 4
            Top = 773
            Width = 17
            Height = 17
            TabOrder = 116
          end
          object CheckBox47: TCheckBox
            Left = 4
            Top = 795
            Width = 17
            Height = 17
            TabOrder = 117
          end
          object CheckBox48: TCheckBox
            Left = 4
            Top = 817
            Width = 17
            Height = 17
            TabOrder = 118
          end
          object CheckBox49: TCheckBox
            Left = 4
            Top = 839
            Width = 17
            Height = 17
            TabOrder = 119
          end
          object CheckBox50: TCheckBox
            Left = 4
            Top = 861
            Width = 17
            Height = 17
            TabOrder = 120
          end
          object CheckBox51: TCheckBox
            Left = 4
            Top = 883
            Width = 17
            Height = 17
            TabOrder = 121
          end
          object CheckBox52: TCheckBox
            Left = 4
            Top = 905
            Width = 17
            Height = 17
            TabOrder = 122
          end
          object TCHB42: TCheckBox
            Left = 4
            Top = 927
            Width = 17
            Height = 17
            TabOrder = 123
          end
          object TESEND82: TEdit
            Left = 21
            Top = 1804
            Width = 173
            Height = 22
            TabOrder = 124
          end
          object TESEND81: TEdit
            Left = 21
            Top = 1782
            Width = 173
            Height = 22
            TabOrder = 125
          end
          object TESEND80: TEdit
            Left = 21
            Top = 1760
            Width = 173
            Height = 22
            TabOrder = 126
          end
          object TESEND79: TEdit
            Left = 21
            Top = 1738
            Width = 173
            Height = 22
            TabOrder = 127
          end
          object TESEND78: TEdit
            Left = 21
            Top = 1716
            Width = 173
            Height = 22
            TabOrder = 128
          end
          object TESEND77: TEdit
            Left = 21
            Top = 1694
            Width = 173
            Height = 22
            TabOrder = 129
          end
          object TESEND76: TEdit
            Left = 21
            Top = 1672
            Width = 173
            Height = 22
            TabOrder = 130
          end
          object TESEND75: TEdit
            Left = 21
            Top = 1650
            Width = 173
            Height = 22
            TabOrder = 131
          end
          object TESEND74: TEdit
            Left = 21
            Top = 1628
            Width = 173
            Height = 22
            TabOrder = 132
          end
          object TESEND73: TEdit
            Left = 21
            Top = 1606
            Width = 173
            Height = 22
            TabOrder = 133
          end
          object TESEND72: TEdit
            Left = 21
            Top = 1584
            Width = 173
            Height = 22
            TabOrder = 134
          end
          object TESEND71: TEdit
            Left = 21
            Top = 1562
            Width = 173
            Height = 22
            TabOrder = 135
          end
          object TESEND70: TEdit
            Left = 21
            Top = 1540
            Width = 173
            Height = 22
            TabOrder = 136
          end
          object TESEND69: TEdit
            Left = 21
            Top = 1518
            Width = 173
            Height = 22
            TabOrder = 137
          end
          object TESEND68: TEdit
            Left = 21
            Top = 1496
            Width = 173
            Height = 22
            TabOrder = 138
          end
          object TESEND67: TEdit
            Left = 21
            Top = 1474
            Width = 173
            Height = 22
            TabOrder = 139
          end
          object TESEND66: TEdit
            Left = 21
            Top = 1452
            Width = 173
            Height = 22
            TabOrder = 140
          end
          object TESEND65: TEdit
            Left = 21
            Top = 1430
            Width = 173
            Height = 22
            TabOrder = 141
          end
          object TESEND64: TEdit
            Left = 21
            Top = 1408
            Width = 173
            Height = 22
            TabOrder = 142
          end
          object TESEND63: TEdit
            Left = 21
            Top = 1386
            Width = 173
            Height = 22
            TabOrder = 143
          end
          object TESEND62: TEdit
            Left = 21
            Top = 1364
            Width = 173
            Height = 22
            TabOrder = 144
          end
          object TESEND61: TEdit
            Left = 21
            Top = 1342
            Width = 173
            Height = 22
            TabOrder = 145
          end
          object TESEND60: TEdit
            Left = 21
            Top = 1320
            Width = 173
            Height = 22
            TabOrder = 146
          end
          object TESEND59: TEdit
            Left = 21
            Top = 1298
            Width = 173
            Height = 22
            TabOrder = 147
          end
          object TESEND58: TEdit
            Left = 21
            Top = 1276
            Width = 173
            Height = 22
            TabOrder = 148
          end
          object TESEND57: TEdit
            Left = 21
            Top = 1254
            Width = 173
            Height = 22
            TabOrder = 149
          end
          object TESEND56: TEdit
            Left = 21
            Top = 1232
            Width = 173
            Height = 22
            TabOrder = 150
          end
          object TESEND55: TEdit
            Left = 21
            Top = 1210
            Width = 173
            Height = 22
            TabOrder = 151
          end
          object TESEND54: TEdit
            Left = 21
            Top = 1188
            Width = 173
            Height = 22
            TabOrder = 152
          end
          object TESEND53: TEdit
            Left = 21
            Top = 1166
            Width = 173
            Height = 22
            TabOrder = 153
          end
          object TESEND52: TEdit
            Left = 21
            Top = 1144
            Width = 173
            Height = 22
            TabOrder = 154
          end
          object TESEND51: TEdit
            Left = 21
            Top = 1122
            Width = 173
            Height = 22
            TabOrder = 155
          end
          object TESEND50: TEdit
            Left = 21
            Top = 1100
            Width = 173
            Height = 22
            TabOrder = 156
          end
          object TESEND49: TEdit
            Left = 21
            Top = 1078
            Width = 173
            Height = 22
            TabOrder = 157
          end
          object TESEND48: TEdit
            Left = 21
            Top = 1056
            Width = 173
            Height = 22
            TabOrder = 158
          end
          object TESEND47: TEdit
            Left = 21
            Top = 1034
            Width = 173
            Height = 22
            TabOrder = 159
          end
          object TESEND46: TEdit
            Left = 21
            Top = 1012
            Width = 173
            Height = 22
            TabOrder = 160
          end
          object TESEND45: TEdit
            Left = 21
            Top = 990
            Width = 173
            Height = 22
            TabOrder = 161
          end
          object TESEND44: TEdit
            Left = 21
            Top = 968
            Width = 173
            Height = 22
            TabOrder = 162
          end
          object TESEND43: TEdit
            Left = 21
            Top = 946
            Width = 173
            Height = 22
            TabOrder = 163
          end
          object TESEND42: TEdit
            Left = 21
            Top = 924
            Width = 173
            Height = 22
            TabOrder = 164
          end
          object TCHB43: TCheckBox
            Left = 4
            Top = 949
            Width = 17
            Height = 17
            TabOrder = 165
          end
          object TCHB82: TCheckBox
            Left = 4
            Top = 1807
            Width = 17
            Height = 17
            TabOrder = 166
          end
          object TCHB81: TCheckBox
            Left = 4
            Top = 1785
            Width = 17
            Height = 17
            TabOrder = 167
          end
          object TCHB80: TCheckBox
            Left = 4
            Top = 1763
            Width = 17
            Height = 17
            TabOrder = 168
          end
          object TCHB79: TCheckBox
            Left = 4
            Top = 1741
            Width = 17
            Height = 17
            TabOrder = 169
          end
          object TCHB78: TCheckBox
            Left = 4
            Top = 1719
            Width = 17
            Height = 17
            TabOrder = 170
          end
          object TCHB77: TCheckBox
            Left = 4
            Top = 1697
            Width = 17
            Height = 17
            TabOrder = 171
          end
          object TCHB76: TCheckBox
            Left = 4
            Top = 1675
            Width = 17
            Height = 17
            TabOrder = 172
          end
          object TCHB75: TCheckBox
            Left = 4
            Top = 1653
            Width = 17
            Height = 17
            TabOrder = 173
          end
          object TCHB74: TCheckBox
            Left = 4
            Top = 1631
            Width = 17
            Height = 17
            TabOrder = 174
          end
          object TCHB73: TCheckBox
            Left = 4
            Top = 1609
            Width = 17
            Height = 17
            TabOrder = 175
          end
          object TCHB72: TCheckBox
            Left = 4
            Top = 1587
            Width = 17
            Height = 17
            TabOrder = 176
          end
          object TCHB71: TCheckBox
            Left = 4
            Top = 1565
            Width = 17
            Height = 17
            TabOrder = 177
          end
          object TCHB70: TCheckBox
            Left = 4
            Top = 1543
            Width = 17
            Height = 17
            TabOrder = 178
          end
          object TCHB69: TCheckBox
            Left = 4
            Top = 1521
            Width = 17
            Height = 17
            TabOrder = 179
          end
          object TCHB68: TCheckBox
            Left = 4
            Top = 1499
            Width = 17
            Height = 17
            TabOrder = 180
          end
          object TCHB67: TCheckBox
            Left = 4
            Top = 1477
            Width = 17
            Height = 17
            TabOrder = 181
          end
          object TCHB66: TCheckBox
            Left = 4
            Top = 1455
            Width = 17
            Height = 17
            TabOrder = 182
          end
          object TCHB65: TCheckBox
            Left = 4
            Top = 1433
            Width = 17
            Height = 17
            TabOrder = 183
          end
          object TCHB64: TCheckBox
            Left = 4
            Top = 1411
            Width = 17
            Height = 17
            TabOrder = 184
          end
          object TCHB63: TCheckBox
            Left = 4
            Top = 1389
            Width = 17
            Height = 17
            TabOrder = 185
          end
          object TCHB62: TCheckBox
            Left = 4
            Top = 1367
            Width = 17
            Height = 17
            TabOrder = 186
          end
          object TCHB61: TCheckBox
            Left = 4
            Top = 1345
            Width = 17
            Height = 17
            TabOrder = 187
          end
          object TCHB60: TCheckBox
            Left = 4
            Top = 1323
            Width = 17
            Height = 17
            TabOrder = 188
          end
          object TCHB59: TCheckBox
            Left = 4
            Top = 1301
            Width = 17
            Height = 17
            TabOrder = 189
          end
          object TCHB58: TCheckBox
            Left = 4
            Top = 1279
            Width = 17
            Height = 17
            TabOrder = 190
          end
          object TCHB57: TCheckBox
            Left = 4
            Top = 1257
            Width = 17
            Height = 17
            TabOrder = 191
          end
          object TCHB56: TCheckBox
            Left = 4
            Top = 1235
            Width = 17
            Height = 17
            TabOrder = 192
          end
          object TCHB55: TCheckBox
            Left = 4
            Top = 1213
            Width = 17
            Height = 17
            TabOrder = 193
          end
          object TCHB54: TCheckBox
            Left = 4
            Top = 1191
            Width = 17
            Height = 17
            TabOrder = 194
          end
          object TCHB53: TCheckBox
            Left = 4
            Top = 1169
            Width = 17
            Height = 17
            TabOrder = 195
          end
          object TCHB52: TCheckBox
            Left = 4
            Top = 1147
            Width = 17
            Height = 17
            TabOrder = 196
          end
          object TCHB51: TCheckBox
            Left = 4
            Top = 1125
            Width = 17
            Height = 17
            TabOrder = 197
          end
          object TCHB50: TCheckBox
            Left = 4
            Top = 1103
            Width = 17
            Height = 17
            TabOrder = 198
          end
          object TCHB49: TCheckBox
            Left = 4
            Top = 1081
            Width = 17
            Height = 17
            TabOrder = 199
          end
          object TCHB48: TCheckBox
            Left = 4
            Top = 1059
            Width = 17
            Height = 17
            TabOrder = 200
          end
          object TCHB47: TCheckBox
            Left = 4
            Top = 1037
            Width = 17
            Height = 17
            TabOrder = 201
          end
          object TCHB46: TCheckBox
            Left = 4
            Top = 1015
            Width = 17
            Height = 17
            TabOrder = 202
          end
          object TCHB45: TCheckBox
            Left = 4
            Top = 993
            Width = 17
            Height = 17
            TabOrder = 203
          end
          object TCHB44: TCheckBox
            Left = 4
            Top = 971
            Width = 17
            Height = 17
            TabOrder = 204
          end
          object BTSEND82: TButton
            Left = 193
            Top = 1804
            Width = 22
            Height = 22
            Caption = '82'
            TabOrder = 205
            OnClick = BTSEND82Click
          end
          object BTSEND81: TButton
            Left = 193
            Top = 1782
            Width = 22
            Height = 22
            Caption = '81'
            TabOrder = 206
            OnClick = BTSEND81Click
          end
          object BTSEND80: TButton
            Left = 193
            Top = 1760
            Width = 22
            Height = 22
            Caption = '80'
            TabOrder = 207
            OnClick = BTSEND80Click
          end
          object BTSEND79: TButton
            Left = 193
            Top = 1738
            Width = 22
            Height = 22
            Caption = '79'
            TabOrder = 208
            OnClick = BTSEND79Click
          end
          object BTSEND78: TButton
            Left = 193
            Top = 1716
            Width = 22
            Height = 22
            Caption = '78'
            TabOrder = 209
            OnClick = BTSEND78Click
          end
          object BTSEND77: TButton
            Left = 193
            Top = 1694
            Width = 22
            Height = 22
            Caption = '77'
            TabOrder = 210
            OnClick = BTSEND77Click
          end
          object BTSEND76: TButton
            Left = 193
            Top = 1672
            Width = 22
            Height = 22
            Caption = '76'
            TabOrder = 211
            OnClick = BTSEND76Click
          end
          object BTSEND75: TButton
            Left = 193
            Top = 1650
            Width = 22
            Height = 22
            Caption = '75'
            TabOrder = 212
            OnClick = BTSEND75Click
          end
          object BTSEND74: TButton
            Left = 193
            Top = 1628
            Width = 22
            Height = 22
            Caption = '74'
            TabOrder = 213
            OnClick = BTSEND74Click
          end
          object BTSEND73: TButton
            Left = 193
            Top = 1606
            Width = 22
            Height = 22
            Caption = '73'
            TabOrder = 214
            OnClick = BTSEND73Click
          end
          object BTSEND72: TButton
            Left = 193
            Top = 1584
            Width = 22
            Height = 22
            Caption = '72'
            TabOrder = 215
            OnClick = BTSEND72Click
          end
          object BTSEND71: TButton
            Left = 193
            Top = 1562
            Width = 22
            Height = 22
            Caption = '71'
            TabOrder = 216
            OnClick = BTSEND71Click
          end
          object BTSEND70: TButton
            Left = 193
            Top = 1540
            Width = 22
            Height = 22
            Caption = '70'
            TabOrder = 217
            OnClick = BTSEND70Click
          end
          object BTSEND69: TButton
            Left = 193
            Top = 1518
            Width = 22
            Height = 22
            Caption = '69'
            TabOrder = 218
            OnClick = BTSEND69Click
          end
          object BTSEND68: TButton
            Left = 193
            Top = 1496
            Width = 22
            Height = 22
            Caption = '68'
            TabOrder = 219
            OnClick = BTSEND68Click
          end
          object BTSEND67: TButton
            Left = 193
            Top = 1474
            Width = 22
            Height = 22
            Caption = '67'
            TabOrder = 220
            OnClick = BTSEND67Click
          end
          object BTSEND66: TButton
            Left = 193
            Top = 1452
            Width = 22
            Height = 22
            Caption = '66'
            TabOrder = 221
            OnClick = BTSEND66Click
          end
          object BTSEND65: TButton
            Left = 193
            Top = 1430
            Width = 22
            Height = 22
            Caption = '65'
            TabOrder = 222
            OnClick = BTSEND65Click
          end
          object BTSEND64: TButton
            Left = 194
            Top = 1408
            Width = 22
            Height = 22
            Caption = '64'
            TabOrder = 223
            OnClick = BTSEND64Click
          end
          object BTSEND63: TButton
            Left = 193
            Top = 1386
            Width = 22
            Height = 22
            Caption = '63'
            TabOrder = 224
            OnClick = BTSEND63Click
          end
          object BTSEND62: TButton
            Left = 193
            Top = 1364
            Width = 22
            Height = 22
            Caption = '62'
            TabOrder = 225
            OnClick = BTSEND62Click
          end
          object BTSEND61: TButton
            Left = 193
            Top = 1342
            Width = 22
            Height = 22
            Caption = '61'
            TabOrder = 226
            OnClick = BTSEND61Click
          end
          object BTSEND60: TButton
            Left = 193
            Top = 1320
            Width = 22
            Height = 22
            Caption = '60'
            TabOrder = 227
            OnClick = BTSEND60Click
          end
          object BTSEND59: TButton
            Left = 193
            Top = 1298
            Width = 22
            Height = 22
            Caption = '59'
            TabOrder = 228
            OnClick = BTSEND59Click
          end
          object BTSEND58: TButton
            Left = 193
            Top = 1276
            Width = 22
            Height = 22
            Caption = '58'
            TabOrder = 229
            OnClick = BTSEND58Click
          end
          object BTSEND57: TButton
            Left = 193
            Top = 1254
            Width = 22
            Height = 22
            Caption = '57'
            TabOrder = 230
            OnClick = BTSEND57Click
          end
          object BTSEND56: TButton
            Left = 193
            Top = 1232
            Width = 22
            Height = 22
            Caption = '56'
            TabOrder = 231
            OnClick = BTSEND56Click
          end
          object BTSEND55: TButton
            Left = 193
            Top = 1210
            Width = 22
            Height = 22
            Caption = '55'
            TabOrder = 232
            OnClick = BTSEND55Click
          end
          object BTSEND54: TButton
            Left = 193
            Top = 1188
            Width = 22
            Height = 22
            Caption = '54'
            TabOrder = 233
            OnClick = BTSEND54Click
          end
          object BTSEND53: TButton
            Left = 193
            Top = 1166
            Width = 22
            Height = 22
            Caption = '53'
            TabOrder = 234
            OnClick = BTSEND53Click
          end
          object BTSEND52: TButton
            Left = 193
            Top = 1144
            Width = 22
            Height = 22
            Caption = '52'
            TabOrder = 235
            OnClick = BTSEND52Click
          end
          object BTSEND51: TButton
            Left = 193
            Top = 1122
            Width = 22
            Height = 22
            Caption = '51'
            TabOrder = 236
            OnClick = BTSEND51Click
          end
          object BTSEND50: TButton
            Left = 193
            Top = 1100
            Width = 22
            Height = 22
            Caption = '50'
            TabOrder = 237
            OnClick = BTSEND50Click
          end
          object BTSEND49: TButton
            Left = 193
            Top = 1078
            Width = 22
            Height = 22
            Caption = '49'
            TabOrder = 238
            OnClick = BTSEND49Click
          end
          object BTSEND48: TButton
            Left = 193
            Top = 1056
            Width = 22
            Height = 22
            Caption = '48'
            TabOrder = 239
            OnClick = BTSEND48Click
          end
          object BTSEND47: TButton
            Left = 193
            Top = 1034
            Width = 22
            Height = 22
            Caption = '47'
            TabOrder = 240
            OnClick = BTSEND47Click
          end
          object BTSEND46: TButton
            Left = 193
            Top = 1012
            Width = 22
            Height = 22
            Caption = '46'
            TabOrder = 241
            OnClick = BTSEND46Click
          end
          object BTSEND45: TButton
            Left = 193
            Top = 990
            Width = 22
            Height = 22
            Caption = '45'
            TabOrder = 242
            OnClick = BTSEND45Click
          end
          object BTSEND44: TButton
            Left = 193
            Top = 968
            Width = 22
            Height = 22
            Caption = '44'
            TabOrder = 243
            OnClick = BTSEND44Click
          end
          object BTSEND43: TButton
            Left = 193
            Top = 946
            Width = 22
            Height = 22
            Caption = '43'
            TabOrder = 244
            OnClick = BTSEND43Click
          end
          object BTSEND42: TButton
            Left = 193
            Top = 924
            Width = 22
            Height = 22
            Caption = '42'
            TabOrder = 245
            OnClick = BTSEND42Click
          end
          object TCHB99: TCheckBox
            Left = 4
            Top = 2181
            Width = 17
            Height = 17
            TabOrder = 246
          end
          object TESEND99: TEdit
            Left = 21
            Top = 2178
            Width = 173
            Height = 22
            TabOrder = 247
          end
          object BTSEND99: TButton
            Left = 192
            Top = 2178
            Width = 22
            Height = 22
            Caption = '99'
            TabOrder = 248
            OnClick = BTSEND99Click
          end
          object TCHB98: TCheckBox
            Left = 4
            Top = 2159
            Width = 17
            Height = 17
            TabOrder = 249
          end
          object TESEND98: TEdit
            Left = 21
            Top = 2156
            Width = 173
            Height = 22
            TabOrder = 250
          end
          object BTSEND98: TButton
            Left = 192
            Top = 2156
            Width = 22
            Height = 22
            Caption = '98'
            TabOrder = 251
            OnClick = BTSEND98Click
          end
          object TCHB97: TCheckBox
            Left = 4
            Top = 2137
            Width = 17
            Height = 17
            TabOrder = 252
          end
          object TESEND97: TEdit
            Left = 21
            Top = 2134
            Width = 173
            Height = 22
            TabOrder = 253
          end
          object BTSEND97: TButton
            Left = 192
            Top = 2134
            Width = 22
            Height = 22
            Caption = '97'
            TabOrder = 254
            OnClick = BTSEND97Click
          end
          object TESEND96: TEdit
            Left = 21
            Top = 2112
            Width = 173
            Height = 22
            TabOrder = 255
          end
          object BTSEND96: TButton
            Left = 192
            Top = 2112
            Width = 22
            Height = 22
            Caption = '96'
            TabOrder = 256
            OnClick = BTSEND96Click
          end
          object TCHB96: TCheckBox
            Left = 4
            Top = 2115
            Width = 17
            Height = 17
            TabOrder = 257
          end
          object TCHB95: TCheckBox
            Left = 4
            Top = 2093
            Width = 17
            Height = 17
            TabOrder = 258
          end
          object TESEND95: TEdit
            Left = 21
            Top = 2090
            Width = 173
            Height = 22
            TabOrder = 259
          end
          object BTSEND95: TButton
            Left = 192
            Top = 2090
            Width = 22
            Height = 22
            Caption = '95'
            TabOrder = 260
            OnClick = BTSEND95Click
          end
          object TCHB94: TCheckBox
            Left = 4
            Top = 2071
            Width = 17
            Height = 17
            TabOrder = 261
          end
          object TESEND94: TEdit
            Left = 21
            Top = 2068
            Width = 173
            Height = 22
            TabOrder = 262
          end
          object BTSEND94: TButton
            Left = 192
            Top = 2068
            Width = 22
            Height = 22
            Caption = '94'
            TabOrder = 263
            OnClick = BTSEND94Click
          end
          object TCHB93: TCheckBox
            Left = 4
            Top = 2049
            Width = 17
            Height = 17
            TabOrder = 264
          end
          object TESEND93: TEdit
            Left = 21
            Top = 2046
            Width = 173
            Height = 22
            TabOrder = 265
          end
          object BTSEND93: TButton
            Left = 192
            Top = 2046
            Width = 22
            Height = 22
            Caption = '93'
            TabOrder = 266
            OnClick = BTSEND93Click
          end
          object TCHB92: TCheckBox
            Left = 4
            Top = 2027
            Width = 17
            Height = 17
            TabOrder = 267
          end
          object TESEND92: TEdit
            Left = 21
            Top = 2024
            Width = 173
            Height = 22
            TabOrder = 268
          end
          object BTSEND92: TButton
            Left = 192
            Top = 2024
            Width = 22
            Height = 22
            Caption = '92'
            TabOrder = 269
            OnClick = BTSEND92Click
          end
          object TCHB91: TCheckBox
            Left = 4
            Top = 2005
            Width = 17
            Height = 17
            TabOrder = 270
          end
          object TESEND91: TEdit
            Left = 21
            Top = 2002
            Width = 173
            Height = 22
            TabOrder = 271
          end
          object BTSEND91: TButton
            Left = 192
            Top = 2002
            Width = 22
            Height = 22
            Caption = '91'
            TabOrder = 272
            OnClick = BTSEND91Click
          end
          object TCHB90: TCheckBox
            Left = 4
            Top = 1983
            Width = 17
            Height = 17
            TabOrder = 273
          end
          object TESEND90: TEdit
            Left = 21
            Top = 1980
            Width = 173
            Height = 22
            TabOrder = 274
          end
          object BTSEND90: TButton
            Left = 192
            Top = 1980
            Width = 22
            Height = 22
            Caption = '90'
            TabOrder = 275
            OnClick = BTSEND90Click
          end
          object TCHB89: TCheckBox
            Left = 4
            Top = 1961
            Width = 17
            Height = 17
            TabOrder = 276
          end
          object TESEND89: TEdit
            Left = 21
            Top = 1958
            Width = 173
            Height = 22
            TabOrder = 277
          end
          object BTSEND89: TButton
            Left = 192
            Top = 1958
            Width = 22
            Height = 22
            Caption = '89'
            TabOrder = 278
            OnClick = BTSEND89Click
          end
          object TCHB88: TCheckBox
            Left = 4
            Top = 1939
            Width = 17
            Height = 17
            TabOrder = 279
          end
          object TESEND88: TEdit
            Left = 21
            Top = 1936
            Width = 173
            Height = 22
            TabOrder = 280
          end
          object BTSEND88: TButton
            Left = 192
            Top = 1936
            Width = 22
            Height = 22
            Caption = '88'
            TabOrder = 281
            OnClick = BTSEND88Click
          end
          object TCHB87: TCheckBox
            Left = 4
            Top = 1917
            Width = 17
            Height = 17
            TabOrder = 282
          end
          object TESEND87: TEdit
            Left = 21
            Top = 1914
            Width = 173
            Height = 22
            TabOrder = 283
          end
          object BTSEND87: TButton
            Left = 192
            Top = 1914
            Width = 22
            Height = 22
            Caption = '87'
            TabOrder = 284
            OnClick = BTSEND87Click
          end
          object TCHB86: TCheckBox
            Left = 4
            Top = 1895
            Width = 17
            Height = 17
            TabOrder = 285
          end
          object TESEND86: TEdit
            Left = 21
            Top = 1892
            Width = 173
            Height = 22
            TabOrder = 286
          end
          object BTSEND86: TButton
            Left = 192
            Top = 1892
            Width = 22
            Height = 22
            Caption = '86'
            TabOrder = 287
            OnClick = BTSEND86Click
          end
          object TCHB85: TCheckBox
            Left = 4
            Top = 1873
            Width = 17
            Height = 17
            TabOrder = 288
          end
          object TESEND85: TEdit
            Left = 21
            Top = 1870
            Width = 173
            Height = 22
            TabOrder = 289
          end
          object BTSEND85: TButton
            Left = 192
            Top = 1870
            Width = 22
            Height = 22
            Caption = '85'
            TabOrder = 290
            OnClick = BTSEND85Click
          end
          object TCHB84: TCheckBox
            Left = 4
            Top = 1851
            Width = 17
            Height = 17
            TabOrder = 291
          end
          object TESEND84: TEdit
            Left = 21
            Top = 1848
            Width = 173
            Height = 22
            TabOrder = 292
          end
          object BTSEND84: TButton
            Left = 192
            Top = 1848
            Width = 22
            Height = 22
            Caption = '84'
            TabOrder = 293
            OnClick = BTSEND84Click
          end
          object TCHB83: TCheckBox
            Left = 4
            Top = 1829
            Width = 17
            Height = 17
            TabOrder = 294
          end
          object TESEND83: TEdit
            Left = 21
            Top = 1826
            Width = 173
            Height = 22
            TabOrder = 295
          end
          object BTSEND83: TButton
            Left = 192
            Top = 1826
            Width = 22
            Height = 22
            Caption = '83'
            TabOrder = 296
            OnClick = BTSEND83Click
          end
        end
      end
    end
    object ts1: TTabSheet
      Caption = 'HID'
      ImageIndex = 1
      object DevListBox: TListBox
        Left = 0
        Top = 0
        Width = 237
        Height = 113
        Align = alTop
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 14
        ParentFont = False
        TabOrder = 0
        OnClick = DevListBoxClick
      end
      object GroupBox10: TGroupBox
        Left = 0
        Top = 113
        Width = 237
        Height = 99
        Align = alTop
        Caption = 'Attribute'
        TabOrder = 1
        object Memo3: TMemo
          Left = 2
          Top = 16
          Width = 233
          Height = 81
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object GroupBox11: TGroupBox
        Left = 0
        Top = 212
        Width = 237
        Height = 263
        Align = alClient
        Caption = 'Send'
        TabOrder = 2
        object Memo4: TMemo
          Left = 2
          Top = 16
          Width = 233
          Height = 245
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnChange = Memo4Change
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 475
        Width = 237
        Height = 41
        Align = alBottom
        TabOrder = 3
        object ReadBtn: TSpeedButton
          Left = 9
          Top = 10
          Width = 68
          Height = 25
          AllowAllUp = True
          GroupIndex = 1
          Caption = #30417#25511
          OnClick = ReadBtnClick
        end
        object CheckBox24: TCheckBox
          Left = 100
          Top = 13
          Width = 46
          Height = 22
          Caption = 'HEX'
          TabOrder = 0
        end
        object Button8: TButton
          Left = 156
          Top = 10
          Width = 68
          Height = 25
          Caption = #21457#36865
          TabOrder = 1
          OnClick = Button8Click
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TCP'
      ImageIndex = 2
      PopupMenu = PopupMenu3
      object GroupBox12: TGroupBox
        Left = 0
        Top = 0
        Width = 237
        Height = 309
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object GroupBox38: TGroupBox
          Left = 2
          Top = 271
          Width = 233
          Height = 36
          Align = alBottom
          TabOrder = 0
          object CheckBox25: TCheckBox
            Left = 8
            Top = 11
            Width = 73
            Height = 17
            Caption = #33258#21160#22238#22797
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = CheckBox25Click
          end
          object RadioButton1: TRadioButton
            Left = 98
            Top = 11
            Width = 55
            Height = 17
            Caption = #25968#25454
            TabOrder = 1
            OnClick = RadioButton1Click
          end
          object RadioButton2: TRadioButton
            Left = 161
            Top = 11
            Width = 54
            Height = 17
            Caption = #36879#26126
            Checked = True
            TabOrder = 2
            TabStop = True
          end
        end
        object GroupBox40: TGroupBox
          Left = 2
          Top = 16
          Width = 233
          Height = 57
          Align = alTop
          Caption = 'Server'
          TabOrder = 1
          object Shape3: TShape
            Left = 193
            Top = 11
            Width = 25
            Height = 17
            Brush.Color = clGray
            Shape = stCircle
          end
          object Edit24: TEdit
            Left = 5
            Top = 12
            Width = 113
            Height = 22
            TabOrder = 0
            Text = '255.255.255.255'
          end
          object Button9: TButton
            Left = 69
            Top = 34
            Width = 45
            Height = 21
            Caption = #24320#21551
            TabOrder = 1
            OnClick = Button9Click
          end
          object Button63: TButton
            Left = 8
            Top = 34
            Width = 49
            Height = 21
            Caption = #33719#21462'IP'
            TabOrder = 2
            OnClick = Button63Click
          end
          object Edit15: TEdit
            Left = 131
            Top = 12
            Width = 49
            Height = 22
            TabOrder = 3
          end
          object Button3: TButton
            Left = 122
            Top = 35
            Width = 102
            Height = 21
            Caption = #36716#21457#26381#21153#22120
            TabOrder = 4
            OnClick = Button3Click
          end
        end
        object CheckListBox1: TCheckListBox
          Left = 2
          Top = 73
          Width = 233
          Height = 198
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ItemHeight = 14
          ParentFont = False
          PopupMenu = PopupMenu2
          TabOrder = 2
        end
      end
      object GroupBox13: TGroupBox
        Left = 0
        Top = 309
        Width = 237
        Height = 59
        Align = alTop
        Caption = 'Client'
        TabOrder = 1
        object Label13: TLabel
          Left = 8
          Top = 15
          Width = 42
          Height = 14
          Caption = 'IP  '#65306
        end
        object Label14: TLabel
          Left = 8
          Top = 34
          Width = 42
          Height = 14
          Caption = #31471#21475#65306
        end
        object Shape4: TShape
          Left = 172
          Top = 9
          Width = 25
          Height = 17
          Brush.Color = clGray
          Shape = stCircle
        end
        object Edit22: TEdit
          Left = 52
          Top = 31
          Width = 110
          Height = 22
          TabOrder = 0
        end
        object Button10: TButton
          Left = 165
          Top = 31
          Width = 50
          Height = 21
          Caption = #36830#25509
          TabOrder = 1
          OnClick = Button10Click
        end
        object Edit21: TEdit
          Left = 52
          Top = 8
          Width = 110
          Height = 22
          TabOrder = 2
        end
      end
      object GroupBox14: TGroupBox
        Left = 0
        Top = 368
        Width = 237
        Height = 148
        Align = alClient
        Caption = 'Send'
        TabOrder = 2
        object Label19: TLabel
          Left = 98
          Top = 105
          Width = 35
          Height = 14
          Caption = #38388#38548':'
        end
        object Label20: TLabel
          Left = 194
          Top = 104
          Width = 14
          Height = 14
          Caption = #31186
        end
        object Memo5: TMemo
          Left = 2
          Top = 16
          Width = 233
          Height = 80
          Align = alTop
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 32767
          ParentFont = False
          TabOrder = 0
          OnChange = Memo5Change
        end
        object Button12: TButton
          Left = 181
          Top = 122
          Width = 50
          Height = 21
          Caption = #21457#36865
          TabOrder = 1
          OnClick = Button12Click
        end
        object RadioButton3: TRadioButton
          Left = 5
          Top = 126
          Width = 73
          Height = 17
          Caption = 'Server'
          Checked = True
          TabOrder = 2
          TabStop = True
        end
        object RadioButton4: TRadioButton
          Left = 69
          Top = 127
          Width = 72
          Height = 17
          Caption = 'Client'
          TabOrder = 3
        end
        object CheckBox26: TCheckBox
          Left = 134
          Top = 126
          Width = 41
          Height = 17
          Caption = 'HEX'
          TabOrder = 4
          OnClick = CheckBox26Click
        end
        object CheckBox27: TCheckBox
          Left = 8
          Top = 103
          Width = 73
          Height = 17
          Caption = #33258#21160#21457#36865
          TabOrder = 5
          OnClick = CheckBox65Click5
        end
        object Edit23: TEdit
          Left = 134
          Top = 100
          Width = 59
          Height = 22
          TabOrder = 6
          Text = '1'
          OnKeyPress = Edit23KeyPress
        end
      end
    end
    object TabSheet8: TTabSheet
      Caption = 'UDP'
      ImageIndex = 9
      object GroupBox7: TGroupBox
        Left = 0
        Top = 0
        Width = 237
        Height = 104
        Align = alTop
        TabOrder = 0
        object GroupBox39: TGroupBox
          Left = 2
          Top = 73
          Width = 233
          Height = 29
          Align = alClient
          TabOrder = 0
          object CheckBox63: TCheckBox
            Left = 8
            Top = 9
            Width = 73
            Height = 17
            Caption = #33258#21160#22238#22797
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = CheckBox63Click
          end
          object RadioButton25: TRadioButton
            Left = 98
            Top = 9
            Width = 55
            Height = 17
            Caption = #25968#25454
            TabOrder = 1
            OnClick = RadioButton1Click
          end
          object RadioButton26: TRadioButton
            Left = 161
            Top = 9
            Width = 54
            Height = 17
            Caption = #36879#26126
            Checked = True
            TabOrder = 2
            TabStop = True
          end
        end
        object GroupBox41: TGroupBox
          Left = 2
          Top = 16
          Width = 233
          Height = 57
          Align = alTop
          Caption = 'Server'
          TabOrder = 1
          object Shape2: TShape
            Left = 193
            Top = 11
            Width = 25
            Height = 17
            Brush.Color = clGray
            Shape = stCircle
          end
          object Edit1: TEdit
            Left = 5
            Top = 12
            Width = 113
            Height = 22
            TabOrder = 0
            Text = '255.255.255.255'
          end
          object Button6: TButton
            Left = 183
            Top = 35
            Width = 45
            Height = 21
            Caption = #30417#21548
            TabOrder = 1
            OnClick = Button6Click
          end
          object Button11: TButton
            Left = 8
            Top = 34
            Width = 49
            Height = 21
            Caption = #33719#21462'IP'
            TabOrder = 2
            OnClick = Button11Click
          end
          object Edit5: TEdit
            Left = 131
            Top = 12
            Width = 49
            Height = 22
            TabOrder = 3
            Text = '5000'
          end
        end
      end
      object GroupBox42: TGroupBox
        Left = 0
        Top = 104
        Width = 237
        Height = 58
        Align = alTop
        Caption = 'Client'
        TabOrder = 1
        object Label12: TLabel
          Left = 8
          Top = 15
          Width = 42
          Height = 14
          Caption = 'IP  '#65306
        end
        object Label15: TLabel
          Left = 8
          Top = 34
          Width = 42
          Height = 14
          Caption = #31471#21475#65306
        end
        object Edit6: TEdit
          Left = 52
          Top = 31
          Width = 140
          Height = 22
          TabOrder = 0
        end
        object Edit7: TEdit
          Left = 52
          Top = 8
          Width = 140
          Height = 22
          TabOrder = 1
        end
        object Button15: TButton
          Left = 196
          Top = 11
          Width = 35
          Height = 40
          Caption = #36830#25509
          TabOrder = 2
          OnClick = Button15Click
        end
      end
      object GroupBox43: TGroupBox
        Left = 0
        Top = 162
        Width = 237
        Height = 354
        Align = alClient
        Caption = 'Client Send'
        TabOrder = 2
        object Memo6: TMemo
          Left = 2
          Top = 16
          Width = 233
          Height = 275
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 32767
          ParentFont = False
          TabOrder = 0
          OnChange = Memo6Change
        end
        object GroupBox44: TGroupBox
          Left = 2
          Top = 291
          Width = 233
          Height = 61
          Align = alBottom
          TabOrder = 1
          object Label34: TLabel
            Left = 192
            Top = 15
            Width = 14
            Height = 14
            Caption = #31186
          end
          object Label16: TLabel
            Left = 93
            Top = 15
            Width = 35
            Height = 14
            Caption = #38388#38548':'
          end
          object Button18: TButton
            Left = 164
            Top = 35
            Width = 54
            Height = 21
            Caption = #21457#36865
            Enabled = False
            TabOrder = 0
            OnClick = Button18Click
          end
          object Edit8: TEdit
            Left = 129
            Top = 9
            Width = 59
            Height = 22
            TabOrder = 1
            Text = '1'
            OnKeyPress = Edit23KeyPress
          end
          object CheckBox64: TCheckBox
            Left = 5
            Top = 39
            Width = 41
            Height = 17
            Caption = 'HEX'
            TabOrder = 2
            OnClick = CheckBox64Click
          end
          object RadioButton28: TRadioButton
            Left = 75
            Top = 39
            Width = 69
            Height = 17
            Caption = 'Client'
            Checked = True
            TabOrder = 3
            TabStop = True
            Visible = False
          end
          object CheckBox65: TCheckBox
            Left = 5
            Top = 15
            Width = 73
            Height = 17
            Caption = #33258#21160#21457#36865
            TabOrder = 4
            OnClick = CheckBox65Click
          end
          object RadioButton27: TRadioButton
            Left = 3
            Top = 39
            Width = 73
            Height = 17
            Caption = 'Server'
            TabOrder = 5
            Visible = False
          end
        end
      end
    end
    object ts2: TTabSheet
      Hint = 'DES:'#13#10#25968#25454#38271#24230#20026'8'#30340#20493#25968#13#10#27599#20010'KEY'#20026'8'#23383#33410#13#10'IV'#20026'8'#23383#33410
      Caption = 'DES/MAC'
      ImageIndex = 4
      ParentShowHint = False
      ShowHint = True
      object GroupBox17: TGroupBox
        Left = 0
        Top = 0
        Width = 237
        Height = 271
        Align = alClient
        Caption = 'data'
        TabOrder = 0
        object memo7: TMemo
          Left = 2
          Top = 16
          Width = 233
          Height = 253
          Align = alClient
          TabOrder = 0
          OnChange = memo7Change
        end
      end
      object GroupBox18: TGroupBox
        Left = 0
        Top = 271
        Width = 237
        Height = 161
        Align = alBottom
        TabOrder = 1
        object Label17: TLabel
          Left = 8
          Top = 37
          Width = 14
          Height = 14
          Caption = 'K1'
        end
        object Label18: TLabel
          Left = 8
          Top = 62
          Width = 14
          Height = 14
          Caption = 'K2'
        end
        object Label27: TLabel
          Left = 8
          Top = 109
          Width = 14
          Height = 14
          Caption = 'IV'
        end
        object Label28: TLabel
          Left = 8
          Top = 86
          Width = 14
          Height = 14
          Caption = 'K3'
        end
        object Label2: TLabel
          Left = 212
          Top = 37
          Width = 7
          Height = 14
        end
        object Label3: TLabel
          Left = 212
          Top = 62
          Width = 7
          Height = 14
        end
        object Label5: TLabel
          Left = 212
          Top = 86
          Width = 7
          Height = 14
        end
        object Label6: TLabel
          Left = 212
          Top = 109
          Width = 7
          Height = 14
        end
        object Edit50: TEdit
          Left = 28
          Top = 32
          Width = 180
          Height = 22
          TabOrder = 0
          OnChange = Edit50Change
        end
        object Edit51: TEdit
          Left = 27
          Top = 57
          Width = 180
          Height = 22
          Color = clScrollBar
          Enabled = False
          TabOrder = 1
          OnChange = Edit51Change
        end
        object CheckBox55: TCheckBox
          Left = 8
          Top = 13
          Width = 91
          Height = 17
          Caption = 'DATA HEX'
          Checked = True
          State = cbChecked
          TabOrder = 2
          OnClick = CheckBox55Click
        end
        object CheckBox56: TCheckBox
          Left = 130
          Top = 13
          Width = 84
          Height = 17
          Caption = 'KEY  HEX '
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = CheckBox56Click
        end
        object Edit57: TEdit
          Left = 27
          Top = 105
          Width = 180
          Height = 22
          Color = clScrollBar
          Enabled = False
          TabOrder = 4
          OnChange = Edit57Change
        end
        object Edit58: TEdit
          Left = 27
          Top = 81
          Width = 180
          Height = 22
          Color = clScrollBar
          Enabled = False
          TabOrder = 5
          OnChange = Edit58Change
        end
        object GroupBox35: TGroupBox
          Left = 6
          Top = 127
          Width = 204
          Height = 27
          TabOrder = 6
          Visible = False
          object RadioButton15: TRadioButton
            Left = 11
            Top = 7
            Width = 73
            Height = 17
            Caption = '128bit'
            Checked = True
            TabOrder = 0
            TabStop = True
            OnClick = RadioButton15Click
          end
          object RadioButton16: TRadioButton
            Left = 123
            Top = 7
            Width = 67
            Height = 17
            Caption = '192bit'
            TabOrder = 1
            OnClick = RadioButton16Click
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 432
        Width = 237
        Height = 84
        Align = alBottom
        TabOrder = 2
        object RadioButton5: TRadioButton
          Left = 4
          Top = 9
          Width = 57
          Height = 17
          Caption = 'DES'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = RadioButton5Click
        end
        object RadioButton6: TRadioButton
          Left = 78
          Top = 8
          Width = 73
          Height = 17
          Caption = '3DES'
          TabOrder = 1
          OnClick = RadioButton6Click
        end
        object RadioButton7: TRadioButton
          Left = 153
          Top = 8
          Width = 57
          Height = 17
          Caption = 'CBC'
          TabOrder = 2
          OnClick = RadioButton7Click
        end
        object RadioButton21: TRadioButton
          Left = 119
          Top = 24
          Width = 105
          Height = 17
          Caption = 'ANSI-9.19-MAC'
          TabOrder = 3
          OnClick = RadioButton21Click
        end
        object RadioButton20: TRadioButton
          Left = 4
          Top = 39
          Width = 113
          Height = 17
          Caption = 'ANSI-X9.9 MAC'
          TabOrder = 4
          OnClick = RadioButton20Click
        end
        object RadioButton22: TRadioButton
          Left = 4
          Top = 24
          Width = 113
          Height = 17
          Caption = 'PBOC-3DES-MAC'
          TabOrder = 5
          OnClick = RadioButton22Click
        end
        object Button65: TButton
          Left = 136
          Top = 39
          Width = 75
          Height = 17
          Caption = 'MAC'#35828#26126
          TabOrder = 6
          OnClick = Button65Click
        end
        object Button56: TButton
          Left = 10
          Top = 60
          Width = 75
          Height = 17
          Caption = #21152#23494
          TabOrder = 7
          OnClick = Button56Click
        end
        object Button55: TButton
          Left = 136
          Top = 60
          Width = 75
          Height = 17
          Caption = #35299#23494
          TabOrder = 8
          OnClick = Button55Click
        end
      end
    end
    object TabSheet6: TTabSheet
      Hint = 'AES:'#13#10#25968#25454#38271#24230#20026'16'#30340#20493#25968','#19981#36275#26411#23614#34917#20805'0x00'#13#10#27599#20010'KEY'#20026'8'#23383#33410#13#10'IV'#20026'16'#23383#33410
      Caption = 'AES'
      ImageIndex = 7
      ParentShowHint = False
      ShowHint = True
      object GroupBox29: TGroupBox
        Left = 0
        Top = 0
        Width = 237
        Height = 516
        Align = alClient
        TabOrder = 0
        object GroupBox30: TGroupBox
          Left = 2
          Top = 16
          Width = 233
          Height = 242
          Align = alClient
          Caption = 'data'
          TabOrder = 0
          object Memo12: TMemo
            Left = 2
            Top = 16
            Width = 229
            Height = 224
            Align = alClient
            TabOrder = 0
            OnChange = Memo12Change
          end
        end
        object GroupBox31: TGroupBox
          Left = 2
          Top = 258
          Width = 233
          Height = 131
          Align = alBottom
          TabOrder = 1
          object Label21: TLabel
            Left = 8
            Top = 13
            Width = 14
            Height = 14
            Caption = 'K1'
          end
          object Label22: TLabel
            Left = 7
            Top = 37
            Width = 14
            Height = 14
            Caption = 'K2'
          end
          object Label23: TLabel
            Left = 7
            Top = 61
            Width = 14
            Height = 14
            Caption = 'K3'
          end
          object Label24: TLabel
            Left = 40
            Top = 40
            Width = 28
            Height = 14
            Caption = 'KEY1'
          end
          object Label25: TLabel
            Left = 8
            Top = 84
            Width = 14
            Height = 14
            Caption = 'K4'
          end
          object Label26: TLabel
            Left = 8
            Top = 108
            Width = 14
            Height = 14
            Caption = 'IV'
          end
          object Label7: TLabel
            Left = 210
            Top = 13
            Width = 7
            Height = 14
          end
          object Label8: TLabel
            Left = 210
            Top = 37
            Width = 7
            Height = 14
          end
          object Label29: TLabel
            Left = 210
            Top = 60
            Width = 7
            Height = 14
          end
          object Label30: TLabel
            Left = 210
            Top = 84
            Width = 7
            Height = 14
          end
          object Label31: TLabel
            Left = 210
            Top = 108
            Width = 7
            Height = 14
          end
          object Edit52: TEdit
            Left = 28
            Top = 8
            Width = 180
            Height = 22
            Hint = '8 bytes'
            TabOrder = 0
            OnChange = Edit52Change
          end
          object Edit53: TEdit
            Left = 28
            Top = 32
            Width = 180
            Height = 22
            TabOrder = 1
            OnChange = Edit53Change
          end
          object Edit54: TEdit
            Left = 28
            Top = 55
            Width = 180
            Height = 22
            Hint = '8 bytes'
            Color = clScrollBar
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnChange = Edit54Change
          end
          object Edit55: TEdit
            Left = 28
            Top = 80
            Width = 180
            Height = 22
            Color = clScrollBar
            Enabled = False
            TabOrder = 3
            OnChange = Edit55Change
          end
          object Edit56: TEdit
            Left = 28
            Top = 104
            Width = 180
            Height = 22
            Color = clScrollBar
            Enabled = False
            TabOrder = 4
            OnChange = Edit56Change
          end
        end
        object GroupBox32: TGroupBox
          Left = 2
          Top = 389
          Width = 233
          Height = 125
          Align = alBottom
          TabOrder = 2
          object Button60: TButton
            Left = 8
            Top = 96
            Width = 75
            Height = 25
            Caption = #21152#23494
            TabOrder = 0
            OnClick = Button60Click
          end
          object GroupBox33: TGroupBox
            Left = 0
            Top = 7
            Width = 232
            Height = 33
            Align = alCustom
            TabOrder = 1
            object RadioButton10: TRadioButton
              Left = 7
              Top = 8
              Width = 73
              Height = 17
              Caption = '128Bit'
              Checked = True
              TabOrder = 0
              TabStop = True
              OnClick = RadioButton10Click
            end
            object RadioButton11: TRadioButton
              Left = 74
              Top = 8
              Width = 80
              Height = 17
              Caption = '192Bit'
              TabOrder = 1
              OnClick = RadioButton11Click
            end
            object RadioButton12: TRadioButton
              Left = 142
              Top = 8
              Width = 60
              Height = 17
              Caption = '256Bit'
              TabOrder = 2
              OnClick = RadioButton12Click
            end
          end
          object GroupBox34: TGroupBox
            Left = 0
            Top = 40
            Width = 231
            Height = 30
            TabOrder = 2
            object RadioButton13: TRadioButton
              Left = 8
              Top = 7
              Width = 89
              Height = 17
              Caption = 'ECB'
              Checked = True
              TabOrder = 0
              TabStop = True
              OnClick = RadioButton13Click
            end
            object RadioButton14: TRadioButton
              Left = 145
              Top = 8
              Width = 51
              Height = 17
              Caption = 'CBC'
              TabOrder = 1
              OnClick = RadioButton14Click
            end
          end
          object Button61: TButton
            Left = 128
            Top = 96
            Width = 75
            Height = 25
            Caption = #35299#23494
            TabOrder = 3
            OnClick = Button61Click
          end
          object CheckBox59: TCheckBox
            Left = 8
            Top = 74
            Width = 97
            Height = 17
            Caption = 'HEX'
            Checked = True
            State = cbChecked
            TabOrder = 4
            OnClick = CheckBox59Click
          end
        end
      end
    end
    object TabSheet4: TTabSheet
      Hint = 
        'SM4:'#13#10#25968#25454#38271#24230#20026'16'#30340#20493#25968#19988#23567#20110#31561#20110'4096'#13#10'KEY'#20026'16'#23383#33410#13#10'IV'#20026'16'#23383#33410#13#10#13#10'SM3:'#13#10#36755#20837#25968#25454#38656#23567#20110#31561#20110'4' +
        '096'
      Caption = 'SM'
      ImageIndex = 5
      ParentShowHint = False
      ShowHint = True
      object GroupBox19: TGroupBox
        Left = 0
        Top = 0
        Width = 237
        Height = 322
        Align = alClient
        Caption = 'data'
        TabOrder = 0
        object Memo8: TMemo
          Left = 2
          Top = 16
          Width = 233
          Height = 304
          Hint = 'Multiples of 16'
          Align = alClient
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
          OnChange = Memo8Change
        end
      end
      object GroupBox20: TGroupBox
        Left = 0
        Top = 322
        Width = 237
        Height = 46
        Align = alBottom
        Caption = 'key'
        TabOrder = 1
        object Edit59: TEdit
          Left = 1
          Top = 17
          Width = 229
          Height = 22
          TabOrder = 0
          OnChange = Edit59Change
        end
      end
      object GroupBox21: TGroupBox
        Left = 0
        Top = 415
        Width = 237
        Height = 101
        Align = alBottom
        TabOrder = 2
        object Button58: TButton
          Left = 24
          Top = 78
          Width = 75
          Height = 20
          Caption = #21152#23494
          TabOrder = 0
          OnClick = Button58Click
        end
        object Button59: TButton
          Left = 128
          Top = 78
          Width = 75
          Height = 20
          Caption = #35299#23494
          TabOrder = 1
          OnClick = Button59Click
        end
        object GroupBox22: TGroupBox
          Left = 2
          Top = 16
          Width = 233
          Height = 28
          Align = alTop
          TabOrder = 2
          object CheckBox57: TCheckBox
            Left = 19
            Top = 9
            Width = 45
            Height = 17
            Caption = 'HEX'
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = CheckBox57Click
          end
          object RadioSm4: TRadioButton
            Left = 119
            Top = 9
            Width = 49
            Height = 17
            Caption = 'SM4'
            Checked = True
            TabOrder = 1
            TabStop = True
            OnClick = RadioSm4Click
          end
          object RadioSm3: TRadioButton
            Left = 169
            Top = 9
            Width = 50
            Height = 17
            Caption = 'SM3'
            TabOrder = 2
            OnClick = RadioSm3Click
          end
        end
        object GroupBox23: TGroupBox
          Left = 2
          Top = 44
          Width = 233
          Height = 30
          Align = alTop
          TabOrder = 3
          object RadioButton8: TRadioButton
            Left = 16
            Top = 8
            Width = 55
            Height = 17
            Caption = 'ECB'
            Checked = True
            TabOrder = 0
            TabStop = True
          end
          object RadioButton9: TRadioButton
            Left = 120
            Top = 8
            Width = 57
            Height = 17
            Caption = 'CBC'
            TabOrder = 1
          end
        end
      end
      object GroupBox24: TGroupBox
        Left = 0
        Top = 368
        Width = 237
        Height = 47
        Align = alBottom
        Caption = 'IV'
        TabOrder = 3
        object Edit60: TEdit
          Left = 2
          Top = 16
          Width = 229
          Height = 22
          TabOrder = 0
          OnChange = Edit60Change
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'HASH/MD5/CRC'
      ImageIndex = 6
      object GroupBox25: TGroupBox
        Left = 0
        Top = 0
        Width = 237
        Height = 516
        Align = alClient
        TabOrder = 0
        object GroupBox26: TGroupBox
          Left = 2
          Top = 16
          Width = 233
          Height = 420
          Align = alClient
          Caption = 'data'
          TabOrder = 0
          object Memo11: TMemo
            Left = 2
            Top = 16
            Width = 229
            Height = 402
            Align = alClient
            TabOrder = 0
            OnChange = Memo11Change
          end
        end
        object GroupBox27: TGroupBox
          Left = 2
          Top = 436
          Width = 233
          Height = 32
          Align = alBottom
          TabOrder = 1
          object CheckBox58: TCheckBox
            Left = 7
            Top = 11
            Width = 60
            Height = 17
            Caption = 'HEX'
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = CheckBox58Click
          end
          object RadioButton17: TRadioButton
            Left = 65
            Top = 11
            Width = 55
            Height = 17
            Caption = 'HASH'
            Checked = True
            TabOrder = 1
            TabStop = True
          end
          object RadioButton18: TRadioButton
            Left = 124
            Top = 11
            Width = 45
            Height = 17
            Caption = 'MD5'
            TabOrder = 2
          end
          object RadioButton19: TRadioButton
            Left = 174
            Top = 11
            Width = 45
            Height = 17
            Caption = 'CRC'
            TabOrder = 3
          end
        end
        object GroupBox28: TGroupBox
          Left = 2
          Top = 468
          Width = 233
          Height = 46
          Align = alBottom
          TabOrder = 2
          object Button57: TButton
            Left = 17
            Top = 14
            Width = 75
            Height = 25
            Caption = #36755#20837#35745#31639
            TabOrder = 0
            OnClick = Button57Click
          end
          object Button62: TButton
            Left = 128
            Top = 14
            Width = 75
            Height = 25
            Caption = #25991#20214#35745#31639
            TabOrder = 1
            OnClick = Button62Click
          end
        end
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'XOR'
      ImageIndex = 8
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 237
        Height = 516
        Align = alClient
        TabOrder = 0
        object GroupBox6: TGroupBox
          Left = 2
          Top = 16
          Width = 233
          Height = 369
          Align = alClient
          Caption = 'data1/data2'
          TabOrder = 0
          object Memo14: TMemo
            Left = 2
            Top = 16
            Width = 229
            Height = 258
            Align = alClient
            TabOrder = 0
            OnChange = Memo14Change
          end
          object Memo15: TMemo
            Left = 2
            Top = 274
            Width = 229
            Height = 93
            Align = alBottom
            TabOrder = 1
            OnChange = Memo15Change
          end
        end
        object GroupBox36: TGroupBox
          Left = 2
          Top = 385
          Width = 233
          Height = 49
          Align = alBottom
          TabOrder = 1
          object CheckBox54: TCheckBox
            Left = 9
            Top = 20
            Width = 58
            Height = 17
            Caption = 'HEX'
            TabOrder = 0
            OnClick = CheckBox54Click
          end
        end
        object GroupBox37: TGroupBox
          Left = 2
          Top = 434
          Width = 233
          Height = 80
          Align = alBottom
          TabOrder = 2
          object Button64: TButton
            Left = 16
            Top = 53
            Width = 75
            Height = 20
            Caption = #35745#31639
            TabOrder = 0
            OnClick = Button64Click
          end
          object RadioButton23: TRadioButton
            Left = 16
            Top = 15
            Width = 122
            Height = 17
            Caption = #20004#32452#25968#25454#20301#24322#25110
            Checked = True
            TabOrder = 1
            TabStop = True
            OnClick = RadioButton23Click
          end
          object RadioButton24: TRadioButton
            Left = 16
            Top = 35
            Width = 167
            Height = 17
            Caption = '8'#23383#33410#20998#32452#20301#24322#25110
            TabOrder = 2
            OnClick = RadioButton24Click
          end
        end
      end
    end
    object TabSheet9: TTabSheet
      Caption = 'QRCode'
      ImageIndex = 10
      object PaintBox1: TPaintBox
        Left = 0
        Top = 0
        Width = 237
        Height = 224
        Align = alTop
        OnPaint = PaintBox1Paint
      end
      object Memo9: TMemo
        Left = 0
        Top = 224
        Width = 237
        Height = 292
        Align = alClient
        Lines.Strings = (
          'qrcode')
        TabOrder = 0
        OnChange = Memo9Change
      end
    end
    object TabSheet10: TTabSheet
      Caption = 'Chart'
      ImageIndex = 11
      object Memo10: TMemo
        Left = 0
        Top = 0
        Width = 237
        Height = 469
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Lines.Strings = (
          #20351#29992#28857#32472#22270#21151#33021
          ''
          #30446#21069#25903#25345#25991#20214#23548#20837#26041#24335#21644#25163#21160#36755
          #20837#26041#24335#65292#25991#26412#26684#24335#20026#65292#27599#34892#19968#20010
          #25968#25454#65292#25903#25345#28014#28857#25968#25454
          ''
          #20030#20363
          '-101.118575'
          #13'-83.913871'
          #13'-91.686536'
          '-94.401629'
          #13'-91.775344'
          ''
          ''
          #25968#23383#31034#27874#22120#21487#30452#25509#20351#29992#24320#28304#36719#20214' '
          'SerialPlot'
          ''
          'SerialPlot'#20855#20307#25551#36848#35265#23448#32593#65306
          'https://hackaday.io/project/'
          '5334/logs')
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object GroupBox46: TGroupBox
        Left = 0
        Top = 469
        Width = 237
        Height = 47
        Align = alBottom
        TabOrder = 1
        object Button16: TButton
          Left = 74
          Top = 16
          Width = 88
          Height = 25
          Caption = #25171#24320
          TabOrder = 0
          OnClick = Button16Click
        end
        object Button20: TButton
          Left = 127
          Top = 16
          Width = 88
          Height = 25
          Caption = #25968#23383#31034#27874#22120
          TabOrder = 1
          Visible = False
          OnClick = Button20Click
        end
      end
    end
    object TabSheet11: TTabSheet
      Caption = 'fft'
      ImageIndex = 12
      object Memo16: TMemo
        Left = 0
        Top = 0
        Width = 237
        Height = 469
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Lines.Strings = (
          #30446#21069#25903#25345#25991#20214#23548#20837#26041#24335#21644#25163#21160#36755
          #20837#26041#24335#21644#20018#21475#36755#20837#26041#24335#65292#25991#26412#21644
          #25163#21160#26684#24335#20026#65292#27599#34892#19968#20010#25968#25454#65292#25903
          #25345#28014#28857#25968#25454
          #20030#20363
          '-101.118575'
          #13'-83.913871'
          #13'-91.686536'
          '-94.401629'
          #13'-91.775344'
          ''
          #20018#21475#36755#20837#26041#24335#26684#24335#35201#27714#65306
          'flag1  flag2 datalen_h  '
          'datalen_l data0 data1 data2 '
          'data3 ... datan xor_data; '
          ''
          'flag1 = 0xAA;'
          'flag2 = 0xBB;'
          'datalen_h'#21644'datalen_l'#32452#25104#19968#20010
          '16'#20301#25968#25454#65292#34920#31034#23454#38469#25968#25454#38271#24230#65292
          'len = (flag1 << 8) | flag2;'
          #27599'4'#20010#25968#25454#32452#25104#19968#20010'32'#20301#28014#28857#25968';'
          'xor_data '#20026'data0'#33267'datan'#30340#24322
          #25110#26657#39564
          ''
          ''
          #25968#25454#38271#24230#24517#38656#20026'4'#30340#25972#25968#20493
          #37319#26679#39057#29575#31561#21442#25968#30001'"FFT'#21442#25968'"'#19968
          #26639#20013#35774#32622#39)
        ParentFont = False
        TabOrder = 0
      end
      object GroupBox45: TGroupBox
        Left = 0
        Top = 469
        Width = 237
        Height = 47
        Align = alBottom
        TabOrder = 1
        object Button19: TButton
          Left = 81
          Top = 15
          Width = 75
          Height = 25
          Caption = #25171#24320
          TabOrder = 0
          OnClick = Button19Click
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'More'
      ImageIndex = 3
      object GroupBox15: TGroupBox
        Left = 0
        Top = 0
        Width = 237
        Height = 516
        Align = alClient
        Caption = 'data'
        TabOrder = 0
        object Memo13: TMemo
          Left = 2
          Top = 16
          Width = 233
          Height = 384
          Align = alClient
          TabOrder = 0
          OnChange = Memo13Change
        end
        object GroupBox16: TGroupBox
          Left = 2
          Top = 400
          Width = 233
          Height = 114
          Align = alBottom
          TabOrder = 1
          object CheckBox61: TCheckBox
            Left = 11
            Top = 9
            Width = 52
            Height = 23
            Caption = 'HEX'
            TabOrder = 0
            OnClick = CheckBox61Click
          end
          object Button5: TButton
            Left = 107
            Top = 84
            Width = 47
            Height = 23
            Caption = 'SendFile'
            TabOrder = 1
            Visible = False
            OnClick = Button5Click
          end
          object Button14: TButton
            Left = 11
            Top = 78
            Width = 91
            Height = 25
            Caption = #25130#22270
            TabOrder = 2
            OnClick = Button14Click
          end
          object ComboBox6: TComboBox
            Left = 11
            Top = 39
            Width = 133
            Height = 22
            Style = csDropDownList
            DropDownCount = 15
            ItemHeight = 14
            ItemIndex = 0
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            Text = #24322#25110#21644
            OnChange = ComboBox6Change
            Items.Strings = (
              #24322#25110#21644
              #32047#21152#21644
              'Hex2Ascii'
              'Ascii2Hex'
              'Ascii'#30721#34920
              'Utf8_2_Gbk'
              'Gbk2Utf8'
              'Gbk2Unicode'
              'Unicode2Gbk'
              #28014#28857#25968#36716'HEX(IEEE 754)'
              'HEX'#21046#36716#28014#28857#25968'(IEEE 754)')
          end
          object Button17: TButton
            Left = 151
            Top = 39
            Width = 66
            Height = 20
            Caption = #35745#31639
            TabOrder = 4
            OnClick = Button17Click
          end
          object Button4: TButton
            Left = 130
            Top = 78
            Width = 91
            Height = 25
            Caption = 'Img2Lcd'
            TabOrder = 5
            Visible = False
            OnClick = Button4Click
          end
          object CheckBox66: TCheckBox
            Left = 151
            Top = 15
            Width = 80
            Height = 17
            Caption = #22823#23567#31471
            TabOrder = 6
            Visible = False
          end
        end
      end
    end
    object TabSheet12: TTabSheet
      Caption = #25903#25345#20316#32773
      ImageIndex = 13
      object Image1: TImage
        Left = 7
        Top = 8
        Width = 224
        Height = 223
        Picture.Data = {
          0A544A504547496D616765BB410000FFD8FFE000104A46494600010101006000
          600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC000110800DB00DC03012200021101031101
          FFC4001F0000010501010101010100000000000000000102030405060708090A
          0BFFC400B5100002010303020403050504040000017D01020300041105122131
          410613516107227114328191A1082342B1C11552D1F02433627282090A161718
          191A25262728292A3435363738393A434445464748494A535455565758595A63
          6465666768696A737475767778797A838485868788898A92939495969798999A
          A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
          D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
          01010101010101010000000000000102030405060708090A0BFFC400B5110002
          0102040403040705040400010277000102031104052131061241510761711322
          328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
          292A35363738393A434445464748494A535455565758595A636465666768696A
          737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
          A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
          E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00EEFE
          257C51FF008579716117F63FDBFED68CD9FB4F95B7071FDC6CD709FF000D35FF
          00528FFE54BFFB5557FDA57FE423A0FF00D7293F98AE9FC01F0D7C1DAA7C3DD2
          353D4B48864B896DFCC9A576233C9E4F340181FF000D35FF00528FFE54BFFB55
          1FF0D35FF528FF00E54BFF00B5576DFF000AF7E177FCFA69BFF813FF00D7A3FE
          15EFC2EFF9F4D37FF027FF00AF401C4FFC34D7FD4A3FF952FF00ED55E89F0D7E
          247FC2C3B6BE9BFB2BEC1F65754C7DA3CDDD919FEEAE2B8EF8A7F0EBC25A1FC3
          4D5754D2B498A0BA8BC9314AAC4E374C8A7BFA13F9D54FD9AFFE419AEFFD768F
          FF00413401EEF5C67C47F1F7FC2BED1AD750FECDFB7F9F3F93E5F9FE56DF949C
          E76B7A7A56978A35FB4D3FC3DAB343A8DBC57B05ACAD1AF9ABB95C21238F5CE2
          BC4BE17EA779F1435DBBD2FC6331D52CEDADFCF8A29780AFB82E78F626803413
          F698DCEABFF08963271FF212FF00ED55EF1693FDA6CE19F6EDF36357DB9CE323
          35F297C54F07268FE3E36BA1691325888A260228D99771EBCD7D49673C569A1D
          AC97322431A41186690ED038039CFBD005FA2ABDADF5A5E8636B7314E178631B
          86C7E55F3278E3E25F8CB4EF1EEADA6E9DABCE90C5726386245071D30071401F
          51D794F8EBE347FC217E2D6D0BFB03ED9848DFCFFB6797F787F7761E9F5AF1EF
          F8585F147FE7EF53FF00C06FFEB57AC7807C2961E38F0C45AF78CB4E377ACB4A
          E8D35C028DB54E178E3B5007AD5B4DF68B4867DBB7CC40F8CE71919A96B35359
          D1A08D625D4ACD5506D0BE7AF18EDD697FB7B48FFA0A59FF00DFF5FF001A00F3
          497E3718BE212F853FE11ECE6F96CFED3F6DF560376DF2FDFA67F1AE97E247C4
          23F0F74CB3BCFECBFB7FDA66316CFB4795B70339CED6CD7CF7757109F8F89702
          54F27FB6E36F3370DB8DEBCE7D2BE99D5B48F0CF8DA18ED6FD6D751481BCC545
          973B4F4CF06803C923FDA5BCC9513FE112C6E2067FB47FFB557BC5BCBE7DB453
          636F9881B19E9919AE265F849E078A17913428432A920EE6E08FC6BE7C3F143C
          786FE4B3B2D5EE9F63B2C71C6818E07E1E82803EBEC8ACEF106ABFD87E1DD475
          6F27CFFB15B493F95BB6EFDAA4E33838CE3AE0D701F05B5CF11EB9A36A52F88E
          5B89268EE02C4678F69DBB474E3D6BADF1FF00FC93CF11FF00D8367FFD00D007
          91FF00C34CFF00D4A3FF00952FFED547FC34CFFD4A3FF952FF00ED55CAFC0CF0
          BE8DE29D7755B7D66C92EA286D95E35727E53BB19E2BD967F86DF0D6D65315C6
          9D611483928F3ED3F913401C27FC34CFFD4A3FF952FF00ED547FC34CFF00D4A3
          FF00952FFED55DB7FC2BEF85DFF3E9A67FE04FFF005EACDB7C2DF8777A8CF6BA
          4D9CEAA704C729600FE0680391F0FF00ED0875DF11E9BA47FC22FE47DB6E63B7
          F37EDFBB66E60338F2C6719E9915EDD5F1FD8595BE9DF1DEC6CAD2311DBC3AE4
          491A0FE10241C57D81401F3C7ED2BFF211D07FEB949FCC577DE17FF937D87FEC
          0F37F26AE07F695FF908E83FF5CA4FE62BBEF0C7FC9BEC3FF6079BF935007C93
          4E8BFD6A7FBC29B4E8BFD6A7FBC2803EAFF8ABFF00241EFBFEBDED3FF46C55CB
          7ECD9FF20BD77FEBB47FC8D753F157FE483DEFFD7BDA7FE8D8AB96FD9AFF00E4
          17AEFF00D768FF00F41340189F117E0FF896F3C41AF78962974F1624BDCE1A66
          DFB157278DB8CF1EB507ECE1FF00239EA9FF005E1FFB3AD6CF897E296BB37C41
          BCF05BC567FD9971762C1D8467CCF2E4C2920E719C31ED53F8AB44B6F81B610E
          BBE166925BABD93EC928BD3BD4260B700639CA8A00EF7C53F16BC37E10D74E8F
          A9457ED72115F30C2ACB86E9C961577E2068777E34F8757DA6696625B8BD585E
          233B155C0911CE4807B035F27F89BC557FE31F110D5B514856E1822110A90B81
          D3B9AFAA7C65E22BCF0A7C2C975AD3D626BAB682DF609572BF33A29C8FA31A00
          F39F095E45F02A0B9B2F1686925D45C4B01D3C79A02A8C1CEEDB839AA96BF0DF
          5CF1478F2D7C75A7C966348BABE8EFA359642B2F961813950A46783C66BCC3C6
          9E3DD5BC75716B3EAB1DBA35B2144F210A8C139E724D7D53F0CBFE49AE81FF00
          5E83F99A00EB715E7FE30F8B5E1CF08EAF368BA9457ED74220C4C30AB2E1871C
          961FCABD02BE51F8ECA1FE2BCCA7A35BC00FE5401E6B792ACF7B3CC99DB248CC
          33E84E6BABF06FC34D77C75697373A4BD9AC76EE237FB44A54E48CF18535E83E
          3DF843E1EF0CFC37B8F10594D7AD791A4040924057E77553C63D18D6DFECD9FF
          0022FEB7FF005F4BFF00A05007CFFAC69571A26B175A65D943716D218E4319CA
          E47A1AF5FF00D9B3FE467D6BFEBCD7FF004315E75F11BFE4A2EBDFF5F6F5E8BF
          B367FC8CFAD7FD79AFFE862803E8E9D4BC1220EACA40FCABE77D1BC0DAB7C2BF
          137FC26FE207B67D26D5A4122DA485E5FDE028B85200EAC33CD6C7C4EF8BDE22
          F07F8DAE348D3A2B26B68E28DC19A325B2CB93D08AF36F147C62F11F8B7409F4
          6D421B15B698A963144437CAC187393DC5007D2DE0CF1D691E3AB3B9BAD252E9
          63B79046FF00688C29CE33C609A93C7FFF0024F3C47FF60D9FFF004035F2B782
          FE266B7E06B2B9B5D2A3B568EE241239990B1CE31C608AFA4754D4E7D6BE08EA
          1A95C8413DCE8D2C8E106064C67A500794FECD9FF2336B7FF5E6BFFA18AE6BE3
          B7FC953BEFFAE30FFE802BA5FD9B3FE466D6FF00EBCD7FF4315CD7C76FF92A77
          DFF5C61FFD0050079B57D25FB377FC8AFAC7FD7E8FFD0057CDB5F497ECDDFF00
          22B6B1FF005FA3FF00401401E68BFF00270B6FFF0061E8FF00F468AFAE2BE475
          FF009385B7FF00B0F47FFA3457D71401F3C7ED2BFF00211D07FEB949FCC5753E
          00F88FE0DD37E1E691A5EA5ACDBC73C56FE5CD0BA938E4F078ADBF895F0BBFE1
          61DC584BFDAFF60FB2232E3ECDE6EEC9CFF79715C1FF00C3330FFA1B4FFE0BBF
          FB6D0076DFF09DFC2BFF009FBD23FF000187FF001347FC277F0AFF00E7EF48FF
          00C061FF00C4D713FF000CCC3FE86D3FF82EFF00EDB47FC3330FFA1B4FFE0BBF
          FB6D006AFC53F887E12D6FE1A6ABA5E95AC413DD4A211142808276CC8C71C760
          0D52FD9AFF00E419AEFF00D768FF00F41355FF00E19987FD0DA7FF0005DFFDB6
          BD13E1B7C38FF85796D7D08D57EDFF006A757DDF67F2B6E063FBCD9A00F9C3E2
          1437371F1735786CC31BA92F824210E09738031EF9AEFF00C0369A8F83B55B9B
          CF88EB2C1A6CD0F956EDA8379A865C83C0E70700D747E28F8422DFC4BA878EFF
          00B6CB1B697FB47EC5F65C6EF2C6ED9BF7F19DB8CEDEFD2BCD3E23FC5B3F1034
          6B5D3CE89F60F227F3BCC175E6EEF948C6362FAD007D19A4A7853C43A4B6A1A4
          D9E9D736C7728952DD7191D7B57C98E3C41E24F10CFA1D8CD797924D34823B5F
          3890C1496E84E380B9FC2BB0F027C663E09F0A8D106822F3123BF9DF6BF2FEF7
          6C6C3FCEB90F09F8B4F85FC7507898597DA7CA795FECFE6ECCEF465C6EC1E9BB
          3D3B50052D7FC2BAD785E5863D66C24B4798168C391F301F4AEC7C21E1FF001A
          DB5C68FADBC77E9A0412C77124BE71F2D6056058E33D300F6ACFF893F114FC42
          BBB1B83A58B0FB2C6C9B7ED1E6EEC9CE7EEAE2BE8AF0669DFDAFF0674FD37CDF
          2BED7A6341E66DDDB3702338C8CE33EB401B5A0F8DFC3BE27BA92DB46D4E2BB9
          A24DEEA80E42E719E4567788BC51E06D2F59369AE4F609A86D52566877360F4E
          715E62744FF867DFF89F2DC7F6F7F687FA279253ECDB3F8B76ECBE7A631814F8
          BC0E3E363AF8DCEA1FD8C646107D8C43F68C797C677EE4EBF4A00EDBE35323FC
          1DD51A3C796DF672B8F4F39315F38785BC3DE2FD66D6793C350DEC90C6E165FB
          3CBB006C77E476AFA3BE33C3F67F833A943BB77962D9338C6712A0AF0FF86FF1
          58FC3DD3EF6D468C2FFED5289377DABCADB818C63636680307C3B65750FC4ED2
          6CB568D8DCAEA70A5C24C7712778C83EB5F59EA9AA7863C13147777C6CF4C49D
          BCB575882EF3D71C0AF33D07E148F12EBDA7FC41FED936E6EAE1350FB0FD9B7E
          DC303B3CCDE33D3AEDFC2BB8F891F0FBFE1616976564753FB07D9A632EFF0023
          CDDD918C6372E2803C6FE20F87755F1EF8D64F127866CDF51D1E448D05CC78DA
          4A0C30E79E08AEAFC657BE16F16782A5F0F78552CAEBC4120882416F08590946
          52FCE07401BBD7A3F823C1C7C1BE114D045F7DAF6B48DE7795E5E771CFDDC9E9
          F5AE3FC1DF054784BC690F8886BE6E8C6653E47D8F6677A91F7B79E99F4A00E5
          FE1B41A4FC3ED36F6CFC7B6F6D6177733096DD6EE20E59000091C1E339AF53F1
          5DC59DDFC28D6EE34F28D6726953B42631852BE59C60578E7ED25FF232E8DFF5
          E6DFFA19AF5AF0E695FDB9F06ECF49F3BC9FB6E9660F376EED9B948CE32338CF
          4CD007877C0CF13E8DE17D7B559F59BE8ED229AD95119C1F98EECE38AF64B8F8
          89F0CAEE6335CEA3A64D29E0BC906E27F12B5C27FC3330FF00A1B4FF00E0BBFF
          00B6D1FF000CCCBFF4369FFC177FF6DA00EDBFE13AF855FF003F5A47FE030FFE
          26AD5AFC4DF875628C967AC585BAB1C911445413F80AF3FF00F86665FF00A1B4
          FF00E0BBFF00B6D2FF00C3332FFD0DA7FF0005DFFDB6803CFAC2F6DF51F8EF63
          79692096DE6D72278DC7460641CD7D7F5E25E1FF00D9EC685E22D37561E2633F
          D8AE63B8F2BEC3B77ED60719F30E338EB8AF6DA003B57CDBE22F88BE27B0F8C3
          36949ACCB16991EA51C463380A23257233E9C9AF61F1B7C49D1BC0735A45AAC3
          79235D2964FB3A2B631D739615F2CF896F62F1A7C44BBB9D343226A776AB089C
          6D2376146EC671CD007D9567AB69D7F218AD2FADEE2451B8AC5286207AF15E0B
          F183C7BE26F0EF8FCD8697AB4D6D6A208DBCB5C6327AD1E18D0AEBE065FCBE20
          F143C57169791FD8E35B025DC3921F2430518C21EF5E7BF13FC5761E35F191D5
          74D8E7481A148C09D42B647D09A00FA0BC69E2C953E194F3E83AA4726B66180C
          4B6F2079492E9BF0A393F2EECFE358DF08BC5DACDC5A6A47C63A8BC520917C81
          7B88891839C6719AE27C33E02D57E18EA165E3DD6E5B59749B142F2C76AE5A52
          25431AE01007DE9173CF4CD617C5EF1FE93E3CBCD366D2A2BA8D6DA3657FB420
          5C927B609A00E9B5EF14F8BEEFE235C582DD5E4BE199AF5627DB1E616B72407F
          9B18DB8279CD3BE2D7843C2F6FE1FB26F07D8DBCB786E7128B26F31826D3D402
          78CE2BD0BC3FFF0026F89FF6069BFF00416AF0AF849E37D33C0BE20BDBFD522B
          99229ADBCA516E818E7703CE48E38A00E4BFE11CD7075D22FBFF0001DBFC2A94
          1697173722DA082496724811A292C71D78FC0D7DAFE18F15D878CFC38DABE9D1
          CE96ECCF1813A80D91D7804D7CD9F09BFE4BA587FD77BBFF00D152D00757F05F
          E1EE9BACD86A8DE25D0D9E48E55117DA1194E31CE2BD6B5AD5748F0CF83752B2
          D2EFED2D66B2B2945BC2B32EE460A4A80339CE6A3F1AFC4BD17C07736B06AB05
          E48D7285D0DBA2B0001C739615F27F8C757B7D7FC5FAA6AB68B22DBDD4E64412
          00180F7C66803D3BE1C6BE3C7DABDDD8F8F6FE3BDB1B783CD812EDC2AAC99032
          3A738268F1A6ADE20F0B78864D37C0325CC5A0A46AE8B629E645BC8CB7201E73
          5E2B5F55FC0CFF00924B1FFD779FF9D00798F807C53AEF8EBC6B65E1AF13DFCB
          7FA55C890CF6B281B58A233AE71E8CA0FE15ED7FF0A8FC0D8FF917EDBF33FE35
          F35F82BC4967E12F89B1EB57E933DB5BCB3875854163B95D460123B91DEBDBBF
          E1A27C1FFF003E7ABFFDF94FFE2E803D0ED750F0FE896D1E9B0DFD95B476CBE5
          AC26751B00ED826AF5A6ABA7EA0EC9677B6F70CA32C22903103F0AF99FC51F0C
          B5BF1147AA78EACE7B25D2AE51EFD2396461288C0CE080A4678F5AD1FD9B3FE4
          68D67FEBCD7FF431401F425CEB5A5D94E60BAD46D60940C949255523F026A1FF
          00849742FF00A0C58FFE042FF8D7CC3F1E7FE4AA5E7FD7BC3FFA00AF33A00F70
          F8F71BEBFAFE95368E8DA8451DA9577B51E6053B89C12B9C1AC6F0578A3C7B63
          AEE87617336A3068F15CC51CAB24256348B70DD92470319A97E117C4ED13C09A
          46A16BAAC17B2497138910DBC6AC00DA0739615EFDA8B0F1A7C3BBCFECDF93FB
          56C24583CFF9705D481BB19C75F7A00E07E3478F2E747D134D93C37ADC693BDC
          B2CA6DE4563B76F7FC6BCAAC3E20FC4BBD314D0EA1A94D6ECE0174872BD79E40
          ACDF1AFC31D6FC09636D77AA4F6524771218D05BBB31C819E72A2BDEBE09FF00
          C92183FDF9FF00F42340167E2178AA787C01349E1CD4D64D6018B62DB38793EF
          0DDF28C9E99A83E0C6B1E24D6343D465F12C974F3A5C858BED09B4EDDA3A71EB
          5E07E00F13D97843E2226B3A8A4CF6D09995961505B2C081C123D6BEA2F05F8E
          F4BF1DD8DC5DE9715CC71DBCBE538B840A49C03C609F5A00D9FEDBD2FED42D7F
          B46D7ED05B6795E6AEEDDE98CF5ABF5F23AFFC9C2DBFFD87A3FF00D1A2BEB8A0
          0F9DFF00695FF908E83FF5CA4FE62BC42C2F25D3B51B6BD836F9D6F2ACA9B864
          6E53919FCABE8DF8E9E0DD7FC537BA449A369EF74B046E242A40DA4918EA6B43
          C17AEF83F43D0B49F0D6B66CA2D7A155B79EDE480338909E0138EBC8EF401C87
          8435ABBF8DDA8CDA0F8B362D9DA45F6B8FEC6BE5B6F042F24E78C31AE0BE27F8
          574EF0778DC695A5998DB88A37FDEBEE6C9EBCE2BDD3E2FF0085352D4FC37671
          78534FDB78B761A4368044DB36B75231C671C562F81B59D0FC17E1E5D27C772C
          306B4B2B4852EE3F35F61FBBF360F1F8D007A4DEF87ACBC55E068B45D44C82D6
          E6DE1DFE536D6F976B0C1FAA8AE2FF00E19F3C17FDFD4BFF000207FF00134CF1
          AF8D745F19F83EFBC3BE11D4FED5AD5D08C5AC106E466D922BB60F18C22B1FC2
          BC83FE15D7C50FF9F3D4FF00F02BFF00B2A00E9EEBC71AB68FE293F0DED45BFF
          00612DC2E9A0BA665F29C853F367AE18F38AEEFF00E19EFC17FDED4BFF000207
          FF00135E4DE1EF85FE38B7F16E977F7DA3DC6C8AF6296595E452400E09279F41
          5F5876A00C2F0BF8534EF08E84347D34CC6D43B3FEF5F7365BAF38AF34F15781
          747F865A4DEF8EB40370757B270D10B9937C7995C46D95C0FE191B1CFA5647C5
          AF0878DB5AF1C4977A15BDEBD8982350D0CFB57701CF1915DAFC3BF87D6F63A1
          69F7DAF5A5CB6B281BCD5B8B979139C8E50B153C1F4A00F9CBC65E3BD5FC7371
          6D3EAC2DC35BA144F263DA304E79E4D7A8E9FF0008BC3575F0A7FE12491AF7ED
          FF00D9B25D7130D9BD5491C63A715EB5AF7C3BF0978925864D4B4A899A20553C
          A631707FDD2335B9A769BA7E95A4C3A5DAC416CE14F2D2263B86DF439EB401F2
          AFC20F04E95E37D7EFACB563388A1B6F353C97DA73B80F43EB5F4D787BC2FA7F
          83FC36DA4E986536C9BDC79ADB9B2DC9E6B52DED34FB472F6D6D6F0B1182638C
          2923F0AC3D67C15E1ED7F506BEBF5B979D942931DE4B1AE074E1580A00F97BC0
          FE1DB1F157C4F8F46D44CA2D6796E0BF94DB5BE5576183F502BDCBFE19F3C17F
          DED4BFF0207FF135AD1FC1FF00025BCDE745A74B1CA33F3A5E4A1B9EBCEEAB1F
          F0ACFC238CECBDFF00C18CFF00FC5D007933F8E356B5F150F86D18B7FEC16B91
          A59263FDEF92C429F9B3F7B04F38AD8F186996FF0003ACADB55F0917373A8486
          DA6FB61F357601BB803183915EC963A268DA7C10C76F67001081B1DD433F1DF7
          1E49F7CD55F14784F44F17D84706B16A6E62818C9181232E1B18CF0450079878
          63C15A57C5DD123F177894CE353B87689FECAFE5A6D43B570307B56C7FC33DF8
          2FFBDA97FE040FFE26B33E1A78F7C27E15F09B691A8EA91DACD05EDC01132B12
          17CC3B79C7A57A068DF11FC29E20D522D374BD5A3B8BB941291AAB0270093D47
          A03401C97FC33E782FFBFA97FE040FFE26BB3D4D4783BE1DDF7F65F3FD97A7C8
          D6FE6FCDCA29233EBD2BC6BF68ABFBCB4F1168EB6D773C2AD68C488E42A0FCE7
          D2BD0848F2FECFB3C923B3BB685292CC724FEECD007CF1E31F895AEF8E2CADAD
          75616A23B790C89E4C454E48C73CD7BFFC13FF00924307FBF3FF00E846BE50AF
          AC3E08A97F8496EAA324C93803FE046803C13E1F786AC3C5BF119347D48CA2D6
          53333794DB5B2AAC473F515E85E2FD4EE3E07DEDBE93E12D8D6D7F1FDA66FB60
          F31B78257823181802B887F855E3F8AFE59ED746BA8D8BB1578E50A704FA835C
          E78A342F1168377043E228AE239E44DD189E4DE4AE71C727BD007D17E15F867A
          06AEFA578DAE9AEBFB5A768EFDC2CA047E66437DDC74C8E99AF56AF8BBE1FEA7
          7FFF0009FF0086E0FB6DCF92751817CBF35B6E378E319C62BED1A00F38F89BF1
          41FE1EDCD844BA42DFFDAD19B26E3CBDB83FEE9CD729A47C2E5F1BEAB65F109B
          57366D7B3A5F7D845BF98136B0F977EE19FBBD71DEBD2BC57A3F8475496DDBC4
          B158BBA2910FDA5C2903BE39AF04F126B9E36D23C457D61E159B518F42824DB6
          496885A2098FE12072339A00F6EF891E3B6F87FA1DB6A2BA70BE335C083CB337
          978F949CE769F4AF358FC14BF1C97FE13092FCE8CCFF00E8FF0065587CF03671
          9DD95EBF4AA5E02B9D4BC4DAC5C5AFC4969A6D2E380C902EA436209B700304E3
          9DA5ABDC7C3D63A269BA3343E1E4B74B205980B76CAEEEF401E38FF0CD3E0F29
          F1DA6AADAAB697D2CDADFC91279BFB9FBFB9B18F333D0F4C545FF0D2F2FF00D0
          A89FF81E7FF8DD607867C63A8F893E22A683E2AD55AE7C3F34F3ADC5BDCB8111
          08AEC99FA32AE3DC0AADF1A74AF0BE977BA5AF8662B38E378DCCDF6670D93918
          CD0074FF00F0D312FF00D0A89FF81E7FF8DD2FFC34BCDFF42AA7FE079FFE375E
          51E16F0D6A1A86BFA41974AB99AC26BA883B794C5190B8079F4C66BD5BE39F83
          3C3BE1AF0B69F73A3E9505A4D25E6C778C6095D8C71FA5007AD7C3EF179F1C78
          593596B11665A578FCA12F998DA719CE07F2AEA1F8435E43F0375CD2AC3E1BC3
          05DEA36B04A2E653B249429C67D0D7AC457505E59F9F6D324B130F95D1B20FE3
          40103B73503B91D0D39FA9A6B0154909958CAFB8E2985DFE94B732C76B03CD21
          C220C935C6DFEAB7BA849884B45103C01E9EF4E55230DC214A53D8ECC48D8E4E
          690C848EE6BCE64D4750B36DD1CECA076278AE8BC37E245D644B038DB7110CB0
          FEF0F5A98568CDD915528CA0AE748267000C9CFF002AD2B5666B490B1FE1358E
          1B073DEB5F4E21ADDF71E3A1CD5C9686713E77F0F7C184F1969779AF36BAD685
          AEEE17C916BBF1B5C8EBBC75FA5617C158BC8F8C3610E73B05C2E71D711B8AD6
          F881E2ED43C33E389F47F0AEAAD6BA46518C36CE0A6F7E5FF12739AF75D03C0B
          E1AD2A7B5D5AC748B782FF00CBDDE7A0F9B2CB86FCF26B32CF18FDA4FF00E464
          D1BFEBD1BFF4335EB5E1BD2BFB73E0DD9E93E7793F6DD2FC8F336EED9B948CE3
          8CF5ADAD77C1DE1FF12CF14FAC69905DC912EC469072A339C53ADF56F0EE8D6E
          9A6C5A858DB476C3CB584CCA3601DB19A00F99BE25FC294F87BA5D95E2EB06FB
          ED3318B69B6F2F6E1739CEE39ABFE01F8D32783FC396BA0AE84B7612563E71BA
          D9F79B3D361FE75DA7C799A3F11E83A541A2BAEA32C574CF225A9F30AAEDC648
          1D055AF855F0E741BDF02DADDEBBA0C675012C859AE1087C06E3F4A00F6281FC
          DB78E4231BD4363D322BE71FDA4BFE469D1FFEBC8FFE86D5E9BF123C4CD0F81E
          E93C2DA9A3EAAAF1AC4969207936EE1BB0073D335C678060B2F13E9B7571F128
          473DFC536CB53A99D8E22C03F2E71C6734011F803E08C79F0FF8AC6BEDB91E2B
          DFB37D9383821B6EEDFEDD715EFB5F30699E37D72DFE2C58E83A7EB128D0C6AB
          1DB456F1B031F925C0DA3DB15F4FD007957C5DF86BAC78F6EF4C974BB8B3896D
          51D5FED0ECB9C91D30A6A97867E2568FE0C4D37C05A8DBDEC9AAD9BAD9492C08
          A612ECDC104B038F9876AF612457C73F116F25D3FE2FEAF7B06DF36DEF9654DC
          32372ED233F95007B17ED1DFF22469BFF6101FFA2DEB8FF865F17340F067837F
          B2351B5D424B8F3647DD0468570DD3AB0AE27C61F13BC41E36D321B0D58DB793
          0CBE72F9516D3BB047AFA135DEFC2EF855E1BF1778246ABA98BBFB4F9D227EEA
          6DAB81D38C50079768BA1DCF8DBC66BA5E9AF1473DF4B2BC46724280033F3807
          B035DF7FC33A78BFFE7FF47FFBFD27FF00115E7FA5EB375E0DF187F69695B3ED
          1652CA9179ABB860864391DF826BE91F83BE3DD67C7167A9CBAC1B7DD6D22AA7
          931EDE08EFCD00753E1FB493C15F0EADA0D44ACADA5D9B34C60390C14163B738
          EDEB8AF07F8B5F14F43F1DF87ECEC34CB6BE8A582E7CE63711AA823691C618F3
          CD7D2BA8D943A969B73633EEF26E6268A4DA7076B0C1C7E75F387C5FF865E1FF
          0004F87ACAFB48173E74D75E53F9B2EE1B7693E9ED401CCF84FE10EBFE31D006
          B3A7DD69F1DB9764DB3C8E1B2BD780A47EB5F427C2789ADBE18E9D0B905A3F31
          091D321C8AF9DBC2BF157C47E13D1D346D37ECBF65F30B7EF21DCD963CF39AFA
          A3C37A65BE93E16B5B5B6DDE598FCCF98E4E5BE63FA9A0095CFCF4DC9348E7E6
          C52861568939DF16CCF158C0003869707F2AC38AE638AD4BB1518E49AE87C591
          BCBA2308D15DC3AED0E703F1AE26C6C85B5ACCDA85C23E4E7626540F4C026B8F
          11F19D943580979BAE94B22B01DB231557C3331B1F16C258858E44647F41C71F
          A815A36F69A7D9C0F242CED23F2C5DCB1FD6B322B692F75210C433BDB0C00EBE
          958C5F2CD58DE51E68D99EA8A01F9BAE2B52C06EB69157BF159318F2A254CF0A
          00FAD6C69BFEAD8E7BFE55E9377479B6B33E37F1B787EEBC29E36BBB2BF789E5
          1289F309246D73B875039C1AF73B6FDA1FC270DA4313586AE591154E218FB0FF
          007EBA4F177C29F0D78AB55B8D6752FB57DA9A30A7CB9B6AE1460718AF9D3E1E
          F8634FF12FC468343D404BF6373367CB7DADF2AB11CFD40A819F50F827C7BA67
          8F2CAEAEB4B82EA28EDE411B8B945524919E30C6BC83C53F023C51ADF8A752D4
          EDAF34B586EA769116495C3007D70869FE30D46E3E08DE5BE99E102A2DEFE337
          137DAC79A7783B783C606057B6784F549F59F09E99A95D94FB45CDBAC926D181
          93E82803C57C33A64FF01AE67D5BC50C97506A282DA15D389760C0EE25B7EDE3
          1F5AE866FDA27C25241220B0D5F2CA40CC31FA7FBF5DEF8C3C13A478E2D2DED7
          57F3FCAB790C89E4C9B4E48C735C64DF007C169048EA350CAA923FD23DBE9401
          E4BF049C4BF17ECE45CE1D2E1867DD1ABD5BE2E7C2FD6FC79AD585E697716514
          76F6E627170ECA49DC4F1853C735E55F04A3117C5FB345CED54B803FEF86AFAC
          49ED401F19785B4B9B45F8BBA2E9B72C8D35B6AF046E6324A92241D335F67570
          3FF0A8FC323C569E24FF004BFED05BA1763F7DF26F07238C74C8AEFA803E7FFD
          A32FAEED350D0C5B5D4D0868A4CF97215CF23D2B84F08FC3CF166ABAAE8FAE1D
          324B8B096E6299A77753B9038C939393C035EF5F11FE17C5F10AE2C6593567B1
          FB223280B0093764FF00BC315BBA7D80F037C3F16A921BCFECAB3760CC36799B
          416F7C7EB401C67C65F04DDEBFE18B2B6F0EE910BDD25D8790428A8766C61D78
          EE457CE9AAE9FE21F09DF7F655F35CD94FB43F92B310307A1E0E2BE91F869F16
          E5F1FEB975A749A3259082DCCFBD6E0C99F980C6368F5AF24F8F676FC4F76F4B
          688FE94012781BC1DAB784BC53A7F8A3C55A7791A0C01DEE2E26DAEA0491B2A1
          239272CEBDBBD749F10229BC7F716537C3906782D5196EFEC67C90189C8CF4CF
          15169FF10E4F8AB6F6DF0EE5D3574D8EFE3086F966F34A7923CDCECC0CE7CBC7
          5E335EA1F0E7E1C47F0F6DAFA28F536BEFB53AB12D0F97B7031FDE39A00E66C3
          C67A5E9FF0FD7C217BA8B2789C59BD99B76DC5FCF6042AEEE99C91CE6BC2BC51
          E13F18683630DC78861BA8EDE493646669F78DD827A64F6CD6EF883FE4E09BFE
          C350FF00E84B5EA1FB477FC899A5FF00D7FF00FEC8D40173E0569F613FC338A6
          B9B3B795C5CCB97922563807D48AF4BB1D4ACF55D285DE9F2092DDB2AAC06070
          707F9579EFC055DDF0B635CE337330FD6BB7F0F684BE1CF0FA698B706E046CED
          E614DB9DCC4F4C9F5A008D89DE73EB48D22A756C54373731C059DCE2BCCF52D6
          350BDD69A3825902648502A2B575490463CC7A55F46B79A7CD0AB02CCBF2F3DF
          B57946A17FA9C1A8AA5AE9EB29DD8989E0A2E79C56A69435797518A313B04439
          60DDC77AD1D7AD9C4EB78A006538720726B9A551548FB43AA8BE47CACC2BE8E4
          82292E831233B987A8FF00EB56FF00819ADA4867B92E86576000EE062B2EF0AC
          B6D93F700E456EF827C3A9244F2A426287390CC726A29B7CD78ABB3A2B4A2E95
          9E8CEA0B771DAB534EE6D6503EF1FE759D737FA0696DE55D5DAF983A8C963F90
          AB9A5EADA45DB14B0B98D989C95CE0FE46BD0E64CF1D57A5CDC9CCAFEA7C8BE2
          51E21D0FC5F7363A85D5E41299F788CCEC7E466CAF43E95F4778FBC3771A97C3
          5960F0FD820D564480C6D02AC6FF00794B7CDC76CF7AF1CF8DDA4EAD07C425D5
          6F6D152D2EBCB48258D8B2B6DC0C138186F6AF79F16F8A5BC19F0FFF00B712D0
          5DB4090AF9264D81B7155EB83EBE948D8F34F005D5A78034FBBB4F88A560BCB9
          944B6C2ED7CE2630307079C0CE6BCFEC35D7BEF8D564FA7DFCC74E97588BCA44
          760850C8380BE9ED59DF11BE20C9F10751B3BC934D5B136D09882ACDE66EC9CE
          7381599E01FF009285E1DFFB0941FF00A305007BAFED15757169E1CD19ADAE25
          859AED8131B95246C3E955FE15FC4DF0D68DE03B6B2D775AD97CB2485D650EE7
          05B8E707B53FF692FF00916745FF00AFC6FF00D00D713E07F82F0F8C7C1D1EBC
          FAE496ACCD20F245B071F2923AEE1FCA803DBBC3DE36F036B5ACC565A25C5A3D
          FB862823B7DAC40049E703B66B8AF8D3E15F1778835ED3A6F0EC1732411DB159
          4C33EC1BB713EA3B579AFC108FCAF8BD631E73B12E173EB88DABEB4A00F98BC2
          3E00F88963E31D16EF50B5BE5B386F629272F75B8040E0B646EE78AFA768A280
          3C7BE356A9E2FD3AF74A1E1896FD11E37337D9549E72319C578A5E78FBC772BC
          DA5DDEB3A8979730C96EE4E4EEE3691EF9AFB2C8AF927C4FFF0027012FFD8621
          FF00D096803A2F8196D3F86BC597F75AE44FA75BC96463496E97CB566DEA7009
          EF807F2AF5CD5ECBE1CEBF7E6F7546D1EEEE4A85F3249549C0E83AD727FB4771
          E08D371FF4101FFA2DEBE688FF00D6A7FBC2803EA8F1BF8374AF0DF832FB5AF0
          7E911DAEB7008CDA5C59A7EF17748AADB71EA8CC3E84D78E7FC24FF163FE7EB5
          DFFBF6DFE15F486A5E22B4F09780A3D72FA29A5B6B5B6837A400173B8AA0C024
          0EAC3BD709FF000D1BE12FFA066B5FF7EA2FFE3940166CBC29A6CBF0D4F89B52
          D2E33E255B092E9EEE54C4C26504863EE081F957CEDADF8C7C43E22B68EDB57D
          56E2F218DF7A24AD901B18CFEB5F5F8BA4F1AF80269F4E5689754B29161171C1
          5DCA546EC67BFA66BE5DF1B7C2BD6FC07A65BDFEA777613453CDE4A8B6776607
          04F3B9471C50043E19D6FC7963A6C70E8136A69A70909C5BA1299CF3DABEB8B0
          BB4BBD060B84944DBA15DCCA7396C73FAD79F7C08FF9254BFF005F137F3AE87E
          18807C076A0F7966FF00D18D4009A8E9CF7A3024C0F4AA09E1B581CC81879846
          306BB61629BC10791D38A3EC09BB2793F4AE79D0537765F3D8E42DED7C8B8C00
          3E61F31C75C5477B0B4F14A88324AF1815D97D823DD9CE0E3D29834B847E58E9
          4E9529423CADDC1CAEEE7934B6176A890346DBA420722BB6D7AE64F0E783EDAD
          ED4ED9A50232E3B64649ADD93458E4984A5F1C600DB55FC4FA19D6344FB3C647
          9F090F113D090318FC4511A5CB76B731CC2752A61E4A96F63CC21D0B53BD48A7
          82069D66270EA73CF7C9ED57FF00B3ACF42659AFEEDA4BC5395B6B56E41FF69B
          B7E15421D5753D36D27B086678A3673BC2F507A1E6A0B2B0BCD4AE562B785E57
          63C9C703DC9AC9596DB9F09195356F6716E7E7DFCBBFCCF498E2D3FC69E1456D
          56C229E3525846E370575CE08AF0CF00F88356F187C4283C35E20BE9750D1A43
          307B39CE50EC5665E3D8A83F857B26B7E22D23E19F83E05D4E4791E4CC6B1420
          17918F52012381EB9AF05F82D2ACDF18EC255042BFDA1867AE0C6F5D6AF6D4FB
          8C3F3FB28FB4F8ADA9A1F1DBC37A3F86F5DD2E1D1EC21B38E5B62EEB12E031DC
          466BD1FC09A4FC3B83C3BA26A3709A447AAC7124AD2BC8A1D641CE7AF5CD49F1
          6BE17EB5E3DD5B4FBBD2EEAC618EDA031B8B977524EE278DAA6BCBB58F80BE27
          D1746BDD52E750D25A0B381E791639642C5541271941CF14CD8EC3F684D674CD
          4BC3BA3C7637F6F72C976C5845206206C3E95E5BA0EB7E3DB3D156DF439B544D
          3416DA2DD094E7EF76AAFE08F01EA7E3EBEBAB3D2EE2D2192DA212B9B9765041
          38E36A9E6BD7B48F1C69DF087453E0AD7A0BAB9D4600CED2D8AAB44449F30C16
          2A7BF3C50079D7C19BD82CBE29595CDFDC241188E7DF24ADB402636EA4FBD7A3
          FC5BF14F895B5AB0FF00842F51BA9AD3ECE7CF36077A87DC7AE3BE315E2BE1BF
          0DDE78DBC52BA4E9D2C314F7064911AE090A00058E7009E83D2BE9BF84DE02D4
          FC07A35F59EA73DA4D25C5C79AA6D9D8803681CEE51CF1401B3E11F12D9CFE1E
          D2A0D435580EAAF0A2CB14928F30C87B11D739AEAEBE475FF938483FEC3D1FFE
          8C15F5C500792FC63F887AEF822EF4B8F47F236DCC6ED279B16FE4118AF03D3F
          56BBD7FE2569FAB5E85FB4DCEA30BC9B170B9DEBD057D9F3D9DADD106E2DA198
          AF4F3230D8FCEB0FC51A65843E13D6258AC6D9244B29995D625054843820E3AD
          0079E7ED1AC0F8234DC107FE2603FF0045BD731F0B3E16F86FC59E0C1AB6A7F6
          AFB5099D7F7736D185E9C62BC567BEBBBA40971753CCA0E409242C01FC6BA9F0
          E782FC6BAE691F6DD0EDEE5EC7732EE8EE020C8EBC64500779A1F8D354F1FF00
          8A22F87FAD987FB0EE1E4864F253649B615674F9BEB1AE6B07E30F81346F04DE
          E99168C2729711B349E6C9BF906B86D1F48D5F54F1147A5E969236A8ECEA8A92
          6D6CA825BE6CFA035F46FC1EF076B5A3D9EA4BE2BB0DD24922983ED2CB29C639
          C7271401E41A37C69F16E81A35AE95666C85B5AA79718783271EE7359BE2FF00
          89BE20F1B69D058EAE6D8C30CBE6A79516D3BB047AFBD1F106C1EE7E2B6AF616
          50AF992DE88A28D405058E0003B0E4D5AFF8535E3CFF00A01BFF00DFE4FF001A
          0067863E2BF893C25A18D234C36A2D43B3FEF21DCD96EBCE6B9AFEDED610B98F
          50BA85598B6D8E5655049C9C0CD2EB5A06A5E18D5C69FABDB1B7B950AEC8581E
          0F4E95ED5E3FF1BF82355F85B75A6695716ADAA3C56E1152D8AB656442DCE3D0
          1A00F37F0CFC54F15785E19E3B2BA59C4CC198DD29948C7A64F15D87873E3978
          C353F13E97A7DCB58F93737714326DB7C1DACC01EFE86B77F673B2B5BAD275A3
          716D0CC5674C19230D8F97DEB98F1A7C29F18DF78DF54BFD2B473F6592E0BC0F
          1C889C71820678A00FA17C4BE1BB2F145A456D7775770244FBC1B59CC6C4E31C
          91DABE65F89E975E0FF1CC9A5E97AAEA3F668E28E45F36E998E48C9E697FE158
          7C4FFF009F3BEFFC0C1FFC55721AE68FAC687E205B1D763912F976332C926F3B
          4F4E726981D46AFF00187C5FAD686FA4DC4D0C7038505E08CA49F29047CD9F6A
          F55FD9EAF6F2FB41D61AF6E269D96E54032B96206DF7AF41BA97C3DE1FF0B45A
          A6AB6D690DA45145E6486DC1C16C01D07A9158707C5BF8776AA56DF55B784372
          4476ECB9FC96819475EF8AFE06F0C6A3369335B4F3CB0B1128B7B7560ADDC12C
          464D739A9FED0DA25B5AC8BA0E85752CE47C8670B1A0F73B4927E9C7D6B86F12
          FC39F14F8ABC49A86BBA3698D73A6DF4C66B79848A03A1E8704E6B7BE1D68C7E
          18EA7797FE3BB48ECACEEE110DBB4CA250D203920019C71488508AD91E5BE20F
          136ABE2FD705EEB170D2C8EC155070B1AE7A28EC2BE9DF08FC26F0CF86751B2D
          774F1762F238890649B72FCC983C63DCD793F8D3C27A878EBC5AFE24F08D82DD
          688EB1AACD16D8D72830DF29C1EA3D2BB9F17F8BF4DF1A78324F0B7856FDAE35
          F904623823DD1B7EED833FCC70380ADDE9947B10607A1AA7AAD85B6B1A4DE699
          744F91770B4326D6C1DAC30707F1AF3DF831E1EF11787745D4A1F114534734B7
          01E212CBBC95DA07A9EF5C1EA5E15F1869BF13A5F12DD47731F87EDB5117734B
          F68CAAC0AC198EDCE718078C520347C65A6C1F042CADB54F08EE5B9D42436F37
          DACF9A3601B86071839ABBE16F04691F167438FC5BE26139D4EE19A3736D2796
          9843B471CF6AE6FE37F8EBC3BE2CD0B4BB7D17501732C372CF228465C0DB8EE2
          BD1BE05B04F85366CC70165989FF00BE8D005FF0CFC23F0CF84F5D8757D305D8
          BA895957CD9B70C3020F18F435DD9E95C23FC64F0246EC8FADA8653823CA7FF0
          A4FF0085CDE03FFA0E2FFDF97FF0A006FF00C2A5F0B8F15AF88C7DABFB416E85
          D0FDFF00CBBC1C8E31D322BBFED5F39E9FE15F186A5F13ECFC49691DCC9A04DA
          9C77492FDA30A61DE0E76E738C76C57D19401E6FF13FE284FF000F6E74F8A2D2
          A3BDFB523312F314DB83F435E69AAFED1177AA69179A7B787218C5CC0F097176
          4EDDC08CE36FBD7B678AAC7C1D7735B9F1445A73BAA9F27ED98CE3BE335CEFF6
          2FC23FF9F6F0F7E69401F3F7C34F0247E3FD76EB4E96FDEC8436C670EB107CFC
          C063191EB5E8D75E3493E08B3783ADAC97568C0F3FED3248613F3F38DA01E9F5
          AF4DD1A4F871E1FBA7B9D226D16CE674D8CF0BAA92B9CE3F302A3D587C34D76F
          4DE6A8FA25DDC950BE64AEACD81D05007CBFE17F16BF867C710F8992D16E1E29
          2593C8693683BD59719C1E9BBD3B57A9FF00C34BDE7FD0B107FE061FFE22BBFF
          00EC5F847FF3EDE1EFCD2AFE9FE0BF86FAB2BB69FA368F742338630A2B63F2A0
          0E2B44F85F0F8CB54B0F88526A925ACB793A5F1B258432A9561F2EFC8CFDDEB8
          AEE3E2678F25F0068969A845A7A5E99EE3C928D294C7CA4E7383E95E517D75E3
          4D0FE257D874F6D4ED3C316D7F1AAC71865B78E00416F60B8CD745F1BEEADFC5
          7E18D3ED3C3F326A77115DF99245687CC655D8C3240ED922803323F04C7F1B2D
          DBC69737CDA4C8E0C1F658E3F380F2F8CEE2475FA5793F843C251F89FC7D6FE1
          A7BB6B74964993CF54DC46C476E991D76FAF7AF7CF83FA8D8F86FC051E99AE5D
          43A7DE7DA24636F72C11F693C1C1F5AECE1F0CF837C3D38F1045A6E9D6324796
          179B42EDDFF2E777BEEC7E34019FF0EBE1DC5F0FAD6F6DE2D45EF45D3AB96788
          26DC0C7A9AEDC5787FC59F12F882EEFF004F3E06D4AEE78162617074E72C0367
          8DD8EF8AF257F1F7C418EFBEC2FAF6ACB75BC47E4990EEDC7B63D6803E93F89B
          E3E97C01A3DA5FC3A7A5E9B89FCA28F294DBF2939CE0FA57CBFE36F1849E33F1
          5B6BB2D9ADAB1444F2964DE3E51EB815EB5F0C74FF001078B35ABBB5F1F5ADE5
          FD8C50799026A284A2C9B80C8CF7C66B92F8B3E05B9B4F1D4F1F87B409534E10
          4640B680ECDD8E7A50074761F1065F8B71C1E00B8D3934D8AF5003791CA6565F
          287983E520673B31D7BD707F137E1F45F0FF0051B1B58B517BD1731190B3C413
          6E0E31D4D725A76A5A8687A925E69F732DA5E43B82C919C32E4107F42457B5FC
          30D4B43F1669D7D3F8FEEED350BA865096CDA9382CA846485CF6CD3191F803E3
          65C42740F0AFF61C463324569F68FB41CE0B01BB6EDF7E99ADDFDA4B9F0BE8DF
          F5F8DFFA01AF0FF14CB0699E3CD426D0DD2086DEECBDABDB9C04C1C82B8AF50F
          83F7771F10B5AD46CBC5D2B6B36D6D6E25862BC3E62A396C6403DF1408EF3E08
          1FF8B450FF00D75B8FFD08D78FFC1CFF0092D369FEF5D7FE8B7AE8BE2243E2FF
          000E78AEE34DF06C5A8D9686912324362A44418AE5FA71D7AD72FF00044B9F8B
          BA69933BCADC6ECF5CF94F9A00F6AF89DF1567F87DAA595A45A4C77A2E613296
          69CA6DF988C743E95E6BAEFED0575AE681A8692DE1D8615BCB7780C82E8929B9
          48CE36F3D6A7FDA4BFE464D1BFEBCDBFF4335E81E10F0278326F00E97A96A5A1
          E9CCC6D1659E79631E9C926901F2857AAF81FE32DC784FC310787A3D162B94DE
          FF00BE6B82A7E73E9B4F4CD7B6E9DE0FF867AB48F1E9FA568B74E8BB99625562
          07AF15F3DFC5FD2AC744F88F7365A55A476B6E91C4C9142B800950781F5A6074
          5E3CF8376FE17F085CF89135A92E1C3C6DE41B70A3E7603AEE3D33E95E415ED9
          F0F23F18788BC5767A678BE2D4AF341923732417AAC612421299CF1C10315ECB
          FF000ACBC138CFFC233A77FDF91480F27F87FF001B2E4CFE1EF0AFF61C5E5BC9
          0D9FDA3ED07382C1776DDBEFD335F42D7CAD67E10D4EC7E3559CB6BA2DCC5A6C
          3ACC6C8C90911AC6241CE7D315F54D007CF1FB4AFF00C84741FF00AE527F315C
          C787BE05F88BC49A059EB16BA8E971C1771F98892BC8180CE39C211DBD6BA6FD
          A57FE423A0FF00D7293F98AF47F055E4BA7FC0DB2BD808135BE9924A848C8DCA
          188FE5401E4BFF000CE1E29FFA0AE8DFF7F25FFE228FF8670F14FF00D05746FF
          00BF92FF00F11595FF000BF7C73FF3F163FF0080A29C9F1EFC72D22837165824
          0FF8F51401078ABE0A6BFE11F0DDDEB97BA86992DBDAECDE90BC85CEE75418CA
          01D5877AEFBF66BFF9066BBFF5DA3FFD04D75BF17267B8F821A94F2637C90DAB
          B607733444D725FB35FF00C8335DFF00AED1FF0023401ECDAF5849AA787B52D3
          E164496EADA48519F3B416520138EDCD785E83A1DCFC06BA975EF11C915F5B5F
          27D9234D3896757CEEC9DE14630A7BD58D5FE2B78AECFE2BB787E27B61A70D46
          3B7E6DC16D85941F9BF13CD6B7ED18449E0DD2C21DC45F745E7F81A8039ED5FC
          197FF19F55FF0084C741B8B6B3B070B008AFCB2CBB93827081863F1AF40F8C30
          B5B7C14D4E0720B471DAA123A12268C557F8060AFC31843023FD2A5EBF5AEE7C
          45A1E9DE2AD0EE345D44B35ADC6DDEB1BED6F95830C1FAA8A00F9B3E12FC4DD2
          7C0363A8C1A959DECED732ABA1B654200031CEE615D4D87C31D5FC57E32B3F1F
          595E5945A6DDDE477E904CCE2608181C1014AE78F5C5723F197C0DA2F82750D3
          21D192644B8899E4F3642FC838AF6EF0B5ECDA67C0EB5BFB6204F6DA4C934658
          646E5562323EA2803D03151DC7FC7B4BFEE1FE55F2A7FC2FCF1CFF00CFC58FFE
          028AF76F85DE27BEF17F8163D4B5896137524D246C63508300E071401F2FE8BE
          19BBF17F8DDB44B19A08AE2E2698ABCE4841B4331CE013D07A54FE39F00EA5E0
          2BCB5B5D4AE6D277B98CC886D9988001C73B9457B678B7C17A27C36D16F3C67E
          1A128D6AD5D4C46597CC4FDE3847CAF7F95DAB37C15A743F1A6D2EB50F19A3C9
          71612082036C7C90108DC72075E698CE3744F80FE23D7B44B3D56DF52D2921BA
          88488B23C81803EB8422BA7F0FE9537C04B99B58F123A5F41A8A7D9624D38966
          5607765B78518C0ED9AF75D234FB3D174BB6D32D1B105B208E30CD9381EB5E43
          FB497FC8B1A37FD7E37FE8069089DBF689F0BDC298574AD603483682638B1CF1
          FDFAE6B49F03EA3F0AF575F881ACDC5ADD69906E2D059B3198F9A0A2E03055E0
          B8CF3EB537C2AF85BE16F14782AD758D516E0DE34D229D93ED1856C0E2BDAFC4
          1E1AD33C47E1D7D13520E6C5C20608FB4FCA411CFD40A00F97BE2CF8FB4DF1F6
          ADA7DDE9B6D77025B40626172AA093B89E36935EFF00A1E9936B5F052DF4BB77
          449AF3493023484ED0CC840271CE39AF06F8C5E0CD17C19AC69D6DA2ACA229ED
          CC927992EFE771151E95F19BC69A46956DA759BDB0B6B78C47186B604E07BD30
          3D83E12FC2ED63C05AB6A177A95E58CE973008905B3392086CF3B945793FC6CF
          F92BD71FEE41FF00A08A79F8F5E3B51F34B663EB682B89F10F88F54F14EB8DAD
          6A3B1EED8282D1C7B57E5E071401F607887C5167E0DF080D6AFA19E6B7856256
          48002C7710A3192077F5A83C0DE3DD37C7DA7DCDE69B6D750476F2F94C2E5541
          2700F1B49E39AF997C41F147C57E26D01F45D44C0D66FB3212DF6B7CA411CFE1
          553C27F11BC47E07B39ED34878238E793CD71343B8938C77FA5007D9F814B5F3
          4784FE35F8C758F1868DA65DCF666DAEEF628650B6E01DACE01C1EDC1AFA5E90
          1F3BFED2BFF211D07FEB949FCC577FE17FF937E8BFEC0F37FE82D5C07ED2BFF2
          11D07FEB949FCC57A67C3FB11AA7C18D2EC19CC6B73A7B425C0C95DDB867F5A0
          0F8F29D1FF00AD4FF7857D11FF000CD3A7FF00D0C975FF0080CBFF00C552AFEC
          D7A7AB06FF008492EB839FF8F65FFE2A803A6F8ABFF2422FBFEBDACFFF0046C5
          5CB7ECD7FF0020CD77FEBB47FC8D761F18ADFEC9F05756B70DB8451DB203EB89
          A215C7FECD7FF20CD77FEBB47FC8D007AB78C2CED878435C9C5BC42616333093
          60DC0843CE7D6BE6FF00841E2ED2B42F115F4FE26BD636AF6BB23F395A51BF70
          3D39ED9AF41F13FC5BBAB8F175F7814E930886E67FECE375E71DCA24C296DB8C
          646EE99A8BFE19AB4FFF00A192EBFF000197FF008AA00EB87C64F87F0DBB4506
          A8B1A9070A96EE067F2AF06F00F8B2DF48F8A56BAB6A9A8CCBA624B70CECCCCC
          30D1B85E3EA45667C44F0845E08F153E8D0DDBDD22C492798E814FCC338C0355
          BC09E198FC61E33B0D0A5B96B64BAF3332A2EE2BB6367E9FF01C500767F1B7C6
          3A278BB51D2A5D16EFED09042CB21D857049CF715DCE81F107C3771F0B2DFC2B
          15FEED62E34F6B28E0F2DB995C1555CE31C9239AF2FF008A1F0EE0F87D7B616F
          06A125E0BA8D9C978C26DC1C7626BBFF0087DF052D2EEC740F149D6A75937477
          7E479031956CEDCE7DA803C9FC4BE00F11F846CE1BAD6AC3ECF0CD27968DE62B
          65B19EC7DAAEF877C07E33D7B451A8E8B6F33D89660196E020C8EBC66BD8FF00
          691FF914749FFAFE3FFA01AD6F819FF24963FF00AEF3FF003A00F1DF83B25CCF
          F1634DB3BD96499009D5E395CB292227EA0F1D457D15AFF8CFC2FE099E2B7D52
          E12C9EE177A2A424EE00E33F28AF94FC3DE2793C1DE3D3AE456C972F6F2CC044
          EDB436E0CBD7F1AF58B2D357F68146D52FA53A3B6987ECEB1C03CD1203F36493
          8C500654BE1EF12CFE3A5F1B44263E1917AB7E67F3F03ECEAC18B6CCE7A03C62
          B77E23EA16DF1734CB3D37C1727F68DD594C679D08F2F6A11B41CB633C9AC0F1
          0FC53B9F0DE91A978023D2E29E0B581F4F176D290CC0A91BB6E319E7A66A4FD9
          B7FE468D67FEBC97FF004314C0F30D534ED6BC21AC8D2B5269AD278B6BB44936
          40079CF0715ED1F11BE28786B5AF86B3E97A4EAAEFA8B0836AAC6EA7E5652DCE
          3D01AE1FE3A2799F166E509C6E86019FF808AEE2DFF66EB09EDA298F88EE4191
          0363ECCBC6467FBD480F9F65B89EE08334D2484700BB138FCEBE95F087C4CF87
          FA778474AB3BFBA816EE1B754941B42C430EBCEDE6BC8BE27FC3E83E1F6A7636
          906A125E0B984CA59E309B7048C704D72DE1DD2D75BF12E99A53CA625BCBA8E0
          320192A1980CE3F1A607A9FC67F197857C4DA2E9B0787E68E49A2B8679024053
          0BB71E83BD6DFC2AF1EF83343F045A69BAC4F0A5F895F2AD6C5CE0B71CE2AD7F
          C33569FF00F4325D7FE032FF00F155E45E36F0AC5E0BF1C1D161BA7BA48BCA7F
          35D0293BB07A0A00FB123D3F4F923575B2B62AC0107C95E9F957CEBFB45DBC36
          FE27D2161863881B324845033F39F4AF68F19F8AA5F05F80DB5B86D52E5E1585
          444EFB41DC42F5FC6BCC2CF495FDA0236D6AFA56D1DF4E3F6458A01E68707E6C
          92718EB401C87C3FF865E2BFF8493C3BAF7F668FECE5BB82E4CBE6AFFAB0E093
          8CE7A57D5B5E15A0FC57BAD13C57A7780534A865860BB8F4E176652188DC1776
          DC633CF4CD7BAD203C1FF683D0F55D5EFF00456D3B4FB8BA11C520730C65B6F2
          3AE2BCE2C67F8A5A659456564DAFDBDB42BB638A30E1547A015F5FD1401F247F
          6AFC5CFF009F9F11FE6F47F6AFC5CFF9F9F11FE6F5F5BD1401F1EEA4DF13F58B
          096C3511AF5D5A4B8DF0CA1D95B041191F500FE15EB3FB3E68DA9691A7EB29A8
          D8CF6AD24A8504C8577707A66BDA68A00E37C51E0CD0DF4ED5757B7D12D5B591
          04934372908F37CE0A4AB03D77640C5705F066F7C6F71E24BE5F13CDAB3DA8B5
          CC62F0B6DDFB874CF7C66BDBE8A00F993E37786F5BD53E22CD7361A5DDDC406D
          A25124511619039E6A97C1EF0BEBBA77C53D1EEAF749BC82DE3F3F749244428C
          C2E064FD48AFAA28A00C8D63C2DA1788248E4D5F4AB4BD788108678C31507D33
          57ECACADB4EB38AD2CE0482DE25DB1C518C2A8F402AC51401E45FB40695A86AF
          E17D322D3ACE6BA912F3732C485881B0F3C56BFC13D3AEF4EF86D05A5FDACB6F
          37DA2626395769C13E95E8D45007937C54F87BA2A7807529F43F0E5A8D50BC46
          37B680799CCABBB18E7A673557F67DD2351D2343D623D46CA7B577B95655990A
          9236FBD7B1D1401F2C6A9E10D56FFE353CB71A25CCDA6CDAB21919A126368CB8
          CE7B6315F4768DE15D07C3F3493691A45A594B2AEC768220A5875C1C56CD2500
          7CCDF18FC37ADEA5F14A6BBB2D2EEEE2DCC7001247112BC28CF35EC1F1164D6A
          DBE184CDA135D26A6AB00436D9F307CCBBB18E7A66BB9A2803E31D634AF1F788
          268E6D5ECB58BD9235DA8D3C6CC5475C0CD7D1DE04F01787EDBC37A25EDC787E
          D23D5638924695E1024590739CF5CE6BD028A005AF98FE2FF86B5BD47E29CF77
          65A55DDC5B948712471165E00CF35F4DD2D0079C7C5CB0BCD47E135C5A59DB4B
          3DC136F88A35CB1C3AE78AF9F747B4F88DA041243A45BEB765148DBDD205750C
          718C9C57D9345007C87E11F0E78A5FE24689A96A5A5EA04FF69432CF3CD137F7
          C12C4D7D79451401FFD9}
      end
      object Label35: TLabel
        Left = 4
        Top = 252
        Width = 218
        Height = 14
        AutoSize = False
        Caption = #22914#26524#35813#36719#20214#23545#24744#26377#25152#24110#21161
      end
      object Label36: TLabel
        Left = 4
        Top = 281
        Width = 221
        Height = 14
        AutoSize = False
        Caption = #35831#25903#25345#19968#19979#20316#32773
      end
      object Label37: TLabel
        Left = 4
        Top = 310
        Width = 202
        Height = 14
        AutoSize = False
        Caption = #26377#20160#20040#24314#35758#25110'BUG'#21487#21457#37038#20214#33267
      end
      object Label38: TLabel
        Left = 4
        Top = 339
        Width = 180
        Height = 14
        AutoSize = False
        Caption = '357853730@qq.com'
      end
    end
  end
  object Comm1: TComm
    CommName = 'COM1'
    BaudRate = 9600
    ParityCheck = False
    Outx_CtsFlow = False
    Outx_DsrFlow = False
    DtrControl = DtrDisable
    DsrSensitivity = False
    TxContinueOnXoff = True
    Outx_XonXoffFlow = False
    Inx_XonXoffFlow = False
    ReplaceWhenParityError = False
    IgnoreNullChar = False
    RtsControl = RtsEnable
    XonLimit = 500
    XoffLimit = 500
    ByteSize = _8
    Parity = None
    StopBits = _1
    XonChar = #17
    XoffChar = #19
    ReplacedChar = #0
    ReadIntervalTimeout = 20
    ReadTotalTimeoutMultiplier = 0
    ReadTotalTimeoutConstant = 0
    WriteTotalTimeoutMultiplier = 0
    WriteTotalTimeoutConstant = 0
    OnReceiveData = Comm1ReceiveData
    Left = 23
    Top = 216
  end
  object SaveDialog1: TSaveDialog
    Filter = '.txt'
    Left = 322
    Top = 177
  end
  object OpenDialog1: TOpenDialog
    Left = 297
    Top = 176
  end
  object Timer2: TTimer
    Enabled = False
    OnTimer = Timer2Timer
    Left = 50
    Top = 41
  end
  object PopupMenu1: TPopupMenu
    Left = 136
    Top = 123
    object N9: TMenuItem
      Caption = #22797#21046
      OnClick = N9Click
    end
    object N23: TMenuItem
      Caption = #31896#36148
      OnClick = N23Click
    end
    object N10: TMenuItem
      Caption = #20840#36873
      OnClick = N10Click
    end
    object N4: TMenuItem
      Caption = #23383#20307'/'#23383#22411'/'#22823#23567
      OnClick = N4Click
    end
    object N16: TMenuItem
      Caption = #23383#20307#39068#33394
      OnClick = N16Click
    end
    object N11: TMenuItem
      Caption = #32972#26223#39068#33394
      OnClick = N11Click
    end
    object N12: TMenuItem
      Caption = #36824#21407#23383#20307'/'#32972#26223#35774#32622
      OnClick = N12Click
    end
    object N13: TMenuItem
      Caption = #26085#24535#21487#35835#20889#27169#24335
      object N14: TMenuItem
        Caption = #21482#35835
        Checked = True
        RadioItem = True
        OnClick = N14Click
      end
      object N15: TMenuItem
        Caption = #21487#20889
        RadioItem = True
        OnClick = N15Click
      end
    end
    object N18: TMenuItem
      Caption = #26085#24535#27700#24179#28378#21160#26465
      object N19: TMenuItem
        Caption = #24320#21551
        RadioItem = True
        OnClick = N19Click
      end
      object N20: TMenuItem
        Caption = #20851#38381
        Checked = True
        RadioItem = True
        OnClick = N20Click
      end
    end
    object N17: TMenuItem
      Caption = #35774#32622#21253#38388#38548#26102#38388
      object N1ms1: TMenuItem
        Caption = '1ms'
        RadioItem = True
        OnClick = N1ms1Click
      end
      object N3ms1: TMenuItem
        Caption = '3ms'
        RadioItem = True
        OnClick = N3ms1Click
      end
      object N6ms1: TMenuItem
        Caption = '6ms'
        RadioItem = True
        OnClick = N6ms1Click
      end
      object N9ms1: TMenuItem
        Caption = '9ms'
        RadioItem = True
        OnClick = N9ms1Click
      end
      object N20ms1: TMenuItem
        Caption = '20ms'
        Checked = True
        RadioItem = True
        OnClick = N20ms1Click
      end
      object N50ms1: TMenuItem
        Caption = '50ms'
        RadioItem = True
        OnClick = N50ms1Click
      end
      object N100ms1: TMenuItem
        Caption = '100ms'
        RadioItem = True
        OnClick = N100ms1Click
      end
    end
    object RawBackUp: TMenuItem
      Caption = #25509#25910#26102#21407#22987#25968#25454#22791#20221
      object N1: TMenuItem
        Caption = #19981#22791#20221
        Checked = True
        Default = True
        RadioItem = True
        OnClick = N1Click
      end
      object N1M1: TMenuItem
        Caption = #26368#22823'1M'
        RadioItem = True
        OnClick = N1M1Click
      end
      object N5M1: TMenuItem
        Caption = #26368#22823'5M'
        RadioItem = True
        OnClick = N5M1Click
      end
      object N10M1: TMenuItem
        Caption = #26368#22823'10M'
        RadioItem = True
        OnClick = N10M1Click
      end
      object N20M1: TMenuItem
        Caption = #26368#22823'20M'
        RadioItem = True
        OnClick = N20M1Click
      end
    end
    object N21: TMenuItem
      Caption = #25442#34892#27169#24335
      object Windos1: TMenuItem
        Caption = 'Windos('#22238#36710'+'#25442#34892'CR/LF'#34920#31034#19979#19968#34892')'
        Checked = True
        Hint = #37319#29992#22238#36710'+'#25442#34892'CR/LF'#34920#31034#19979#19968#34892
        RadioItem = True
        OnClick = Windos1Click
      end
      object UNIXLinux1: TMenuItem
        Caption = 'UNIX/Linux('#25442#34892#31526'LF'#34920#31034#19979#19968#34892')'
        Hint = #37319#29992#25442#34892#31526'LF'#34920#31034#19979#19968#34892
        RadioItem = True
        OnClick = UNIXLinux1Click
      end
      object MACOS1: TMenuItem
        Caption = 'MAC OS('#22238#36710#31526'CR'#34920#31034#19979#19968#34892')'
        Hint = #37319#29992#22238#36710#31526'CR'#34920#31034#19979#19968#34892
        RadioItem = True
        OnClick = MACOS1Click
      end
    end
    object N25: TMenuItem
      Caption = #28165#38500#25509#25910#31383#21475#26102#30830#35748
      object N26: TMenuItem
        Caption = #38656#35201#30830#35748
        RadioItem = True
        OnClick = N26Click
      end
      object N27: TMenuItem
        Caption = #19981#38656#35201#30830#35748
        Checked = True
        RadioItem = True
        OnClick = N27Click
      end
    end
    object N24: TMenuItem
      Caption = #25171#24320#25193#23637#21151#33021
      object String1: TMenuItem
        Caption = 'String'
        OnClick = String1Click
      end
      object HID1: TMenuItem
        Caption = 'HID'
        OnClick = HID1Click
      end
      object CP1: TMenuItem
        Caption = 'TCP'
        OnClick = CP1Click
      end
      object UDP1: TMenuItem
        Caption = 'UDP'
        OnClick = UDP1Click
      end
      object DES1: TMenuItem
        Caption = 'DES'
        OnClick = DES1Click
      end
      object AES1: TMenuItem
        Caption = 'AES'
        OnClick = AES1Click
      end
      object SM1: TMenuItem
        Caption = 'SM'
        OnClick = SM1Click
      end
      object HASH1: TMenuItem
        Caption = 'HASH/MD5/CRC'
        OnClick = HASH1Click
      end
      object XOR1: TMenuItem
        Caption = 'XOR'
        OnClick = XOR1Click
      end
      object QRCode1: TMenuItem
        Caption = 'QRCode'
        OnClick = QRCode1Click
      end
      object Chart1: TMenuItem
        Caption = 'Chart'
        OnClick = Chart1Click
      end
      object FFT1: TMenuItem
        Caption = 'FFT'
        OnClick = FFT1Click
      end
      object MORE1: TMenuItem
        Caption = 'MORE'
        OnClick = MORE1Click
      end
      object N28: TMenuItem
        Caption = #25903#25345#20316#32773
        OnClick = N28Click
      end
    end
  end
  object Timer3: TTimer
    Enabled = False
    OnTimer = Timer3Timer
    Left = 81
    Top = 41
  end
  object pm1: TPopupMenu
    Left = 492
    Top = 554
    object N2: TMenuItem
      Caption = #28165#31354
      OnClick = N2Click
    end
    object N67: TMenuItem
      Caption = #20840#36873
      OnClick = N67Click
    end
    object N22: TMenuItem
      Caption = #22797#21046
      OnClick = N22Click
    end
    object N6: TMenuItem
      Caption = #31896#36148
      OnClick = N6Click
    end
    object N3: TMenuItem
      Caption = #22238#36710#21457#36865
      OnClick = N3Click
    end
    object N7: TMenuItem
      Caption = #21457#36865#25991#20214#35774#32622
      RadioItem = True
      object N8: TMenuItem
        Caption = #36830#32493#21457#36865
        RadioItem = True
        OnClick = N8Click
      end
      object N1K1ms1: TMenuItem
        Caption = #27599'1K'#24310#26102'1ms'
        Checked = True
        RadioItem = True
        OnClick = N1K1ms1Click
      end
      object N1K10ms1: TMenuItem
        Caption = #27599'1K'#24310#26102'10ms'
        RadioItem = True
        OnClick = N1K10ms1Click
      end
      object N1K50ms1: TMenuItem
        Caption = #27599'1K'#24310#26102'50ms'
        RadioItem = True
        OnClick = N1K50ms1Click
      end
      object N1K100ms1: TMenuItem
        Caption = #27599'1K'#24310#26102'100ms'
        RadioItem = True
        OnClick = N1K100ms1Click
      end
    end
  end
  object JvHidDeviceController1: TJvHidDeviceController
    OnArrival = JvHidDeviceController1Arrival
    OnEnumerate = JvHidDeviceController1Enumerate
    OnDeviceChange = JvHidDeviceController1DeviceChange
    OnDeviceDataError = JvHidDeviceController1DeviceDataError
    OnRemoval = JvHidDeviceController1Removal
    Left = 400
    Top = 43
  end
  object IdTCPServer1: TIdTCPServer
    Bindings = <>
    CommandHandlers = <>
    DefaultPort = 0
    Greeting.NumericCode = 0
    MaxConnectionReply.NumericCode = 0
    OnConnect = IdTCPServer1Connect
    OnExecute = IdTCPServer1Execute
    OnDisconnect = IdTCPServer1Disconnect
    ReplyExceptionCode = 0
    ReplyTexts = <>
    ReplyUnknownCommand.NumericCode = 0
    Left = 338
    Top = 41
  end
  object IdTCPClient1: TIdTCPClient
    MaxLineAction = maException
    ReadTimeout = 0
    OnDisconnected = IdTCPClient1Disconnected
    OnConnected = IdTCPClient1Connected
    Port = 0
    Left = 300
    Top = 42
  end
  object IdIPWatch1: TIdIPWatch
    Active = False
    HistoryEnabled = False
    HistoryFilename = 'iphist.dat'
    Left = 271
    Top = 39
  end
  object Timer4: TTimer
    Enabled = False
    OnTimer = Timer4Timer
    Left = 113
    Top = 40
  end
  object IdAntiFreeze1: TIdAntiFreeze
    Left = 370
    Top = 39
  end
  object PopupMenu2: TPopupMenu
    Left = 58
    Top = 125
    object N5: TMenuItem
      Caption = #26029#24320
      OnClick = N5Click
    end
  end
  object PopupMenu3: TPopupMenu
    Left = 95
    Top = 125
    object IP1: TMenuItem
      Caption = 'log:'#26174#31034'IP'
      OnClick = IP1Click
    end
  end
  object IdHTTP1: TIdHTTP
    MaxLineAction = maException
    ReadTimeout = 0
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = 0
    Request.ContentRangeStart = 0
    Request.ContentType = 'text/html'
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    HTTPOptions = [hoForceEncodeParams]
    Left = 239
    Top = 40
  end
  object SaveDialog2: TSaveDialog
    Left = 354
    Top = 177
  end
  object SkinData2: TSkinData
    Active = True
    DisableTag = 99
    SkinControls = [xcMainMenu, xcPopupMenu, xcToolbar, xcControlbar, xcCombo, xcCheckBox, xcRadioButton, xcProgress, xcScrollbar, xcEdit, xcButton, xcBitBtn, xcSpeedButton, xcSpin, xcPanel, xcGroupBox, xcStatusBar, xcTab, xcTrackBar, xcSystemMenu]
    Options = [xoPreview, xoToolbarBK, xoCaptionButtonHint]
    Skin3rd.Strings = (
      'TCategoryButtons=scrollbar'
      'TPngSpeedbutton=pngspeedbutton'
      'TPngBitBtn=pngbitbtn'
      'TVirtualStringTree=scrollbar'
      'TVirtualDrawTree=scrollbar'
      'TTBXDockablePanel=Panel'
      'TAdvPanelGroup=scrollbar'
      'TComboboxex=combobox'
      'TRxSpeedButton=speedbutton'
      'THTMLViewer=scrollbar'
      'TDBCtrlGrid=scrollbar'
      'TfrSpeedButton=speedbutton'
      'TfrTBButton=speedbutton'
      'TControlBar=Panel'
      'TTBDock=Panel'
      'TTBToolbar=Panel'
      'TImageEnMView=scrollbar'
      'TImageEnView=scrollbar'
      'TAdvMemo=scrollbar'
      'TDBAdvMemo=scrollbar'
      'TcxDBLookupComboBox=combobox'
      'TcxDBComboBox=combobox'
      'TcxDBDateEdit=combobox'
      'TcxDBImageComboBox=combobox'
      'TcxDBCalcEdit=combobox'
      'TcxDBBlobEdit=combobox'
      'TcxDBPopupEdit=combobox'
      'TcxDBFontNameComboBox=combobox'
      'TcxDBShellComboBox=combobox'
      'TRxLookupEdit=combobox'
      'TRxDBLookupCombo=combobox'
      'TRzGroup=panel'
      'TRzButton=button'
      'TRzBitbtn=bitbtn'
      'TRzMenuButton=menubtn'
      'TRzCheckGroup=CheckGroup'
      'TRzRadioGroup=Radiogroup'
      'TRzButtonEdit=Edit'
      'TRzDBRadioGroup=Radiogroup'
      'TRzDBRadioButton=Radiobutton'
      'TRzDateTimeEdit=combobox'
      'TRzColorEdit=combobox'
      'TRzDateTimePicker=combobox'
      'TRzDBDateTimeEdit=combobox'
      'TRzDbColorEdit=combobox'
      'TRzDBDateTimePicker=combobox'
      'TLMDButton=bitbtn'
      'TLMDGroupBox=Groupbox'
      'TDBCheckboxEh=Checkbox'
      'TDBCheckboxEh=Checkbox'
      'TLMDCHECKBOX=Checkbox'
      'TLMDDBCHECKBOX=Checkbox'
      'TLMDRadiobutton=Radiobutton'
      'TLMDCalculator=panel'
      'TLMDGROUPBOX=Panel'
      'TLMDSIMPLEPANEL=Panel'
      'TLMDDBCalendar=Panel'
      'TLMDButtonPanel=Panel'
      'TLMDLMDCalculator=Panel'
      'TLMDHeaderPanel=Panel'
      'TLMDTechnicalLine=Panel'
      'TLMDLMDClock=Panel'
      'TLMDTrackbar=panel'
      'TLMDListCombobox=combobox'
      'TLMDCheckListCombobox=combobox'
      'TLMDHeaderListCombobox=combobox'
      'TLMDImageCombobox=combobox'
      'TLMDColorCombobox=combobox'
      'TLMDFontCombobox=combobox'
      'TLMDFontSizeCombobox=combobox'
      'TLMDFontSizeCombobox=combobox'
      'TLMDPrinterCombobox=combobox'
      'TLMDDriveCombobox=combobox'
      'TLMDCalculatorComboBox=combobox'
      'TLMDTrackBarComboBox=combobox'
      'TLMDCalendarComboBox=combobox'
      'TLMDTreeComboBox=combobox'
      'TLMDRADIOGROUP=radiogroup'
      'TLMDCheckGroup=CheckGroup'
      'TLMDDBRADIOGROUP=radiogroup'
      'TLMDDBCheckGroup=CheckGroup'
      'TLMDCalculatorEdit=edit'
      'TLMDEDIT=Edit'
      'TLMDMASKEDIT=Edit'
      'TLMDBROWSEEDIT=Edit'
      'TLMDEXTSPINEDIT=Edit'
      'TLMDCALENDAREDIT=Edit'
      'TLMDFILEOPENEDIT=Edit'
      'TLMDFILESAVEEDIT=Edit'
      'TLMDCOLOREDIT=Edit'
      'TLMDDBEDIT=Edit'
      'TLMDDBMASKEDIT=Edit'
      'TLMDDBEXTSPINEDIT=Edit'
      'TLMDDBSPINEDIT=Edit'
      'TLMDDBEDITDBLookup=Edit'
      'TLMDEDITDBLookup=Edit'
      'TDBLookupCombobox=Combobox'
      'TWWDBCombobox=Combobox'
      'TWWDBLookupCombo=Combobox'
      'TWWDBCombobox=Combobox'
      'TWWKeyCombo=Combobox'
      'TWWTempKeyCombo=combobox'
      'TWWDBDateTimePicker=Combobox'
      'TWWRADIOGROUP=radiogroup'
      'TWWDBEDIT=Edit'
      'TcxButton=bitbtn'
      'TcxDBRadioGroup=radiogroup'
      'TcxRadioGroup=radiogroup'
      'TcxGroupbox=groupbox'
      'TOVCPICTUREFIELD=Edit'
      'TOVCDBPICTUREFIELD=Edit'
      'TOVCSLIDEREDIT=Edit'
      'TOVCDBSLIDEREDIT=Edit'
      'TOVCSIMPLEFIELD=Edit'
      'TOVCDBSIMPLEFIELD=Edit'
      'TO32DBFLEXEDIT=Edit'
      'TOVCNUMERICFIELD=Edit'
      'TOVCDBNUMERICFIELD=Edit')
    SkinStore = '(Good)'
    SkinFormtype = sfMainform
    Version = '5.60.03.29'
    MenuUpdate = True
    MenuMerge = False
    Left = 359
    Top = 230
    SkinStream = {
      B0B5000014760800D676B1987A5C760C073F2EADC831803E97FBDBF9F8F83F7F
      CDD7BADACB33968E06F3ACC02868D19408762D6F67D303BBDF5F9FD3000FA927
      F4035304FC286DFF156826417613574404DFFE1033DF3F39EFEF05C049430B88
      B3105D6414F5BC2F62312D3C2682BC3022BF1332D9579A15528F20F42EF9E72C
      C4A6E5F85980C98A6929028652A0F55C12EDCB51254F216B430DA9BA13819436
      5415A9C4A61CFF58E9976906D94B2721A82586A2AB3D69B2B4AA26FC2A861128
      23309E482224827E960C6A1A6A07AA80E31180CEFC407832B2D12016FE517F57
      F39FDC5EE7FCED7BB1EBD3E57D733C5D91DD60C12A685427B028C1C5FB530751
      784AFFEC3C0632E3C637C932741212D5F980DE9871D220B8DF7A25CFE0A57D08
      4EEBFB5DF847C67A3DB5A982B75A5D3C7BD7C680991C87EF18217FF5AEC070B1
      4F60F7B24FA9B32A1FB76E20C8DF9F4E03B772EEB039E95733FF112644580AE1
      50BC5320379C5EA8E1B9577403BC27BB337A80001F4552D3703B23907725C577
      5624D0BA69D9607ED00220575065EEA80C2396A1524D7F4B63D2D2B85F9A2297
      BE4EEAFF81024ECEFD42DAFBBDB2D2F82225EDAFB67B6340693C5345750D01F7
      79FEE9889A035F6732E009E51C48C43A148B8242604E74FE64B6407DFAF05AA9
      78CE4202EF842EBC832FDEBFF9F7B3FC84DBFE2D9CDFEFBE99DCDBD7E1ED823D
      16DE0EC8DB1B33739BE7CFA7C5697C3C5CBAD05D2AF587E156E0F7638A77CAB5
      C4198FDB4E4C1D79E632990F3940BEBDAB823D488B594BCB4A37E4BD74D0252A
      4B94F6733FC7483C10EAC7A9FD4C6BE02A0A83E8E44BD1953E54F20AEA2413D4
      AE134CF9E863305934D33D437F3A29EF95529879457D523B88D2F2F773E9ABCD
      F442CF4429A9BAB8C329710D8EB5E6D6C0739836FB274EEBE2E6E7106EB15398
      CA8E481E5AFAD255C44B64318E64B5AF68B8753BDC6F98EE33D6AA42E71EB64A
      0DCCF1B55DF7307538B1E89312AB903C0E46AED573B7C621DBA1DB33BEC71FFE
      A5EC7DBC56210455734BCBEE2592FCA2CAC2842EFF25C7A6295657053157BC48
      28E439CA7400E7C517E5C6E2F9AB852796B4D1AC753D226DBE37D1D7C29ECBAC
      2E30BF40547EAF4D93D8BFD67FA5641E071AA9EEE654A792B34D95A5D4DE4586
      90D2F9F7B6884EDFE3A4F7552EB07BD564E5DF7F66169A369B52E783753738F5
      D2B7FE15E0354BAB1CD96DEEC4B761A922007A2D5EE851DD81A51BD62759BDB6
      FA5231125956B39B7931D1348E59EA56F278297AA2633AC9D8EB6B91995D796D
      2A0E766F35630ED255E66212A6C57B22B249061526196927A5D0A1F129FA508E
      3C5C8BEF5A93BF11574CA91AEA11183B948230A1CCA98EF4CA650DE8E9462B97
      0027701405D8FDDEF06D13B320B846E11658C5D1DAFEAD05CD6AA1433DD5348F
      BE93B650F4CA5E41D6DE54CF36162C72A0084160F2DD5BA8D3603A1BBEFE5856
      7577E6C19E5B5DF4BE493CD52304ED712E237EFD5ED5C7366D325A8E7ECFD63E
      676CD502BAF6E994907C678E7B030EEFF2C0F307065586AA6D3A1CF404E56904
      53E3F806F4468ABCCB4B7C97B464E048AC4D2DBE50E5917011F150D1A807D321
      9902EF3CBFC57C95C477863FF9B861225BC45597680A576F5391FC1A1AE10B7A
      E747DF440F413762F2A1F9CBEDD4EB06C6E7FD3A7302E79DFCEE6A4AFC2E895D
      BE6F7552A0DECE8E2DB703FAB4D9976EEEDCFA8EE921D4B58C177B73BFE47197
      8ECA8719762F06F6B55B49DEE08043284FD2D89FB05C758764DC714E88BB84F8
      75B4CED7358C57E088AE56FCF7BAC78C4AE6A75F8300EC801AD80723018980D8
      0073003B90188019101AC01F6801C9C4DFAB01F32BB18FFC407DA4472C577731
      E1A52EE017AF01C601CA01D501F19018FCC1AAB2B5B6B716FDEB69BEB80C2018
      4031806300D401A803B8C0730A80D601DD701C281C403FE60348E9557E611D5F
      C757F480E901D203B007DA82F27C17A3BA3A7E13D77E74B9AE07A0FF80FF01FE
      03FC07C6C06D210188062018807C6406859E801F4901D1163E055DA6AFD357E9
      01A406901A807DA4373C25E282FC4C2FD3C2A8F3FDB74D80DEF9DE03800E0038
      00E003800E203980E603980E603EF427D877BF3ABF980E603C00FC03E36033B4
      E2B79B09A7B8529F954DAA1B343185F00FC03F00FC03F00FC03F00FE03F80FE0
      3F80FE03B403102BAFF8BD41E59D0F00EA00305F7BE6B7D3FCFAAFFE03F80FE0
      3201D42EFC42DAA370FE5C66F64FF36BB88806403201900C806403201900CC06
      603301F1901B482C8F28D37E6BF920F09DE9575288ACE94F8406F18A54CE802D
      6EAFBF489A936855403E2DF4FA1AB0CC06603301980F9DDDFE9822F46011C82A
      5E289FAEB34AA6D187531980DE59CFA80D4077500614DADEA03501A80D4074D8
      06AB3D806CC6D80754A935A37CC0640873BABFF37EFF8D96FFB7F2CDD681C168
      40B1AA8C4F540E1915D19D5277511B33B8DB8019BAAFEF158EBF2BA80D406A03
      8982CB782B43A939F8D678C8E535C3A0F35D1310D3ADF2A9B8BFD68D7819406A
      039080658B7BEC036018800E828F8D806C03701B80E1101D3FF7DE701B80E240
      1B03635B803F7595FA580C80710D207736D8E972A7C5C75FCC6432F5D6A5CB2B
      52F717EA4EEE17F3501FCA1100CE0D81E44445890EAEDAB1D806C03601B80EF5
      DB905497AB51E478547DC205D4ED47827F4D3E80DE8FDC06E03901CD8BC80E6C
      6E407403A01E00DCE0BF82759E700FAD52EFE0067F806080AE5FBEEB9FB2FD9E
      2F1FDB2F183D67BBE8AE8FDEEDC0D01AC1F2C06EFAADC2F58ED1ED07AC3C01E0
      0F007803EB9751EB7B5EBD4584547FF2A4F26DF007940F80313D01B69C95E00F
      007803C01B9A03CA3FC01B9603F434CCAA4D68065354EA6035EA5C5DC9A867B9
      67903DE51038D1BD77F45BC1DBF7511E3301FBE92DC06F3D471B55F957F803C0
      1E00F0072D76ED210475BAA3E2F6AF2A2F007803C01E80F407C03EBA3E01F00F
      80790E1BEF787EF4974EC28A6F667B96555B499BDC335CD5F11211A57C251D20
      39274754EC080E0B55FF00F807C03E01CD5DDC551F964F6EC6CCDCDA12AA8FE0
      1F00FC901F4DBF00F807C03E01F00F807C03E01A82EAD8CBB3F8F806572D95C5
      B271B0C71699B84342F4BE921F73CE8DF530A152E800E40BD254FFD6037BB587
      C03E01F00F8072BC3C12A76EC6EF6A2F5AA3F807C0378E03EACBE01F00F807D7
      47E03F01F80FC07C4712D19F499D87CACCC57014232F7A231EB793333CDC16DA
      93AAA3F93868AEC5C554A035F958FE03F01F80ED980D62F6D4E29BD7DAFAEC8D
      46A3C4FD01F80FC07E03F01F80FC060060060060062EBF4B7DF799DFAA80EECD
      A7D9527A6DBAB12EB5264BCBFE90F6FF67CAC55B006A7CEA7565801801801A8C
      01F2F6C6B73DB3B2A4D49DD6A2FA6A1ECA8BDEA9F003003E1406E1B60060077D
      01891F003003003003003D4203BC753D15DADF4441E50AF1454ECCC67E0A73FB
      7842F357B1ECE52C4D4BE71AAD7FF50394B4F01D457F4A1F05D6B1C00C00C00C
      E680D8FA7FD46876F04E6C9F1D91E3EDDF8CFD8A03003D280C5658018018018B
      A30035CE071FC5801F9807846BA90EF5F302FF2506DF59E1B9217880FF088C33
      E2F36495677E50873F82FEDFD2CADB0FAC75D8EB2DE95C95E00F217F7AAC3003
      003003003F2BBB5656FDCBBACCA90D93FBC4DD6FE9A3F2656D95F89E62624AC0
      0C00C00EE003C94B801801801801801801E92032F5DBA0957E696AF9545E5217
      C80F9CD7F351FD6CAFC3D5DD3706FF93CB7C6B333BFEF841814742C06C948E8A
      D4FABF003003003003EAECD4D51FE8D2B0F999668E677DBBEB64FE9527D6CBDD
      A97003203203203E400768768068068068078780D5EFA0195705E38E25DD81DE
      D867AF627D784E123DFC483F9227ED49F8B3A4B82E4DD28FFD3D6C335338955F
      F7EB29AEDA9735C480F0050ABB403403403403D35DBD2F7773371AC70D9527EE
      C9ECF6FC5F6D4BFBCA7FCC0FA01A01E100FD726806806806806806807D701F6F
      FAAD6D00D00CEC01DD49F5F5AF2AC1F9CF5EF2F84BF0623CC7B2E6C6F7177C97
      E61CD7E80DAAF05E59F94A53D7DFBD8686960E9673E3747D8EFA67F6A3DA6C4E
      BEF01DD956E806806806807F0BB3A0005F31F3C8BDE717A7DB3939E6735D93B4
      A936952E806806DF01FAE2D00D00F4C064836806806E901BF92F403403403403
      C38077BE3EED356F401DDF1F6525BB61C6E7B093D95260F5AE71B3CCCA76E73E
      CEB52663F9C6D6A3D90B62B2C0789AB7403403403403A975FDD9272EDA93E2F7
      B021BADB3E99237398D2D0D6A5EC6CBD00D00F74031FC9E806807BC03B175680
      6807D0036F9AF7100D00CEC06B0CE807F9630EC0BF3DCE3DF2967D6B8C60F149
      78FDFF1D49E2B9F1CB6D6E84AF2D5DAD47BE96D707003D9D57A01A01A01A018F
      BABFF5E6CBAD4980DB5A696A73B1EDF6FE9B2B1D4BBA5898F5A9B403403EA80C
      764680680670064BA74034034034037200DA3CB403403B400FD18C7CD8D335BB
      9FC01B9C799F02754C58AB364FC5D23366BDCF6D839B2C5C2CB2D870262706DC
      7E60A997A46D52B5A1E6780609D53A01A01A01A01BAB9F7F75F91F117F2EA4ED
      5FF50F0D95E9B854B9B65E6A9B4034031901B1AFC2100D00DC8066BAB4034032
      B019BF63403403A6034B3D00CD58FDE64F7D99B6A46EF66FDD8066407D93903C
      E07664ED9993C61FA34DF193EEE3FD9B54B9167BF6FA932D36033DDA1549ECF4
      8BCC0600FA954680680680680607747E7BB33F733A6D5275CFB7CD1CFEDFFBB5
      4BF00F6266BFA4A5DF666CA9F403403403403AB80F6CC79E4034033280E183B4
      03403C8038DE3DA4806807CC01E4B05DFBD7B81FB2733101F9240B824E181386
      5DF500E19831FB30900E20BC603904E22FEAED8FEEBA0A7C99E923E255C7A773
      A93CFDF00EBE5700F0E3527547AC77A9F9D478716FAA01A01A01A01F3D01CEE8
      EC9FB3CF7104FA03A9EF8592A97C17BB2FEC997BC629DFC7FA07403403BB01CE
      CAD80D80F580EB75EC06C06C06C06C06C07E580658C9EBDFBA80EB967D60BD20
      3AE5EF565FCBA702D498AFD61BF241DF80FC86FF670EBB2B33DEFADF6A5FD3B4
      2D2F3CB06F6A4FDA9791ED1A9B3DF8075DA1FC03B890C06C06C06C07C8064FC4
      3AEC6EDF75CB82F3B115276D95DAA5CC3E92559A4B025A725F82C05E01180D80
      E5803B6F0D80D80F600DA06C06D49B01F4C076C61B01C019C80EFDEBB80FF654
      EE07072BEF8CADE8003C55B86071324E288FDF47CFF501990D96479E00F04FF5
      07FFEF7B0BFA4EE706CBFECC3E4A782A460F72F153F8D9D800675FB080F4B057
      A603603603803A801F068DE0203A4A5CD0EC3EA456EE04065549F501D071342F
      0070079380CF049BC01C01D880EC5F85100700767019306F007007007007AD80
      F2DE02FD45F3F805FCEF3F55DDF04F925C7F709997C11F833EFFCAD0468E22E2
      01CA01E01E584F00BC671E928B4C401D6FE9B2AC1A1F44D99D62DDE44D9DD67B
      DEBC801E3697007007007007E00614D0F303018E1801C7B4F861E6EC6C7EA7E5
      01C01F2913D70501E00E00F280728FE00E00E00E00E00E00CE199D4579203C52
      D9C07D74064A7BD3430FF9737874756D5F410068F7BB3120350D440EB80B8038
      038038030200F1517EC038D99E0ADDA1387B4092406AC5EF007281E00E00FD8B
      DDE2FC01C01FE01848A5A600E00E00E00F4201E1A89A600FCB7D569FA042E820
      0F6174603E319828D3EAE90555D01DBEA680380380380371407FAA0ED45550D0
      B7764DD7D1C4398F3D0200E00E00E00E00E00E00E00E00E00E00F5200D7A86C8
      AD80F2EC01B06FA01F4047C857BC39B044A5A8C5F0070070070074C01E6DD924
      D1DFF091EAE031D9AD3E6D70FF3401C01C01C01C01C01C018D80D7600E00D440
      3881C01AC80FB149D5FF2E55FCABF803803803803FA82E0C37D836F6EB67A01D
      BA5CF87165D36AEE6D4B8C401C01C01C01E3F0188401C01C01C0181407167C01
      8A00D5DB1FD55DCABF957F0070070074079786DE22D871F6CFBAE638EEE83A8F
      D7B6EC80EBE7A03A03A03A03A03A03A03A03A03A03A039D84F8ADEFDABFA03A0
      3A03A035C80C037000F5E163A7223F1C8888AC97D83014074074074074074074
      07407407407407407B806E080C94E6D8F2CFAF00DB01B5F7BEB8FD3EB1D5FD01
      D01D01D01BD2EFC53B32879719061EC9F8BF9653407407407407407407407407
      4074074073780FDB890A9F6AF6C9C73CEC5F380FFD70FAE0146F048D4BBABB28
      9B66ABB7DFD22EA4C2BE980CE3FA7E4F587407407407407BAEECA4482916EB77
      8EA5DCDE082C31EA6C3365F6301846F46D01D01E1C03E0F18DA80E80E80E80C7
      803AB3E80EB1BA0325A93EA11E403E66950FC7FA377FF396FEF7E4A653F03FE9
      8179AA321C6E09F0BC828919DF1AD99E03DD01EFAAFFF563DF95E80E80E80F5A
      0B0768FF3549EE77BA10ABD76ABBDD4FB13EB50BD424F536E8E8E2A0E4A80E80
      C90060BD51EA03A03D4018D7A63A80E80E80E80DCA0332495CD407407AB80C43
      AFFA501B5657B0D48880E37E6056E621E85A9F0289C9081639E22D4BBC96A5F7
      53527A0E1EB500F522A00F01BFC17F1E021D55DDAC7A03A03A03A03E176E301B
      D5AAA3FE5351FA1BB02CB51FF98291240768FE80E80E80ED8BD01DB1BA03A03A
      03A03E905F58BACFC00FE54BC400F1E01F4BA4C357BAF9D97ECF176AF65EF690
      8E74419CECF76D080790FB00639D5BA6563C379F36B0E80E80E80E80F4ECA8FC
      FB5F9A8B56A3FE549D9B7A03B40F406E4A03FD5495D01D01D01D01CD40768FE8
      0D2C062CD35EA4D701FCAA7FE03CD4B8BB74399EE5863966A661F678FE5DD20F
      773FB9179A2807E932B6036ECD4B7AAFB57F407407407407FEEDF1EDCF797547
      E7DABB517407407407407407407AE8F00F00F00FCBC3700E1F87FAD09709676A
      CF72D3ACF46F7B86B74F3E3E9EC94FE0EE9C036896D54E8603016AFF00F00F00
      F00D7BBBAD47D84E5ED8DA1C5B957551F8078069A03D36F80780780780780780
      7807806E5757397674BE01852B6575D93885944120A953466DF79994B7F13F3E
      544654BB006905C8EA7FC60389D5878078078078069BC3BE403B6C6DBA8B6EA3
      F00F00D3E03D597807807807AE8F00F00F00F00C77C4BFBFD26072D4E364F7BD
      48D8440257CEDCF3CD02EA76A93AD47F8E0260576EA1900C41AC7C03C03C036A
      80682B9F8507F29F6BD3B23BBA8FB6D407807807807807807807807807807807
      AEBE027DF7150942C1DD769F5A93B572A1336AA9377BE3E6CF6FFB81F46E600D
      D3C79CACBC03C03C03848060AE57219ED9D6A4D32ED751622A1EB516D54FE01E
      01CE80D86DE01E01A700F48FE01E01E01E01E01C8E037EEA7BDB6B6A441D615D
      EF8398DE2763878AABCCB8AC9634EB630BE8EF4319F5FC501FFB6E80D42BFAB8
      FAC5F58F80780780709406B0EC5F8A47D5105EC9EBB23AFB76D4FDEA03C03900
      1EACBC03C03C03D7478070900FBF8BC039301FB35C48EF5D045E2141B44F3C2B
      90BA80D1857EE441C288673ABFA7FFF772AF6FE9CC74BFE0C6E16B32338EDF0C
      07517B5958780780780780686EEC55ED6C6708AEA43B27F322B41E87AD23E0AD
      95F9D8C4F495E01E01E018C80EAA5F00F00F00F00F00F00FE801D92EDFEFABF5
      E6AF5A8BAC85D4075CD7DAA3EFB2BE5EC781E5BA8322D3C1C67C244C8E544A2D
      C01ACBA3056EA55FE01E01E01E01DEECD1954B728102E035C789967B777D93B5
      5277D97B2A5F00F00F00F00DD003543BC03C03C03C031D00F5EFE01D9305F41C
      4BF6076EC33B36277F09D2C7B6A41EF2277A93F8C97ED21C2FF52E1EDEC176D5
      C73183514CA6B8C8CA7A34072BEBD5DE01E01E01E01A65DBBEFB0B26BB64D332
      A4EFB2767B7E25C2A5EFCA76303F807807F80777278078078078078078060A03
      F372A9ADE01E01A1406DA4F57AD63983F4BEBDC8F0962788F63D97746EE2ED79
      F9AE225C0616E05F2A45FE6255EFDD767A4842FDBC63156F887D33DEA3E22452
      634031655BE01E01E01E0189DD9B1F9B649707790C6DF6F9C3E7A6EA6FD93BB5
      26ED4BE01E01D100EEE2F00F00FE80EF06F00F00D2406FC97E01E01E01E01F76
      03F6B1F75F56EA01C263ED14B7124EDCF8127CEA4FE5D5CE382F441F9EEC4AEA
      9374FE52BEA3E152D84F0063D56F80780780780705D7FCF0E37CEA4FD49B3FC6
      6A31018DD79EFC85D9F52F06CBF00F00ED4077F93F00F00FE00E0BABC03C03F8
      03620AD4F00F00EC007B0CF806C5C61C6EFCE1B1EF01660AE3B83C24BEFEFFDE
      A4EADF2C0CF65290CD29987547D7BCCCAF00340AAFC03C03C03C030AEAC668B0
      87549F8693E31873787FAFB7E9D95DEA5D128944EA9BC03C03BC01F2C8F00F00
      C201F2E9F00F00F00F00EE2038674B00F00D6407238C78B1A7CB771B006363CD
      9E0343B1C55F327F2C91F335EF6A229CF4E821C681193E369E9E2649F244C707
      2D9AF8432DC03676A9F00F00F00F00C7B9F1EEBE94002072A4E0A2C7DEBB2BAD
      F9A97E6CBF9537807807D003645A6F300F00EE807CBABC03C03F101F3F63C03C
      034007967E01D331F4193A4CCFD5234B9CD8B8063E0F9BF907F00FCC9DF33269
      87C0A6F093F19E1F0754B8AF33E68A935279837FC6AA93F452B4371D01C7D51E
      01E01E01E01C7DD1B65D9B48A071D52732F47145A84BC65D52FE2374D34DBFE0
      DDB33ED4FE01E01E01E01DF807CEB757E01E01D6C07CC1DE01E01B20381BECDD
      807806E4039F5C8FEF5BC0FF480AC00FE6805F1276F09F72EEC80FB9830D4F95
      80FA0B8406809F45FE1A0C64CB7F8C499C247F4ABEF4EFB5274D1DF0616127E6
      ED1527270221F53FDA8F0048F5C03C03C03C03E0003EDD1A97ECF81E622F01C9
      7ABFEAEA5EFCFD97FAD3BC490DFE4E1E07C03C030403ED95E01E01B003EDD7E0
      1E01E01E01E01E01A5A03A164FDEFDF407ECB3B00B8407ECBDC397F197383DE6
      2BE10DC4418601CC1B9B387ED958C5DAEABEA5FE0148A3F15414FA937D4BC1F9
      554DC13C07EDA1BC07230B00F00F00F00F980DDF887ED8DFBEEB6E4CEE38549F
      B657EA9747F9B6058936E22DE2BD015795FEF00F00EE603F6F0F00F00D901E40
      F00F293C034101FB1878078074603F77AFC039B2A7E03CD95F8555AA0F01FAAD
      E703CF927A047F56AB7B980D786E823E8407E27D907FA6F9240B93E44FD97F45
      02CC448ECCDFEE5FEA7FFB3B780D83D4F01DB19A178078078078077C03E9E6BC
      440738FB451D87CCA0134880EAA93680721E0A0BE01E01B6406C8C4E700F00F8
      40766E0D8C03C03E400E3837C03C03C03C033B00EC1F63574BE76816E579FCA7
      6F04E17B8FEE4732FF23FE67D9FB1C216DF4AD103D403F83EA09FC17FCE3BFC9
      DE22039A736B84E42BB63B3397BA4063B3B997B3CDD603FED2F00F00F00F00DB
      01AE342D2180772A403FF69FEC3EAD8DC754FEA03C033B44F368C7FC03C03EC0
      1EA3FC03C03C03C03C03C03A8675D159601FE96EB03C08071A7BB4330E54E6FE
      E8E119BC7880D729AD04406D5721F69017807807807806D101FEA2DB00FF667C
      7A2F145D2664F201D517BE01EA07C03C036C5F5F17F00F00DB0193163C300F00
      F00F00D4003AF15D1E03E3D397C39A6CC2EA03F85D680FD466EE34FD35B47B10
      0762AEF00F00F00F00EC501B7506507F1621C1200752B1700ECA40E765CF010F
      481E98082C82EB34E720FC84890DDA0AEB0B8AE0E5A5ED11291798EE504682FC
      AE482AF017620D9C80425EF31055A57156B8E21A9BD441640CBC241FCD2C595B
      8DD714D4C343A436907AF4EF867B56FDA2ACF6A261E82E0B121240EFB7E792E2
      24F25A60060060060060060060060060060060060060060060039401F9D3C03C
      03C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03A001FF48F80
      78078078078078078078078078078078078078078078078078078074003FFA07
      8078078078078078078078078078078078078078078078078078078075E0384D
      BE9807807807807807807807C07C07C07C07C07C07C07C07C07C07C07C06EE00
      F4B3D7659F0B4DC07C07574077BF01B82039CE03E03D16D67801CDF01F01F01F
      01F01B8603EBFFB77A7003F19F80FCEBDE806F48A166D3193FC07C07C07C07C0
      7C07C07C07C07C07C07C07C07C07C07C07C07C07C073C80D8EE483C07C07C07C
      07C07C07C07C07C07C07C07C07C07C07C07C07C07C07C078100E71F95380F80F
      80F80F80F80F80F80F80F80F80F80F80F80F80F80F80F80F80F4101C28109C07
      C07C07C07C07C07C07C07C07C0600C0180300600C0180300600D480353B1BCF7
      9C2E5003007C5406A50078180E74018035EDB71803B000600C01803006FD80C5
      FE377E3803233803275F04038F615FFE8229CBE5AF90039D534FFA66903F2AA4
      16F6903E3BED2C82FD63A0AF07E424E1AF0AF27535AFC074E05EC5EDF4FDAF43
      8EFB753C287D090B8ADEDF75C75DC58000000000000000000000000000000000
      00000000000D20034F4745FB5E25A5180E3C1FB01B56719E403B707EC47BF776
      601EDEE6D12AC076E0FDED37603E007EEA2F7536E7638957D4DEEABDE79CD53B
      50781F03E07E441F7B23C0F81F03F9F0F80F01E03C0780F01E03C0780F01E03C
      0780F01E03C0780F01E03C0780F01E03C0780F01E03C0780F01E03C0780F01E0
      3C0780F01E03C0780F01E03C0780F01E03C0780F01E03C0780F01E03C0780F01
      E03C0780F01E03C0780F01E03C0780F01E03C0780F01E03C0780F01E03C0780F
      01E03C0780F01E03C0780F01E03C0780F01E03C0780F01E03C0780F01E03C078
      0F01E03EEC1FC24699FBB7672BFCEDD573D7FB920C1C1FB01EC1F47E3CA2B8C0
      7EC8BA43EA79D9E67803EEF0CE79807EA96BEEFEA99E93FDD97CEFA6D069CB60
      FBC3A7D396345AB07CEF02A7FA4D75CA24FBB7BD60EA03F63BCDBE60620F6EF7
      E5D7141EB4EC5D35A07ECCBB183E7568D58803D08FEBC1F3B4C55F4FE3296247
      A67B954EF03ED156F2B943E901F86C0EDBFAEA95C8A47B24F601FB982ACFD6B4
      6BB4553ED23E707F12AAE9FB6BA6EFFEDDD5822A1FA01EE33D53888F83ADB908
      B4BA707E32ACBFDB19DC0224BF603F617985567FE16BE8F22ED003D2177B19E6
      62F201ECA74A0FB0C0E1C87E95F1C97E2F0FA8A20F7EAC3648CBD83F6234C0FA
      AF6018D2F5BFF25645CF8573C3ED41F4BCA4664BDDB93722AE07EC26B6D69D22
      5F081F211A7C5AAF83D8700B2A32C5197E60062F8BFFF16401E322016FA803F7
      1F24CC2EC5475BC1F989151DB05586EF99FB167C4F849BC0F88915CFC43E2FBE
      1A00E95C1FBD75FDE3DCDF585311800000000000000000000000000000000000
      000000005680373EFD201BE243E4C072AB704DD07D63F30CC17840F90375E83F
      74F7B01E461394B701F537201F03E07E3587C0780F01F1983F9F1CD807C0F81F
      03E07C0F81F03E07C0F81F03E07C0F81F03E07C0FF701FC9C7D300F81F03EB80
      7FA7C260FC0F81F03F8A1FA0A601F03E07DA50F9A1F03E07CD5F80F01E03CA7F
      03E07C0F81F03E07C0F81F03E07C0F81F03E07C0F81F03E6A7C0F81F03E67FC0
      F81F03E67FC0F81F353E07C0F81F353E03C0780F01E03C0780F01E03C0780F01
      E03C0780F01E03C0780F01E03C0780F01E03C0780F01E03C0780F01E03C0780F
      01FD007A58580FBCD100F403D291F307C52603CA07E350C02980EBF07F19301E
      683DBB51ED980F3007AFE603D707D55A5C2803F107C745CF2803F707B65970F4
      01FB83FDB8157401FE83D4784ADC01DA20F638CD0D007FC0FBE7EDB5401A607C
      36CC45006981F1994A7401C6D3D383DC31E07401AE07C8D57AA8035E0F5F78FD
      5006CC1EF2BB268037DFD23C0F9A193C1F16D16B40FB560DFDCACAAB16007C3F
      6F5AF9DAD40DB9E418CB40F76839BED652341515FA7F00AC07BA2637D47785B0
      67B0207EC3728DF4DAA3BA607A3E36096C0BB207801D97EDB94674D3BD5003EB
      F80D42FC0EFA2007A0A402DAFD9DAB618402F4AF32B6C07103D6FD080FF4059B
      7F0142A005EAA2809C1E299660290E2009C36028E534029DD02020FF0FF0FF0F
      F0FF0FF6E7FB4E33190040080100200400801002004008010020040080100200
      4008010020040080100200400801002004008010020040080100200400801002
      0040080100200400801002004008010020040080100200400801002004008010
      02004008010020040080100200400801202404809012024048090120240480A3
      C022108CE82405E77F01468005F7B4808DA31992E029F62808D76029DE20029E
      080049FE9FE9FE9FE9FE9FE64FF5FDA024048090120240480901202404809012
      0240480901202404809012024048090120240480901202404809012024048090
      1202404809012024048090120240480901202404809012024048090120240480
      9012024048090120240480901202404809012024048090120240480901202404
      809012023402EE38504AB163AA00C4180066E800F9AF7019BF843B618082CC8E
      225F019F2082F603BBC8CC5C86F4080340BE8AD4008010020040080100200400
      8010020040080100200400801002004008010020040080100200400801002004
      0080100200400801002004008010020040080100200400801002004008010020
      0400801002004008010020040080100200400801002004008010020040080100
      20040080100200400040021F64B93E7801340040733042268103E2284070EA81
      7BB1410FE19319800710D6DA5329F985A40C47F5CE8E34746AD964C5FDFE3240
      198647202998058B0469820AFF004D9C83F695A6C414329D2CB4126932780E97
      FA9516AC36B7C7AD03C7F082880A80B22C02115C20116BBDE8DBCD8CAEC3F106
      86F7D9D93FCCFEE4AAF0ACFB063DD6F462A58BFA17E0AFC2AAD01605575399BD
      C384983C6A323B372A5C0C653E297CE7F59F2130D89820BD2A8940AADF2BD202
      00005F95685377C7A3E5D593488BC1E9F4FC48371BC0372B62DCB48AE9EB8B4D
      5404066AE7E7BCCBF67BFE56C46B16EF566C5AE7BF4272AC5583AF8CF038F336
      20163FFA4E808B580B6C7E213F88DF7C5F5EDF07C6A4C3BDE43BDD96DDE6FD92
      AC6C54D7E6DB6EBD410102E80540456A022278C97D8D005EAB5E87C82DBB6A3D
      80ED634C7A37AA63EC885FEC397AE6AAB405F555D6EC10C06E71FDDE15EB0713
      21DF881EF6668D89C2C6C651E5C4CC202D7987B28088B0162B4418A0E2751014
      1DA0AF1D8F936BC2BE0D7FD003572024D540471ABE20B63F02E07467CBFBDC6F
      6F15DDA4093187E30A09289BD547A37BE5FAF88005034CFA20226405AC3805A0
      8B2BDE5AB291E68F4CBAAF4795C02B35B0E852265FE27AAC070E2B1FD1C90500
      0AC90144402ECF27C3B5D97D86D177D2F57E03A7D7FB46C5DA96FF29110DD717
      71A5E91A9CD0CE0AAB405E955C353569D7C634F112E4DE7FD118A8DDB6283BF3
      B9452AB98700AEF31882020C00B1600111556E25BA29E3F56F468232AA4F90DE
      081F19D64F0DF109AA80BD1AB08839BE30D34220EADC7B7F07F0A8D98AB8B1EF
      261C03F3800E0D9A4058100BA470080F122570FD702A977675CE65947FC29FD7
      4F7F7BC6604FCF0B7BFE05F74210155D805CC171AE77FA4D5DE2D31A0D91B691
      2200AC843735474014C9558004A64AABB8AAC12DBC98E825AF9DF1E77704BB19
      5A40CF0E005D2F2D3DF501202FA5A7F022D4DE65DFB97B397D999FD0BFB1EC3C
      CAB34B84B93A9355017935799C385CACEBBC06092D43C43FB3B8356738366045
      2832199D1C8FF3231D5015640203C1C3463AEAE0CDC5B522E9FEC929F28A7EA8
      89FEB7BF69285A5D9C37CEE80097002FCB1F926417CA42FB6E90A9DBE55E8EEB
      5E56549ED7F281F4724850E4E5EE2AB12C005F4557C777E3AFFB339C66C70FD6
      E7B5A6F461E77BBAEDBDFC66DF69E0059F00BF2DD1F85ECE9E3FA6AC7FB77E83
      E09BE27E4358525477523C7315B106A802C26AC2AE768661C7903FD15FEEF58D
      898BDEA29DCC771EF16827211CBF8DFFC750049E80482D3080911CA5D8AA3A72
      73DDBCAE3BFFFCA4DA2934690EE4CEDA93AACFB76FBABCE0099C02FF6FD99002
      DDAAD0F61236567ABF3D7E2FD8A147ADBAE356564AEBB8D29C2667EB85562ABF
      D2AB9FB9A4D21924FDCF859636AECF63A6CE47D6E3AA65CD4CCDA3A802D8005E
      FB3DC746C7DDA90AB32CD25B9D3BEBE92897F4C1AA00910D578A2F1A2E224A63
      3CA633B74E28D59ADEBE74EB1FF1C9F96C6725AFA00100290AD4DCAC5B62A5EC
      B5C7549521D02074F1378D890CE9734061BAE9BE5B9E02B510029E00252FC7DC
      1A7DC798B5C35E43D7A57EF27BE956B93D2BD51F88C2CAAED1C84A401482ABEC
      685F6E150FCEEFB4ED4D25E3C7DF6432AB91AFFDA7A1CD58005D0805D69D3BD1
      E9A540EF4AAA60B4BFF55648DA57A1EE86A80251357D76C304EFF7BF4FAA7DA0
      60D33DB796E85A4E8D5CEF75928EAD3A1F954B8E0080173ADDEB9E1B96EF17D2
      EEFE37D1BA5BF765F3FC80DC7F4B1D2E3EAF8EE3E5AA00A4300A11002F1AEB59
      C1DBF14893BCDE6254888DFD77BE9AD3B6A2155EA752CBB6344A401492ABDEEF
      7679A88DFB0F0F37C58A6B3F1EDC4E8D3EFE8DA45F5802ECE028EF357B5B3FB5
      A157BF616B75E1EFFCCD3E5A5D9B9E70D5005E73572F35B2674D08E1D28DF3F7
      697EA49EA6A4FF3B4D5A353D0002FEE028AF2C3EEF478EA6A76FAF027EFEA272
      7D97DDDD60F2414FB5D0D14D32CD3BC2EFC802C5C05A757FDF0F8F57C4A1523F
      A761E8D67BDD637B161AE439F04B00173557D0A145A237E27C8FE8CD6C7CFAD9
      7137CF2347E00A2A01515C026096E1D847D5E88DF3DE27BFC5C38FBF83786A80
      258D5DF41AB4FC6076F08DFD75637F03F3CF42EFB0366EF4BAF8A8D31C80149E
      02A5E6F07F3D73737639795959B7F95741E86C1139CFF83A26688846ADB2B137
      CA00B51016E5E9FFDCE65F3C2926FCAC1DECF10931BFFBC64A46AAAFF39C8E00
      897802EC6CB4AAF67EFB3B4A37F938FD607295468F12F7253A47802D4005A1CF
      BBDC40918DF9BD5FA67AB7F9F5B60D5004E1C69D26AE177F1C9C6F42D4CB68F1
      4F7174B793524D5CDE3A802D64050D02BC96939AE46CE34C70DE298DFCA547E9
      9DDA06C4DEA531CACF656200B8E0135BEC4D88D96CB98B15C88EF14FD4585FF0
      483DFC11B7556F2DB25882AB0056955E276BCE7A9D1F8DEA37E8B67E5E46E3BC
      BE1F790CEFBED4C78E57B6600B850162733DED4A7CC9F37EF1E08DFD7FB131FF
      D806A802DC6AC9FF7EE8603EBA51BE492D1FBD87B69AC4E5CD4DF2BDE98C005C
      980BE8F73F1A79A975EC92C9C11BFB131EDD4F47A4BF9C4945CB8AFF99BA9005
      E10091B2B6F04BFE45CBE6F6E923FCB637BC2106E9A7BC78579E2FBA855600A5
      955F4A1279CFEF5AF98FBA13D28438E462B21386AD1FB2A7DA00BEF013351A1B
      D4DE5396C676AB6F1FEF7FAF218BEFCB0EC1AA00B71ABFB6355DBF91BE9B7F5B
      8AAF338F0B114D1951C801708025346555B54D3C6F76BA74EFBAFC6AF6024467
      693F77B9C3005F700A37A3CDADE17869BE7E399A180B46FABE198DCBBD955DAF
      884B0015CAAB7162E292EC6F746753559DFAAD32FEDA4200400BE5D5CC2D7397
      28C6FBF64398CCEE4586A802DA6AF39CEA84E08D5D8DF4576EE61D563D6742DA
      D5B5F2C50024B8095B50FF44EFCAA871B0EEA1BAB546FD5CF6650E29A596F2C7
      EBB428A90D0AC004960146F82DB45153303F1735B13E5E5BF1BFD1CCAAAB1540
      02CD1AEB45B9CC8FE380E35CE84B15637E139CB7AE68FA4E7BD4F88010026457
      FDF07F81D2FB4DF8DF9AFFDE31FF686A802446BA4A3571E08EFADDC6FE1A1F73
      5D44975CB9971AB99C5BD76008017937B8BD68715941A294A8DEF18CF6DF3A57
      1FEC1AD8F5BCAFCB479AC06BE58026A0091753B8DDC31FE8EFE92DEEC6AB1BE9
      16A861858AAC4A401307174AA995472CD85AC340DC750A8DEB7B3C9145EB1776
      3001002A80A1AD2AF72FB79172B7EA6A8B8DEB08FB22A17878096004B1C2F6A0
      B355730BF5ECCCAF46FFCC0A1F8AEDFBCD2563AF9E9EE27004D30135C0DCC6E3
      ECEAA6A2EFE2E51211572FF4C634AFDC4A3E3E701A160DFB3DB7AC6004DB0158
      EFBEA174EA71FDFD30EDDB02E37FAF95FB646F5AAB12C0057955E37064E0F8F5
      A93155C3646EEC8DF6543FB5A6963DCC300400AB2DBF6D2A645955F72A6F77EA
      CF7CAA75E192FFB3659D012C00B926AB2F971B23E3A84A5637F89FF7343EBD52
      5DF3E527E1CAD0F79BC35000B8A01530C7DFCD9BB167862DD65FD8D2DF2BAEED
      F1F62DEFC2A9DCCD9CB1EB9F547D005DD00B7373D7415F2B36F5A6B3294492FF
      F87BBA46FAB995C2A80058AAAD91D7E5CD9CAEC5573A83C3E52E37EA59669DB6
      B1EF8020059547BF5FBB736705237B57A54F0335777FE4354017166AEA5FAA88
      FCD7AEF7FF920AFE6A8ECF8D5FE58A2600BC6EBC801002AD65ACEBB5D64DE8BF
      B83AEF39DB3F3DE5EA4DE5DF5B791BC6D137A1D2802C9004FC01345870E87BF1
      E2B71AF4F396C8DE8EC8DF90AAC4B0015071B348E5ADEF7F821CEA4C8E668951
      F7BED702FC2E401002580BC30C2BE38F8E3F12FC6E2B4AEF99456D227E2BA1AA
      00B2E3574F3AEE8CBDF58646F9D7DB29D9F0B0F89CAB7F95BE0D00585F000801
      2EA5B380933B2C1D76DEFC9F82C2E2F1B37D6599A08493D51DE3404B8B5AC00A
      4402F7D1FDD84BC9B7BBDFDB64C97AD58364D8DF88AAF3455600BFD6FFD138B7
      904B171AA7BBA4AEE6EBECBA6A51C9E5EC802005E804CEA0163197DFB8E5E5AC
      246B15993B06A802D2CD5CBCD10969AD0FBE90F8B2B2F8B85FD5A391D5F2F6FB
      2E90D39FBFB405A2008015A2532A5BF16672BB57AEF6CFFE5F0294ABEE34E9FC
      BDA3B38D51BA0080166D1F2041A9AFFF9C9B9AF5D0AE95AB551A00AAC016217E
      AA89EB2F33CBF07B0A5BFBA2077EF1AD4400000EF801B055BE6FFFF37DD5E410
      1E65AF0ACA637F007007506ACD1475B7F2530E6EC2F2CA724DAD133DF02A404D
      047017014ACCED36A9C5C1551C39A47C19ECA0B749780306291CDBD5880DB20A
      0F950EAAF80B24955ECA96B464035C1A0E40C4277B3C007E2700CDD1AAAEC7AA
      1429C4900E00E00678E7A76F6E1834FABCFB0DE998C14EF22F800C226C196599
      C95763F30112FD34DF8B6E075572EF017013405A08182C11BBB4B2962E85F03D
      2480EF45F800EE26A7ADA9AA15D3C7A43714F3F8578038019C5DB79D858805BD
      6E5261E06B792359B7F44E00E00741724CC13A64D4EDF9F27DF6415CA21BC5E2
      D4048DE6D7000895687DD1274DAD90EE28005863495BDE2E61AE99BBC9E18725
      6E3CEC380B80B09936261ADD85BD57998A33C6C5CA49C189038EE6F333E7D2CE
      00E0099193060998B90CB8DA739286C8ABFAA94638EC0E168446050058EE02E0
      26808985605714826C615491EE3A48AB8DE0A5AA7AEB405DD6853927C05466AB
      1AE8A6F07E47A3879392926E654E9A8DE9B92E152B538F06869608C0A5C55088
      C353DE022680800E3700A7E82151B69E8ADA9CB66A88D89BB8D2C6DF2EF23D8A
      614C9563DB9C9870E2FCB0D378C47010E005B83729021D2F276466D74460978C
      CA45B6A28F7945F6562C606349AD48E34580D111A908FB54D5475577CB557BCA
      C478AA325B4CCA14472652D42851ACB35F40D243638B57AD11D9A737D3302CEC
      D4EB80A6A00AC14A655E9B9A6CA6A67578E4501C4AFD3EC360B01B499E5D4891
      0AF57E54A72C59A81053E6AF354E6A95A740CBB0EBAB3DA42F5363494E274456
      2297380120600D23601D09ED03C824217DAE70BA08EFC3F70010D558465C07C0
      7C07C068003C47741C67C66140CF0B148F96C076F39402EEFB9675466395E52C
      CB28100B37941E36439A0BB0D6E98A303B269849B680E8A753E7394FC67C6733
      0CECEC479A2E2539A24D758D2DD0B46834BE86A316E9F518D915AACA4CF6CD6A
      80F66B8C56750FCA7A93CED1AD5EF43C3919C208709418A8ED3F6CE23AE734F2
      0064A1700EE3C229D9FBF33D28813DB24019DC26FA83418CD1F37E22839DDB9F
      ED5AA176235076F2EB0F22E29B75B9CA36B5079DB5AD98AC4947D48CD289FEA0
      F2066FFAC71941E955D6F72072170E506B76158F43E31B60C52BDC5A415B8F5F
      7326A3C8883B6B2106A8FAB198C49D8A8FEA0E3CA0E7EE3C650793D48DADC1FB
      550723EE564E39B6BF172CEA5F595E46E25D28E52E94A0A3DCF3E8947DFBA3B9
      19C88FB233B253DF0CFA838548B9053EC458AE36A0E710E782F206775D66F391
      F48B19D9B51F071514AE3A68DB9D1CD47CE3FC5C66494F8833EA0CD67572A2B9
      ED5D41CEB2E793724673C17CEC9E783F9E11E8D1B7E17DDE56A6A947BE3FCB03
      39B9FC619F5075113E4A83B3D952C2CA0DF8639E8FC919818C94464BE7937968
      DC66DD1B7E57E79672D1BE33EA0D3919925C13AF0A3A6D1CC19E3CDEB77CBD1D
      5EFA1A034851E0700525E91EE67775B96F51F598BB8AD0B9469BF8CFA8376C67
      8570290A1E8E3E922F4A1FA593D305F4C9BD347F4DA3D389FFABFC07C07C0792
      031A274F8A3364FAD37648F5545C5459A4E1CC980333D69030380B80B80AD808
      EED8322293B4C3D7EF0B8E838849268B1F42F3866F0D62D9478BDC5D3DBD04C9
      5F68F3787C46534059762681A6B68E4ED32DEF059C0B19AD95DB895755585237
      0AAFCD56849B136AA11E11CA40F5C7655552FC4BF332FCCC3F16EFCE34091A88
      ED527710216C8BC0A41BEDF7B5E5F88C76BC381A92130484E4A7CCD502FB7131
      085782889A817DFAE652DF52138797D65947760029476C691F13C96A8FE4A52F
      F946AB4F8E7C5B40C62A0170CA943BBB6241AD9B9BC182A40470CD459D067271
      917A984D3134180A74C13222AC9526B87B4F7033DA9A38CF84AAEBA571F35069
      AC368C1F3CE78A4321F0C17339069A7A9A90ED52708EE39D762C4E7B1B7DE534
      9BCA6C5C0AA8C36684D4784D49B93CF528ED8E11834FCC44CAB5A1CCC00B4EF5
      1C2958F52B4829DB4C99CDA74AAD95D4977C01739BCBDB9A623F579D0B10A883
      41EA60206892C67C35D7A0834246EA32B8E67E56A08EC25D9B016FA5A366ED0C
      9B7056A47C0DACF52123780B74ADFAED0D4A1D03AF20707C15D00B002820D862
      58F22CE81BCDE88E84329114C40F1249B2A9DBB0A09043AC3B2735812FADC586
      C5EA4646F1503082D511C5BE8A0D19E2B90F9C502147D268A0DAB65C76D32C05
      23B28D87855AE14281DBD23719E2819E45D79C5E2FAB900BC83075E76E9E2910
      C2214C4D51DC82D05B6289C320ED04D29BC85A0C9317A9191B8CF16430790A43
      18EA4F45068D54DF60F165D1CB54506115074D2EED2628354BDD2C5EA5F4AA35
      53D83EE307800F46B3B9A8B5E88B8B846AB646048A0D8648C2B369F4D291B94F
      14AAA72A43E54F92D95EEA0D2B70C121E9ECE12C8412419D345FC906930D93D4
      948DE09E2F21F1F643E1F670EA2B3B147AE6E22DDD0C9069B0D9BD49C8DCE78A
      75533B70DC54B993442BA9069DB85BA97DE98EAB020D89EA572553B70F52E82E
      A5D031945C6834D86CDEA4E46E73C53AA99DB80154BD0546FA113BE20D4B70A6
      0DEDE07D0CC77B7E41A96E14C1BFC0FC7EC0FA2662FF8D06A3D4A48DD278A955
      34B70A60DC94E69EAF52B23759E2AC7AADB86FE69EAF52A20ACF156AA6D6E1F3
      4E4E734F67A9691BB4F15AAA6D6E0D8B34FC05C05C041BA6B26A4EEAE97362E1
      493ED80E4FB0BE14C836E90CD01940FB718F80B3300BA1E130D18FBB52DF016F
      80B3CFBE42667100F9F7A20E87C0591015C3EC4604409C248697E42D7063B808
      A011F5804DC064251F781918FE02CE00AF1F6E6FF32DF768EC585259541C0450
      0AA76405D5F1B62CFB76A1ADF0168C0578FB138B8AEE827C9E60515D047E0230
      0ACA0EAACD0AFE391E2CFBC2AF01F80B7602BC7DD688A7135386E66734EE347F
      01180422831829D6580E71AB78B3EC5214EF0103809B8FBC2C9195740EF1F62C
      7F701180458EA6A29F6BDDCB1D618E3EDC42639C042602BC7D97C715676C471F
      72CEE7808C02CF7D938D7F3BC247F7F64E3EF9E11F9C022101381F6F112347DD
      1179A77DD7F780460159403141ED6316A3164E3EFCDDAF78045202BC7DB30FCB
      5B95C873BD45A3AE01180563F891FD4FBFE7F37C38FBFD3FBA6011E009C4FBE0
      7A93B620F3677380460158FE997FE9584558E91C7DFB7F3CF8048802BE7DB238
      238AFAB53860148044BE07407B937B7BA57D7FB40FB2AE704300984057CFBEB7
      FD6CBC1F2D1D6CBF700A402B28029FE4EEE2C49B377A07D83B5A8B804C602763
      ECA40647F819F274597EE014804FE56BC55510BC8D5E93F0543EC7A20E7804DC
      02BE7D86246FC601FB1FCDBD46E014805C9D063BEE49C307501FCE143EEE3CAF
      B804DE02BE7D90315E47F1E83640700A40285CED46C1D734D3387F3BAC7DF316
      1C3804EA02E27DD0B66A41AD70D7098BBA7B00A40234B5B7BDA7CE6D47F8E858
      FB233F61C027701713EE945FA537AB6B2038057C0449B9DFC8518739C9996FAC
      AB1F674FDFFC027D01713EC6E8C955991D2407008A02A5FE383D57638F3F87C7
      9E62C7DDC89A9780500027A3EEB6E2402AAABC02E002B1FC4148E730A0272DD9
      C2E7DC29CA9580516027AF7CC6CA300B900ACD0915DB4D77C569DE8B2E7D87CB
      12300A2C05EF7D5F8017000AF70F9C7ED5B69B2932E7DB102C2F00A60058FB12
      83A84A17256EC57C39ADC173EE0B09FDC029A0163EE023FAFABEA3384DB63F6B
      C02F901009F780455898FA22F7C021A40529ABC020D00476000D97A88FC24073
      3492C03406C119DF382FF230D77C31605F10B9C06C03DF038D8C7ABA2F012176
      4A90723DC480B62D2C5D58C0B18D8C9C02A0327D2595BFB02DBF9BCCF5D1D2B7
      AE54E014E30158B4B1756303C87AA3C64E015119356D1F84691BC52DD7CAFD11
      E0FCF5AFB2FC02BE202F9F1D8B4B174AC1D44007CD531938054E64CA11FB293F
      7CA263624C3A02B1996302C63632700AA0C9B4C1D9DCE62F805C5D6D8C0E3415
      8C9C02108C9C4560110718C9C02C64E01632700B1938C9C02C64E01632700B19
      3887C02C64E01632700B1938058058C9C02C64E01632700B1938C9E02E64F017
      32780B993C87E02E64F01732780B993C05C05CC9E02E64F01732780B993CC9E0
      2E64F01732780B993C87E02E64F01732780B993C05C05CC9E02E64F01732780B
      993CC9E02E64F01732780B993C87E02E64F01732780B993C05C05CC9E02E64F0
      1732780B993CC9E02E64F01732780B993C87E02E64F01732780B993C05C05CC9
      E02E64F01732780B993CC9E02E64F01732780B993C87E02E64F01732780B993C
      05C05CC9E02E64F01732780B993CC9E02E64F01732780B993C87E02E64F01732
      780B993C05C05CC9E02E64F01732780B993A064EFEC3915C0572EBA93E02DB52
      72955CEA4DED559ACBDC8AE02B9757302E637527C0554A4EB1A00D9E5CF14AA5
      2ADD6DAEEB7633A1B96972EAE605CC6EA4F80AB549EACCAB824337A806B49612
      456E697015A0015CB4B97573030637527C0562A4F040C9EF64EB26A93A657DA7
      24EA70168A02C16982EB06060C6EA4F809A0A4F80B8081002EA4F80BA93E02EA
      4F80BA93E02E029D2A4FDF3F0104E857909C056229FA15E02B8D0AF21380AEC5
      3F42BC059BA15E45701678A7E85780B4B42BD05E026B89BE85780861A15332EB
      48B01C99000CDE006CD600F2F3007994C02C206A4F206300B866D001AE0FE5FF
      010C6026B6DF675C04B36602D192E77F013AC778385CEFE02940EF84402721B8
      0B4A5BF38139DFC05DC7F21B80BB8FE02E02EE3F80B80BB8FE02E02EE3F80B80
      BB8FE02E02EE3F80B80BB8FE02E02EE3F80B80BB8FE02E02EE3F80B80BB8FE02
      E02EE3F80B9DFDC7F01771FCEFE02EE3F90DC05DC7F01701771FC05C05DC7F01
      701771FC05C05DC7F01701771FC05C05DC7F01701771FC05C05DC7F01701771F
      C05C05DC7F01701771FC05CEFEE3F80BB8FE77F01771FC86E02EE3F80B80BB8F
      E02E02EE3F80B809663BDAFB8FE02A653BF4892E77F0153077C9B5F3BF80B752
      87F74E77F017A1FE437017A1FE02E02DE182FE02E0208C044A1F93B997017014
      A300B0E2AEBFA5A00BB940BD77FEA570044CBC99001056006F1E00E530079719
      018BCE51102F4A08B907AC5C2035C00A70551814BB4902ACA61428A0586AA173
      DC04CD80878802AF56355D32213A53931A942569685F50377A295C1FA5F0A8E0
      21500AFC80201ACCA675192DBD29454D0A24129CB2422ABEAA380AE2ABBB5B6B
      0A5121D319C9EB3B9ABC0401355EEF9AB6B262A27A940AEA936B6DACF5CD5E02
      111AA0583560021E53D227D25243C8F7685B0096C6390CAD1CB80B80B0D1818A
      598E53DCB4C42BABA84E5B02194B4EE569439D6923AF01260040711D50730FB6
      CC528ABB604F9AFB454DF9B5F52CE1543DC05C05168D692AC549CB3520E0F0EB
      977AAC43F357808AC6AB36237C82953F4B4FD15C09D9E6AF01060CB11D626A92
      37C3CA4CC9BA9FEBD2BF42ABC97F9431888C475C1C1C04BA014D930C03E5940D
      C15AA1610DAE5479BE15671B11C91F3DE4FCE6B808740250B59BEEDF4CCC46F8
      E823AF3B2B7AD113F8CFD9F12AE73AABE0220E3153308E56D3B3D4A98A2D7AAA
      A7A2265CD5E02CC355150A34DAD856D9FCF5A46DD9006AF9ABC042B356C5988C
      8EBB2F88DFAA6A85D6A2B75F1714981EB49DB1983778090C026989649D12D051
      2D2376237E5DC11F0FAE8345F3BA5E623965B4CEE024980AA751A2968D62A8DA
      BDE237B4259F3AB14CAA2C241A37557C0594AAD7E25C4F5D6FEADA8528E0FB94
      9F3578096F355C8580035F996B5ED2A52AAE0B32A057357808C38A1BA6AC6BD1
      AC50DE237A05EAB02B7657120B64F5E6540F8E02638098C02D59151452409FA5
      0537107F9D685C6A18B971FAB441D6F8EF69A7BDB3F809B20204FBBABA0DE359
      5B9EC237CEB87744D60F665F2657557C051AAAF245EF3527EE73D120BA35FEF5
      8D1BF9ABC051C6A838558E2614E1744DF757B96EDA69A61457357808C3879BA6
      ACB075D9085EFD350B228822CACB2B1BC7D62D99CAC1BBC04F5011E9A175A6D8
      2B61A1E237D1DE12316B79BBD757DDA623950EE0F809F4029A1CDC3D28176237
      C5893E3349F43022AF697CDBAB10FD55F0104AAB62580A7A654952A3E9E8D898
      5C72539ABC04126ABCE0D5B79A846F6AAF0D62617168FCD5E024C6B40B606AA3
      CDB6C88DE13B95F7DA89769928AAB3777C1711D780A34020EEE4C5BBB57B5C0C
      4E4320B16359ED609DB6065BE016AE825B301A7014AC05B2C4022920211603B8
      C3ABC69BB0520662C55B77D08E5D55F016B155D182F8E4237B837E0D161CD5E0
      2279AA8306AE261611BE394902C54AC8735780A4465A30356799D781464E37AE
      D924C0DEEABA161648CC59AABBE029C00A82CB1C1BB40C37C42558D9CE6F7D1C
      3877E1BBD10ADA00DBA90BCE029F80A6D4858EC0689FE154846F04302DD92626
      CD6287AE567557C0526AAF60BB2430D0EB400DF9525B3570B9ABC0499AB98E5B
      8CD8FAE6CADAA8AC5B30A617735780B68326809EE360D5895F434394178F6237
      C8EF34996830D4A71C72B085F470156802CEB86B0AC0E3937152FB54237E7EA9
      3A574225D5D72B0D92B1E02A78096A034D446D679198A42E772886637DA5EC15
      5D542397017010D5593BADD19A517CC6F7544098D80939ABC04EA35734417038
      DF27CCC2F35780B6A3285E15C64D57930AB7B1BD67567386CE761AEC975D9D6C
      66AF380AC00158DDAD98BA78899CD3526DF8BCEB3D60CEABCC79B3A53272998E
      BC055D01665A9E173ECABAD46FA2C91D0EB0557B565D2CE024D55E6BD76D12AA
      751AB90BD5C2BDCD5E0212354BDFC6F2CC7C286F9ABC052C32CEAD767368E95F
      CF34EA08E6838DEA1FAD2AB67423ACADDCC78E02B3808C36DCB16B6C710CF4EA
      15FD76637C4A1DCA42369093A9B1E02E02B44E251A5BD62674652CF55C38DE9D
      6692F5BAA8E02C41ADBB7359AF5B94BFD3CD598E37F3578084CD59D685D60D4E
      3562F9CD5E026617EF64D5D55FB64AEB39ED842379F1B172572C9332BA5703F3
      181A9A8B438095202CA987340E3A114A96476B9566FCF0CCDB592EA325598E4E
      B80B8094A12A55EAACF555874EAA0CDEFB8EF75796869DAF551C052AAADEBBA3
      80F085AA991DCB43975DB71CD5E02963565E03DDDAD561668964A66922758E6A
      F013A0D5DF7DBA69BA0ABDDD3755171C21B4765A1C3F6FB30B15E02698B11C70
      1592026D893B897B2D122B70F57E4848116CA66722F7D9560A523E02E02180A2
      A95894A3259FAC88484514754CB43EAA3808D0AAF31CAD968CF6D4A757A52B70
      F357808D66AC64F8F26AB35C5DF9DB8557110C59CD5E02A22F4EDF32E0422CC6
      8E50EC6555C8C9EF8FCFDC71F49D5831BF6295E024BB9C05710126C38EDD9462
      2B6736CF7E28CD5B2A358FB5D5BBF017016E4059CCBD02EA6F2DB3D301B1E79A
      55955E6893A5FC04F556E9346DE5E93CCDEFD95EF20E6AF014B1AAE342B8C896
      D09E6F7F081181E6AF010D8D5CCC430679C871C9B0467EAF07273187C803171C
      D016A44E8B5E02BD80B5C107759DF9BA8CCA2F239A544FB57E00E00980055AF0
      AC30DB2D527C48F687D55F010D2A8C8F9B47CEFB9C62EC2432EBF12F35780A74
      D504E5D38A59C8B0C2144CCE109C96357933C05935584071341B9274F99D4656
      58F6DE9FC7219B11AEFB0CE02AD696B4111B11055293098AF7F803800B82C645
      A4D2A2CFA36EEA58106BA59C0580AAEF1547E6879ABC05666A8D4FE078A8BC55
      E697012A80859A2A53418C264AD31325A4A570D87B5078F9E53F00700018C436
      9EAEBB115244ABD5024D47EACE36B6B824F503DCC241AC851BC05C05615202A1
      4801F9802DA259271E092624D22895244894E91F005017B031049EAD57D6DFA5
      C31732B35247102119F3B78038000C631C50B3870B25F25F59C9AB44DF78E6AF
      001B4598DCB9203D3CC5B3B4F474B0A1B0204AA29ECD3628CB273A1BED5DEFA1
      228780AD70162B20232A450A099D2EBC92D340D2D29375613A866B332A875D70
      49D1D7C8574BFA4DBE5BAF5CEB41594CB1134E024AC02C49B32B449AD7A45CB0
      6094D72A8A6245435CD7A5116AEDDC7BC1F5F16D0635A034DF395E2C1EE9C254
      AAD98E101AA006F793381EB22EBEBB4B45060D0A1C13A8914D6DE8F902A9ABC9
      FE6AE4D58A9E374522B5BD697D4070E23626354F6A5443CD1E2C26AF7A9CD549
      6B454948407CC985C572AB98942F4B503D4A0B9A76C20066B2035FF203B54F20
      795F901E97B95D5030E07AFDA803241F2E20A2BC20169DD2011B06C78BDB017E
      9D13D7805AAD14A8E878BD0217D28A606C05AAF9052111DE2F318BE81B6095E0
      153C03172446AF17BD42F22B9B8C02A8678B717A242FDB9B5BC02B2695F648EC
      3E2F398BD809889000236A97F196BC5ECA0BEDB7E9A008E436F269AA3E2F520B
      E2963975404446312372C5717C822FB42A07008B6D65BC5E528BF0596DA1E02D
      C805D985021C5F3E0BE904B7C02DDB934D85079C5F3F8BF1EDA15010496BF326
      3C5F668BDEAE2F268084C990E66C66717E2C2FB2B5A70043875F4CE2F17DBC2F
      B4BEC04017D0E2F5A8BCA4A7BB480A951F6C120B4E2F660BD4E2D9DC05C046B0
      16B45E5A3EA743138AF5BF3C47AE4C438AEFF90FCB715CBD5EFB6B451B610054
      8385F48FC9E326EAE4B1025F85EB7917ECAFF23578F20B262B0EA50138408481
      0C94DFF60C2F2B1067BDE018717933DF9FC331CEDB516F2B4058764CD8CDCFCE
      BB778417CDD160CDEE92558C8058727031AA2F72CF231DF64EC5094029A35DE3
      847CCE8649F84675294666F25758C67C98AD27CE2C2314D354492268C5FB1297
      4CC0527C5EEE338D52C1168141EA645ECD36A5EEE9A70E5B3A8015FAA9B4B0D1
      161F8F4159E08E647F163B04BEAFA82E46E2B7F7A14BB260051B8A131F6D640F
      E7A51671A83FDFF8CB384895F93E9133015FB0602D96CC1A764C71019CDBAEDE
      B98457D8331A69C17338C0C5D68027675850E684858942D46D84F628F06C7056
      6494FBAC9E7FDB5EA74BB0165A6CCD1121BF98DDEE67612D4F9317E46A75D450
      CF7800FE31D6C6A809E4125A320FD9196702E659D1DF21D9C4F0D4477790B9CB
      F209AE7016034874F6053A207D6DE2B2C4EA74996878EB290B56D8AC5C242804
      60861455FD09CC9470D5E58E17F8A5690A726A98736050023D0CF0557151F996
      383FC721F3A0869D0385DF89627DF9DCAA324C6B5F56C6B3B08ABBB515C87669
      41D684D4C97A2802D68BB07040679778E39FE5AC2BA59DD2312C015E5D63011E
      BDAA7FB6974BA88B298219F955024F501FBFAE0C3F9FF7949A9D413E4E252644
      8F31052147F936F717A798872F44D805C46904F1FF8FFC7FE3FF1FF3F0EE2470
      9FAEBEB427C02274F1341B923FCD5F623F8FA2F4D01711AC7FE3FDBE3FC3BB75
      3BE8A4971B59013A485D220DE4C26FFBA937E32A576E6659479DB06DCE676E01
      7E1DC520E3FAD581AE1977FEB6853A15C4A57E4A70D04FC3D0E1E7A0FF536842
      21BC039E7E5536EA02653FA1132AB3F409FE2CECA74B5232A316AFC021F476FF
      2085DE670D4F4FF5E4167CC27E03FFE5010F0F17E2CF8F5B0D2A1FF7E5585E8F
      DF2556FC89DF6009C25FCE9FEE0B00408BFDBBAD7DD2A79A57F4C01008926C9F
      5A863FD1DCF112578D7364180274154BB97994CE2D2727F930F025A81501392A
      1CF8FF0D6F8982E635C9BF15C14740520BFF7627DF2D0DC88BFE2CB42EB35E8E
      3A0218E8F40817A178F1A5D91FE7710FF94917A03C5A162301492500927F931C
      B09F1E692E0130FF135FFA9FE40D1E3274FBCFE2A01481A77A67CEAB3F2AE9BF
      AC5D7A7F3A5621DDA58BFFE2CFFF843EB135FB02ABBD7D697DD17FE2D04F60EF
      DBAAACC688D5D89FB6E9FFE4B50E453FEB483E167FAFCC91250DD3C872649980
      9E3A31E9FEAC0A7F9437D95F9761A725699D058605F0495BC1316F4FF5C8C7FD
      B04FFC6B8D64E3FFAD7CDBE9876479BC20281355C7FAFF6E7E83E9BD0B256A36
      607E80AA023D071A08A30726841FFD597D5517FB87C7FDF04FE3FDAD79F48B81
      9553FEEA24DF0E15335F9BB959C9B1A00AD6B7524D4C7D9294DB647FFE3F2D77
      1E0D6983BEAEFF94DFF8EC7CF785F53335437CE6FBD0FF719BAD3D0FE8741411
      602763D0592461AAA0A262FBC7FB69E88F78BD6C9FE5871CC09578FEE069E847
      ADC4791786273911B127DCBF8917102CD02B1CFC4B8E05AB3A015720C4BDA73E
      3B51A79A6F7BDFF0DE51077C2F73C795628D028490139DB556BD5E4898A447E0
      BEEF7FD63C561BCCF9C94C009C670B2FD94404C413C78FCBEBE77C278EF7FFA3
      BB157901DAB6FE968DC99FD3C2C9F99755EFFCCCECEAFD7F12A8CA01749CABEA
      7C7A059EFBEC8C2002902D545F5FE8914D069487D49CAA00BBEECCA25FDE3303
      BD7F1B6C0533EB7BBFEF5FFDF22CC5FDFFF87D3E79562B28BFBFFBA38D95BEFF
      6A87C05F7DFEF35BEFFAFC67D9FDFE444281CD9AFF001A0F00DA0F01F81780F0
      FDE01F2779C55831E07CDADA80C107DA1A9F78041D0119BCBBFB9005F98AB6ED
      5AD7ABB1505FE348550BEC1FF8CFC2FC7FE3FF0BF1FF8FFC7FE3FF1FF8CFC7FE
      3FF19F85F8FFC67E17E3FF1FF85F8FFC7FE3FF1FF8FFC67E3FF1FF8CFC2FC7FE
      33F0BF1FF8FFC2FC7FE3FF1FF8FFC7FE33F1FF8FFC67E17E3FF19F85F8FFC7FE
      016F5FF7AFEF24FDEE4A45B7064CCAF9DE1FFDF39CC5A2FCEFE0FCE36E564060
      FCEE7B9F00B00B00B00B00B00B00B00B00B00B00A40054DD3BF0095905B72013
      E761E027A18185F85ED24B11DB1AFC02A8F0B5160B9D270D663064DE01714696
      17E178BA97B6C0BAA49CE84D2C2FC2FD1D9B44FC65922C0BC2FC2FA2C5F5CE72
      CCB959804B2E17E17F40D4DB69BFCBFF0BF0BC5C9CBD25F85F85FA6FF587E17E
      17ECCBEC77E17E17D5C96710CBFE016016017D19CBC02C02C02C02C02C02C02C
      02C02C02C02C02B23531AB7E66AE74FE6BF37F3DEF31F8DF9AF826CAFCD4E7AF
      EBFD6E739E0ED75D215F6FADB8082B00B009F805036A79955742942C0A4278E1
      67131094B58ED7BAE05511E8AB64CBB0E110652C0533805804FC027C14795070
      644EAF600DB68DABF8B2B2791639EB529BE8031428018DDAB013BE016014300B
      ACDAC8A5C9A6305959238727D6B773E548C348250580BE98058043A020908025
      167929E4D9DEB0E72717BDDBE7CE1A620598E4C02474E4B504DA529F82BE6F06
      2338627B7016879B1C7A7C4F6ABDEC040FE0158F0084E017322C050A424345AA
      055BA0A1DCCA58F701A432DC7C98482CD4B15AA6AEC2AFE145D38202F05F00A5
      18081E3C055DD50AFE027EEC74269F0BFAF4F2C91AB15BA74241AC18B9728973
      A504F00919EC0437C0298C0217C042C000B87B85599184CAE638E30BE59378E4
      AB27473E4E8A8A23AAEADC1109200279C02316022B67DFA0CA4BE5E735E554E6
      F9E2BFCF2C9281490D79986017CDAD50E1C72504E4FD92C84B04A4F1D9A907CB
      27DCEC0D96B3DD200CA66348FC9DFFFF11B9BFC01701240272BD09C025A00853
      C02FCF0162A0D789B5EAE928C5616E1220F0CD0B71ED13880A2916803A45BAAB
      8D00021CB80250B30096A01775411AB9BC473046DB5304E3242734DF313050EF
      65A1F73B44AA65232E5FC051AA01049092840208B9010D63732202338051E02F
      9F80524683F32942AEE0595ECB3CBBC7206E2B8EBBE1315C29C2AB9D07E59380
      5C514C0BEEA24700975086CBD07AF180420B4E85D0CF62B232595BD2C7260116
      0DB916F016FE29F19EF557B7EB417E91E6F523DB92EF704A1DC9763A5FB5C04B
      94CFD70132F8052644041BC020AAF0110F00A7DD4B008F27E013CC04E6E8F497
      9DB7374A397C9B5C44AA0E7F1C49CA99338FEC57A0088639300978E41FF0962A
      CF46E1A403E5938E4664B271C98E4C726393009F43911C20EF0E9F9FDC75DD52
      0775E45E5D21F00A99FAEBA0100FBE4051BE014A45E014028041AE0098D4022B
      E012EBC9A0121F009B3008000431CA2EA93E471C429E73DBFE5C4DAB167EBFDF
      E41E3271C98E4623905492C146E5C9B186CCB271C912964E3931C98E4C726392
      243916FFA80E03A5BE66F54755FFFF27686E404729A0B802434FFEE00ABE9A2B
      794A01015008478053174023BE0141BC9C012368C02E8057132F419F5EDFB4EA
      3B33663CE05A35E1320A01C0D72C1F96663931C9600E41536C96E71C4401B21C
      B271C92B564E3931C98E4C72639233393D276E519B7027BEF5F5ABDEAE67A484
      EA17F8EA3C607504FFCEAFC76C11D58ED9F197A85AEE1EA4DFA6E54903FA3DEC
      13207A62152778E4C72718E4153C177E7DC5423C41964E392E62C9C7263931C9
      8E4C724E6722A4EC609700B8EF71F55EA49E6383B74F66392EACCF72CBDFA6CA
      40F5499393FDFE9821C7278B9688F63931C88B49DCCF620FF15277964E3907B5
      938E4C7263931C98E4B14E4549D8C06E029BF648AFA4B89BD613C027B6DF8ED8
      69ECFE9D5A7B2C4EC52EACC5E8AA5FA4ED87F4D91844FD1F95BE3931C8C4727E
      93BBBE5938E4EDAB271C98E4C7263931C96B9C9E93B84351BB47E39FED473FDA
      39BD7E0104D3D9A7B18B39E97C459F4F49DAF2A4EEE0663931C97FC9DE5938E4
      1520B63931C98E4C726393F093B434D7BC73B94A2D5ECEF65B50FFE4EA51DBA7
      B34F63272768BF24E8A366CB0CFFA7747F1C98E40749DE5938E43AA0B63931C9
      8E4C726392CF93BB0C4A427C9F9E2F78E7F9A0A9E6A43AB4F669ECE849DCF7AA
      8D9CBD151FA4EFA6C5BFA4EF1C98E4ED9277964E3917682D8E4C7263931C98E4
      F8591C81427709F1674579F6695C5EB8F009DE76E9ECC750364EFAE9843D2768
      C61074F8CCC7263938693BCB271C8E4416C7263931C98E4C725E2F3BCA4B3513
      DECD5DE2F77FCFB24A8F3AB4F669EC3139E7E95DD1E2730968DB8E2058FBE516
      3931C80C93BCB271C904416C7263931C98E4C725CF93B2BCF178D5FF717BD3CD
      F0EAD3D9A7B17793B07F49DF7FA4EC2A67C03D2778E4C720764EF2C9C7247505
      B1C98E4C7263931C9D149DD87F6FECD054AF3D3CD6F3D3A7B34F64949DDA3993
      B662E8D678CFC726393E8C9DE5938E43AD64E3931C98E4C7263938593BF7178F
      05F7179E8BF87549CF6727569EC4660CC164EE797FBFFDB8CA41F3D3DEE3931C
      8BD49DE5938E43AD64E3931C98E4C7263907304E3DFA8E7F6489473A534A39D4
      0205A7B34F65C92768640BD52777A8952778E4C72777277964E390712C9C7263
      931C98E4C721E24ECFC4583DFCBA4E2F2A79AECF4FD19ECBFD3D9054F67FA4ED
      7CA93B3BD32503D277DFF78E4C72329C86D277964E391AE5938E4C7263931C98
      E4409C9C178A0F7F60A59E4E73E3600EABE9ECBBED669EC8727B1F49DCEA261E
      64EF8927A9F31C98E4653900A4EF2C9C725895938E4C7263931C98E45C413F79
      DEC67F8E7F71792BA3223C73A80574EAD3D9A7B0E60C9AE1DD64F302D00CE7D2
      AC82B78AF66EFD88050094AEB8FC664250092C3F3F80AF8CCB3F01717C05F326
      FCCC961BE0248A0172FC6DDB04A80E0054B0565F0128C03693E0270500BB6F80
      9A540205F0138A1A40153A3F0029E10E6CE0244AC3B0CD32C9CB26785938E4C7
      263931C98E45EC13E5276C6CFF24485D754F32A7A74F669EC871DA7B3C72740E
      E7698AB7A9B657D134A220EA7FBB9E4023698B2C042D570024E752AC4697B521
      7E802069178646C2C11ADF80A51402F2F809E45A804DBD8020CE5C009148D974
      0150929AE8026026F6C66D4029B5009C7E02A1EC013D7C0447E94C0245F6819C
      66A435E12ABEB4E593964D1EB271C98E4C7263931C8DC3917277762B0FE48BFC
      5EFCF32716B3AB9C94041D5A7B38B9EC6993E4193B689ABD40B8554648D9B8DF
      28414E407B8A77580AD20FE024F6C9AB014F7C0544B01443584580A7C2BCFC05
      4A27E2F8C29F5F80953901527C04D1F0174B6FC05ED580A7CEBA00B8EB99B901
      4C7519740117FC0495B3E0278AC0403F017A7B00481F015F45F8808B924CD67C
      F893389B15A72C9CB26C75938E4C7263931C98E4701C9F93B6B76779648A5A8F
      F1860FF24BD0EDE93D84384F6B1CB3D9E792BBDC9EC7F82C593AC9DB4939FB7C
      FBB703ECAC0E6DE62B08861A379A62DDF0118B119F80A34C90E4AAD13DFC00B9
      AB0139F43D160230F49BEA0238331C7ACA2134F80AA8B0103FC055F580B9B6BE
      005CCEC0299B6F0F6015A2E0F3B809A368A8580815602AAFC05E2F5015A7E02E
      A475BA771FCE0373F3494FAF807964E5937B2C9C7263931C98E4C723D0E46347
      E76AE66F13CD2E12E79A367569ECD3D9EB27DC2CC66B93BF031D9678B210E96F
      2E4099AB555CBAEEEB0117D575BB80BE149CAED7CFF2D602BCFC045CB010302F
      B80803E0264E0047E87F01793E0285A29B7C057DF809DACDDC05CB2FEA022235
      08E0056AEE0298FC04941DDC04D9F0159C0808FFE02BFD447E3D3A349E0D4C7B
      1964E593CAAC9C7263931C98E4C723F4E4EEBBE884F727C02C02C02C02C02C02
      C02C02C02C02C02C02C0224602470CAD28443D39772BDA8B00361F01A13F03D4
      7C1C10DF05F314765C05EA4087DC800580380380380380380380380380380380
      3803803803803803803803CE007CB19DBA803CFB3601E01E01E01E01BD700D6B
      8F86462153931D97058078078078078067080D6BAF0070070070070070070070
      0700700700700700700700700700700764BF00F00F00F00F00EF7E4BC1B00F00
      F00F00F00C2201EC97E00E00E00E00E00E00E00E00E00E00E00E00E00E00E00E
      00E00EC97E01E01E01E01E01EC97E01E01E01E01E01EC97E00E00E00E00E00E0
      0E00E00E00E00E00E00E00E00E00E00E00E00EC97E01E01E01E01E01EC97E01E
      01E01E01E01EC97E00E00E00E00E0080C36859DC0770300F00F00F00F00F00F8
      01E657803803803803803B25F807807807807807B25F807807807807807B25F8
      03803803803803803803803803803803803803803803803803B25F807807C07C
      07C07F25FF25FC07C07C07C07F25FF25FC03C03C03C03C03C03C03C03C03C03C
      03C03C03C03C03C03C03F25FC07C07C07C07C07F25FC07C07C07C07C07F25FC0
      3C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03F25FC07C07C0
      7C07C07F25FC07C07C07C07C07F25FC03C03C03C03C03C03C03C03C03C03C03C
      03C03C03C03C03C03F25FC07C07C07C07C07F25FF25FC07C07C07C07F25FF25F
      C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03F25FC07C07C07
      C07C07F25FC07C07C07C07C07F25FC03C03C03C03C03C03C03C03C03C03C03C0
      3C03C03C03C03C03C03F25FC07C07C07C07C07F25FC07C07C07C07C07F25FC03
      C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03F25FC07C07C07C07
      C07F25FF25FC07C07C07C07F25FF25FC03C03C03C03C03C03C03C03C03C03C03
      C03C03C03C03C03C03F25FC07C07C07C07C07F25FC07C07C07C07C07F25FC03C
      03C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03F25FC07C07C07C
      07C07F25FC07C07C07C07C07F25FC03C03C03C03C03C03C03C03C03C03C03C03
      C03C03C03C03C03F25FC07C07C07C07C07F25FF25FC07C07C07C07F25FF25FC0
      3C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03C03F25E00C01803006
      00CC9780300600C01803325E0040080100200400801002004008010020040080
      100200400992F00600C0180300664BC0180300600C01992F0020040080100200
      400801002004008010020040080100264BC0180300600C01992F325E00C01803
      00664BCC9780100200400801002004008010020040080100200400801325E00C
      0180300600CC9780300600C01803325E00400801002004008010020040080100
      20040080100200400992F00600C0180300664BC0180300600C01992F00200400
      80100200400801002004008010020040080100264BC0180300600C01992F325E
      00C0180300664BCC978010020040080100200400801002004008010020040080
      1325E00C0180300600CC9780300600C01803325E004008010020040080100200
      4008010020040080100200400992F00600C0180300664BC0180300600C01992F
      0020040080100200400801002004008010020040080100264BC0180300600C01
      992F325E00C0180300664BCC9780100200400801002004008010020040080100
      200400801325E00C0180300600CC9780300600C01803325E0040080100200400
      801002004008010020040080100200400992F00600C0180300664BC018030060
      0C01992F0020040080100200400801002004008010020040080100264BC01803
      00600C01992F325E00C0180300664BCC97801002004008010020040080100200
      40080100200400801325E00C0180300600CC9780300600C01803325E00400801
      00200400801002004008010020040080100200400992F00600C0180300664BC0
      180300600C01992F002004008010020040080100200400801002004008010026
      4BC0180300600C01992F325E00C0180300664BCC978010020040080100200400
      8010020040080100200400801325E00C0180300600CC9780300600C01803325E
      0040080100200400801002004008010020040080100200400992F00600C01803
      00664BC0180300600C01992F0020040080100200400801002004008010020040
      080100264BC0180300600C01992F325E00C0180300664BCC9780100200400801
      002004008010020040080100200400801325E00C0180300600CC9780300600C0
      1803325E0040080100200400801002004008010020040080100200400992F006
      00C0180300664BC0180300600C01992F00200400801002004008010020040080
      10020040080100264BC0180300600C01992FE4C9780300600C01A03E04018F53
      7ED80FF565A98000C2D80C89D81ED2D80F83360BCDE92E3286360B994ED7280F
      383D21EE5E008010020051E02EBA3480901202405C100AA143CDED9D9A1AE2E6
      29039CFF05F48337F5D0888F4BCBEBD55CA4AAEBFD7200A526F383E1152FC45F
      146DFE315EA4EEB9A3F137175763B85F1115D9B2D8D77B95E35F06E1EA0CEA67
      DC73AFEC4FFD2C77794B568D4D46AD5547FBC9F9D1E7036400400B36010A1C50
      E008010A1C50E2870042871438A1C010021438A1C50E00850E28714380200428
      7143A02404A87543AA1D012A1D50EA8740480950EA87543A02543AA1D50E8090
      12A1D50E809012A1D50EA87404A87543AA1D01202543AA1D50E80950EA87543A
      02404A87543A02404A87543AA1D012A1D50EA8740480950EA87543A02543AA1D
      50E809012A1D50E809012A1D50EA87404A87543AA1D01202543AA1D50E80950E
      A87543A02404A87543A02404A87543AA1D012A1D50EA8740480950EA87543A02
      543AA1D50E809012A1D50E809012A1D50EA87404A87543AA1D01202543AA1D50
      E80950EA87543A02404A87543A02404A87543AA1D012A1D50EA8740480950EA8
      7543A02543AA1D50E809012A1D50E809012A1D50EA87404A87543AA1D0120254
      3AA1D50E80950EA87543A02404A87543A02404A87543AA1D012A1D50EA874048
      0950EA87543A02543AA1D50E809012A1D50E809012A1D50EA87404A87543AA1D
      01202543AA1D50E80950EA87543A02404A87543A02404A87543AA1D012A1D50E
      A8740480950EA87543A02543AA1D50E809012A1D50E809012A1D50EA87404A87
      543AA1D01202543AA1D50E80950EA87543A02404A87543A02404A87543AA1D01
      2A1D50EA8740480950EA87543A02543AA1D50E809012A1D50E809012A1D50EA8
      7404A87543AA1D01202543AA1D50E80950EA87543A024046543A024048090171
      E02CAE3D012024048088C054A7C607E0F9B8A980020AD80CBCD81F17C0529C1B
      D82FB4A573CC18D82EB94A2CC03CE0F28234CBD96C639C556F2BF30A45FE5FDD
      94CD887DE5A255A017AADD6D59C5934D577DFB9180AA7CBAC8D760D7F6EC9CCC
      F4FFD20FFD54FCF71E7949FE9FEAFF4FF4FF4FF4FF2533FAA7480901202405AA
      00A6812EE4805B66B049C457F13DEC005B1354B0BF0FE6B5DC80B3392F60371D
      7AD006A5505EE27FAD33D3FD3FD3FD3FD3FD3FD3FD3FD3FFDF33D96B40480901
      349FB01665D5C982F0E73F58E2774FBC05B1354B8C2EB5FD0E4B6093B2FF4863
      13118FF45802D86B61DBD0C86C65E21DE5D032F8A8E2828055667A7FA7FD61F9
      E9DFE29FE9FE9FE9FE9FE9FE9FE5A7FB2D680901202693C0A5138A90A9268A37
      90059938F9BA9AFD1D89B029C38FADCDF049ED8BC60593392D2D73786297A395
      2FE387783ED80B608E40168E5C4098DFBDBDFFBC20B6E4B6FBF7E2B1F38F3C14
      AEC9FE014CF13E08FCFB8FF228FCF24FFF10FCE458F70FF873F198FC5F35F647
      FF2EBB87E42F40D6402FBB05D2C0B8BFC20AFB57FF8949EAFF4FF8697FB2D680
      901202693F946FCA528202E49C7CCCB52FB7BB4A51898FD22FFBA9480EE3E1FD
      5526229079C7E927CC52946C4FD67E14BDD0B4E1ED87E07E6B17C9587818FFE1
      F330E27EB3F3613F23B0F32BC318044CCFF36BC3FCC236F31E298C21F3F8015B
      D2966BD2E70888C7F9DDE194422315FF00EF254BFE41DE0DCFFFEB02C467FCC9
      DE349E80BCD07130FB767C8FF4BE022B121B9F0BFFD82F8113E75F3BC9002937
      1E036DEABB6395CA1C7DC8BF4271FB4057B49F1489FE95FEDDA02301BE23E4FD
      F06A4227F29334CCB6A4079BF7259B2A6C3E482F94B0FBE3FF10F9A84C7811C9
      2627F887489FF0B60D0F7ABE2A4F8EB06B6DEAED2739F34F2932687F4E584F00
      BBE648531FA39C260582FFE08CC9FFD57B7527FB1E788C231C231EBFCCBBCE00
      BFDAFFD1BBCEC2F925FF927FF701043BCE73D3001481FF22EF3B93F2ABFCE4C7
      8C8704E4A84B8165663F3AC7AF0473C05969EFABC9FA6E41F05FFE550007969C
      33C002FA727CD149FDCD29182FBE47F95B3C015D0A5267023E035C0ED59C79BE
      63E8DC7F88A5E5C94A5C7C9B9F97405CBE9CD4927FA11F21AE874E4C7E51C9E5
      023B3859A8140F53F4B4A55513F9F6198BE27E79C9E5023E4FE8205E3A1F221E
      749B45355FF5A79DC6F574D7F9F5EDE505F5C22D5998ACD27D81E76B73168BFE
      E879F7845A017FD01DE6B8BE557FDAD2605E23F0E715BBBFBD1E7B24FDFD7FE4
      257C53BB6E05E1D31E8BCAA2F753EEA7E2BA9FEF9C14E111DF54870417EB23FE
      50DB0E0C4942BCCE85F88052AE3E18BE9B95F42E3FBE197B017C7259871E7736
      EC97F71E789FE847A17E4014589F14E7F884050ACA2B2FE1B7C88458BDAF8C2F
      BCCEFF29FE6D4A45944F8C73FC422313FF9088F49FF8F38FB59FD9E2F0D6A9D2
      FF26D834817EA0FFEFB1D73FBCBA02F05F9FBDB591C22D6D27FE3CEA09FAB3FF
      AEF332062D73F1FFD5FF2772B467FD78BD3FE49FF664FCA3FD9EF45FB18BDD4F
      BA9F8AD27F694AF017FEF3EB729C70B12417CC174292C050131FA07C29F538FE
      2E6138F8442838FEB92DA98CC264BAECE8B8FD791EBC633CDA58EE852880A8D0
      FF28EAE9F9E0CA7DE23FEDD7B6BE63FFA02FE7CC1C4FBD9FF44EAE9F9EEDB45F
      1FFFCF3F1A52F19BCF26114979F7C2FC5CEFD33FF41E77EFC7F8079E393FEAC2
      2976BE07A454D0B03DC48977F4617D442F7BDC42DBBFC877952C4BEEEFFF44FD
      542C108FB1FECF7A2F753F67A72EFCB4EA7C23A9FB6A67FF683EBCE146341E5F
      F0A6C4C7F7C1706632C648ACEA7FBAA505C7C4D3D5B82ABC906DA04B8BEEAF42
      3D0BFD00B5531F97A0FB33259994FDFF49948C8FF09FF8648BB163DFE14E07E7
      DC4FFA1FFF602C108FA93E0A106D3FED685428ED69815B7CDF17331C997F81B0
      2E7E7FEAD8170CFC5A605BEA4FFC779FA79E6846A36BE251BC3736CBCBDB8B82
      11A95FF8623FF98105CDC0082C6BBFC8EF069DB68845A4F45EEA7DD4FBA9FC6C
      04115C0693FBED4F405987FC42A94BA110C35D0EDA77FD39FA021BC610780818
      3424238D13CF4221D015D52902A4F850BF65DC97D7D590F44FC8720C815BEA40
      440B4CFF14815C178B4088CFFB098F078FFC67760A747F667F8D211717E590FF
      185F757E4657F8F9EBBA7E1B0FF2C9E6B35FE3AF9BC7998046087FFA0523ABE8
      20765F404345F76BFEF1FF7A948AD77F871CFF6EC22411693D17BA9F753EEA7E
      49A93EF9D94E0D1DBF49E6F6FEC80106F3CD385634358B29F7F46400B532A448
      344DC7F61D17AB0CEC8FB96CC91131FA34B25BB89E6BD093989F88FF00CA7E52
      7FE71D079A87FC0D8677CF5D5A4FE007FE7FA3F853FCAB02C4BE686BFCAB02C4
      7CC057FF663CDC3FED6663C31FF8402883FE5F9538180B263A53FFC21FFF0401
      BB49E464DE3C0BFAA87F77E3802A05ECE7710145117D4E4FBA9F753F69527F69
      4B1F9DFB6B901E011FB7CEE0CDB1F0D781C7F8D3F80B008B80F3318C2E82CC85
      CCB8F3C3BC8FDB3D065D6EB92C7AA59623D7A02E877BF8709CBF3EA7FFD11F1C
      289F6D92229E9FC3CFC874FFC43F378FF347E13D75C67E77AF0639FFF033BE2A
      73E280A2D5FF9402B2AFF870FFDCE63D3FD3FD3FCE3D4FA02404809016580170
      DE779BA5BF48BFE77C491B71FCA68C0630147E386EE30BA05C8143AE3FBF7E71
      97B9D992270CA55CBBC6E63B4A44B8FD1047E7399F75E53E2E7E6F9FF4B3F0EC
      4FFE87FEA1CEFE2D37F14BFEDC0444AFFE68FF8BD27A7FA7FA7FB49E80901202
      693C89A4FFC81A88CB8DE898971F1E8C4B6307A3402E8989C5E083173B85325B
      BCDF2DDEFE1DF9C1C9C402DDFBE80BB9DCECD6F69FC7618DF8667A7FDAF3FEAE
      7E78FF824FF4FF57FA7FA7FA7FA7FDD19ECB5A02404809A4F9D5291326BCF80B
      AE27BF0F6051792DB2CB9F7F8D1F263002EC23E19E9FE9FE9FE9FE9FE9FE9FE9
      FE9FF4433D96B40480901349F4CD4FCA5CF89C8B2D7CAF00427D01997D03C7FD
      03919F407C24AD71418E83F114F628078C1FB706920240480901658008808404
      8090120240480901202404809012024048087201098280901202405B8A02E330
      8090120240480901202404808C101413680901202405FC202334101202404808
      B680AE4D404809012029F0023C250120240480AC700A6F8A0240480901628014
      0F540480901202CDE02FDD140480901202404809012024050800857020240480
      90108805297A638F451CCAF00463D01C35D03CF8E81F71F407CA4B149418083E
      D19403C60F09665316ECEE76BFFB822EE517BA68EEE2CBE5777CF43DEF270E92
      2F45E8BD17A2F45E8BD17A2F45E8BD17A2F45E8BD17A2F45E8BD17A2F45E8BD1
      7A2F45E8BD17A2F45E8BD17A2F45E8BD17A2F45E8BD17A2F45E8BD17A2F45E8B
      D17A2F45E8BD17A2F45E8BD17E962F0FB6F0473F50F6DC9FDF6CCE1C80F6DC58
      FCE04288B202C2805D2EAA74BE4481405F2F65894ED01169DCA5D03D5E131FA9
      C6B9017F10174B43C7C787D74640405808FA8353B53BEB37BD7DEDB6DC6655F4
      044F00BA5A405F1D301001DC0082BF94ED4EFC4BEF7BE7834051080A12FB0A8A
      CFD040543E02D6218CBD50553BC91DC07F4E5D0168D1FACB0443077FA02C635B
      500A19442ABECCD5405E04047B2808B05D5C85EA9DA9DE65F63217E680A80016
      54F74ABF9A02A880594BD50553BCABEEBE9D216023C0A71014CC0510A5CC9EF7
      4DE2FD308202E820B01160EBB015D88EA838F19099CBD53B53B57DA5FABED5F6
      AFB405EEAFB3799666F0A8C202E8222008B08A02DD01185466D9E74F6E71C0F4
      053FE894C046300BA20DD11E0852D84274599CBD53B53BCABEF93CBFB93D3E82
      02E8222808C08A022C8059EDC47935072005A6D5AC826BA8E37C81D2BED4ED4E
      D5F68095F6AFB57DA02FA97D901BBC80AC101447587404EC660162005C1D46C1
      1060471C246EAC36A82A9DABED015597DF1AB94055B012BED0160D7DFAFC7D40
      45800BE4BEFCBD59D4056980AB5EECA8413C9D9B7DE32084C2401748006C1353
      D9B462CA0F1B00BA5A702FB53B53B0C77EE30CD50680A65011B74E902808AADD
      6753B4046A772E803EB21FC713420794A81E80E26F00F167E03EECF00F9FBC0A
      70039EE273807941FAC7B3F7E171EF3D20E7F571617F859AD93FE1A9F1467D93
      99C9D80BC268A6DF73BD4AA100A39ABB893E75679D8183B62DEF0E0FF637CDD4
      07FCDDECFA7218D863D7FF8CCF5871FA123F71DEF224FF92B3EBF66C5EBEDA3A
      DEB2C8E697384FF3F33F0E9A0F967245F019FE34898CFBFE9BD467A4FF2A9F98
      2FD3A6EE6619F314FFBDACE8867EE24FF31FF8859FF314FC3997A4FA67A67A67
      A67A67A67A678305F6589E99F8F27D33EA59EF4CF4CF4CF5DFE2467B13F9AC3F
      CBC3C024CF4CFE753F8333CEA67C5E3BFA7FBC02A863BCC133FE613E99E99E99
      E99E99E99E99E25D4FD8F4DEEBFC674FF68DC7D4BAFFB2E57E1DDC7E565E2733
      E394FEB10ACB2FB4F1FFF0F696E5F22784C51FA0FCF17CDFBCC884FF9FD93BCE
      F45F77F3BC3EDBF08C6D8BDB7867BDA716729FDB7B99B67580051F6BF9EC357A
      49268BB85988E20AF8D0C5FCA354F5AEB8D37DF0BF9508D34468B11622C4588B
      11622C459982CB0962C4860C2ADC618D589C52A4D422C4B85BD5762D7F8A6881
      FFF04230CF9C5EAE8B11622C4588B11622C4599C2C9918D07F939D76790BFD4A
      D7812D23B7DC1268E2D9D4B461EDC57B311ADF4C22555EF7F57A38A3E4689F9F
      FD1622C4588B11622C4588B3005965BE56DBBCD113F3B3265853CFF4D87CBEC1
      1AFD82FA8CE1FAF5C21CEDA8C2342E715FE160DCE7D96B36EA35BE8B11622C45
      88B11622C459102CB75FEBF07E3C4F2C56A855793E75306B400B8C7708EB6CCD
      377C6BFD14AF776A78D1EDE5F51622C4588B1165745833F39060ACD805E7FABA
      51622CB60B2E163BC77B151E7232A21F10F94BF50976801782318590EC424286
      A177BBFDF37C7B8FE4A2C4588B11622CAE8B20677349C4588B2EA2CCC99B1586
      496CBD27465809055F99EF54D547204E4056C5D34BE168B116650593A6B60160
      C1D808B11660059B262AC3A468B116598594FA6657212F97EC4A35A1FB10F68A
      30D6C1AFA3DF62F2A3BF6639C12B53A977391622C850590C6287C7C4588B3062
      C2AD819A7198D249DED204588B0F167C6A5BB623BF2DECFBDCD4A8BB59C002F3
      31A62616E9CD35B975FA1A93F34CAFC98E44BFCC87C11622C86C590CBF9C6D05
      1622CFE22CEFB11299B009CA8B11661859EB45EE8597694CC90A59F5AC5B638F
      92004EB99C82FB6E8CEBF4120C9C9E83B02894D1622CA1858337A49A4AF3D162
      2CB24595BE7A11622CC58B2E059E2B277D34603765DE5E7F0D33BBB7E1020191
      D5773C8EBF13E77B8593963B116EB2A2822C459FB0B30DC344588B3222CD82B3
      37B2C3D7FD6E00ADF0B3611622CC90B27C8FC7FFA01EC4F77FE1DA432EFBCC5C
      E0E0F7676C795B16077A87A526EDB1ECF2644F44DDFC691622CCF0B0B123C7C3
      793C9BE8B11611C58A8D459E28B3A4E5F717EEC8AA5674FB6708E4F13752FF92
      1F1D8E6306E4EFEF1F5D46EB139965F4184120FF169268B11622C4588B11622C
      4597E2C389CEBFBB9EEAC9862FE1E4E6B2F7E985AD70FD338C2D703B3E855E58
      290860A51622C4588B11622C4588B394587536F7173DE2054E1614FEC8051AAA
      E8D3D93C21D9F2052A0AD0991622C4588B11622C4588B350B231FD08390BE88D
      A00A4BA0390BA030CF7A070D9F407C7C61BDA17C4B7C3FA801DF10D01A034072
      00198A63AD72573A9332F742C238997871A4C749F718F1A7C79FBBD00A7DF5D3
      599B11D00EC81BD2EDF80C59074078B00E9F8C061BE2E9C6E80EF70FE1680197
      A9FA786020D0EDD7D20CE7A42353C590DB5E62DAC6A64352F40603D0180EB205
      10EF80C9D01A038701D2FC00613680F9E0A6BA908776F5AD66C807F31B2ED1CE
      182EEBD40E7FA032DF92166D6A1FA006588DA03407A513390BDC9AA0D791DF20
      EAEB0FB17A09FB0B3DE80C87810B9AFBA1FA407FA3680D019CB5CB10F398DE91
      26135BEE7896C6F3B734B1B85DFAA3A8F00E0BC0339F00CABC039FF00CC78407
      3680D018100E9BC030AF00CDBC032D43FF41FA5F6E2D8C1712E36F98DA9F5A97
      A2B591BF99B48EEB9F00FC89F77913EF37E101F91B40680EC01F96BF3FE03F2D
      BE8F28BE9F21FD9E43FB497CC3ED3B57E4DC9B74313AF5BD3DD2BDA573776C63
      73EB63381DBB54EE0FE01E67807BBE01DC9E03DF40680EAB88070BE01C078065
      9B54FA35F00FCC7FE8EB6D6899C7F5976EE9CAF85CE80F55B5DE9E7DAEEBEE6D
      2BBF03697BBE44F2FC89E5F6F200C5203406700F18478079DE01FEB5FC8BFCC5
      F90F9FF00EABC83F676D05E22F7463C1B75EE901BBB131DD7B16EAEFD8B7221F
      7FF43FCC8CE01EFA03406780FCE9E66F777807E43F3777C038EF2271BB19F938
      DBAE3359E41C8D7FEE6786BE3B7C4EC6FEBAB6261BA3950FD7D8C74FC4864680
      D019E03CD3C07F1E5A7F5FA2E2FC87E242CBF6EC8E02D87B884AC1A9C0F13086
      F4899B95F7B1C2C1BC878ED8A7322663F44F122E80D01D803F3A7E9F2D3EAF22
      7D9C3E44FC85F3ECF90F8CDFAEAEAFBFDB3F35C33FCB4BB080F487F21995BF25
      4F00E5178E44FA0061D01EC001FEE8FA0063BF5AFFA27E6B7FA17AE2BC85DCEF
      D9734BB9BFC83E0785F0C92FCCEC698C9E01CBDC4E433B8997F80EA6E28D543D
      1F40680F100EA973C07307FA3FF6F9D5ED790FE1F4E58BDFABDA73FAD3E55965
      3262B6599E90BD43852C018D3C6371A54003022E7A37623E80D01E301816FE50
      2D7F3AB9BDDE43F6FD537C3B91F646CD77EC1E5B4763939AAFA41F47506EE69A
      BADF01800FD2031C80D01E180C66E6A33977D1E6FAF32BF7666FAB723EB47C52
      F786ACD3EA418E7CFF66FC1D9DC700F4EC01C5A034077C01EA6301EB7D7B8CE3
      54844E276C7AF145D994B453F7782D257F21E8010064400FAEB021E6C8F3717B
      9E4AB4F9C4D95EB9DFC1BA00400800037EFFE7BF0149957584CAFA92922B9DCF
      8332E5A47200CA4053FD847156D1E2B8DD57A572FA26077E3FBE858741865C8D
      31515E2F8AE5FB25C0A72CD38A7D349F883F637DC52B7FCBE277C88E4203C9A0
      3DF4065BB0076AF80E95D50FE08FC8F1718DDA64389A733ABC6843F28E25171A
      E73B7B4D7B4EEABF440B5D6BC4794DF1875490707B0015DF80E47F80EA27C07D
      8BF82F6F3F93E10C7C17BF32F4203F1EDF058007EF166A72F212A7ABB46079F7
      7447AB951679FF05CBDDCA81BF7DA8048A08F87E9BE1B35817AAF89C54540DF5
      D542A889084BED1509B84221A72BAE057AB6A067632FE3BA284FCE30F87FE27F
      7BB855F8ACE6EEE7C4E2E1A76C24277C5667CFBA1A7837A057C386067E372DD5
      9FF1DE17C1FB761C75EB401F001DDA00F802BD1000002F72800001AC08000015
      6D400000EB7D000002ADA800001DC92000001DFC800001DCB1000003C3240000
      0AE6A000007594000003B9400801007AC00A77F0DE6CA000002DEFE1BAFEFE00
      7774800001DDDD000002B8D000003C33400000D62800001D6DA000007EAB4000
      00EB72800001A39000003ADB4000002BF4000008DAAC40080100200E9DD1AAED
      0062002DEFE1BF26800000C3BF86F29D000001E3DFC00AE620000034F7F00330
      4800000D9DFC3778F7F003BB240000070F7F003479000001E5DFE377377F8DDE
      7DBCFF79FF945FA36800000EFEFF1B9362000003CBBFC6F087C841AE155DFA7B
      3F0B9B000736801EAF200ECF7007FB10ABF605CDBA56787F2E0DF8E3840680D0
      1EA403BB957D6AD739740374A6565BD6ACA2490CCCAB329A080D01A03FC07F9F
      FFA0FFA1EA8F40680D01E617DA29196802C8F7CC9EEE0656233E95BB91F351DD
      964F01932034077CC03B7F3C7D95E67E4F8DBAF80C6A37F176A4E80D01A93A03
      40680D01DC8E3A964A7024EEB3664B4E256E7ED3AD63E62D17D2E7456D080D01
      A03FC07F9FFFA0FFA1FFA2EA9740680D01FE37A7CF3B9E2C152263CD1F073BCE
      C49F8B99FF047CCFEDFC0649F680627F01D1203407200310F680EBBCEC475B39
      33AF5439C67949B9ACE0A49EB527527406A4E80D01A03D9FB071F5623ADBC4CD
      0FDD192F6AEDA66D40427F92DA0D0D4D4EB5880D01A03FC07F9FFFA0FFA1FFA2
      FF47FD0D53E80D01F4401C5E7A44D09A41A79347624CE79ABBD1DF01CFF357E0
      DC0072BEF01D29C03EEA509B72F01FBCA00624FF92D94062D018DE03ED5603A0
      24294EB9C3B3F3C25F2FFE93F4EE1AE36A76A4EA4E80FD724E80D01A03D2A039
      8AA764B59BFD83C8FBFA743D7015C9449C6520CFD95A63649E3A0340680C2030
      9FE241C487FAF7CB7CF3F70BFB9D77254FA034063980E9F4BCDCB17FB2FDD1BC
      F0FD1BEED9F80EEBE0398FCD5E101C784D7E10184DF800E86501F80806685003
      18201D104D3FC807FE7C80FF001CA800E9AB9E73B58F6EEA570165EB5F3B9D9B
      293A93A032612740680D01F0681E61C1963C6CAFE195301AAB1B03EAB077BFA3
      3AD01A034065E2B120E087D4AD5FD8AD70C0023EA9F40680CAF0193A824A5CB7
      77CF52F87C4268FE7A3782134F83FF580EAF9661500F7B67840775580E7FD080
      74C680FC7080E68D018D100E40268FEC0318201C7203F86033DE19C33AA2C6DE
      F0BACC72A4C6549D49D019AE93A0340680FFB80E43E0F64FCFC9475CB55D17A1
      6CFDA034077A2317B46245C48FAA7D01A0393C07CCF6EFBA2FD9E1D67E7D93A6
      8F6B7B8E69FC790063FF34F8407F9B3DFC20309B3FACD09A7100F6CE01F90806
      136F880640403A609A7FB00C64DA03C100F15018AA2C5B1B17B4FF61F9660A79
      49D49D01C9E93A0340680DA876A1D01A036A1D01A0340753543D1C97A262B9C2
      68F87BCFAF9E133FCDA2FEBCD1FAFFA68C269F2CD184D3EACD1FFA69EBE68D9A
      7CEC363D27E2D89D095543A93A93A85DB693A0340680DA876A1D01A036A1D01A
      03407CB2A1FA7D93F7E8ECBF1EB84CFEE77A32E8AE2534EE8CDD19F0533F4804
      1FB636A1D49D49D49FD3A4E80D01A036A1DA8740680DA8740680D01D8750F980
      99FF2E3099FE7AFCB3D1D789DCB099F180CDE90A4D3D9E8CE3668DD19A0389FB
      3F8E67C80B6A1D49D49D49EF549D01A03406D43B50E80D01B50E80D01A03306A
      1CBFA0BECC87967A3A43CB31CCF868AE58796F5CD1BA337466CCFD0F50EA4EA4
      EA4ED43A0340680DA8740680D01B50E80D01A03D4AA1CA0B0F2CFA7AC3CB3D30
      99FE69A3AE69DD19BA33667FFF543A93A93A93B50E80D01A036A1D01A03406D4
      3A0340680C54A8736ECC15099FF7099FE41CCFEEF732CD15E59A37466E8CD99F
      2FA875275275276A1D01A03406D43A0340680DA8740680D0191E23C3D933FEFD
      94F41CCFEC07967BA1E59EA5803D59A77467C468CD99FD599F5275275276A1D0
      1A03406D43A0340680DA8740680D01EC6A87E09B77B93FDC3CB3E5ECD10EA099
      FE1B34563F2DC8E9A37466E8CD99F5275275276A1D01A03406D43B3F680D01B5
      0E80D01A03B7D43FB04CF87966399FE90F2CC7A2B159A37466E8CE5F9A3E22A1
      D49D49D49DA8740680D01B50E80D01A036A1D01A034078AA87AA57A267C7E59F
      6F6687A2B89A2F7466E8CE769A7C8D43A93A93A93B50E80D01A036A1D01A0340
      6D43A0340680E9A1393F83B03E67CAFC7653E43CB3E6D9F0D068AE24D1EB9A33
      E89A37466E8CE3099F5275275276A1D01A03406D43A0340680DA8740680D01DE
      6A1C7E59D933E1E59D933F600F85D19BA33FBF4676FA875275275276A1D01A03
      406D43A0340680DA8740680D01D7D43F24267E050B267E0F2CC267C7A2BC7A2F
      DBFA3309A37466E8CE4FA8BAEAA1D49D49D49F0349D01A03406D43B50E80D01B
      50E80D01A03D7150F543CB3EA99FFCEB2CA68E2E8CF1E8BDD19BA33DDA69F91A
      87527527527D7293A0340680DA876A1D01A036A1D01A03407EA6A1F4DDFCCC1C
      CF95ECC15099FF6E7B267EC01E39A37466E8CF02F4677D50EA4EA4EA4FFD5274
      0680D01B50ED43A03406D43A0340680EF4A87A94133FF7547E59FE04CFE3D15C
      3A2F7466E8CC94D19D9B50EA4EA4EA4F8F24E80D01A036A1DA8740680DA87406
      80D01E3FA87D103CFB1F967403CB31E8AFF4345639A3EEF2EE7F346E8CDD19C6
      734798A875275275272A493A0340680DA876A1D01A036A1D01A03407FB2A1F19
      E7DE89A5BE59868ACC068ACC8FCB7C934F93467D3C134671C9A3EC34665BE5FB
      A3338A8F099F1713CB6A1D49D49D49C8D49D01A03406D43B50E80D01B50E80D0
      1A03C32A1E9E133E7C267EDD15D0099F1E8AFD7CD1BA3374674FBA33EF99F171
      F50EA4EA4EA4FA524E80D01A036A1DA8740680DA8740680D01E002E211CCF94D
      01E59C068AE1F2DC268C3CBB7466E8CC9FD19CE04CBEB4CFA93A93A9397549D0
      1A03406D43B50E80D01B50E80D01A03232A1F6E133E7E1D15CD3FE6E068AE7F3
      47A0F2FF80D19C94D1DE34679268DD19FA4CFF7C484CDD2C9543A93A93A9393C
      93A0340680DA876A1D01A036A1D01A03407ABAA1CF140F2CF4F9F7348B457E93
      46E8CDD199FF34784D19D44333FDE399F13099F9445543A93A93A93C8D49D01A
      03406D43B50E80D01B50E80D01A03C5EA1F35BCF334733E3D158FCB3D303CFC1
      E8AF2E8BF57F467EB346E8CC8DD19F3EA2C733F19AD50EA4EA4EA4FAB24E80D0
      1A036A1DA8740680DA8740680D01EE6A87DB3C13463F2CF484CFC3A2BFC82301
      D2668CBF4670F97EE8CDD19DF54787967F4399F97D6A87527527527A0493A034
      0680DA876A1D01A036A1D01A034064ED43FAC267F6FDF4561E59E91CD1868AE6
      8C07EF346E8CDD19F56E8CC733F210CCFF7E4A875275275273F12740680D01B5
      0ED43A03406D43A0340680E61A87CFFF099F0D15FEFCE8AFFA013CB7FAF2EDD1
      9D21A33B668DD19E4A8FDA0A8B90F2AE599FE745543A93A93A93F7493A034068
      0DA876A1D01A036A1D01A034079DD43F1A199FB668EDD15C5CD1FFC19A3BB68C
      F24D1BA332E7467D4F2FFB9A332FA8B24CFCE0545CB543A93A93A93EE493A034
      0680DA876A1D01A036A1D01A034067FD43D3BCCF11FE1347FF6E8AF349A2B24D
      193CBBC2F46726F2FECB4661346E8CE55A33FAF2FE83A330F2CF78933E70133F
      C61AA875275275279112740680D01B50ED43A03406D43A0340680F14179884CF
      9789130CCFFDE8AED01DB346E8CDD19BA3324CFD3CCA67D49D49D49E5224E80D
      01A036A1DA8740680DA8740680D01FC750E1A2BCECBA2BBB45E49A7C73467066
      8DD19BA33EEF4664F2CC933F45E50F915EA1D49D49D49E91A4E80D01A036A1DA
      8740680DA8740680D01F29A87CE099F200EEF2DC3457DE9A37466E8CE98D19FF
      7907A6F4268C6FA87527527527B7893A0340680DA876A1D01A036A1D01A03407
      A6D43E171FE1CE7E884CF800FF7CB78668CEE69DD19BA33C9E5FE374662E24CF
      D5F92F8FA87527527527DF12740680D01B50ED43A03406D43A0340680F81543E
      843347FBE5BE5D17BA33CB346E8CEFB4677797F3868CC267F0C267FA628E2A87
      527527527DF693A0340680DA876A1D01A036A1D01A0340733543F0FC808024CF
      84D1C4F2DF2F9763E68CF2797F6ED19FF4D1BA33D6F467E8133FB51D543A93A9
      3A93CC293A0340680DA876A1D01A036A1D01A03407DB2A1E0E268AE343CB7BA6
      9FE8E8CE1F2FE60E8CE19A37467ECD19DDA2FCDA3327B378350EA4EA4EA4FC75
      2740680D01B50ED43A03406D43A0340680E73A87969A3F4F46679D19BA33FE1A
      33BA67F1FC80499FDE24CFDFE51A87527527527A312740680D01B50ED43A0340
      6D43A0340680EB9A8709A08668F3796F77976E8CDD19BA3324CFEF4333F87181
      33FE8F8543A93A93A93FAE93A0340680DA876A1D01A036A1D01A03407B3D4395
      0267FC8EBA68E29A71C9A374664D19CE668FDDD19C5E5FFD3A330F2CE06DFE84
      CFC854AA1D49D49D49F8F49D01A03406D43B50E80D01B50E80D01A03F9F50FFE
      0087346E8CDD19FE7A33E1D7D19F4F6079662C3CB3BCD43A93A93A93FCE49D01
      A03406D43B50E80D01B50E80D01A03C4EA878A691D19C39A3746744F2FCB7D19
      93CB3E18A67E0F6D2795839A2AA1D49D49D49FF049D01A03406D43B50E80D01B
      50E80D01A03A32A1EB0D19C6F2FDD19BA33B6FA330F2DE299F9E192A1D49D49D
      49C6949D01A03406D43B50E80D01B50E80D01A036A1DD19D1BA33BA68DD198CB
      A3324CFF5FA790B73F4A87527527527CF24E80D01A036A1DA8740680DA874068
      0D01B50E3CE8CF368BE5F46623CBF1F74662668DD19FCDA33E7D00D15FA79349
      33F872DFFD43A93A93A93C3C93A0340680DA876A1D01A036A1D01A03406D43E0
      F97EE8CEED19FCBCBF7467976C6C3CB7D3B1F0133F4F5B543A93A93A93CBA4E8
      0D01A036A1DA8740680DA8740680D01B50EE8CDD19DD346E8CC933FCFC11A67E
      9475BD182A1D49D49D49C752740680D01B50ED43A03406D43A0340680DA87746
      71F346E8CE607467D24F2CFB7C9BC5B5E42D09F099F1C55F50EA4EA4EA4EB8B4
      0680DA876A1D01A036A1D01A03406D43BA331DD19E03CBFCBF467506FA33BBCB
      3C5F34F66384CFC17956D696CE5A87527527FB92740680D01DB603CA7E7F6C0B
      948CA3FC0672AFC5D9CC25BFDCCBB3F680D019F0AC883910F222E447D53E80D0
      1D6E03A572AC077F8337ECD1D63A33099FEBBA33279879BCC78B477FC1356207
      7477B61357768DC5798E76A1346F1F5263C2A4C95167BA43D9BD8F6F3BF8287C
      C36F75A7949D49D49ED349D01A034078C00E3A1B9B92BB4ED1A3ABEE8C7696DB
      E563FA2162D60277246C9A0340680F30CC87F9107221FEBEA3A049CB11C1EA16
      154FA034078200E9B2EE6CEFB0BC785267FF068CF300EB535FA049ABA5E8ECFD
      DE65DC033F9BCCF0D1FDFA43C9E67CF15A432BF33C951FFC15161A2B83F74679
      98B56E21949D49D49F0E49D01A03407A900F7738379D698643A83BDC5D40D43B
      A3AC3EA573A1776B8D77AB279A680D01A03200C87F9107221E445C11F2A6A9F4
      0680F2C01EAAB5C0FFB0F2CF3668473401ABA33C803D5D1DDE030D1F900725A4
      38BCCFCD8DE67E7018690C26CE4B4862A6CC3487100C26BF4126BC365E4D9385
      45FF76FDF9C9F50EF4F852B2DE02527527527CB52740680D01ED003AE415D7D8
      6E04B1FF7CA26C5CC996500AC61254FFAADA0340680C80329FE541CC87991754
      BA0340680CC37F4C84A9ECB172ACBE1B45FA0239A00A768CEB33475DD1FE49AF
      8BCD7C2690CAB48FB633CDFBFCE3CF3663348E1E6DCFE7ECF2D8B391E72E645C
      F30B2B3CFC949D49D49D49D01A0340680CDB1786501F6D963FD62F8F3A349331
      AB3501A0340660198FF320E643D51E80D01A033AD8B95E73ABF99E345CCEF0D1
      BF880EF01E22EC078F9701D52201F36201FE91807F588078DC200F7D2803B7EF
      FC855E03C15E03AAE2B1FDF38AB75BE9A4C5CF24C293A93A93A9399BFF5C5E1B
      9DDD734430024E001CAC003DE78038A5003F564AC5785CB202A20FE3002EE501
      A034079EAF655830D5EA034072A3331FED5EA0340680D01A0340680D01A03406
      80D01A03406FB9A6F73663A50907DCD40680E20CCC7FBEE6A0340680D01A0340
      680D01A0340680D01A03407B5B60E34C5DC19E26C46CBA1216BF8317D23D7647
      1FDB6A2D3ED3A840680D01980663FCC83990F322E08FE0654FA0340680D01A03
      40680D01A0340680D01A03407C306CA5834E3AB1B47F23F8709FCAD1F8C9D19C
      04F66FD8D6A0027E3A0340680CC0331FE641CC87FE66E61A4C59F2E4636654FA
      0340680D01A0340680D01A0340680D01A03407D006E59A6E7015471B4E985D4D
      A4236F30B71F7328D01A03406762B320E087D7EB250C16AD6B047D53E80D01A0
      340680D01A0340680D01A0340680D01F521B2888E9FF37214B0D6B71BD0B67ED
      01A03BF119BDA3322E647D53E80D01A0340680D01A0340680D01A0340680D01B
      50E80D01A036A1D01A0340680D01A0340680D01A0340680D01A036A1DA874068
      0DA8740680D01A0340680D01A0340680D01A0340680D01B50E80D01B50EA9F40
      680D01A0340680D01A0340680D01A0340680DA8740680D01B50E80D01A034068
      0D01A0340680D01A0340680D01B50ED43A03406D43A0340680D01A0340680D01
      A0340680D01A0340680DA8740680DA8754FA0340680D01A0340680D01A034068
      0D01A03406D43A0340680DA8740680D01A0340680D01A0340680D01A0340680D
      A876A1D01A036A1D01A0340680D01A0340680D01A0340680D01A03406D43A034
      06D43AA7D01A0340680D01A0340680D01A0340680D01A036A1D01A03406D43A0
      340680D01A0340680D01A0340680D01A03406D43B50E80D01B50E80D01A03406
      80D01A0340680D01A0340680D01A036A1D01A036A1D53E80D01A0340680D01A0
      340680D01A0340680D01B50E80D01A036A1D01A0340680D01A0340680D01A034
      0680D01A036A1DA8740680DA8740680D01A0340680D01A0340680D01A0340680
      D01B50E80D01B50EA9F40680D01A0340680D01A0340680D01A0340680DA87406
      80D01B50E80D01A0340680D01A0340680D01A0340680D01B50ED43A03406D43A
      0340680D01A0340680D01A0340680D01A0340680DA8740680DA8754FA0340680
      D01A0340680D01A0340680D01A03406D43A0340680DA8740680D01A0340680D0
      1A0340680D01A0340680DA876A1D01A036A1D01A0340680D01A0340680D01A03
      40680D01A03406D43A03406D43AA7D01A0340680D01A0340680D01A0340680D0
      1A036A1D01A03406D43A0340680D01A0340680D01A0340680D01A03406D43B50
      E80D01B50E80D01A0340680D01A0340680D01A0340680D01A036A1D01A036A1D
      53E80D01A0340680D01A0340680D01A0340680D01B50E80D01A036A1D01A0340
      680D01A0340680D01C0380700E01C03AA1EA8780700EA8780700E01C0380700E
      01C0380700E01C0380700E01D50F00E01D50F29F80700E01C0380700E01C0380
      700E01C0380700EA8780700E01D50F00E01C0380700E01C0380700E01C038070
      0E01D50F543C03807543C0380700E01C0380700E01C0380700E01C0380700EA8
      780700EA8794FC0380700E01C0380700E01C0380700E01C03807543C0380700E
      A8780700E01C0380700E01C0380700E01C0380700EA87AA1E01C03AA1E01C038
      0700E01C0380700E01C0380700E01C03807543C03807543CA7E01C0380700E01
      C0380700E01C0380700E01C03AA1E01C03807543C0380700E01C0380700E01C0
      380700E01C03807543D50F00E01D50F00E01C0380700E01C0380700E01C03807
      00E01C03AA1E01C03AA1E53F00E01C0380700E01C0380700E01C0380700E01D5
      0F00E01C03AA1E01C0380700E01C0380700E01C0380700E01C03AA1EA8780700
      EA8780700E01C0380700E01C0380700E01C0380700E01D50F00E01D50F29F807
      00E01C0380700E01C0380700E01C0380700EA8780700E01D50F00E01C0380700
      E01C0380700E01C0380700E01D50F543C03807543C0380700E01C0380700E01C
      0380700E01C0380700EA8780700EA8794FC0380700E01C0380700E01C0380700
      E01C03807543C0380700EA8780700E01C0380700E01C0380700E01C0380700EA
      87AA1E01C03AA1E01C0380700E01C0380700E01C0380700E01C03807543C0380
      7543CA7E01C0380700E01C0380700E01C0380700E01C03AA1E01C03807543C03
      80700E01C0380700E01C0380700E01C03807543D50F00E01D50F00E01C038070
      0E01C0380700E01C0380700E01C03C62F58F1A6CC53290847E2B5EFA0CDC0C93
      E7E2D053F700E019F0ACC83990F3A2E747E53F00E01C0380700E01C0380700E0
      1C0380700E01F7E1B2AF7C725BCD528B5F3F4B01161BF93A3FF5C5498062365B
      5A80E01C03CE3339FE741CE8788370DEB5873FC17E9CD29F80700E01C0380700
      E01C0380700E01C0380700FE10DE014D5DAC065419432D87C51BF40A83E44FB1
      DA1EFC08CFE71FCD380700E019C0673FCE839D0F3A2E08FE9D94FC0380700E01
      C0380700E01C0380700E01C03807850BB794D7D44E60D44A378A7D8BA134FDD6
      DDC9BE8A31DF00E01C03380CE7F9D073A1E745E52F00E01C0380700E01C03807
      00E01C0380700E01FFFB17478C9916D10A5264CBA6040BA4E8C0380700CE0339
      FE741CE87CA3E01C0380700E01C0380700E01C031082D01C3AEF8D2EF7DB0D5F
      31899849D4FF755707A0FEFFF8B2AF3276D2AC87216BD1F76946D2F7CFF90EA0
      BDF6DD8C9DF14D42F7FD94DFFADA0D1C1F1107ECF9FE1EE40D631A18DABE3E0D
      E24FB488A6FF6BB7755C47C249F08D24DE114C2F9118F322861580BC55C0882E
      331037987D9D008B08CF677D9FA69916F8B401081C18BC1A82E1F90EA1E0120E
      848E094826E8CD20A3A0A4853EC639E8F9C93CD215A40B482E81D6E1B3BAB4CF
      E1EE96A44F3A1E397872611031D31510693225187006A0025725A78B5791BD03
      6C0A0CE107E10468E462A88F4B22822AC2443C145135C46F20FEC12265CB64AD
      4CCD542C0088165C1284CB8C2EE4DB8E0C28E5F16039E2A8B03C992A5E20511B
      DCCBFF6BD1F9122CA9827220C20B1A05D1C13ED821A39E82B1356410C85483CC
      75490B02AC01ACB010CFE35087DC9885267536A6834FBC7E07C5390240361380
      B631048071D5B823A7328D0A9CC0E1453E6070A2944FD22452CC272AB3A28C09
      7718F8871DEAD98EA533F343AD55C74D1155E285CEA7622B0FA7C32765BBCE0F
      736D6B49F2C809C7CBEE395DB45A0783EA35F35CE919DC3BC95E34BF73410E5B
      09BD27823B764B5A11C5FB323B18EBB619BD678478D94E1F1D3C154DF10E3753
      B995C26AB89C0247E3E1044AE500AA61FD8258E88CEE3E8870EBB4F0245137DE
      4BABA060FE77D768512023E20B863AD0092576F0107D07081E584280D07FBDD6
      6A785C6E4D8096B5C783CCE95EC47BBD5F6A8BDB727434EA9EE290DCF2A6C7C4
      330ACD33CD5BB1A412D75C33A990B4F8EE9B383FFB92780E9F12DE49271CCF3A
      34128F2BD7A286706270EB3835121C781613A45D02F35377A176A28119B60106
      637D136A7C1A9CD7E176A00C66B07EC07197C7C73899D20616271A14D4653A93
      E945D3260ED74AF402CE1EA748D9FAF1B1C717A8686704805919FB72E7319EB3
      F7811D1320535B8B72D6131CA481110783F6423FB14EAD0B41AB1F99A3E0DF9B
      D554197DA7E5CDAEF03AAA6BD5B73CA9BCBFC71595FF9C2493F80AF1006F91E5
      30F98CE6477F781C117892BE34AF3EECC0F4B8FE315D9ACCC2B33F97C504AED7
      36A7406D243BD045A45DAA144A3713C29088410E84894C48221360F1507A2054
      AA5B2092958B6422D0223A2B710CE7099288675E5CC42D3F50F2D45563BCFE10
      160C03FA7BFDD7A674BD4B680FE0086005B718238180C7AECC17213072ADF978
      1E8BE4055A505852ED916CE108910EF0280502F41AEFB22EFFC23641A44CF692
      CD544A97FC2D2509270C10A108E6FE121690988FE10D03D7389A511DC5039463
      0D574F882A11377500B2BF4EA016D30BA80061EBA8006BC77A303E9643425D10
      8E1596E2C8CA44BE7EA66500C4CAF02A5042DD78FE688816BC0D000F465C444D
      F81544890D7AB4F445BD57D58060F4502D520C27D08D7FA7D0B62643E85B7B9E
      31E85D4A1FA7FE3A3D942D241B3F7B126946BE16D6D584D8A30B23F4F9A40960
      A783D4EC5B8B21A7D998CB68E2750961976DE10A58A12B67B5BD13A3B7E34E58
      D9425A155EFD194E98608F83646E890A03A734567344E689238E7344AE764E6A
      1CDB0CE6A1B8802A6450BF839CD452A834E6B34B6069371CC23E5E3AB83943C6
      D4E656BC727C2EE87317FF9F99A63E21F9DFF09B1DFA35D8D977C326AE089643
      270B340394DE0C61000C11083A152E18D80F1732B33CCF0641C877B0D00E11F1
      5CEEC3CD072E233726D1801E809AADED2CCFA100B6678F403D5116408BC63BC2
      22022D5C1FAB9B5C0197A74CEE90D9F76573DE7E0FB5F43CC7407E676913D316
      3A393A7F056A4969809E6172AE203CC9040E2965BE17FF4E913A84DDC6B8C8AB
      84B313ADC60463E61BB5C26DF405E51F61ACCC77DD510CA32E8F8BF22544AB81
      F2E09DF1D97310163A793524F6A3A5B43458D7D9A8BBDF961C2836FD56CD951B
      1AA5868245B7A16CEF53FC47228D66D34824CD351015D4CE85C73EBBB32A2979
      F289C9717C5D6713723D4FCE4F5C992A3464D7FC743C2E5D170FC9265962A03C
      ED30DF738BD9D41FFF8F875278DCCE35EF2F242633BDFCE670A56B2CEEF0B0E7
      FD29341E1E63B0C3CB86A9FE548D53AA94257552C0E8758ACBEC9C23AEF10DB8
      1EDF53BAFC6E5E0DC7F31917339341C0B626F762FE3E32CED8C7D19DB7365DE0
      2F2D3AABF04BE4EF5F07B988DCA1BE0887BDC42EC187A9CA5822479C86B31A50
      53B57E63CF1E7F720AC65E23BCDA81CD6DC84B928BED991C1BF992DB06345B29
      E20D7DA72C66A7E3D444D4EEF34D57FA82EB4D47D0172D355B128E053E713571
      2B1A998461EE0BC38D0D96EFE626ADE32940F4682C429AFEFCC2C1442197CAF2
      1FA046BBC58CF40BCDE45D3A2D70E72E5E3C6D085CDCFE9CDC6EEB7B31C8FBF7
      A0CCCB8ECD4E2EA693472BA3EC9287BD25AF852D6538015BED64F27DC81FFE5F
      EDBF056FB903C7F71C8B60207A7B2ECFC7DC60DE41836582397F303531895D3F
      E03B294EA5B3A391D3ACEDF7679DC8D67B2E3764D3C28C52192913EEB3A0D7FB
      CE7263B376FB96DC1F98F093FE31D4174D92DF77977DA2283386F85C48872CEB
      9663FB5F901E6EAE0C4FF70C0183FD1B9830E1D824366AF2380DE37E67B7B2A2
      391107BCDFC9DDD38F544E9E7E563CFFA1E69BDE6279BA8304F3B7061D3CE3B7
      5CA79E1ED55C4F3DFA9E73679FE4279E53CCF17F9E79F89279CD9E742279EBAB
      9CC127983079CC4B16597FCB3367CF18D6F6D6B97FA13C55CBE62E58E0FEB079
      F8F3CC0C455B3D6E1C8CFF58A23B1B0528302CA995054CB3B2AA447DC5979D53
      A956E63DC5F2FC9C5D218DB23B7D0F4F6EDFAB81E2AC01E5FC9FABED82B466D7
      5C54B51D860D7FDB2B3F8BDB64C33137190FF9837CCCA4E78867AE7DC5EC18EA
      FF5EC77997AF1FF77DFF7F93874748BCC1087284206A74C77092164EC6FB6777
      9B477714CFF8F9C073F5DBB49C0E89BBEB4F3995A884CD735F5BFB3CE3EFAD2F
      1067746B3D07DD8D511A06D65693C7DEDD7CBA1EDB4095EDC93D0FEC2705352E
      C72DBC35A82CEC057E62D539E6E6E10B0BCF9F46047D3E8D10F803A90B4542D6
      50BF379AF4867DAB1DE4D30BE0A42F03155AB18FE1D7AD58E17D9B756389FAF6
      E0DEBB56221098993DD6E9443DF30C76C126F1021B89DC708E04FD5082628720
      3F4CA3C8254ADBC97B5E1D89E29C4D7B69F1C1A061B4FC9DD8967B4F35767C33
      EB3CD846A774BA2E9E3B40A92882D06FFEB93953A6CF5635C7F44648F0FEB3C9
      0532150398DCB40207E9945E16CE8B229393EAE7E971DA5A78E58F3E6B6C3A95
      C514B4F0EB858BD00E175C629CE8FADB768ABD1FAF34A3554AA08AD71F1D683F
      B42D508BE430AA11A74D6A3D18675947B07DD1F05DC76A79FEC822E01185F0D8
      204240C1F06F5AA584395D52C20DD63A9CF8873FE21D4991639D9A3990613A86
      A3634FF13C6083B0A76AE078B03B00ECCC130893BF00B0D4AC7BED355441C3BF
      CEFBF8C773C5B0C70F9B18C3982D3359AC6784E4914F9869E9DA3E104FF0952A
      3C29849ACBF62C6AB31FF0C29B07C78DAB6EC0E4906A7236582AE009C93EC022
      10E0BE2412C1414E13A826150D3846C8C3977C37230E59263230F0F5F930F906
      1C889CB735F82EE0927040CE22207EE3C731B2F73F15572932AFB8C954B3635E
      10C7E09D3CF538806CC518A2039181282F918078651F71F7728165A7C5F54612
      F0FA0EF2E16F161EEFE819082AB18A03B61A4BB7E3F0AE596252846798AFFA23
      24E0A5AE81A102AC920AE058882A8D25D401E63CCECD667859C4651CAAA0C6DF
      56D3FC1F9FD179444FD6D6D97F735A8D545D5B519B9A2B9238AB04A9396AA39E
      30E55CB3068A22763D67D5D5CEE3CBF3237388EF936EE3AEA89C36554825D6F2
      594583E25F52D3AC0FEE1BD7CFD56191CE96D0A41995D373AC3A06ECB8EF0FA4
      7B0473B8F04828EBD3857B2D9B5173581A6E1BB6CB97E4E747E7074FDF73B912
      2624B436CFE1417A384197FA8431F05A8285BA7DD4A7F9794F5729074CD3FC12
      2F9DF75CCF757C15A20EC527EA0CB06FBB3F0C9381120D504E7BDD4936BF4C55
      1F7C285858A5C94BFF78A86E5B88D9FFA0433C4675BEB44661B881FF1B10A0AC
      81FDA11FFB49587AF75BE58BBAB7455972EE214B239E48BF9CBC9F986F472B35
      D870DD6EE9BD5969EF710FD38A69621FA4591310F947BD38A9F79EF6554FEC61
      9B26A9F31DBD66EDDCEDB864D7A8EFA88477178963A255041C28CD9B92A303CC
      DC9682EEB373DA1D8B9B920875519BA9F00955EDD4363FE6A0C055319FE11207
      5DF5617FB0E8C7962481E3CC0BC0DB1F5A7B760F81B71249A37CF4CFB81E048F
      015E727B8883127389197E6317F97058C2C61630B1858C2C63DD931BB0121D3E
      7F8FEA6A2E423B6A8775BDFA54A1AE29221CA53F5EF94BA9BB661F85FF079EF2
      10E35B062B2CDD9D59C5F93730B8CBE12312E1C24D465C57DF42C61D99852FFC
      3FC5B9E297477B1BC506EEC6BC07B45845275662E5FD44ED7B5D386E5B6DBB13
      725292C8EB5F306E40A9AEFFE091433A28DC2F4D044228A91ECF31D1104B500B
      39B952E9DE0351FC7F4751395B25EABB0829DE772BF4BE0FAC73446311043D90
      43558E9AB434175E979A5D4FFC1156DCEB118C11C6A8E28E28E4B1A963B67497
      D16179EF2FA57C2EDE40DC5FA51ECFE2FB295693EBF6DC59BD40BD11FAFB2D2F
      4A2A38FDE5FE6D17D9C0D2C196048B6C0AD2F4A05E45BCDD4C6FE46860A0CA4E
      84EAEE9FB01A721A864769E1EF97E35A9668A1DF43BD7EBA9E2F63F43092CAC6
      1672BC01B467A3C41F1B3B54772653B95592D2912179FEE1B2AD52ABBC9BFC0E
      F697F87ABC6AC64199E36BA8FB4579E6ECD14DAE2D2BC821C6684ABA3270FE0E
      F8BEDA9572FDAFE9302E57C3FB890921B9AF8DD579249D30D5D116D34225FCB9
      2234030BF9DAA85F2A500C2FE8EBB154D500CF4DD1F5FF0FBE5830F4E2FD885F
      CFFC25F0C27392B712D0D7C9915921AA616D93FA7C6E5C1DD0C8BA04A6FFBC1D
      7C989FD709126619B07E8D4565EBFF9F547260DFA900BDF8393FD9D8844BADD8
      C47EFE20EA28392889B7F71C9C861D97939E048F60B895903AF53DE8761E8F76
      4F38A4722107961A3D60B94AC66A960A9BCA26207FDD1DD2B2CAC53D260D7118
      4C90087C69E8C63017E57D1D5ECDA2B4AA122DADC8A50D38B8BAB1A730039424
      31F26D00050849ADB774BB5AC9FD86343966BBE4505EAD3E376F3DD1DA3BF5EE
      0F95107BB7EA9F1303A8D7397BF7807D0F41A7B056AFD86AF923E0211D59973F
      CEBBF36CFDEF11A9A52A1AF6AF4D68F2B8228D478F6B9484DE579F45D86A8C9B
      7AEE55108FDB2EC44E1ADBF7DBA75011FECA1E24F5527EB4D0EF3B1EFAA8C05E
      F709CFECB1FC376163D787E4E4B09D8B3F12F6AC656D157887F872AF29EC18FC
      F25FE5E5280FDED42A3F826532B2CF7000E7725CCE907CB29920365851A99C1A
      027CBF3A3C7CB63695C8AE1FD40C006E0B10F9043D272BDA0BDFA57AD202A6FF
      98235BF1971D4CB18547A3D5FCB189DBB1E6532DB29FA091F117E726F8C7FA1B
      B2D0C6526E73A80510261C0C0212AE09F985CD6592C547B8361BB0A5285D4B01
      6307DF768817EE138E0E209D1EF151A032D590307D2595335D50C18F3E0C1CB6
      742860DF92E6649982567394C1CB3A9AB30AA1EFB84E181ECFE8C0C9A18B1806
      A0B7B1386017F3E0FA82A80CCC96B9A2FC1CC38A009928BC5FDAC245F7194D84
      F18990BE94214AB52E1C9B24C508495CC5067E61A70C534B57B9F2EFF3005893
      FBFFE6A101FD7205DF1C807D37FAA66179F8DFB487F41274DF40AE38E3F90041
      6F0FFC166F2FFEF8B9FF85A3C6FC7F2EE7FBC7D17F99EBF4FFC04053FCF3919F
      FDE34E482625FEA80EDFF335B01FF4080CFE7EE84FF07B9E9F85E48EFF028FD0
      0658AFA41D079D506F887E48162207620814077A3B76075480FE03BDE006B6E6
      07C78EB035CE7240F7833A01BBA3D006F516076EFE682D0FF482BC39807C980C
      0F277BE4079C5FA506D0EE908AB3D018C498057FC108FFC383C346D9907F020B
      D84D7806E0FF413937BB0C0ADFD87E6F7AC362DBAC903E60E89E7FC4131F080D
      7E7083D9E60DA60FC867725720D6C3F9E0F345FAC1DB148590BEB8377931C879
      E8427222E3C1C735B3841DE1B90EE341B8FEC683A63FFF926F390FDE9D504078
      DF87077EE05B314EB8B722DFF0AAEF75C162101F76FDE021DE0856DA9E68B03C
      9A815BC8170FD80BEE76322C03B9B901F622DAF4598581D8762EC39AE206ECC1
      15A768015CC5E6E2DBD9F848064079819C887B08B5BAB81161720CE445E90559
      313A8AEBD338100BE9C8B5B93441EB1E119F05181EC7F266381056A22AF9E3C3
      B1C1106EDC3BB20180F59B108E2A40BA991E6781EBFF6643DA3411249E324379
      A0C1B23FE3E536A08F940A243E937B58C0D94C87F01B58181EEC18FE02A663DF
      43924233D1E2F2CD48178B201FF87BDE9D0FB3EC00ED08F3DF47DC40FE80EC3B
      410C213B3B0201DAC80E181A312203F7D8D98C28A13C7B615CDE9A0040080100
      20040080289F940280500A0140280500A01D00032A2B40790074A4DCAB7E5467
      981FD301CA95E54D75477CB0AF2C25CB08F1300E58779620D4ED4ED4ED4EF549
      D9605D01E180F212F2C33CB10FECCB1CB14F795972CC5CB28F2CA1FA603965BE
      40753B53B53B53BAA4EE5AD34079407C4E39700E5A93BB53BCF5465AB396CDF8
      A6F2E25CB5579E9FCB527EC6032D59CB5972D9BA9DA9DA9DA9DEAC9D81501C40
      0F8A772F01F3E45A9D92772E95F14EE7E1DCF8FF9F19F3E3FCA1019726E6379F
      44D4ED4ED4ED4EEF29DCFD5FCA030F88AA2CC074A17AA88EAD3BC60D3FD98CFE
      2C9FCCB7B1042C6404EE7F1BCFCBB9F91F3F22F89273F25EAC07C493988E7E57
      CC2F3F44D4EFDA4EFC17EFD7129DFEA49EC7664EF9187FEA77C847804EE7FB1E
      601F24EE68DD406C081FAD4EF31273F9BE603A84DF987EBDBB9FBE10CFE77E7F
      49E831E7F51F301CFEABC307E7F5DCC2F3FD1D4ED4ED4ED4EF169DF341FBB660
      3D24BD542FD007215759257528FCC272417F693BE6093C1DEFBE923E0E2F9880
      7261F04AFD110B4F865703A02E41BE005C807B7D6C56623E5AF0794ED4ED4ED4
      EE493BE409FFC980E25D9FDC0FCB00C1F8E3AA4FF529BD8277B007CC1E1103F6
      A4FFE0EAB2565A403D29FEC81FD00785C584661FBED90AD4ED4EE593B8781C32
      53BAE13BF98380A6007A497E89BFD42FAEEB6F6649D7B5B7B10A7A5AEBDD0F04
      93BDECAFAE2407C024EB44771C7CBE62B92DFDBD4ED4ED4ED4EE593B397F0C65
      80F526E1D15C51CD3FBD37B04EFC03C24788794F24ACA21CBCA1FC700E2E35FD
      E1FC6F3F57313D8C579953B53BA378653BEA76A77A93BFFF410A9007C68E7FD0
      BD8E62A4EF920E4877A032A2AEF8F8F40F2D71A03E5473AD11F07C6E998AF8E4
      FEB53B53BA1B9C680D4ED4ECC60CA5300F5A5FCE07F6806E77CCA9D9BECF41EE
      06CB8BF2F938F5604BF338A03D7D0FEF03FA00F721A4D987F2697EFE65A9DFB3
      A771ADAFA02ECF9DF53BB24EEADC766FC07AD27B685FA00C00DDED2F6E40E699
      8E3DE19FB10618F850431C94E866038EB878A13B50B9FB382D037BB09EFFB5A9
      DF8E9DFD13BFD5BBEB6F6A76022760383963807AD1FAD0D8F43F76FB0EF59EFF
      2A7602B330DF79BC46FBCAB420182307983EB807601F07C131DF698EDB7BE62D
      4EFBFA778CBD3F287D4EC064EFEF56B240072C5F1E17BF88EDDBBC72E39EAB66
      E7AE7DF201D08B153BFCD4DC376F1EA9AC3DE501F524FA83FD403EA49CC476A7
      F9740981B8E253B1489DDFABD6FBC9B9EB9F7C21C879ED93BEA619CD3BEC23F5
      2760FC32D01C880E43E699F807EC4EC2EE11F60461AD70BF0D6C1410DB018B75
      E26837FCF1DD53B53B53B53BA64EF1F8EF501C900FE78FE2051CE94A772E38F9
      FD5F9647C830DE161666B4D7535A8B8C41B01DD451BFE8DFF53B53B53B53BA64
      EFA0EEAA039201F624E7B33DF059A775DFDC4EF7A1CFFA01F591FA27F763860C
      3A407C64032567303ED23D4AD4ED4ED4ED4EE9E4EE69FFDBAEED44A031E64CDE
      7A5E77809BF725670ECE31801CC6F6445A95FEE4EEE8A76A77B9A7680500A014
      0280500A01403EE77E21A95A7739B4005735C4630EA207F34417B9A201F206CD
      F40BD3D063943FA0939BFA056B1A4F678799E9D67D6D9F8DB3F9A79E1690C73B
      3D0DE2221E556B09A6BA678F5DE072F936DA18DCA250A6817E2460DBBE8609C5
      486C8ACF431F62BDF372C47D8DCE261420B001BEE77FE8DF98B5C0DD4103F8C3
      79F4EC2CF723BC7BCE37C439E98E5FC49BFFD44B4E72E9CAD2D7ACF373DA4D67
      F54CFCB51B87492F344B61B39499CC2E6542E12FF099FE2B3F3869FC0D3D29D9
      8104E9F76D3DF5A7B434DA4CEF02E26B78AF786E49D7B0DBE2355D3D4DC1E10F
      CDC95E0D09171B36C405A0C130A004775435E85490DB3100D24DE4DA77A69BF1
      A68D77E71ED36EC90007ACCC588F86407DDCDFBC87786D338B9DE7E1E9FD6DA5
      5F907FD9F9C1F3A06A0407E99F7FF9743E4F742694F3A6E679C39EFAB7223F07
      5FE8403203D8604443D90FBBD4C61F3BC108222F2E7AEA8C34F81FCA81C3E2CD
      62E1A48CFA7194B0F26E26F4455F54F95719FD7C6DC12D4087F5B4EBA4DC3215
      D30C3AE976201C9279A6BAA530CE0B3F444550FD019193F344DB0E1EE66C7F81
      1B29C4F76C8FF02D343E99FC8211C11F3382AA40BC2100C30FA62469FBBF7FCF
      72479E9B5FD207F2CF6DDC275C36D3543D74DB4AADA6AD75C5E963168F4D0020
      040080100200400D38A0140280500A01402804101FCB3E58D9F34781ACD2ADFA
      519EA07F44074A57A535D99DF4C2BD30974C23D6203A61DE988353B53B53B53B
      CBA774F9D501FC80F5797B4C33D310E04D639629FDFE88D33174CA3D3287EE80
      E996FA01D4ED4ED4ED4EED53BA6B4D01EA01F538E9C03A6A4EF64EF5D5196ACE
      9B3799F829712E9AABD74FE5A9391A032D59D35974D9BA9DA9DA9DA9DE749D88
      101C980FB1CF4F01F6E45A9D92774E95F64EE9E1DD38FFB719F6E3FCEB019726
      E837A744D4ED4ED4ED4EFDE9DD3D5FD40308846A8FCA03B01DDBA7614927749D
      D3E37A79774F23E9E458CC73D3CBBA795F503D7D1353B53B53B53BFAA775FD8F
      580EA89D804FF003FC09C209DF6F9AFB80FF78DB0AA766F144F6F9DFC7D2752F
      EBF4DD880EBF55EB07F813D7EBBA83EBFA3A9DA9DA9DA9DFF13BE688009301D4
      93B209FFC06082782A772695D53F8D5062C4EF9C5E3CF90094A3F1797EC40391
      6CB3F8EE1180301905F601DF04FC09F77AD92D81FC9630EA9DA9DA9DA9DCE277
      D230193403D9D1C704C1C061227EE13B9F71CF7C37A87CF207EECFC047A6DC59
      B64EEA078280FC09F3FCD1AD81F8FCE3429DA9DA9DA9DCF277F10D073603AA27
      68130A018689D4EC409DC54816BAF9E7C324EF7771F7D93BA81E1C03F027B2F4
      66B607D0756E2A76A76A76A773C9D98C78C9B01D513B4098A00C5C4EA770F4EF
      BC6C34F9F0B4F24DCA33D265BBFA81E2E03F027D4F473F607DD46FBA53B53B53
      B53B9F4EFFBD0567807544EE02634031C13A9DC7B1C7D30D3C7C80F999577C5C
      F9F0B6DD913A81E6003F027E306C0EC0FE4A37DD4ED4ED4ED4EE853B19873F40
      03AA2772131E018F89D4EE649D98EAF43EF895DCBF2BA31F52404BB7947503CE
      007E44FBF1933D81FAB79F9F32D4ED4ED4ED4EF527736CBAA800639FC377A3B7
      EC39E7D86339B78E6EE838E621844E6687B1E11CF5179BBB91D80FC523F3FB5A
      9DA9DA9DA9DEA4EC171F35100E0839EFC76A773F4EC152E79FF33F84FF9AB7C4
      030CD91D45E1F96A3BED39ABFFACC5A9DA9DA9DA9DEA4EFF33E969007E39271A
      3B53BA6277FBADB87EAE9CF8597C4A0388093A8CEE0DF36C370767C2A76A76A7
      6A77409D87EDD74074803B9C71FBA03314EC46E11F60C642E333F61722425380
      18FF666EC37FDEDDD53B53B53B53BA04EF47A72501D300FEB8FE2451CE92A772
      D75FAFA5F4C8FAC6C3B2CAD616D9E70B83032B7203BD3E37FF1BFCA76A76A76A
      77469DF219D34076003F249D569F007553BB30FC4EF8A30DBB01F511FBA7F7C6
      B01670407D82032567503EA23D4AD4ED4ED4ED4EE934EEA9F8374FCD884066AC
      8C3D132D06C937F24AD61D9CE2A03A8DEF88B52BFD89DD994ED4EF6C4ED00A01
      40280500A01402804D1DF8627167E990DC01487184203F901FC3205FD64807D3
      C343DC2F41DBBE50FEE24EDBEC1599F93DAE1E6BA75AF1C6BE36D7E69EB3FA18
      19EBF58778D89955ACC6FAE9AE3D78843B45E24C5439C6E6206F205602F6BD60
      13868A1A34BED03DC3FC90E78C078760F08902FFF804987F28061FECF0E9101F
      BE63D5BF616BB91E239F543E228EA06722421FDEC33BF3977E5696BD6B9B9F12
      6B5FAA6BE5A8DC6A2979A25B0DACA4D66175EFB8493F84D7F15AF9C35FE06BE9
      4EB1E08F7FBB6BEFAD7D9DAED2693A96C860C77BD87353BF10E414C56FEA6E5F
      54E10E6B586448B8E434CF6DB00B967A23BB143284A730D79A190A6F37DFBCB5
      DF8D746BC944FC436A7A07852EB6C90C6103ED6C7F443BC86F9C5D7F5F7F7DE2
      D9E3FCB0FFA4A3C35E2294407F543903F130D1EB89DE9E77DCCEF0B864E52470
      E5F700100C80FA28C910F6586D74F2B0D9F9C7488BCA429EC32E8648858202F2
      E6E7F0D2467D78CA5879372D8E22AFA70D1C3DBEFE36E516A045FB6937D2AE51
      0AEB861E34BB3808493CFB8D529D6989C3B672C70EE138FC3A276E7019894387
      C09DD203BB37B1F81F9C473D6EC108CF23A3BB64205E14806271CF427A3D5FFF
      D1C023CF3D613207F92379D43B8C36D7543E34DB5AADAEAD78C5E96316AF4D00
      20040080100200400D38A0140280500A01402804401FDEFE58DBF3475BACD2AD
      FAD19F007F40075A57AD35DB9DF5C2BD70975C23D8203AE1DEB88353B53B53B5
      3BC7A775F9D501FD80F5797B5C33D710E779639629CD3D11AE62EB947AE50FE3
      01D72DF503A9DA9DA9DA9DDDA775D69A03D403F071D78075D49DD89DF1AA3250
      76D9BCAFC14B8976D55F1A7F2D49E4C0196ACEDACBB6CDD4ED4ED4ED4EF494EC
      7880E9407E0E7B780FC722D4EC93BB74AFC2776F0EEDC7FE38CFC71FEB300CB9
      3761BDBA26A76A76A76A76689DDBD5FD4030783A91BA4077E2BC001DEA762524
      EB24EEDF1BDBCBBB791F6F22C6E73DBCABC101DBCAFB05EDE89A9DA9DA9DA9D9
      AA776FD8F500CC049E20AF1806A77474ECD658D71F3BF8FA4CD873DBEB3C701D
      BEBBB0DEDFA3A9DA9DA9DA9D9CA77D12C274C03389278E2BC901A9DD253BE81F
      A08E80CDC63CDF402201CB84E24F9F4B2FDC00F6BA995E02FCD791129DA9DA9D
      A9DD1277DE3E2880EA006E38E9E9DF20A7823B240FD9A302E34DCAB969801ED7
      DA6DC0DF2FD27653B53B53B53BA24EFE608F101D400D4EC309DCB5096BAFD31C
      424EF67A007AF40751F7CA7037A4F5D794ED4ED4ED4EE893B33972C80EAC06A7
      75C4EFACC45A3BB037624DCA21D78A1FE1301EF7BFA7C0DF121FE229DA9DA9DA
      9DD1A77FDEC3280EB406A7773C71F7C1D24740627455DF0E8EC415EB9780F9C7
      71FC0DF9A9E0AA76A76A76A77469D8AC5A54075E0353BBFA7667AFDA3C82DA52
      FCAEA71D6A12FCCF3380F926277C0DF7F5DF5E65A95A9DA9DA9DEA4EE5D57620
      3B801A9DE1FCCB15CDB9135882D1330A351334FE7F3603378943C0DF928BEBF6
      B52B53B53B53BD49D84E7E6407780353BC5A76129B5E2035F6A2037F18807DDF
      80062FA2C3BEE89AC6FACC5A95A9DA9DA9DEA4EC06B53203BE01A9DE353B03B7
      AF1EBEFD68D7FC9C203BC37CDC0EC2D66D29DA9DA9DA9DD22762FB77D01E100D
      4EF369D8DD923EC30A8DEE8018E08E0C0800CEBAF4381B81EDDA53B53B53B53B
      A54EF47BF4101E180FEF8FE2E49DD754EE4C71FE7ABF6C8FC8547B2BEEA37996
      51C03A3A0D007847C6E0897FA9DA9DA9DA9DD3277B26B5901E180FC12768EFBA
      4964EEF32893BE59A33F007D447F09FE11E44B40081FAB00C559D80FA88F52B5
      3B53B53B53BB8D3BB27E59D56FA06019DB031F6CC30FBA4DFC12B6877018680E
      C37CC22D4AC0C4EEA4A76A77ADA7680500A0140280500A0140270EFC2D36AAF5
      B8F000A537725232C40FDC082CCA8407DCCB44E02F4B812441FE049DF7D82B23
      F27B6C3CDB4EB6E38DBC6DB7CD3E50B51235DDEF2ED31251AB5A2E35D36C7AF2
      948FC9B8F0B2E632A391ACF2A917DAF7906DBA996953FC4A12FFED4BA268B25E
      4665220BFEE4605AFB64607BAE4BAA207EDA4F7F1D85B6E4794F3F72F78C3C91
      BCF9B97FFC9571CE5E395A3AF5B66E7BC9ADBEA9B796A4F0AB22E688EC36D293
      6985E4D66361FE137FC56FE70DFF81BFA53AE7E55E3EEDBFBEB7F676FB49ADF6
      D7A58F1BF225CB6706973946C38F5373FDA7C97360888245C6A5B1096A906D08
      411DDBA59E534096BD21785379FF1DE5BEFC6FA35E6AD03CB72881195472AB52
      C2103EE72C284779BE338BC9F1F871BC5ACDAFCE40AC2C72E489B4081FF92E63
      1C6CB4FC22F8A79E3733AE5A59FBB984BA7FA16203103EFE72243D8A5B9D5CE4
      B90067A445E365576C9FCB44C6D721DA3403D869233EFC652C3C999BCD9157D7
      969E67538D3EE916A0477C72B8D26E910AEF861E34BB4FFD249E8DC6A9607646
      65E9556F97A0D4CA5C5556B90FCCA64BDC630521FE3D89FB9BD06796DF948467
      F3D4D9C0102ECE4031B9E5AB5D3EB05FF3C123CF2DB36207F9A76DD63F9C36DB
      543DF4DB6AADBEAD79C5E96316EF4D0020040080100200400D38A0140280500A
      01402804EA1FF65E58D7734775ACD2ADFBD19F207F38077A57BD35D71DF7C2BD
      F0977C23E1803BE1DEF88353B53B53B53BB7A777F9D501FE00F4F97B7C33DF10
      F2E1639629CDBD11BE62EF947BE50FE401DF2DF703A9DA9DA9DA9DD9A777D69A
      03D203EE71DF8077D49D989DF3AA3250F7D9BC4FC14B8977D55F3A7F1D499380
      31D59DF5977D9BA9DA9DA9DA9DE809D9B901DA00FB1CF7F01F9E45A9D8A777E9
      5F64EEFE1DDF8FFCF19F9E3FED4018F26EE37BF44D4ED4ED4ED4ECD13BBFABE8
      0F480D4EF349DDFE37BF9778F23F1E45FD603C795F81BE3D1353B53B53B53B39
      4EF1FB1EA0184C2964DBE03C815E500EC53B3C92798A76634067AF9DFBFD2701
      39F1F59E580F1F5DE06F8FD1D4ED4ED4ED4ECE53BE39A35B801E123CF09C1C2F
      A807314EF79AE3F364FCE4EF94DAAE5AC71929720187880E3DC927D1C5A17E40
      76417101EE76B39C88F872A2453B53B53B53BA54EFC4DC6AE807CBB33D09CF82
      FA807F02775D0B9FA77C057EB96F883F6EAA15954782D5D277803E780EDC2FBE
      0F45E83FA1F33DA9DA9DA9DA9DD3A77ED19019003C247A413D417EF49F1023B6
      53BD3AF7A84EE7AC075D7F595C0A77B9E00FE8C7C01F5C07DC2FDEBEFC88E9C2
      F33D08EB3F55753B53B53B53BAE4ECC68CF6480F08BED84F742F7A23639E813B
      E537189714665053718877EA94116C80FBC03D817D823E4F9727A09DFCDBC653
      B53B53B53BAE4EFF3D877300744D19EC11F08DF00375B7A24EFD236B65A83D40
      2AEF8E4B5E32D02E12780FF911F905F31D65FA09EF5A44D4ED4ED4ED4EED53B2
      5795B9803A6A77B447C637C00D4EF609D98E4F696F1570C7F2BB4CF52C0E8537
      49F01FF223F20BDFA196F413ECED7D7996A76A76A76A77B53B8B4FCA5380F09D
      C40DDFB0F729D92E0DD999BF2C99A8F2E99AB3F3E8A03EAE7CE0ACCE5597413E
      2B4FAFDAD4ED4ED4ED4EF6A761797832301D623FC48DE889DD33ECF8C4EC2D46
      BCC16CE9CC17665880F9F40076639FD0ACAF36077DF3359FF798B53B53B53B53
      BDA9D83D5A9CA4067E127CC0DD4EF849D84D9D79EFF96B4EB7DE8C077C39FD0B
      EC8CF0742B0D53A8A76A76A76A77749D95E0EF407A0036BAF8BF6195D813EC31
      29D965849DC97206980CEF7F3BA1B85E0D853B53B53B53BBA4EF3BCB5101E900
      FEF8FE5638E3BEA9DC18E309E780647E44A7BF63633B8B6D9DD9F41C5203BD38
      370A4FF14ED4ED4ED4EEF13BCE37AE80F4C07E893C6B7F74B309DE5AB9277BF4
      255E80FB48FE93FBF879988B103FDA018AB3C01F691EA56A76A76A76A77A4277
      84FCF366D51A6033E5627F48A59B689BFA25710EE36F401E06FC045A959089DD
      114ED4EF644ED00A0140280500A014028063DDF8}
  end
  object Timer5: TTimer
    Interval = 3000
    OnTimer = Timer5Timer
    Left = 143
    Top = 41
  end
  object ColorDialog1: TColorDialog
    Left = 323
    Top = 233
  end
  object IdUDPServer1: TIdUDPServer
    Bindings = <>
    DefaultPort = 0
    OnUDPRead = IdUDPServer1UDPRead
    Left = 348
    Top = 74
  end
  object Timer6: TTimer
    Enabled = False
    OnTimer = Timer6Timer
    Left = 180
    Top = 46
  end
  object IdUDPServer2: TIdUDPServer
    Bindings = <>
    DefaultPort = 0
    OnUDPRead = IdUDPServer2UDPRead
    Left = 355
    Top = 111
  end
  object MainMenu1: TMainMenu
    Left = 197
    Top = 110
    object N29: TMenuItem
      Caption = #36890#20449#31471#21475
      OnClick = N29Click
      object N33: TMenuItem
        Caption = #21047#26032#20018#21475#21015#34920
        OnClick = N33Click
      end
      object N59: TMenuItem
        Caption = #21253#38388#38548#26102#38388
        object N1ms3: TMenuItem
          Caption = '1ms'
          OnClick = N1ms3Click
        end
        object N3ms3: TMenuItem
          Caption = '3ms'
          OnClick = N3ms3Click
        end
        object N6ms3: TMenuItem
          Caption = '6ms'
          OnClick = N6ms3Click
        end
        object N9ms3: TMenuItem
          Caption = '9ms'
          OnClick = N9ms3Click
        end
        object N20ms3: TMenuItem
          Caption = '20ms'
          OnClick = N20ms3Click
        end
        object N50ms3: TMenuItem
          Caption = '50ms'
          OnClick = N50ms3Click
        end
        object N100ms3: TMenuItem
          Caption = '100ms'
          OnClick = N100ms3Click
        end
      end
    end
    object N30: TMenuItem
      Caption = #26174#31034
      OnClick = N30Click
      object N34: TMenuItem
        Caption = #23383#20307'/'#22823#23567
        OnClick = N34Click
      end
      object N35: TMenuItem
        Caption = #23383#20307#39068#33394
        OnClick = N35Click
      end
      object N36: TMenuItem
        Caption = #32972#26223#39068#33394
        OnClick = N36Click
      end
      object N37: TMenuItem
        Caption = #36824#21407#23383#20307#35774#32622
        OnClick = N37Click
      end
      object N38: TMenuItem
        Caption = #26085#24535#21487#35835#20889#27169#24335
        object N39: TMenuItem
          Caption = #21482#35835
          Checked = True
          OnClick = N39Click
        end
        object N40: TMenuItem
          Caption = #21487#20889
          OnClick = N40Click
        end
      end
      object N41: TMenuItem
        Caption = #26085#24535#27700#24179#28378#21160#26465
        object N42: TMenuItem
          Caption = #20851#38381
          Checked = True
          OnClick = N42Click
        end
        object N43: TMenuItem
          Caption = #24320#21551
          OnClick = N43Click
        end
      end
      object N45: TMenuItem
        Caption = #25442#34892#27169#24335
        object WindosCRLF1: TMenuItem
          Caption = 'Windos('#22238#36710'+'#25442#34892'CR/LF'#34920#31034#19979#19968#34892')'
          Checked = True
          OnClick = WindosCRLF1Click
        end
        object UNIXLinuxLF1: TMenuItem
          Caption = 'UNIX/Linux('#25442#34892#31526'LF'#34920#31034#19979#19968#34892')'
          OnClick = UNIXLinuxLF1Click
        end
        object MACOSCR1: TMenuItem
          Caption = 'MAC OS('#22238#36710#31526'CR'#34920#31034#19979#19968#34892')'
          OnClick = MACOSCR1Click
        end
      end
      object N66: TMenuItem
        Caption = #26174#31034#32531#20914#19978#38480
        object N1M2: TMenuItem
          Caption = '1M'
          RadioItem = True
          OnClick = N1M2Click
        end
        object N5M2: TMenuItem
          Caption = '5M'
          RadioItem = True
          OnClick = N5M2Click
        end
        object N10M2: TMenuItem
          Caption = '10M'
          RadioItem = True
          OnClick = N10M2Click
        end
        object N20M2: TMenuItem
          Caption = '20M'
          RadioItem = True
          OnClick = N20M2Click
        end
      end
      object N46: TMenuItem
        Caption = #28165#38500#25509#25910#31383#21475#26102#30830#35748
        object N47: TMenuItem
          Caption = #38656#35201#30830#35748
          OnClick = N47Click
        end
        object N48: TMenuItem
          Caption = #19981#38656#35201#30830#35748
          OnClick = N48Click
        end
      end
      object N44: TMenuItem
        Caption = #26102#38388#26174#31034#35774#32622
        object N63: TMenuItem
          AutoCheck = True
          Caption = #26174#31034
          Checked = True
          RadioItem = True
          OnClick = N63Click
          object N64: TMenuItem
            Caption = #26174#31034#26085#26399#21644#26102#38388
            OnClick = N64Click
          end
          object N65: TMenuItem
            Caption = #21482#26174#31034#26102#38388
            OnClick = N65Click
          end
        end
        object N62: TMenuItem
          AutoCheck = True
          Caption = #19981#26174#31034
          RadioItem = True
          OnClick = N62Click
        end
      end
      object HEX1: TMenuItem
        Caption = 'HEX'#26174#31034
        OnClick = HEX1Click
      end
      object UTF81: TMenuItem
        Caption = 'UTF8'#26174#31034
        OnClick = UTF81Click
      end
      object N60: TMenuItem
        Caption = #26242#20572#26174#31034
        OnClick = N60Click
      end
      object N61: TMenuItem
        Caption = #28165#38500
        OnClick = N61Click
      end
    end
    object N31: TMenuItem
      Caption = #21457#36865
      OnClick = N31Click
      object N53: TMenuItem
        Caption = #28155#21152#22238#36710#25442#34892
        object r1: TMenuItem
          Caption = #22238#36710#39'\r'#39
          OnClick = r1Click
        end
        object n54: TMenuItem
          Caption = #25442#34892#39'\n'#39
          OnClick = n54Click
        end
      end
      object HEXvudp1: TMenuItem
        Caption = 'HEX'#21457#36865
        OnClick = HEXvudp1Click
      end
      object N55: TMenuItem
        Caption = #22238#36710#21457#36865
        OnClick = N55Click
      end
      object N56: TMenuItem
        Caption = #21452#20987#21457#36865#25991#20214#35774#32622
        object N57: TMenuItem
          Caption = #36830#32493#21457#36865
          OnClick = N57Click
        end
        object N1K1ms2: TMenuItem
          Caption = #27599'1K'#24310#26102'1ms'
          Checked = True
          OnClick = N1K1ms2Click
        end
        object N1K10ms2: TMenuItem
          Caption = #27599'1K'#24310#26102'10ms'
          OnClick = N1K10ms2Click
        end
        object N1K50ms2: TMenuItem
          Caption = #27599'1K'#24310#26102'50ms'
          OnClick = N1K50ms2Click
        end
        object N1K100ms2: TMenuItem
          Caption = #27599'1K'#24310#26102'100ms'
          OnClick = N1K100ms2Click
        end
      end
      object N58: TMenuItem
        Caption = #21457#36865#31383#21475#32553#25918
        OnClick = N58Click
      end
    end
    object N32: TMenuItem
      Caption = #25193#23637#21151#33021
      object N49: TMenuItem
        Caption = #22810#23383#31526#20018
        OnClick = N49Click
      end
      object HID2: TMenuItem
        Caption = 'HID'
        OnClick = HID2Click
      end
      object CP2: TMenuItem
        Caption = 'TCP'
        OnClick = CP2Click
      end
      object UDP2: TMenuItem
        Caption = 'UDP'
        OnClick = UDP2Click
      end
      object DESMAC1: TMenuItem
        Caption = 'DES/MAC'
        OnClick = DESMAC1Click
      end
      object AES2: TMenuItem
        Caption = 'AES'
        OnClick = AES2Click
      end
      object SM2: TMenuItem
        Caption = 'SM'
        OnClick = SM2Click
      end
      object HASHMD5CRC1: TMenuItem
        Caption = 'HASH/MD5/CRC'
        OnClick = HASHMD5CRC1Click
      end
      object XO41: TMenuItem
        Caption = 'XOR'
        OnClick = XO41Click
      end
      object QRCode2: TMenuItem
        Caption = 'QRCode'
        OnClick = QRCode2Click
      end
      object Chart2: TMenuItem
        Caption = #25968#25454#27874#24418#65288'Chart'#65289
        OnClick = Chart2Click
      end
      object FFT2: TMenuItem
        Caption = 'FFT'#35745#31639
        OnClick = FFT2Click
      end
      object N50: TMenuItem
        Caption = #26356#22810#20854#23427#21151#33021
        OnClick = N50Click
      end
      object N51: TMenuItem
        Caption = #20851#38381#25193#23637#21151#33021
        OnClick = N51Click
      end
    end
    object N52: TMenuItem
      Caption = #25903#25345#20316#32773
      OnClick = N52Click
    end
  end
end
