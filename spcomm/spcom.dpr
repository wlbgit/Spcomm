program Project1;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  Unit2 in 'Unit2.pas' {Form2},
  Unit3 in 'Unit3.pas',
  Unit4 in 'Unit4.pas',
  Unit5 in 'Unit5.pas',
  util_utf8 in 'util_utf8.pas',
  Unit8 in 'Unit8.pas' {Form8},
  DelphiZXingQRCode in 'DelphiZXIngQRCode.pas',
  Unit_chart in 'Unit_chart.pas' {Form9},
  Unit_fft in 'Unit_fft.pas' {Form_fft},
  Complexs in 'Complexs.pas',
  FFTs in 'Ffts.pas',
  Unit_des in 'Unit_des.pas',
  SM in 'SM.pas',
  Unit_CRC in 'Unit_CRC.pas',
  ScrnCap in 'ScrnCap.pas',
  Unit_tool in 'Unit_tool.pas';

{$R *.res}
{$R manage.res}
//{$R serialplot.RES}
//打开CONSOLE后可直接使用WRITELN在控制台输出信息；
//{$APPTYPE   CONSOLE}


procedure Halt0;
begin
    Halt;
end;
begin
  Application.Initialize;
  Application.Title := 'Spcom V7.3';
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  //Application.CreateForm(TForm6, Form6);
  Application.CreateForm(TForm8, Form8);
  Application.CreateForm(TForm9, Form9);
  Application.CreateForm(TForm_fft, Form_fft);
  Application.Run;

asm
  xor edx,edx
  push ebp
  push OFFSET @@safecode
  push dword ptr fs:[edx]
  mov fs:[edx],esp
  call Halt0
  jmp @@Exit
  @@safecode:
  call Halt0;
  @@Exit:
end;

end.
