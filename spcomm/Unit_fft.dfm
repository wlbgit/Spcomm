object Form_fft: TForm_fft
  Left = 258
  Top = 84
  Width = 1114
  Height = 747
  Caption = 'FFT'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 880
    Top = 0
    Width = 226
    Height = 713
    Align = alRight
    TabOrder = 0
    object GroupBox3: TGroupBox
      Left = 2
      Top = 15
      Width = 222
      Height = 119
      Align = alTop
      Caption = 'FFT'#21442#25968
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 19
        Width = 64
        Height = 13
        AutoSize = False
        Caption = #37319#26679#29575'Fs'
      end
      object Label2: TLabel
        Left = 160
        Top = 19
        Width = 35
        Height = 13
        AutoSize = False
        Caption = #28857'/s'
      end
      object Label3: TLabel
        Left = 8
        Top = 43
        Width = 91
        Height = 13
        AutoSize = False
        Caption = #25968#25454#28857#25968'N'
      end
      object Label4: TLabel
        Left = 160
        Top = 38
        Width = 56
        Height = 31
        AutoSize = False
        Caption = #28857'('#26368#22810'30720'#28857')'
        WordWrap = True
      end
      object Label5: TLabel
        Left = 8
        Top = 66
        Width = 102
        Height = 32
        AutoSize = False
        Caption = #39057#29575#20998#36776#29575'D=2PI/(N*T),T=1/Fs'
        WordWrap = True
      end
      object Label6: TLabel
        Left = 160
        Top = 72
        Width = 36
        Height = 13
        AutoSize = False
        Caption = '*2*PI'
      end
      object Label7: TLabel
        Left = 8
        Top = 99
        Width = 106
        Height = 13
        AutoSize = False
        Caption = #39057#29575#26368#22823#20540
      end
      object Label8: TLabel
        Left = 160
        Top = 98
        Width = 32
        Height = 13
        AutoSize = False
        Caption = 'Hz'
      end
      object Edit1: TEdit
        Left = 114
        Top = 15
        Width = 40
        Height = 21
        TabOrder = 0
        Text = '250'
      end
      object Edit2: TEdit
        Left = 114
        Top = 40
        Width = 40
        Height = 21
        TabOrder = 1
        Text = '256'
      end
      object Edit3: TEdit
        Left = 114
        Top = 68
        Width = 40
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 2
        Text = '1'
      end
      object Edit4: TEdit
        Left = 114
        Top = 95
        Width = 40
        Height = 21
        Enabled = False
        TabOrder = 3
        Text = '127'
      end
    end
    object Memo1: TMemo
      Left = 2
      Top = 186
      Width = 222
      Height = 87
      Align = alClient
      ScrollBars = ssVertical
      TabOrder = 1
    end
    object GroupBox5: TGroupBox
      Left = 2
      Top = 521
      Width = 222
      Height = 190
      Align = alBottom
      TabOrder = 2
      object PageControl2: TPageControl
        Left = 2
        Top = 47
        Width = 218
        Height = 141
        ActivePage = TabSheet5
        Align = alClient
        Style = tsButtons
        TabHeight = 1
        TabOrder = 0
        TabWidth = 1
        object TabSheet5: TTabSheet
          Caption = #25163#21160#36755#20837
          object Button2: TButton
            Left = 5
            Top = 3
            Width = 102
            Height = 25
            Caption = 'FFT'#35745#31639
            TabOrder = 0
            OnClick = Button2Click
          end
          object Button1: TButton
            Left = 108
            Top = 3
            Width = 102
            Height = 25
            Caption = #36755#20837#25968#25454#28165#38500
            TabOrder = 1
            OnClick = Button1Click
          end
          object Button4: TButton
            Left = 5
            Top = 28
            Width = 102
            Height = 25
            Caption = #25968#25454#20445#23384
            TabOrder = 2
            OnClick = Button4Click
          end
        end
        object TabSheet16: TTabSheet
          Caption = #25991#20214#36755#20837
          ImageIndex = 2
          object Edit36: TEdit
            Left = -1
            Top = 2
            Width = 194
            Height = 21
            TabOrder = 0
          end
          object Button12: TButton
            Left = 195
            Top = 5
            Width = 15
            Height = 17
            Caption = '...'
            TabOrder = 1
            OnClick = Button12Click
          end
          object Button13: TButton
            Left = 1
            Top = 26
            Width = 102
            Height = 25
            Caption = 'FFT'#35745#31639
            TabOrder = 2
            OnClick = Button2Click
          end
          object Button14: TButton
            Left = 106
            Top = 26
            Width = 102
            Height = 25
            Caption = #25968#25454#20445#23384
            TabOrder = 3
            OnClick = Button4Click
          end
        end
        object TabSheet6: TTabSheet
          Caption = #20018#21475#36755#20837
          ImageIndex = 1
          object Label38: TLabel
            Left = 2
            Top = 40
            Width = 208
            Height = 14
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clTeal
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Button5: TButton
            Left = 0
            Top = 18
            Width = 102
            Height = 22
            Caption = #25968#25454#26684#24335#35828#26126
            TabOrder = 0
            OnClick = Button5Click
          end
          object Button3: TButton
            Left = 104
            Top = 18
            Width = 102
            Height = 22
            Caption = #25968#25454#20445#23384
            TabOrder = 1
            OnClick = Button4Click
          end
          object CheckBox2: TCheckBox
            Left = 0
            Top = 0
            Width = 97
            Height = 17
            Caption = #26242#20572#36755#20837
            TabOrder = 2
          end
          object CheckBox3: TCheckBox
            Left = 87
            Top = 1
            Width = 120
            Height = 17
            Caption = #25968#25454#22823#23567#31471#20132#25442
            TabOrder = 3
          end
        end
        object TabSheet17: TTabSheet
          Caption = 'Excel'#36755#20837
          ImageIndex = 3
          object Label84: TLabel
            Left = 5
            Top = 51
            Width = 155
            Height = 13
            AutoSize = False
            Caption = #39029'        '#34892'        '#21015'         '#21015#25968
          end
          object Label85: TLabel
            Left = 0
            Top = 112
            Width = 63
            Height = 13
            AutoSize = False
            Caption = #26368#22823#34892#25968
          end
          object Label86: TLabel
            Left = 104
            Top = 113
            Width = 64
            Height = 13
            AutoSize = False
            Caption = #38388#38548#26102#38388
          end
          object Label87: TLabel
            Left = 187
            Top = 114
            Width = 34
            Height = 13
            AutoSize = False
            Caption = #31186
          end
          object Shape4: TShape
            Left = 105
            Top = 26
            Width = 20
            Height = 22
            Hint = #21047#26032#20018#21475#21015#34920
            Brush.Color = clGray
            ParentShowHint = False
            Shape = stCircle
            ShowHint = True
            OnMouseDown = shape1MouseDown
          end
          object Button15: TButton
            Left = 162
            Top = 66
            Width = 45
            Height = 20
            Caption = ' '#30830#23450
            TabOrder = 0
            OnClick = Button15Click
          end
          object Edit41: TEdit
            Left = -7
            Top = -3
            Width = 202
            Height = 21
            TabOrder = 1
          end
          object Button16: TButton
            Left = 197
            Top = -1
            Width = 14
            Height = 20
            Caption = '...'
            TabOrder = 2
            OnClick = Button16Click
          end
          object UpDown1: TUpDown
            Left = 24
            Top = 64
            Width = 16
            Height = 21
            Associate = Edit42
            Min = 1
            Max = 32676
            Position = 1
            TabOrder = 3
          end
          object UpDown2: TUpDown
            Left = 63
            Top = 64
            Width = 16
            Height = 21
            Associate = Edit43
            Min = 1
            Max = 32676
            Position = 1
            TabOrder = 4
          end
          object UpDown3: TUpDown
            Left = 103
            Top = 64
            Width = 16
            Height = 21
            Associate = Edit44
            Min = 1
            Max = 32767
            Position = 1
            TabOrder = 5
          end
          object Edit42: TEdit
            Left = 2
            Top = 64
            Width = 22
            Height = 21
            TabOrder = 6
            Text = '1'
          end
          object Edit43: TEdit
            Left = 41
            Top = 64
            Width = 22
            Height = 21
            TabOrder = 7
            Text = '1'
          end
          object Edit44: TEdit
            Left = 81
            Top = 64
            Width = 22
            Height = 21
            TabOrder = 8
            Text = '1'
          end
          object Edit45: TEdit
            Left = 121
            Top = 66
            Width = 36
            Height = 21
            TabOrder = 9
            Text = '1'
          end
          object CheckBox5: TCheckBox
            Left = 2
            Top = 90
            Width = 81
            Height = 17
            Caption = #33258#21160#35835#21462
            TabOrder = 10
            OnClick = CheckBox5Click
          end
          object Edit46: TEdit
            Left = 61
            Top = 108
            Width = 26
            Height = 21
            TabOrder = 11
            Text = '1'
          end
          object Edit47: TEdit
            Left = 159
            Top = 110
            Width = 26
            Height = 21
            AutoSize = False
            TabOrder = 12
            Text = '3'
          end
          object CheckBox6: TCheckBox
            Left = 8
            Top = 29
            Width = 95
            Height = 17
            Caption = #33258#21160#20445#23384#22270#29255
            TabOrder = 13
            Visible = False
          end
          object Button17: TButton
            Left = 135
            Top = 24
            Width = 75
            Height = 25
            Caption = #25171#24320#25991#20214
            TabOrder = 14
            OnClick = Button17Click
          end
          object Button18: TButton
            Left = 162
            Top = 88
            Width = 45
            Height = 20
            Caption = #20445#23384
            TabOrder = 15
            OnClick = Button18Click
          end
        end
      end
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 218
        Height = 32
        Align = alTop
        Caption = 'Panel5'
        TabOrder = 1
        object Label28: TLabel
          Left = 5
          Top = 8
          Width = 62
          Height = 17
          AutoSize = False
          Caption = #25968#25454#26469#28304
        end
        object ComboBox3: TComboBox
          Left = 65
          Top = 4
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 0
          Text = #25163#21160#36755#20837
          OnChange = ComboBox3Change
          Items.Strings = (
            #25163#21160#36755#20837
            #25991#26412#36755#20837
            #20018#21475#36755#20837
            'Excel'#36755#20837)
        end
      end
    end
    object GroupBox6: TGroupBox
      Left = 2
      Top = 134
      Width = 222
      Height = 52
      Align = alTop
      Caption = 'Db'#26684#24335
      TabOrder = 3
      object Label12: TLabel
        Left = 78
        Top = 9
        Width = 139
        Height = 15
        AutoSize = False
        Caption = 'Db = log10(v^2/(Fs*N))*10 '
      end
      object Label14: TLabel
        Left = 79
        Top = 30
        Width = 17
        Height = 13
        AutoSize = False
        Caption = 'n ='
        Visible = False
      end
      object Label83: TLabel
        Left = 145
        Top = 30
        Width = 25
        Height = 13
        AutoSize = False
        Caption = 'd ='
        Visible = False
      end
      object RadioButton3: TRadioButton
        Left = 10
        Top = 15
        Width = 64
        Height = 17
        Caption = #40664#35748
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = RadioButton3Click
      end
      object RadioButton4: TRadioButton
        Left = 10
        Top = 29
        Width = 63
        Height = 17
        Caption = #33258#23450#20041
        TabOrder = 1
        OnClick = RadioButton4Click
      end
      object Edit8: TEdit
        Left = 99
        Top = 27
        Width = 37
        Height = 21
        TabOrder = 2
        Text = '288'
        Visible = False
      end
      object Edit40: TEdit
        Left = 165
        Top = 27
        Width = 42
        Height = 21
        TabOrder = 3
        Text = '20'
        Visible = False
      end
    end
    object GroupBox7: TGroupBox
      Left = 2
      Top = 273
      Width = 222
      Height = 248
      Align = alBottom
      TabOrder = 4
      object Label21: TLabel
        Left = 158
        Top = 14
        Width = 30
        Height = 13
        AutoSize = False
        Caption = #21152#31383
      end
      object Label39: TLabel
        Left = 5
        Top = 14
        Width = 74
        Height = 13
        AutoSize = False
        Caption = #19968#32423
      end
      object Label40: TLabel
        Left = 5
        Top = 35
        Width = 74
        Height = 13
        AutoSize = False
        Caption = #20108#32423
      end
      object Label41: TLabel
        Left = 5
        Top = 57
        Width = 74
        Height = 13
        AutoSize = False
        Caption = #19977#32423
      end
      object shape1: TShape
        Left = 115
        Top = 14
        Width = 15
        Height = 15
        Hint = #26174#31034#35774#32622#31383#21475
        Brush.Color = clGray
        ParentShowHint = False
        Shape = stCircle
        ShowHint = True
        OnMouseDown = shape1MouseDown
      end
      object Shape2: TShape
        Left = 115
        Top = 32
        Width = 15
        Height = 15
        Hint = #26174#31034#35774#32622#31383#21475
        Brush.Color = clGray
        ParentShowHint = False
        Shape = stCircle
        ShowHint = True
        OnMouseDown = Shape2MouseDown
      end
      object Shape3: TShape
        Left = 115
        Top = 53
        Width = 15
        Height = 15
        Hint = #26174#31034#35774#32622#31383#21475
        Brush.Color = clGray
        ParentShowHint = False
        Shape = stCircle
        ShowHint = True
        OnMouseDown = Shape3MouseDown
      end
      object ComboBox1: TComboBox
        Left = 139
        Top = 31
        Width = 76
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 0
        Text = 'None'
        Items.Strings = (
          'None'
          'Hanning'
          'Hamming'
          'Blackman'
          'Blackman-Harris'
          'Flat-Top')
      end
      object filter1_Box: TComboBox
        Left = 32
        Top = 10
        Width = 81
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 1
        Text = #19981#28388#27874
        OnChange = filter1_BoxChange
        Items.Strings = (
          #19981#28388#27874
          #21345#32819#26364#28388#27874
          #19968#38454#20302#36890
          'IIR '#28388#27874
          'FIR '#28388#27874)
      end
      object CheckBox1: TCheckBox
        Left = 7
        Top = 78
        Width = 106
        Height = 17
        Caption = #21435#38500#30452#27969#20998#37327
        TabOrder = 2
        WordWrap = True
        OnClick = CheckBox1Click
      end
      object PageControl1: TPageControl
        Left = 2
        Top = 96
        Width = 218
        Height = 150
        ActivePage = TabSheet4
        Align = alBottom
        DragKind = dkDock
        MultiLine = True
        Style = tsButtons
        TabHeight = 1
        TabOrder = 3
        TabStop = False
        TabWidth = 1
        object TabSheet4: TTabSheet
          Caption = 'none'
          ImageIndex = 3
        end
        object TabSheet1: TTabSheet
          Caption = 'kalman'
          object Label15: TLabel
            Left = 70
            Top = 40
            Width = 44
            Height = 13
            AutoSize = False
            Caption = 'LastP'
          end
          object Label17: TLabel
            Left = 70
            Top = 62
            Width = 34
            Height = 13
            AutoSize = False
            Caption = 'Kg'
          end
          object Label19: TLabel
            Left = 70
            Top = 88
            Width = 23
            Height = 13
            AutoSize = False
            Caption = 'R'
          end
          object Label18: TLabel
            Left = 175
            Top = 62
            Width = 20
            Height = 13
            AutoSize = False
            Caption = 'Q'
          end
          object Label20: TLabel
            Left = 176
            Top = 111
            Width = 48
            Height = 13
            AutoSize = False
            Caption = 'help  ?'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            OnClick = Label20Click
          end
          object Label16: TLabel
            Left = 174
            Top = 40
            Width = 35
            Height = 13
            Caption = 'Now_P'
          end
          object Label74: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #19968#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Edit9: TEdit
            Left = 7
            Top = 35
            Width = 60
            Height = 21
            TabOrder = 0
            Text = '0.02'
          end
          object Edit11: TEdit
            Left = 7
            Top = 60
            Width = 60
            Height = 21
            TabOrder = 1
            Text = '0'
          end
          object Edit10: TEdit
            Left = 108
            Top = 35
            Width = 60
            Height = 21
            TabOrder = 2
            Text = '0'
          end
          object Edit13: TEdit
            Left = 7
            Top = 85
            Width = 60
            Height = 21
            TabOrder = 3
            Text = '0.543'
          end
          object Edit12: TEdit
            Left = 108
            Top = 60
            Width = 60
            Height = 21
            TabOrder = 4
            Text = '0.001'
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'one'
          ImageIndex = 1
          object Label9: TLabel
            Left = 2
            Top = 29
            Width = 52
            Height = 13
            AutoSize = False
            Caption = #25130#27490#39057#29575
          end
          object Label10: TLabel
            Left = 151
            Top = 29
            Width = 30
            Height = 13
            AutoSize = False
            Caption = 'Hz'
          end
          object Label75: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #19968#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Edit5: TEdit
            Left = 56
            Top = 24
            Width = 86
            Height = 21
            TabOrder = 0
            Text = '40'
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'IIR'
          ImageIndex = 2
          object Label27: TLabel
            Left = 181
            Top = 93
            Width = 26
            Height = 16
            Caption = 'help'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            OnClick = Label27Click
          end
          object Label13: TLabel
            Left = 0
            Top = 115
            Width = 203
            Height = 13
            AutoSize = False
            Caption = #36824#26410#23548#20837#21442#25968
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label76: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #19968#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Button7: TButton
            Left = 0
            Top = 84
            Width = 100
            Height = 25
            Caption = #25991#20214#23548#20837#31995#25968
            TabOrder = 0
            OnClick = Button7Click
          end
          object Edit37: TEdit
            Left = 0
            Top = 33
            Width = 211
            Height = 21
            TabOrder = 1
          end
        end
        object TabSheet7: TTabSheet
          Caption = 'FIR'
          ImageIndex = 4
          object Label29: TLabel
            Left = 6
            Top = 51
            Width = 37
            Height = 13
            AutoSize = False
            Caption = #31383
          end
          object Label30: TLabel
            Left = 6
            Top = 74
            Width = 49
            Height = 13
            AutoSize = False
            Caption = #25351#23450#38454
          end
          object Label32: TLabel
            Left = 6
            Top = 96
            Width = 38
            Height = 13
            AutoSize = False
            Caption = 'fc1'
          end
          object Label33: TLabel
            Left = 134
            Top = 96
            Width = 27
            Height = 13
            AutoSize = False
            Caption = 'fc2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clInfoText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = False
          end
          object Label34: TLabel
            Left = 6
            Top = 28
            Width = 47
            Height = 13
            AutoSize = False
            Caption = #31867#22411
          end
          object Label31: TLabel
            Left = 132
            Top = 76
            Width = 78
            Height = 13
            AutoSize = False
            Caption = '2 '#30340#25972#25968#20493
          end
          object Label36: TLabel
            Left = 6
            Top = 118
            Width = 43
            Height = 13
            AutoSize = False
            Caption = 'beta'
            Visible = False
          end
          object Label37: TLabel
            Left = 132
            Top = 121
            Width = 79
            Height = 13
            AutoSize = False
            Caption = ' Kaiser'#31383#31995#25968
            Visible = False
          end
          object Label77: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #19968#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object ComboBox4: TComboBox
            Left = 57
            Top = 47
            Width = 69
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            ItemIndex = 0
            TabOrder = 0
            Text = #30697#24418#31383
            OnChange = ComboBox4Change
            Items.Strings = (
              #30697#24418#31383
              #22270#22522#31383
              #19977#35282#31383
              #27721#23425#31383
              #28023#26126#31383
              #24067#25289#20811#26364#31383
              #20975#22622#31383)
          end
          object Edit15: TEdit
            Left = 57
            Top = 70
            Width = 69
            Height = 21
            TabOrder = 1
            Text = '60'
            OnChange = Edit15Change
          end
          object Edit17: TEdit
            Left = 57
            Top = 93
            Width = 69
            Height = 21
            TabOrder = 2
            Text = '40'
            OnChange = Edit17Change
          end
          object Edit29: TEdit
            Left = 165
            Top = 93
            Width = 40
            Height = 21
            TabOrder = 3
            Text = '40'
            Visible = False
            OnChange = Edit29Change
          end
          object Button6: TButton
            Left = 153
            Top = 5
            Width = 51
            Height = 25
            Caption = #35774#32622
            TabOrder = 4
            Visible = False
            OnClick = Button6Click
          end
          object ComboBox5: TComboBox
            Left = 57
            Top = 24
            Width = 69
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            ItemIndex = 0
            TabOrder = 5
            Text = #20302#36890
            OnChange = ComboBox5Change
            Items.Strings = (
              #20302#36890
              #39640#36890
              #24102#36890
              #24102#38459)
          end
          object Edit16: TEdit
            Left = 57
            Top = 114
            Width = 69
            Height = 21
            TabOrder = 6
            Text = '0'
            Visible = False
            OnChange = Edit16Change
          end
        end
        object TabSheet8: TTabSheet
          Caption = 'calman2'
          ImageIndex = 5
          object Label11: TLabel
            Left = 70
            Top = 37
            Width = 44
            Height = 13
            AutoSize = False
            Caption = 'LastP'
          end
          object Label22: TLabel
            Left = 174
            Top = 37
            Width = 35
            Height = 13
            Caption = 'Now_P'
          end
          object Label23: TLabel
            Left = 70
            Top = 59
            Width = 34
            Height = 13
            AutoSize = False
            Caption = 'Kg'
          end
          object Label24: TLabel
            Left = 175
            Top = 59
            Width = 20
            Height = 13
            AutoSize = False
            Caption = 'Q'
          end
          object Label25: TLabel
            Left = 70
            Top = 85
            Width = 23
            Height = 13
            AutoSize = False
            Caption = 'R'
          end
          object Label26: TLabel
            Left = 162
            Top = 106
            Width = 48
            Height = 13
            AutoSize = False
            Caption = 'help  ?'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            OnClick = Label20Click
          end
          object Label78: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #20108#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Edit6: TEdit
            Left = 7
            Top = 32
            Width = 60
            Height = 21
            TabOrder = 0
            Text = '0.02'
          end
          object Edit7: TEdit
            Left = 108
            Top = 32
            Width = 60
            Height = 21
            TabOrder = 1
            Text = '0'
          end
          object Edit14: TEdit
            Left = 7
            Top = 57
            Width = 60
            Height = 21
            TabOrder = 2
            Text = '0'
          end
          object Edit18: TEdit
            Left = 108
            Top = 57
            Width = 60
            Height = 21
            TabOrder = 3
            Text = '0.001'
          end
          object Edit19: TEdit
            Left = 7
            Top = 82
            Width = 60
            Height = 21
            TabOrder = 4
            Text = '0.543'
          end
        end
        object TabSheet9: TTabSheet
          Caption = 'noe2'
          ImageIndex = 6
          object Label42: TLabel
            Left = 2
            Top = 29
            Width = 52
            Height = 13
            AutoSize = False
            Caption = #25130#27490#39057#29575
          end
          object Label43: TLabel
            Left = 151
            Top = 29
            Width = 30
            Height = 13
            AutoSize = False
            Caption = 'Hz'
          end
          object Label35: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #20108#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Edit20: TEdit
            Left = 56
            Top = 24
            Width = 86
            Height = 21
            TabOrder = 0
            Text = '40'
          end
        end
        object TabSheet10: TTabSheet
          Caption = 'iir2'
          ImageIndex = 7
          object Label44: TLabel
            Left = 0
            Top = 115
            Width = 203
            Height = 13
            AutoSize = False
            Caption = #36824#26410#23548#20837#21442#25968
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label45: TLabel
            Left = 179
            Top = 93
            Width = 26
            Height = 16
            Caption = 'help'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            OnClick = Label27Click
          end
          object Label79: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #20108#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Button8: TButton
            Left = 0
            Top = 84
            Width = 111
            Height = 25
            Caption = #25991#20214#23548#20837#31995#25968
            TabOrder = 0
            OnClick = Button8Click
          end
          object Edit38: TEdit
            Left = 0
            Top = 33
            Width = 211
            Height = 21
            TabOrder = 1
          end
        end
        object TabSheet11: TTabSheet
          Caption = 'fir2'
          ImageIndex = 8
          object Label54: TLabel
            Left = 6
            Top = 55
            Width = 37
            Height = 13
            AutoSize = False
            Caption = #31383
          end
          object Label53: TLabel
            Left = 6
            Top = 78
            Width = 49
            Height = 13
            AutoSize = False
            Caption = #25351#23450#38454
          end
          object Label52: TLabel
            Left = 6
            Top = 99
            Width = 38
            Height = 13
            AutoSize = False
            Caption = 'fc1'
          end
          object Label51: TLabel
            Left = 132
            Top = 99
            Width = 33
            Height = 13
            AutoSize = False
            Caption = 'fc2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clInfoText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = False
          end
          object Label50: TLabel
            Left = 6
            Top = 33
            Width = 47
            Height = 13
            AutoSize = False
            Caption = #31867#22411
          end
          object Label48: TLabel
            Left = 132
            Top = 81
            Width = 78
            Height = 13
            AutoSize = False
            Caption = '2 '#30340#25972#25968#20493
          end
          object Label47: TLabel
            Left = 6
            Top = 120
            Width = 43
            Height = 13
            AutoSize = False
            Caption = 'beta'
            Visible = False
          end
          object Label46: TLabel
            Left = 132
            Top = 122
            Width = 79
            Height = 13
            AutoSize = False
            Caption = ' Kaiser'#31383#31995#25968
            Visible = False
          end
          object Label49: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #20108#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object ComboBox8: TComboBox
            Left = 57
            Top = 52
            Width = 69
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            ItemIndex = 0
            TabOrder = 0
            Text = #30697#24418#31383
            OnChange = ComboBox8Change
            Items.Strings = (
              #30697#24418#31383
              #22270#22522#31383
              #19977#35282#31383
              #27721#23425#31383
              #28023#26126#31383
              #24067#25289#20811#26364#31383
              #20975#22622#31383)
          end
          object Edit24: TEdit
            Left = 57
            Top = 75
            Width = 69
            Height = 21
            TabOrder = 1
            Text = '60'
            OnChange = Edit24Change
          end
          object Edit23: TEdit
            Left = 57
            Top = 96
            Width = 69
            Height = 21
            TabOrder = 2
            Text = '40'
            OnChange = Edit23Change
          end
          object Edit22: TEdit
            Left = 165
            Top = 96
            Width = 40
            Height = 21
            TabOrder = 3
            Text = '40'
            Visible = False
            OnChange = Edit22Change
          end
          object Button9: TButton
            Left = 155
            Top = 11
            Width = 51
            Height = 25
            Caption = #35774#32622
            TabOrder = 4
            Visible = False
            OnClick = Button9Click
          end
          object ComboBox2: TComboBox
            Left = 57
            Top = 29
            Width = 69
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            ItemIndex = 0
            TabOrder = 5
            Text = #20302#36890
            OnChange = ComboBox2Change
            Items.Strings = (
              #20302#36890
              #39640#36890
              #24102#36890
              #24102#38459)
          end
          object Edit21: TEdit
            Left = 57
            Top = 117
            Width = 69
            Height = 21
            TabOrder = 6
            Text = '0'
            Visible = False
            OnChange = Edit21Change
          end
        end
        object TabSheet12: TTabSheet
          Caption = 'kalman3'
          ImageIndex = 9
          object Label55: TLabel
            Left = 174
            Top = 47
            Width = 35
            Height = 13
            Caption = 'Now_P'
          end
          object Label56: TLabel
            Left = 162
            Top = 111
            Width = 48
            Height = 13
            AutoSize = False
            Caption = 'help  ?'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            OnClick = Label20Click
          end
          object Label57: TLabel
            Left = 175
            Top = 69
            Width = 20
            Height = 13
            AutoSize = False
            Caption = 'Q'
          end
          object Label58: TLabel
            Left = 70
            Top = 95
            Width = 23
            Height = 13
            AutoSize = False
            Caption = 'R'
          end
          object Label59: TLabel
            Left = 70
            Top = 69
            Width = 34
            Height = 13
            AutoSize = False
            Caption = 'Kg'
          end
          object Label60: TLabel
            Left = 70
            Top = 47
            Width = 44
            Height = 13
            AutoSize = False
            Caption = 'LastP'
          end
          object Label80: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #19977#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Edit25: TEdit
            Left = 108
            Top = 67
            Width = 60
            Height = 21
            TabOrder = 0
            Text = '0.001'
          end
          object Edit26: TEdit
            Left = 7
            Top = 92
            Width = 60
            Height = 21
            TabOrder = 1
            Text = '0.543'
          end
          object Edit27: TEdit
            Left = 108
            Top = 42
            Width = 60
            Height = 21
            TabOrder = 2
            Text = '0'
          end
          object Edit28: TEdit
            Left = 7
            Top = 67
            Width = 60
            Height = 21
            TabOrder = 3
            Text = '0'
          end
          object Edit30: TEdit
            Left = 7
            Top = 42
            Width = 60
            Height = 21
            TabOrder = 4
            Text = '0.02'
          end
        end
        object TabSheet13: TTabSheet
          Caption = 'one3'
          ImageIndex = 10
          object Label61: TLabel
            Left = 151
            Top = 29
            Width = 30
            Height = 13
            AutoSize = False
            Caption = 'Hz'
          end
          object Label62: TLabel
            Left = 2
            Top = 29
            Width = 52
            Height = 13
            AutoSize = False
            Caption = #25130#27490#39057#29575
          end
          object Label81: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #19977#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Edit31: TEdit
            Left = 56
            Top = 24
            Width = 86
            Height = 21
            TabOrder = 0
            Text = '40'
          end
        end
        object TabSheet14: TTabSheet
          Caption = 'iir3'
          ImageIndex = 11
          object Label63: TLabel
            Left = 0
            Top = 115
            Width = 203
            Height = 13
            AutoSize = False
            Caption = #36824#26410#23548#20837#21442#25968
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label64: TLabel
            Left = 179
            Top = 93
            Width = 26
            Height = 16
            Caption = 'help'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            OnClick = Label27Click
          end
          object Label82: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #19977#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Button10: TButton
            Left = 0
            Top = 84
            Width = 103
            Height = 25
            Caption = #25991#20214#23548#20837#31995#25968
            TabOrder = 0
            OnClick = Button10Click
          end
          object Edit39: TEdit
            Left = 0
            Top = 33
            Width = 211
            Height = 21
            TabOrder = 1
          end
        end
        object TabSheet15: TTabSheet
          Caption = 'fir3'
          ImageIndex = 12
          object Label65: TLabel
            Left = 132
            Top = 121
            Width = 79
            Height = 13
            AutoSize = False
            Caption = ' Kaiser'#31383#31995#25968
            Visible = False
          end
          object Label66: TLabel
            Left = 6
            Top = 120
            Width = 43
            Height = 13
            AutoSize = False
            Caption = 'beta'
            Visible = False
          end
          object Label67: TLabel
            Left = 132
            Top = 80
            Width = 78
            Height = 13
            AutoSize = False
            Caption = '2 '#30340#25972#25968#20493
          end
          object Label69: TLabel
            Left = 6
            Top = 32
            Width = 47
            Height = 13
            AutoSize = False
            Caption = #31867#22411
          end
          object Label70: TLabel
            Left = 132
            Top = 100
            Width = 38
            Height = 13
            AutoSize = False
            Caption = 'fc2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clInfoText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = False
          end
          object Label71: TLabel
            Left = 6
            Top = 98
            Width = 38
            Height = 13
            AutoSize = False
            Caption = 'fc1'
          end
          object Label72: TLabel
            Left = 6
            Top = 78
            Width = 49
            Height = 13
            AutoSize = False
            Caption = #25351#23450#38454
          end
          object Label73: TLabel
            Left = 6
            Top = 55
            Width = 37
            Height = 13
            AutoSize = False
            Caption = #31383
          end
          object Label68: TLabel
            Left = 68
            Top = 3
            Width = 74
            Height = 13
            AutoSize = False
            Caption = #19977#32423#28388#27874
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Edit32: TEdit
            Left = 57
            Top = 116
            Width = 69
            Height = 21
            TabOrder = 0
            Text = '0'
            Visible = False
            OnChange = Edit32Change
          end
          object ComboBox9: TComboBox
            Left = 57
            Top = 28
            Width = 69
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            ItemIndex = 0
            TabOrder = 1
            Text = #20302#36890
            OnChange = ComboBox9Change
            Items.Strings = (
              #20302#36890
              #39640#36890
              #24102#36890
              #24102#38459)
          end
          object Button11: TButton
            Left = 154
            Top = 12
            Width = 51
            Height = 25
            Caption = #35774#32622
            TabOrder = 2
            Visible = False
            OnClick = Button11Click
          end
          object Edit33: TEdit
            Left = 167
            Top = 95
            Width = 40
            Height = 21
            TabOrder = 3
            Text = '40'
            Visible = False
            OnChange = Edit33Change
          end
          object Edit34: TEdit
            Left = 57
            Top = 95
            Width = 69
            Height = 21
            TabOrder = 4
            Text = '40'
            OnChange = Edit34Change
          end
          object Edit35: TEdit
            Left = 57
            Top = 74
            Width = 69
            Height = 21
            TabOrder = 5
            Text = '60'
            OnChange = Edit35Change
          end
          object ComboBox10: TComboBox
            Left = 57
            Top = 51
            Width = 69
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            ItemIndex = 0
            TabOrder = 6
            Text = #30697#24418#31383
            OnChange = ComboBox10Change
            Items.Strings = (
              #30697#24418#31383
              #22270#22522#31383
              #19977#35282#31383
              #27721#23425#31383
              #28023#26126#31383
              #24067#25289#20811#26364#31383
              #20975#22622#31383)
          end
        end
      end
      object filter2_Box: TComboBox
        Left = 32
        Top = 31
        Width = 81
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 4
        Text = #19981#28388#27874
        OnChange = filter2_BoxChange
        Items.Strings = (
          #19981#28388#27874
          #21345#32819#26364#28388#27874
          #19968#38454#20302#36890
          'IIR '#28388#27874
          'FIR '#28388#27874)
      end
      object filter3_Box: TComboBox
        Left = 32
        Top = 52
        Width = 81
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 5
        Text = #19981#28388#27874
        OnChange = filter3_BoxChange
        Items.Strings = (
          #19981#28388#27874
          #21345#32819#26364#28388#27874
          #19968#38454#20302#36890
          'IIR '#28388#27874
          'FIR '#28388#27874)
      end
      object CheckBox4: TCheckBox
        Left = 139
        Top = 78
        Width = 89
        Height = 17
        Caption = #25972#27969
        TabOrder = 6
        Visible = False
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 880
    Height = 713
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 1
    object Splitter2: TSplitter
      Left = 1
      Top = 544
      Width = 878
      Height = 2
      Cursor = crVSplit
      Align = alBottom
    end
    object Splitter3: TSplitter
      Left = 1
      Top = 300
      Width = 878
      Height = 1
      Cursor = crVSplit
      Align = alTop
    end
    object Panel3: TPanel
      Left = 1
      Top = 301
      Width = 878
      Height = 243
      Align = alClient
      Caption = 'Panel3'
      Constraints.MinHeight = 150
      TabOrder = 0
      object Chart2: TChart
        Left = 1
        Top = 1
        Width = 876
        Height = 222
        AnimatedZoom = True
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginBottom = 1
        MarginLeft = 1
        MarginRight = 1
        MarginTop = 1
        Title.Text.Strings = (
          #24133#20540'-'#39057#29575)
        BottomAxis.AxisValuesFormat = '#,##0.######'
        BottomAxis.LabelsSeparation = 1
        DepthAxis.AxisValuesFormat = '#,##0.######'
        DepthAxis.LabelsSeparation = 1
        LeftAxis.AxisValuesFormat = '#,##0.######'
        LeftAxis.LabelsSeparation = 1
        LeftAxis.TickLength = 5
        Legend.Visible = False
        RightAxis.AxisValuesFormat = '#,##0.######'
        RightAxis.LabelsSeparation = 1
        TopAxis.AxisValuesFormat = '#,##0.######'
        TopAxis.LabelsSeparation = 1
        View3D = False
        View3DWalls = False
        Align = alClient
        TabOrder = 0
        OnMouseMove = Chart2MouseMove
        object Series2: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          PercentFormat = '##0.### %'
          SeriesColor = clRed
          ValueFormat = '#,##0.######'
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object StatusBar2: TStatusBar
        Left = 1
        Top = 223
        Width = 876
        Height = 19
        Panels = <
          item
            Width = 300
          end
          item
            Width = 450
          end>
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 878
      Height = 299
      Align = alTop
      Caption = 'Panel2'
      TabOrder = 1
      object Chart1: TChart
        Left = 1
        Top = 1
        Width = 876
        Height = 276
        AnimatedZoom = True
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginBottom = 1
        MarginLeft = 1
        MarginRight = 1
        MarginTop = 1
        Title.AdjustFrame = False
        Title.Text.Strings = (
          #21407#22987#27874#24418)
        Title.Visible = False
        BottomAxis.AxisValuesFormat = '#,##0.######'
        BottomAxis.LabelsSeparation = 1
        BottomAxis.MinorTickLength = 3
        Chart3DPercent = 1
        DepthAxis.AxisValuesFormat = '#,##0.######'
        DepthAxis.LabelsSeparation = 1
        LeftAxis.AxisValuesFormat = '#,##0.######'
        LeftAxis.LabelsSeparation = 1
        LeftAxis.TickLength = 5
        Legend.Alignment = laTop
        Legend.ColorWidth = 30
        Legend.DividingLines.Style = psDashDot
        Legend.Frame.Style = psDashDotDot
        Legend.Frame.SmallDots = True
        Legend.Frame.Visible = False
        Legend.HorizMargin = 1
        Legend.ShadowSize = 0
        Legend.TextStyle = ltsPlain
        Legend.TopPos = 0
        RightAxis.AxisValuesFormat = '#,##0.######'
        RightAxis.LabelsSeparation = 1
        TopAxis.AxisValuesFormat = '#,##0.######'
        TopAxis.LabelsSeparation = 1
        View3D = False
        Align = alClient
        TabOrder = 0
        Anchors = [akLeft, akTop, akRight]
        object Series1: TLineSeries
          Marks.ArrowLength = 8
          Marks.Frame.Style = psDashDotDot
          Marks.Frame.SmallDots = True
          Marks.Visible = False
          SeriesColor = clRed
          Title = #28304#27874#24418
          ValueFormat = '#,##0.######'
          Pointer.InflateMargins = False
          Pointer.Style = psCircle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series4: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clGreen
          Title = #32463#22788#29702#21518#27874#24418
          ValueFormat = '#,##0.######'
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object StatusBar1: TStatusBar
        Left = 1
        Top = 277
        Width = 876
        Height = 21
        Panels = <
          item
            Width = 450
          end
          item
            Width = 400
          end>
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 546
      Width = 878
      Height = 166
      Align = alBottom
      Caption = 'Panel1'
      TabOrder = 2
      object Chart3: TChart
        Left = 1
        Top = 1
        Width = 876
        Height = 145
        AnimatedZoom = True
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginBottom = 1
        MarginLeft = 1
        MarginRight = 1
        MarginTop = 1
        Title.Text.Strings = (
          #21151#29575#35889)
        BottomAxis.AxisValuesFormat = '#,##0.######'
        BottomAxis.LabelsSeparation = 1
        DepthAxis.AxisValuesFormat = '#,##0.######'
        DepthAxis.LabelsSeparation = 1
        LeftAxis.AxisValuesFormat = '#,##0.######'
        LeftAxis.LabelsSeparation = 1
        Legend.Visible = False
        RightAxis.AxisValuesFormat = '#,##0.######'
        RightAxis.LabelsSeparation = 1
        TopAxis.AxisValuesFormat = '#,##0.######'
        TopAxis.LabelsSeparation = 1
        View3D = False
        Align = alClient
        TabOrder = 0
        object Series3: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          PercentFormat = '##0.### %'
          SeriesColor = clRed
          ValueFormat = '#,##0.######'
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object StatusBar3: TStatusBar
        Left = 1
        Top = 146
        Width = 876
        Height = 19
        Panels = <
          item
            Width = 350
          end>
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 1018
    Top = 205
  end
  object SaveDialog1: TSaveDialog
    Left = 1050
    Top = 199
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 532
    Top = 114
  end
end
