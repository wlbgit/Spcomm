unit Unit_fft;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart,FFTs,Complexs,
  Math, ComCtrls, IniFiles, Unit_tool, util_utf8, OleServer, ExcelXP,
  OleCtnrs,Comobj, WordXP;

type
  TForm_fft = class(TForm)
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Edit2: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    Edit3: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    Edit4: TEdit;
    Label8: TLabel;
    Memo1: TMemo;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    Label12: TLabel;
    Edit8: TEdit;
    Label14: TLabel;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    GroupBox7: TGroupBox;
    ComboBox1: TComboBox;
    filter1_Box: TComboBox;
    Label21: TLabel;
    CheckBox1: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Edit9: TEdit;
    Label15: TLabel;
    Edit11: TEdit;
    Label17: TLabel;
    Edit10: TEdit;
    Edit13: TEdit;
    Label19: TLabel;
    Edit12: TEdit;
    Label18: TLabel;
    Label20: TLabel;
    Label16: TLabel;
    Edit5: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    TabSheet3: TTabSheet;
    Label27: TLabel;
    TabSheet4: TTabSheet;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    Button2: TButton;
    Button1: TButton;
    Button4: TButton;
    TabSheet6: TTabSheet;
    Button5: TButton;
    Button3: TButton;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    TabSheet7: TTabSheet;
    ComboBox4: TComboBox;
    Label29: TLabel;
    Label30: TLabel;
    Edit15: TEdit;
    Label32: TLabel;
    Label33: TLabel;
    Edit17: TEdit;
    Edit29: TEdit;
    Label34: TLabel;
    Button6: TButton;
    ComboBox5: TComboBox;
    Label31: TLabel;
    Edit16: TEdit;
    Label36: TLabel;
    Label37: TLabel;
    Button7: TButton;
    Label13: TLabel;
    Label38: TLabel;
    filter2_Box: TComboBox;
    filter3_Box: TComboBox;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    TabSheet8: TTabSheet;
    Edit6: TEdit;
    Label11: TLabel;
    Edit7: TEdit;
    Label22: TLabel;
    Edit14: TEdit;
    Label23: TLabel;
    Edit18: TEdit;
    Label24: TLabel;
    Edit19: TEdit;
    Label25: TLabel;
    Label26: TLabel;
    TabSheet9: TTabSheet;
    Label42: TLabel;
    Edit20: TEdit;
    Label43: TLabel;
    TabSheet10: TTabSheet;
    Button8: TButton;
    Label44: TLabel;
    Label45: TLabel;
    TabSheet11: TTabSheet;
    Label54: TLabel;
    Label53: TLabel;
    Label52: TLabel;
    Label51: TLabel;
    Label50: TLabel;
    Label48: TLabel;
    Label47: TLabel;
    Label46: TLabel;
    ComboBox8: TComboBox;
    Edit24: TEdit;
    Edit23: TEdit;
    Edit22: TEdit;
    Button9: TButton;
    ComboBox2: TComboBox;
    Edit21: TEdit;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    TabSheet14: TTabSheet;
    TabSheet15: TTabSheet;
    Edit25: TEdit;
    Edit26: TEdit;
    Edit27: TEdit;
    Edit28: TEdit;
    Edit30: TEdit;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Edit31: TEdit;
    Label61: TLabel;
    Label62: TLabel;
    Button10: TButton;
    Label63: TLabel;
    Label64: TLabel;
    Edit32: TEdit;
    ComboBox9: TComboBox;
    Button11: TButton;
    Edit33: TEdit;
    Edit34: TEdit;
    Edit35: TEdit;
    ComboBox10: TComboBox;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label35: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label49: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label68: TLabel;
    TabSheet16: TTabSheet;
    Edit36: TEdit;
    Button12: TButton;
    Button13: TButton;
    Button14: TButton;
    CheckBox4: TCheckBox;
    Edit37: TEdit;
    Edit38: TEdit;
    Edit39: TEdit;
    Edit40: TEdit;
    Label83: TLabel;
    Panel1: TPanel;
    Splitter2: TSplitter;
    Panel3: TPanel;
    Chart2: TChart;
    Series2: TLineSeries;
    StatusBar2: TStatusBar;
    Panel2: TPanel;
    Chart1: TChart;
    Series1: TLineSeries;
    Series4: TLineSeries;
    StatusBar1: TStatusBar;
    Panel4: TPanel;
    Chart3: TChart;
    Series3: TLineSeries;
    Splitter3: TSplitter;
    StatusBar3: TStatusBar;
    Panel5: TPanel;
    ComboBox3: TComboBox;
    Label28: TLabel;
    TabSheet17: TTabSheet;
    Button15: TButton;
    Edit41: TEdit;
    Button16: TButton;
    UpDown1: TUpDown;
    UpDown2: TUpDown;
    UpDown3: TUpDown;
    Edit42: TEdit;
    Edit43: TEdit;
    Edit44: TEdit;
    Label84: TLabel;
    Edit45: TEdit;
    CheckBox5: TCheckBox;
    Edit46: TEdit;
    Label85: TLabel;
    Label86: TLabel;
    Edit47: TEdit;
    Label87: TLabel;
    CheckBox6: TCheckBox;
    Timer1: TTimer;
    Button17: TButton;
    Shape4: TShape;
    Button18: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure filter1_BoxChange(Sender: TObject);
    procedure Label20Click(Sender: TObject);
    procedure Label27Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
    procedure ComboBox5Change(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure ge(Sender: TObject);
    procedure Edit15Change(Sender: TObject);
    procedure Edit17Change(Sender: TObject);
    procedure Edit29Change(Sender: TObject);
    procedure Edit16Change(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure filter2_BoxChange(Sender: TObject);
    procedure filter3_BoxChange(Sender: TObject);
    procedure Edit21Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure ComboBox8Change(Sender: TObject);
    procedure Edit24Change(Sender: TObject);
    procedure Edit23Change(Sender: TObject);
    procedure Edit22Change(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure ComboBox9Change(Sender: TObject);
    procedure Edit35Change(Sender: TObject);
    procedure Edit34Change(Sender: TObject);
    procedure Edit33Change(Sender: TObject);
    procedure Edit32Change(Sender: TObject);
    procedure ComboBox10Change(Sender: TObject);
    procedure ComboBox4Change(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure shape1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Shape2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Shape3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Chart2MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button12Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button17Click(Sender: TObject);
    procedure Button18Click(Sender: TObject);
    
  private
    { Private declarations }
    function get_pra():Boolean;
    procedure rec_other_windows_msg(var msg: TCopyDataStruct); message WM_user+100;
  public
    { Public declarations }
    procedure init();
    function fft_cal_dis(data : PDouble; datalen : Integer):Boolean;

  end;
  function fft_cal_dis_ext(data : PDouble; datalen : Integer):Boolean;
  procedure fir1_conv(data : pDouble; h_len, data_len: Integer; target: pDouble);
  procedure fir2_conv(data : pDouble; h_len, data_len: Integer; target: pDouble);
  procedure fir3_conv(data : pDouble; h_len, data_len: Integer; target: pDouble);
var
  Form_fft: TForm_fft;
  fft_have_open_flag : Boolean = False;


implementation

{$R *.dfm}
var
    w0 , w1, w2 : double;
    Fs,N:integer;
    T,D:single;
    input,output:array of TComplex;
    HzLine:single;//过滤频率幅值
    db_flag:Boolean=False;
    db_n : double = 288;
    db_d : double = 20;
    cal_flag : Boolean = False;
    kal1_lastp, kal1_nowp, kal1_out, kal1_kg, kal1_q, kal1_r : double;
    kal2_lastp, kal2_nowp, kal2_out, kal2_kg, kal2_q, kal2_r : double;
    kal3_lastp, kal3_nowp, kal3_out, kal3_kg, kal3_q, kal3_r : double;
    DC_Component: Double;
    low1_a,low2_a,low3_a: Double;
    //*****************************************************
    iir1_Coeffs : array of array[0..5] of Double;
    iir1_Statex : array of array[0..5] of Double;
    iir1_Statey : array of array[0..5] of Double;
    iir1_gain   : array of Double;
    iir1_order  : Integer = 2;
    iir1_input_status : Integer = 0;// 0:未成功导入，1：导入成功

    fir1_type : Integer = 0;   //0-3:低通、高通、带通、带阻
    fir1_win_type : Integer = 0; //0-6;矩形窗/图基窗/三角窗/汉宁窗/海明窗/布拉克曼窗/凯塞窗
    fir1_order : Integer = 10; // 滤波器阶数
    fir1_fs : Integer = 250;   // 采样频率
    fir1_fc1,fir1_fc2, fir1_kaise : Double; // 截止频率
    fir1_h : array of Double;  // 系数缓存
    fir1_state : array of Double;// 临时状态缓存
    fir1_changed : Boolean = True;
    //**************************************************
    iir2_Coeffs : array of array[0..5] of Double;
    iir2_Statex : array of array[0..5] of Double;
    iir2_Statey : array of array[0..5] of Double;
    iir2_gain   : array of Double;
    iir2_order  : Integer = 2;
    iir2_input_status : Integer = 0;// 0:未成功导入，1：导入成功

    fir2_type : Integer = 0;   //0-3:低通、高通、带通、带阻
    fir2_win_type : Integer = 0; //0-6;矩形窗/图基窗/三角窗/汉宁窗/海明窗/布拉克曼窗/凯塞窗
    fir2_order : Integer = 10; // 滤波器阶数
    fir2_fs : Integer = 250;   // 采样频率
    fir2_fc1,fir2_fc2, fir2_kaise : Double; // 截止频率
    fir2_h : array of Double;  // 系数缓存
    fir2_state : array of Double;// 临时状态缓存
    fir2_changed : Boolean = True;
    //**************************************************
    iir3_Coeffs : array of array[0..5] of Double;
    iir3_Statex : array of array[0..5] of Double;
    iir3_Statey : array of array[0..5] of Double;
    iir3_gain   : array of Double;
    iir3_order  : Integer = 2;
    iir3_input_status : Integer = 0;// 0:未成功导入，1：导入成功

    fir3_type : Integer = 0;   //0-3:低通、高通、带通、带阻
    fir3_win_type : Integer = 0; //0-6;矩形窗/图基窗/三角窗/汉宁窗/海明窗/布拉克曼窗/凯塞窗
    fir3_order : Integer = 10; // 滤波器阶数
    fir3_fs : Integer = 250;   // 采样频率
    fir3_fc1,fir3_fc2, fir3_kaise : Double; // 截止频率
    fir3_h : array of Double;  // 系数缓存
    fir3_state : array of Double;// 临时状态缓存
    fir3_changed : Boolean = True;
    MsExcel: Variant;
    csv_file_flag : Boolean = False;
    csvFile: TStrings;


const IIR_50Notch_B0 =0.90239774423695518;
const IIR_50Notch_B1 =-0.55771247730967288;
const IIR_50Notch_B2 =0.90239774423695518;

const IIR_50Notch_A0 =1;
const IIR_50Notch_A1 =-0.55771247730967288;
const IIR_50Notch_A2 =0.80479548847391036;



var
    fft_data : array of Double;
    fft_i : Integer = 0;

procedure del_fft_data(data : Pointer; len : Integer);
var
    i , j, fft_len, mode: Integer;
    xor_result : Byte;
    pdata : PByte;
    pdata_tmp, pdata_tmp1: PByte;
begin
    if len < 5 then
        Exit;

    pdata := data;
    i := 0;
    mode := 0;
    if Form_fft.CheckBox3.Checked = True then
        mode := 1;
    while (i < len-5) do begin
        if (pdata^ = $AA) then begin
            dbg_msg('data0='+ inttohex(pdata^, 2));
            Inc(pbyte(pdata));
            dbg_msg('data2='+ inttohex(pdata^, 2));
            if (pdata^ = $BB) then begin
                xor_result := 0;
                fft_i := 0;
                Inc(pbyte(pdata));

                dbg_msg('data3='+ inttohex(pdata^, 2));
                pdata_tmp := pdata;

                fft_len := pdata^ shl 8;
                Inc(pbyte(pdata));
                dbg_msg('data4='+ inttohex(pdata^, 2));
                fft_len := fft_len + pdata^;
                Inc(pbyte(pdata));
                dbg_msg('fft_len='+ inttostr(fft_len)+ ' len - i - 4=' +  IntToStr(len - i - 4));
                if (fft_len < (len - i - 4)) and ((fft_len mod 4) = 0) and (fft_len <= 4096)then begin
                    pdata_tmp1 := pdata;
                    for j := 0 to fft_len - 1 do begin
                        xor_result := xor_result xor pdata^;
                        inc(pbyte(pdata));
                    end;
                    dbg_msg('xor_result='+ inttohex(xor_result, 2) + '  pdata='+ IntToHex(pdata^, 2));
                    if xor_result = pdata^ then begin
                        dbg_msg('check ok');
                        SetLength(fft_data, fft_len div 4);
                        for j := 0 to fft_len - 1 do begin
                            if j mod 4 = 0 then begin
                                fft_data[fft_i] := BytesToSingle(pdata_tmp1, mode);
                                dbg_msg(IntToStr(fft_i) + ':' + FloatToStr(fft_data[fft_i]));
                                inc(fft_i);
                            end;
                            inc(pbyte(pdata_tmp1));
                        end;
                        fft_cal_dis_ext(@fft_data[0], fft_i);
                        SetLength(fft_data, 0);
                        Form_fft.Label38.Caption :=  GetCurTime(2) + ' 收到一包数据('+inttostr(fft_i)+')';
                        i := i + fft_len + 5;
                        Continue;
                    end;
                    Form_fft.Label38.Caption :=  GetCurTime(2) + ' 校验失败';
                end;
                dbg_msg('>>pdata='+ IntToHex(pdata_tmp^, 2));
                i := i + 2;
                pdata := pdata_tmp;
            end
            else begin
                Inc(i);
            end;
        end
        else begin
            Inc(i);
            inc(pbyte(pdata));
        end;
    end;
end;

procedure TForm_fft.rec_other_windows_msg(var msg: TCopyDataStruct);
begin
    //ShowMessage('>>>'+ PChar(msg.lpData));
    //Memo1.Lines.Add(IntToStr(Message.cbData)+ '  ' +PChar(message.lpData));
    if (ComboBox3.ItemIndex = 2) and (CheckBox2.Checked = False) then
        del_fft_data(msg.lpData, msg.cbData);
    FreeMemory(msg.lpData);
end;
(*
  频率：单位时间内完成完整圆周运动的次数,用f表示,单位是赫兹,符号是Hz.
        当单位时间取1 s时,f=n.
  转速:单位时间内质点转过的圈数,常用符号n表示,单位符号是r/s
*)
procedure TForm_fft.init();
var
  maxHz:integer;
begin
  //获取原始信号每秒采样点数Fs
  Fs:=strtoint(Edit1.text);
  //获取原始信号的采样率T
  T:=1/Fs;
  //获取原始信号的长度N（总点数）
  N:=strtoint(Edit2.text);

  if N > 30720 then
  begin
    Edit2.text:='30720';
    N:=30720;
  end;
  //获取频谱的分辨率D，单位是2*PI/s
  D:=1/(N*T);
  Edit3.Text:=floattostr(D);
  (*
    计算最大频谱maxHz，因为幅值谱是偶函数，相位谱是奇函数
    都关于y轴对称，所以只需要计算一半数据N/2
    频率分辨率与频率点数的乘积D*(N/2)=1/(2*T)为最大频谱值
  *)
  maxHz:=round(1/(2*T));
  //最大频谱值（从0开始，所以最大频谱必须减一）
  Edit4.Text:=inttostr(maxHz-1);
end;

procedure TForm_fft.Button1Click(Sender: TObject);
begin
    Memo1.Clear;
end;

function kalman1_filter(fdata : Double):Double;
begin
    kal1_nowp := kal1_lastp + kal1_q;
    kal1_kg := kal1_nowp / (kal1_nowp + kal1_r);
    kal1_out := kal1_out + kal1_kg * (fdata - kal1_out);
    kal1_lastp := (1 - kal1_kg) * kal1_nowp;

    Result := kal1_out;
end;

function kalman2_filter(fdata : Double):Double;
begin
    kal2_nowp := kal2_lastp + kal2_q;
    kal2_kg := kal2_nowp / (kal2_nowp + kal2_r);
    kal2_out := kal2_out + kal2_kg * (fdata - kal2_out);
    kal2_lastp := (1 - kal2_kg) * kal2_nowp;

    Result := kal2_out;
end;

function kalman3_filter(fdata : Double):Double;
begin
    kal3_nowp := kal3_lastp + kal3_q;
    kal3_kg := kal3_nowp / (kal3_nowp + kal3_r);
    kal3_out := kal3_out + kal3_kg * (fdata - kal3_out);
    kal3_lastp := (1 - kal3_kg) * kal3_nowp;

    Result := kal3_out;
end;

procedure iir1_cal(x,y : PDouble; Len , numStages : Integer);
var
    i,j : Integer;
begin
    i := 0;
    for i := 0 to numStages - 1 do begin
        iir1_Statex[i][0] := 0;
        iir1_Statex[i][1] := 0;
        iir1_Statex[i][2] := 0;
        iir1_Statey[i][0] := 0;
        iir1_Statey[i][1] := 0;
        iir1_Statey[i][2] := 0;
    end;

    for i:=0 to len -1 do begin
        iir1_Statex[0][0] := x^;
        for j:=0 to numStages - 1 do begin
            iir1_Statey[j][0] := (iir1_Coeffs[j][0] * iir1_Statex[j][0] + iir1_Coeffs[j][1] * iir1_Statex[j][1] + iir1_Coeffs[j][2] * iir1_Statex[j][2]) * iir1_Gain[j] - iir1_Statey[j][1] * iir1_Coeffs[j][4] - iir1_Statey[j][2] * iir1_Coeffs[j][5];
            y^ := iir1_Statey[j][0];
            iir1_Statex[j][2] := iir1_Statex[j][1]; iir1_Statex[j][1] := iir1_Statex[j][0];
            iir1_Statey[j][2] := iir1_Statey[j][1]; iir1_Statey[j][1] := iir1_Statey[j][0];
            iir1_Statex[j+1][0] := y^;
        end;
        //Form_fft.Memo1.Lines.Add(FloatToStr(y^));
        inc(pdouble(x));
        inc(pdouble(y));
    end;
    //ShowMessage('i=' + IntToStr(i));
end;
procedure iir2_cal(x,y : PDouble; Len , numStages : Integer);
var
    i,j : Integer;
begin
    i := 0;
    for i := 0 to numStages - 1 do begin
        iir2_Statex[i][0] := 0;
        iir2_Statex[i][1] := 0;
        iir2_Statex[i][2] := 0;
        iir2_Statey[i][0] := 0;
        iir2_Statey[i][1] := 0;
        iir2_Statey[i][2] := 0;
    end;

    for i:=0 to len -1 do begin
        iir2_Statex[0][0] := x^;
        for j:=0 to numStages - 1 do begin
            iir2_Statey[j][0] := (iir2_Coeffs[j][0] * iir2_Statex[j][0] + iir2_Coeffs[j][1] * iir2_Statex[j][1] + iir2_Coeffs[j][2] * iir2_Statex[j][2]) * iir2_Gain[j] - iir2_Statey[j][1] * iir2_Coeffs[j][4] - iir2_Statey[j][2] * iir2_Coeffs[j][5];
            y^ := iir2_Statey[j][0];
            iir2_Statex[j][2] := iir2_Statex[j][1]; iir2_Statex[j][1] := iir2_Statex[j][0];
            iir2_Statey[j][2] := iir2_Statey[j][1]; iir2_Statey[j][1] := iir2_Statey[j][0];
            iir2_Statex[j+1][0] := y^;
        end;
        //Form_fft.Memo1.Lines.Add(FloatToStr(y^));
        inc(pdouble(x));
        inc(pdouble(y));
    end;
    //ShowMessage('i=' + IntToStr(i));
end;
procedure iir3_cal(x,y : PDouble; Len , numStages : Integer);
var
    i,j : Integer;
begin
    i := 0;
    for i := 0 to numStages - 1 do begin
        iir3_Statex[i][0] := 0;
        iir3_Statex[i][1] := 0;
        iir3_Statex[i][2] := 0;
        iir3_Statey[i][0] := 0;
        iir3_Statey[i][1] := 0;
        iir3_Statey[i][2] := 0;
    end;

    for i:=0 to len -1 do begin
        iir3_Statex[0][0] := x^;
        for j:=0 to numStages - 1 do begin
            iir3_Statey[j][0] := (iir3_Coeffs[j][0] * iir3_Statex[j][0] + iir3_Coeffs[j][1] * iir3_Statex[j][1] + iir3_Coeffs[j][2] * iir3_Statex[j][2]) * iir3_Gain[j] - iir3_Statey[j][1] * iir3_Coeffs[j][4] - iir3_Statey[j][2] * iir3_Coeffs[j][5];
            y^ := iir3_Statey[j][0];
            iir3_Statex[j][2] := iir3_Statex[j][1]; iir3_Statex[j][1] := iir3_Statex[j][0];
            iir3_Statey[j][2] := iir3_Statey[j][1]; iir3_Statey[j][1] := iir3_Statey[j][0];
            iir3_Statex[j+1][0] := y^;
        end;
        //Form_fft.Memo1.Lines.Add(FloatToStr(y^));
        inc(pdouble(x));
        inc(pdouble(y));
    end;
    //ShowMessage('i=' + IntToStr(i));
end;
procedure filter_1_cal(indata, outdata:PDouble; datalen : Integer);
var
    data_tmp , data: PDouble;
    i : Integer;
    result1,low_last_result : Double;
begin
    if Form_fft.filter1_Box.ItemIndex = 3 then begin  // iir
        if iir1_input_status = 1 then
            iir1_cal(indata, outdata, datalen, iir1_order);
        Exit;
    end
    else if Form_fft.filter1_Box.ItemIndex = 4 then begin  // fir
        if fir1_changed = True then begin
            fir1_changed := False;
            Form_fft.Button6.Click;
        end;
        fir1_conv(indata, fir1_order+1, datalen, outdata);
        Exit;
    end;
    data := indata;
    i := 0;
    low_last_result := 0;
    while (i < datalen) do begin
        result1 := data^;
        // kalman
        if Form_fft.filter1_Box.ItemIndex = 1 then begin
            Result1 := kalman1_filter(Result1);
        end
        else if Form_fft.filter1_Box.ItemIndex = 2 then begin  // low pass
            //Memo1.Lines.Add(FloatToStr(result)+','+FloatToStr(low_last_result));
            result1 := low1_a * result1 + (1 - low1_a) * low_last_result;
            low_last_result := result1;
        end;
        outdata^ := result1;
        inc(i);
        if i = datalen then
            break;
        inc(pdouble(data));
        inc(pdouble(outdata));
    end;
end;

procedure filter_2_cal(indata, outdata:PDouble; datalen : Integer);
var
    data_tmp , data: PDouble;
    i : Integer;
    result1, low_last_result: Double;
begin
    if Form_fft.filter2_Box.ItemIndex = 3 then begin  // iir
        if iir2_input_status = 1 then
            iir2_cal(indata, outdata, datalen, iir2_order);
        Exit;
    end
    else if Form_fft.filter2_Box.ItemIndex = 4 then begin  // fir
        if fir2_changed = True then begin
            fir2_changed := False;
            Form_fft.Button9.Click;
        end;
        fir2_conv(indata, fir2_order+1, datalen, outdata);
        Exit;
    end;
    data := indata;
    i := 0;
    low_last_result := 0;
    while (i < datalen) do begin
        result1 := data^;
        // kalman
        if Form_fft.filter2_Box.ItemIndex = 1 then begin
            Result1 := kalman2_filter(Result1);
        end
        else if Form_fft.filter2_Box.ItemIndex = 2 then begin  // low pass
            //Memo1.Lines.Add(FloatToStr(result)+','+FloatToStr(low_last_result));
            result1 := low2_a * result1 + (1 - low2_a) * low_last_result;
            low_last_result := result1;
        end;
        outdata^ := result1;
        inc(i);
        if i = datalen then
            break;
        inc(pdouble(data));
        inc(pdouble(outdata));
    end;
end;
procedure filter_3_cal(indata, outdata:PDouble; datalen : Integer);
var
    data_tmp , data: PDouble;
    i : Integer;
    result1,low_last_result: Double;
begin
    if Form_fft.filter3_Box.ItemIndex = 3 then begin  // iir
        if iir3_input_status = 1 then
            iir3_cal(indata, outdata, datalen, iir3_order);
        Exit;
    end
    else if Form_fft.filter3_Box.ItemIndex = 4 then begin  // fir
        if fir3_changed = True then begin
            fir3_changed := False;
            Form_fft.Button11.Click;
        end;
        fir3_conv(indata, fir3_order+1, datalen, outdata);
        Exit;
    end;
    data := indata;
    i := 0;
    low_last_result := 0;
    while (i < datalen) do begin
        result1 := data^;
        // kalman
        if Form_fft.filter3_Box.ItemIndex = 1 then begin
            Result1 := kalman3_filter(Result1);
        end
        else if Form_fft.filter3_Box.ItemIndex = 2 then begin  // low pass
            //Memo1.Lines.Add(FloatToStr(result)+','+FloatToStr(low_last_result));
            result1 := low3_a * result1 + (1 - low3_a) * low_last_result;
            low_last_result := result1;
        end;
        outdata^ := result1;
        inc(i);
        if i = datalen then
            break;
        inc(pdouble(data));
        inc(pdouble(outdata));
    end;
end;

function TForm_fft.fft_cal_dis(data : PDouble; datalen : Integer):Boolean;
var
    i ,k: Integer;
    //fdata :array of Double;
    result1, result2, result3: Double;
    x_min,y_min, x_max, y_max, ydb_min,xdb_min, ydb_max, xdb_max: double;
    data1, low_last_result: PDouble;
    s_rms, f_rms, sabs_rms, fabs_rms : Double;
    c_fre,c_fre1,c_fre2 : Double;//重心频率
    msf,msf_1,msf_2 : Double;//均方频率
    cdb_fre,cdb_fre1,cdb_fre2 : Double;//重心频率
    msf_db,msf_db1,msf_db2 : Double;//均方频率
begin
    //SetLength(fdata, datalen*sizeof(double));
    //Move(pdouble(data)^, fdata[0], datalen*sizeof(double));
    i := 0;
    s_rms := 0;
    f_rms := 0;
    sabs_rms := 0;
    fabs_rms := 0;
    data1 := data;
    while (i < datalen) do begin
        Series1.AddXY(i, data1^);
        s_rms := s_rms + data1^ * data1^;
        sabs_rms := sabs_rms + Abs(data1^);
        inc(i);
        inc(pdouble(data1));
    end;
    s_rms := Sqrt(s_rms / datalen);
    sabs_rms := (sabs_rms / datalen);

    i := 0;
    data1 := data;
    DC_Component := 0;
    if CheckBox1.Checked = True then begin      // 去掉直流分量
        while (i < datalen) do begin
            DC_Component := DC_Component + data1^;
            inc(i);
            inc(pdouble(data1));
            if i = datalen then begin
                DC_Component := DC_Component / datalen;
                //ShowMessage('dc='+ FloatToStr(DC_Component));
                break;
            end;
        end;
        data1 := data;
        i := 0;
        while (i < datalen) do begin
            data1 ^ := data1^ - DC_Component;
            if CheckBox4.Checked = True then
                data1 ^ := Abs(data1^);
            inc(i);
            inc(pdouble(data1));
        end;
    end;
    data1 := data;
    dbg_msg(format('>>>%.12f', [data^]));

    if filter1_Box.ItemIndex <> 0 then
        filter_1_cal(data1, data, datalen);
    data1 := data;
    if filter2_Box.ItemIndex <> 0 then
        filter_2_cal(data1, data, datalen);
    data1 := data;
    if filter3_Box.ItemIndex <> 0 then
        filter_3_cal(data1, data, datalen);
    //ShowMessage('x0='+floattostr(data^));
    i := 0;
    w0 := 0; w1 := 0; w2 := 0;
    k := 1;
    //Memo1.Clear;
    // Memo1.Lines.Add(IntToStr(datalen));
    while (i < datalen) do begin
        result1 := data^;
        // hanning window
        if ComboBox1.ItemIndex = 1 then begin
            k := -1;
            Result1 := (0.5 * (1-cos(2*pi*(i+1)/(datalen-k)))) * result1;
        end
        else if ComboBox1.ItemIndex = 2 then begin    // hamming
            k := 0;
            Result1 := (0.54 - 0.46*cos(2*pi*(i+0)/(datalen-k))) * result1;
        end
        else if ComboBox1.ItemIndex = 3 then begin   // Blackman
            //Memo1.Lines.Add(IntToStr(i) + ':' +  FloatToStr((0.42 - 0.5*cos(2*pi*i/(datalen-k)) + 0.08*cos(4*PI*i/(datalen-k)))));
            Result1 := (0.42 - 0.5*cos(2*pi*i/(datalen-k)) + 0.08*cos(4*PI*i/(datalen-k))) * result1;
        end
        else if ComboBox1.ItemIndex = 4 then    // Blackman-Harris
            Result1 := (0.35875 - 0.48829*cos(2*pi*i/(datalen-k)) + 0.14128*cos(4*PI*i/(datalen-k)) - 0.01168*cos(6*PI*i*(datalen-k))) * result1
        else if ComboBox1.ItemIndex = 5 then begin    // Flat-Top
            Result1 := (0.21557895 - 0.41663158*cos(2*pi*i/(datalen-k)) + 0.277263158*cos(4*PI*i/(datalen-k)) - 0.083578947*cos(6*PI*i/(datalen-k))+0.006947368*cos(8*PI*i/(datalen-k))) * result1;
        end;
        //Memo1.Lines.Add(IntToStr(i) + ':' +  FloatToStr(Result1));
        f_rms := f_rms + Result1 * Result1;
        fabs_rms := fabs_rms + Abs(Result1);

        Series4.AddXY(i, Result1);
        input[i-1].Re:=Result1;
        inc(i);
        //if i > datalen then
        //    break;
        inc(pdouble(data));
    end;
    f_rms := Sqrt(f_rms / datalen);
    fabs_rms := (fabs_rms / datalen);

    StatusBar1.Panels[0].Text := '源数据 均方根='+ Format('%.8f',[s_rms]) + ',绝对值平均值='+Format('%.8f',[sabs_rms]);
    StatusBar1.Panels[1].Text := '过滤后数据 均方根='+ Format('%.8f',[f_rms]) + ',绝对值平均值='+Format('%.8f',[FABS_rms]);
    //进行正向FFT变换
    ForwardFFT(input,output,N);

    //由于得到的幅频图和相频图都是关于Y对称，故只需N/2的数据
    //将(N-1)/2取最大整值:trunc((N-1)/2)+1
    //然后再-1，因为起点为0
    //最后得到trunc((N-1)/2)
    series2.Clear;
    series3.Clear;
    x_min := 0;
    y_min := 0;
    x_max := 0;
    y_max := 0;
    c_fre := 0;c_fre1 := 0;c_fre2 := 0;
    ydb_min := 0;xdb_min := 0;ydb_max := 0;xdb_max := 0;
    //Memo1.clear;
    for i:= 0 to trunc((N)/2) do
    begin
        //频谱幅值
        Result1 := ComplexMag(output[i]);
        if i = 0 then begin
            Result2 := Result1/DATALEN;   // 恢复正常幅值，除了0点，其它需要*2
        end
        else if i = 1 then begin
            Result2 := Result1/DATALEN*2;   // 恢复正常幅值，除了0点，其它需要*2
            // 0点为直流分量，不参与比较
            y_max := result2;
            x_max := i*d;
            y_min := result2;
            x_min := i*d;
        end
        else begin
            Result2 := Result1/DATALEN*2;   // 恢复正常幅值，除了0点，其它需要*2
        end;

        // Hanning 恢复系数
        if ComboBox1.ItemIndex = 1 then begin
            Result2 := 2 * result2;
            Result1 := 2 * result1;
        end
        else if ComboBox1.ItemIndex = 2 then begin   // hamming 恢复系统1.852
            Result2 := 1.852 * result2;
            Result1 := 1.852 * result1
        end
        else if ComboBox1.ItemIndex = 3 then begin    // Blackman
            Result2 := 2.381 * result2;
            Result1 := 2.381 * result1;
        end
        else if ComboBox1.ItemIndex = 4 then begin   // Blackman-Harris
            Result2 := 2.817 * result2;
            Result1 := 2.817 * result1;
        end
        else if ComboBox1.ItemIndex = 5 then begin   // Flat-Top
            Result2 := 4.6431 * result2;
            Result1 := 4.6431 * result1;
        end;
        result2 := Abs(result2);
        // 自定义
        if (db_flag = True) then begin
            Result3 := (Log10(result2 / db_n) * db_d + 300) - 300;
            if (i = 1) then begin
                ydb_max := result3;
                xdb_max := i*d;
                ydb_min := result3;
                xdb_min := i*d;
            end;

        end
        else begin   // DB 默认
            if i > 0 then begin
                if result1 * result1/(N*Fs) > 0 then
                    Result3 := 10*(Log10(2* (result1 * result1)/(N*Fs)));
                if (i = 1) then begin
                    ydb_max := result3;
                    xdb_max := i*d;
                    ydb_min := result3;
                    xdb_min := i*d;
                end;
            end
            else begin
                //ShowMessage('n='+inttostr(n)+' fs='+inttostr(fs) + 'result1='+floattostr(result1));
                if result1 * result1/(N*Fs) > 0 then
                    Result3 := 10*(Log10((result1 * result1)/(N*Fs)));
            end;
        end;

        if result2 > y_max then begin
            y_max := result2;
            x_max := i*d;
        end;
        if result2 < y_min then begin
            y_min := result2;
            x_min := i*d;
        end;
        if result3 > ydb_max then begin
            ydb_max := result3;
            xdb_max := i*d;
        end;
        if result3 < ydb_min then begin
            ydb_min := result3;
            xdb_min := i*d;
        end;
        c_fre1 := c_fre1 + i*D * Result2;
        c_fre2 := c_fre2 + Result2;
        series2.AddXY(i*D, Result2);
        //series2.Marks.Visible:=True;

        //Series2.Marks.Style:=smsValue;
        //Memo1.Lines.Add(IntToStr(i)+':'+ FloatToStr(Result1));
        //频谱相位
        //series3.AddXY(i*d,ComplexPhase(output[i]));
        series3.AddXY(i*d,Result3);
    end;
    if c_fre2 <> 0 then
        c_fre := c_fre1 / c_fre2;// 重心频率
    c_fre1 := 0;
    msf := 0;
    for i:= 0 to trunc((N)/2) do
    begin
        //频谱幅值
        Result1 := ComplexMag(output[i]);
        if i = 0 then begin
            Result2 := Result1/DATALEN;   // 恢复正常幅值，除了0点，其它需要*2
        end
        else if i = 1 then begin
            Result2 := Result1/DATALEN*2;   // 恢复正常幅值，除了0点，其它需要*2
        end
        else begin
            Result2 := Result1/DATALEN*2;   // 恢复正常幅值，除了0点，其它需要*2
        end;

        // Hanning 恢复系数
        if ComboBox1.ItemIndex = 1 then begin
            Result2 := 2 * result2;
            Result1 := 2 * result1;
        end
        else if ComboBox1.ItemIndex = 2 then begin   // hamming 恢复系统1.852
            Result2 := 1.852 * result2;
            Result1 := 1.852 * result1
        end
        else if ComboBox1.ItemIndex = 3 then begin    // Blackman
            Result2 := 2.381 * result2;
            Result1 := 2.381 * result1;
        end
        else if ComboBox1.ItemIndex = 4 then begin   // Blackman-Harris
            Result2 := 2.817 * result2;
            Result1 := 2.817 * result1;
        end
        else if ComboBox1.ItemIndex = 5 then begin   // Flat-Top
            Result2 := 4.6431 * result2;
            Result1 := 4.6431 * result1;
        end;
        result2 := Abs(result2);
        c_fre1 := c_fre1 + (i*D - c_fre)*(i*D - c_fre)* Result2;
        msf := msf + (i*D)*(i*D)* Result2;
    end;

    StatusBar2.Panels[0].Text := Format('min(%.3fHz,%.8f)',[x_min, y_min]) + Format(' max(%.3fHz,%.8f)',[x_max, y_max]);
    if c_fre2 <> 0 then
        StatusBar2.Panels[1].Text := Format('幅值平均值:%.8f,重心频率:%.3f,频率方差:%.3f,均方频率:%.3f,方差频率:%.3f',
                                         [c_fre2*2/N, c_fre, c_fre1/c_fre2, msf/c_fre2, Sqrt(msf/c_fre2)]);
    StatusBar3.Panels[0].Text := Format(' max(%.3fHz,%.8f)',[xdb_max, ydb_max]);
    cal_flag := true;
    Chart1.BottomAxis.Automatic := True;
    Chart1.LeftAxis.Automatic := True;
    Chart2.BottomAxis.Automatic := True;
    Chart2.LeftAxis.Automatic := True;
    Chart3.BottomAxis.Automatic := True;
    Chart3.LeftAxis.Automatic := True;

    //Chart2.BottomAxis.Automatic
    //Chart2.BottomAxis.Automatic := false;
    Chart1.Refresh;
    Chart2.Refresh;
    Chart3.Refresh;
    y_min := Chart1.LeftAxis.Minimum;
    y_max := Chart1.LeftAxis.Maximum;
    Chart1.LeftAxis.Automatic := false;
    Chart1.LeftAxis.Minimum := y_min - (y_max-y_min)/25;
    Chart1.LeftAxis.Maximum := y_max + (y_max-y_min)/25;

    y_min := Chart2.LeftAxis.Minimum;
    y_max := Chart2.LeftAxis.Maximum;
    Chart2.LeftAxis.Automatic := false;
    Chart2.LeftAxis.Minimum := y_min - (y_max-y_min)/25;
    Chart2.LeftAxis.Maximum := y_max + (y_max-y_min)/25;

    y_min := Chart3.LeftAxis.Minimum;
    y_max := Chart3.LeftAxis.Maximum;
    Chart3.LeftAxis.Automatic := false;
    Chart3.LeftAxis.Minimum := y_min - (y_max-y_min)/25;
    Chart3.LeftAxis.Maximum := y_max + (y_max-y_min)/25;
end;

function TForm_fft.get_pra():Boolean;
var
    tmp : Double;
begin
    Result := True;
    if filter1_Box.ItemIndex = 1 then begin
        if (TryStrToFloat(Edit9.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('LastP 参数有误');
        end;
        kal1_lastp := tmp;
        if (TryStrToFloat(Edit10.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('NowP 参数有误');
        end;
        kal1_nowp := tmp;
        kal1_out := 0;
        if (TryStrToFloat(Edit11.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('Kg 参数有误');
        end;
        kal1_kg := tmp;
        if (TryStrToFloat(Edit12.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('Q 参数有误');
        end;
        kal1_q := tmp;
        if (TryStrToFloat(Edit13.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('R 参数有误');
        end;
        kal1_r := tmp;
    end
    else if filter1_Box.ItemIndex = 2 then begin  // low pass
        if (TryStrToFloat(Edit5.Text, tmp) = False) then begin
            Result := False;
            raise Exception.Create('截止频率有误');
        end;
        low1_a := tmp;
        low1_a := (2*pi*t*low1_a)/(2*pi*t*low1_a+1);
    end;

    if filter2_Box.ItemIndex = 1 then begin
        if (TryStrToFloat(Edit6.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('LastP 参数有误');
        end;
        kal2_lastp := tmp;
        if (TryStrToFloat(Edit7.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('NowP 参数有误');
        end;
        kal2_nowp := tmp;
        kal2_out := 0;
        if (TryStrToFloat(Edit14.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('Kg 参数有误');
        end;
        kal2_kg := tmp;
        if (TryStrToFloat(Edit18.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('Q 参数有误');
        end;
        kal2_q := tmp;
        if (TryStrToFloat(Edit19.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('R 参数有误');
        end;
        kal2_r := tmp;
    end
    else if filter2_Box.ItemIndex = 2 then begin  // low pass
        if (TryStrToFloat(Edit20.Text, tmp) = False) then begin
            Result := False;
            raise Exception.Create('截止频率有误');
        end;
        low2_a := tmp;
        low2_a := (2*pi*t*low2_a)/(2*pi*t*low2_a+1);
    end;

    if filter3_Box.ItemIndex = 1 then begin
        if (TryStrToFloat(Edit30.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('LastP 参数有误');
        end;
        kal3_lastp := tmp;
        if (TryStrToFloat(Edit27.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('NowP 参数有误');
        end;
        kal3_nowp := tmp;
        kal3_out := 0;
        if (TryStrToFloat(Edit28.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('Kg 参数有误');
        end;
        kal3_kg := tmp;
        if (TryStrToFloat(Edit25.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('Q 参数有误');
        end;
        kal3_q := tmp;
        if (TryStrToFloat(Edit26.Text, tmp) <> true) then begin
            Result := False;
            raise Exception.Create('R 参数有误');
        end;
        kal3_r := tmp;
    end
    else if filter3_Box.ItemIndex = 2 then begin  // low pass
        if (TryStrToFloat(Edit31.Text, tmp) = False) then begin
            Result := False;
            raise Exception.Create('截止频率有误');
        end;
        low3_a := tmp;
        low3_a := (2*pi*t*low3_a)/(2*pi*t*low3_a+1);
    end
end;

procedure TForm_fft.Button2Click(Sender: TObject);
var
    bfile:TextFile;
    i,j, flag:integer;
    s:string;
    result,x_min,y_min, x_max, y_max : double;
    data_buff : array of Double;
    strdata,str_tmp : string;
begin
    init;
    setlength(data_buff,N);
    setlength(input,N);
    setlength(output,N);

    get_pra();

    flag := 0;
    Series1.Clear;
    Series2.Clear;
    Series3.Clear;
    Series4.Clear;

    //RadioButton4.OnClick(self);
    if (ComboBox3.ItemIndex = 0) or (ComboBox3.ItemIndex = 3) then begin    // 手动输入计算
        i := 0;
        flag := 0;
        j := 0;
        DC_Component := 0;
        strdata := Trim(Memo1.Text);
        strdata := StringReplace(strdata, ',', #13#10, [rfReplaceAll]);
        strdata := StringReplace(strdata, chr(9), #13#10, [rfReplaceAll]);   //tab
        strdata := StringReplace(strdata, ' ', #13#10, [rfReplaceAll]);
        strdata := StringReplace(strdata, ';', #13#10, [rfReplaceAll]);
        Memo1.Text := strdata;
        str_tmp := '';
        //去除空白行
        if Memo1.Lines.Count >= N then begin
            while (i < Memo1.Lines.Count) do begin
                if (Memo1.Lines[i] <> EmptyStr) then begin
                    str_tmp := str_tmp + Memo1.Lines[i]+#13#10;
                end;
                inc(i);
            end;
        end;
        Memo1.Text := str_tmp;
        i := 0;

        if Memo1.Lines.Count >= N then begin
            while (i < Memo1.Lines.Count) do
            begin
                if (Memo1.Lines[i] <> '') then begin
                    s:= Trim(Memo1.Lines[i]);
                    if (TryStrToFloat(s, Result) = True) then begin
                        data_buff[j] := result;
                        inc(j);
                        if j = N then
                            break;

                    end
                    else begin
                        if (flag = 0) then begin
                            ShowMessage('第'+inttostr(i)+'行数据有误');
                            flag := 1;
                        end;
                    end;
                end;
                inc(i);
            end;
        end;
        if(j < N) then begin
            ShowMessage('数据不够，至少' + inttostr(N) + '个数据');
            setlength(data_buff,0);
            setlength(input,0);
            setlength(output,0);
            Exit;
        end;
    end
    else if ComboBox3.ItemIndex = 1 then begin
        //if OpenDialog1.Execute then
        begin
            if FileExists(Edit36.Text) = False then begin
                ShowMessage('文件不存在');
                Exit;
            end;
            Assignfile(bfile, OpenDialog1.FileName);
            reset(bfile);
            i := 0;
            DC_Component := 0;
            while (not eof(bfile) and (i < N)) do begin
                readln(bfile, s);
                s:= Trim(s);
                if (s <> '') and (TryStrToFloat(s, Result) = True) then begin
                    data_buff[i] := result;
                    inc(i);
                end
                else begin
                    if (flag = 0) then begin
                        ShowMessage('第'+inttostr(i)+'行数据有误');
                        ShowMessage(S);
                        flag := 1;
                    end;
                end;
            end;
            CloseFile(bfile);
            if(i < N) then begin
                ShowMessage('数据不够，至少' + inttostr(N) + '个数据');
                setlength(data_buff, 0);
                setlength(input, 0);
                setlength(output, 0);
                Exit;
            end;
        end;
    end;
    fft_cal_dis(@data_buff[0], N);
    setlength(data_buff, 0);
    setlength(input,0);
    setlength(output,0);
end;

procedure TForm_fft.RadioButton4Click(Sender: TObject);
var
    dbn : Double;
begin
    if RadioButton4.Checked = True then begin
        db_flag := True;
        if TryStrToFloat(Edit8.Text, dbn) then begin
            db_n := dbn;
        end
        else begin
            ShowMessage('n 输入有误');
            db_n := 288;
            Edit8.Text := '288';
        end;
        if TryStrToFloat(Edit40.Text, dbn) then begin
            db_d := dbn;
        end
        else begin
            ShowMessage('d 输入有误');
            db_n := 288;
            Edit40.Text := '20';
        end;
    end;
    Label12.Caption := 'Db = log10(v/n)*d ';
    Edit8.Visible := true;
    Edit40.Visible := true;
    Label14.Visible := true;
    Label83.Visible := true;
end;

procedure TForm_fft.RadioButton3Click(Sender: TObject);
begin
    if RadioButton3.Checked = True then
      db_flag := False;
    Label12.Caption := 'Db = log10(v^2/(Fs*N))*10 ';
    Edit8.Visible := False;
    Edit40.Visible := False;
    Label14.Visible := False;
    Label83.Visible := False;
end;

procedure TForm_fft.Button4Click(Sender: TObject);
var
    bfile:TextFile;
    i : Integer;
begin
    if cal_flag = False then begin
        ShowMessage('请先计算');
        Exit;
    end;
    SaveDialog1.Title := '保存源数据';
    SaveDialog1.FileName := 'source_'+formatdatetime('mmdd_hhmmss',now);
    SaveDialog1.DefaultExt := '.txt';
    SaveDialog1.Filter := 'Text files (.txt)|*.txt';
    if SaveDialog1.Execute then
    begin
        Assignfile(bfile, SaveDialog1.FileName);
        Rewrite(bfile);
        i := 0;
        while (i < N) do begin

            writeln(bfile, format('%03d', [i]) +': ' + format('%.8f',[Series1.YValues[i]]));
            inc(i);
        end;
        closefile(bfile);
        Chart1.SaveToBitmapFile(ChangeFileExt(ExtractFileName(SaveDialog1.FileName),'')+'.bmp');
    end;
    SaveDialog1.Title := '保存过滤后的数据';
    SaveDialog1.FileName := 'filter_'+formatdatetime('mmdd_hhmmss',now);
    SaveDialog1.DefaultExt := '.txt';
    SaveDialog1.Filter := 'Text files (.txt)|*.txt';
    if SaveDialog1.Execute then
    begin
        Assignfile(bfile, SaveDialog1.FileName);
        Rewrite(bfile);
        i := 0;
        while (i < N) do begin

            writeln(bfile, format('%03d', [i]) +': ' + format('%.8f',[Series4.YValues[i]]));
            inc(i);
        end;
        closefile(bfile);
        Chart1.SaveToBitmapFile(ChangeFileExt(ExtractFileName(SaveDialog1.FileName),'')+'.bmp');
    end;
    SaveDialog1.Title := '保存FFT数据';
    SaveDialog1.FileName := 'fft_amplitude_'+formatdatetime('mmdd_hhmmss',now);
    SaveDialog1.DefaultExt := '.txt';
    SaveDialog1.Filter := 'Text files (.txt)|*.txt';
    if SaveDialog1.Execute then
    begin
        Assignfile(bfile, SaveDialog1.FileName);
        Rewrite(bfile);
        i := 0;
        while (i < N/2) do begin

            writeln(bfile, format('%03d', [i]) +': ' + format('%.6f',[Series2.YValues[i]]));
            inc(i);
        end;
        closefile(bfile);
        Chart2.SaveToBitmapFile(ChangeFileExt(ExtractFileName(SaveDialog1.FileName),'')+'.bmp');
    end;

    SaveDialog1.Title := '保存相位数据';
    SaveDialog1.FileName := 'fft_phase_'+formatdatetime('mmdd_hhmmss',now);
    SaveDialog1.DefaultExt := '.txt';
    SaveDialog1.Filter := 'Text files (.txt)|*.txt';
    if SaveDialog1.Execute then
    begin
        Assignfile(bfile, SaveDialog1.FileName);
        Rewrite(bfile);
        i := 0;
        while (i < N/2) do begin

            writeln(bfile, format('%03d', [i]) +': ' + format('%.6f',[Series3.YValues[i]]));
            inc(i);
        end;
        closefile(bfile);
        Chart3.SaveToBitmapFile(ChangeFileExt(ExtractFileName(SaveDialog1.FileName),'')+'.bmp');
    end;
end;

procedure TForm_fft.filter1_BoxChange(Sender: TObject);
var
    result : Double;
begin
    PageControl1.TabIndex := 0;
    shape1.Brush.Color := clGray;
    get_pra();
    if filter1_Box.ItemIndex = 1 then begin
        PageControl1.TabIndex := 1;
        shape1.Brush.Color := clGreen;
    end
    else if filter1_Box.ItemIndex = 2 then begin  // low pass
        PageControl1.TabIndex := 2;
        shape1.Brush.Color := clGreen;
    end
    else if filter1_Box.ItemIndex = 3 then begin  // iir
        PageControl1.TabIndex := 3;
        if iir1_input_status = 0 then
            Label13.Caption := '还未导入参数'
        else if iir1_input_status = 1 then
            shape1.Brush.Color := clGreen;
    end
    else if filter1_Box.ItemIndex = 4 then begin  // fir
        PageControl1.TabIndex := 4;
        shape1.Brush.Color := clGreen;
    end;
end;

procedure TForm_fft.Label20Click(Sender: TObject);
begin
    if filter1_Box.ItemIndex = 1 then begin
        ShowMessage('LastP:     上次估算协方差'+ #10#13
              +'Now_P;      当前估算协方差'+ #10#13
              +'Kg;         卡尔曼增益'+ #10#13
              +'Q;          过程噪声协方差'+ #10#13
              +'R;          观测噪声协方差'+ #10#13
              );
    end
end;

procedure TForm_fft.Label27Click(Sender: TObject);
begin
    ShowMessage('相关参数由matlab中的滤波器设计中获取'+#13#10
                + '暂时只支持滤波器结构为直接 I 型，2阶和4阶的手动输入设置'+#13#10
                + '文件导入系数最多支持1000节，如导入出错，请检查文件格式和排版问题'+#13#10
                + '在matlab设计好后，导出fcf文件，即可获取相关参数'+#13#10
                + '例如：' +#13#10
                + 'SOS 矩阵: ' +#13#10
                + '1  -0.954025006332091862759625655598938465118  1  1  -1.458078578669458780225909322325605899096  0.702593196989995449897037360642571002245'  +#13#10
                + '1   0.694413318048010874683484416891587898135  1  1  -1.098157733333514185503076987515669316053  0.327258469060791090665674119009054265916' +#13#10
                + '定标值:' +#13#10
                + '0.233767173977171227594595848131575621665' +#13#10
                + '0.085028059426773758278095272089558420703'+#13#10 +#13#10

                + '则将 1  -0.954025006332091862759625655598938465118  1 依次填入分子中的三个参数'+#13#10
                + '将 -1.458078578669458780225909322325605899096  0.702593196989995449897037360642571002245  依次填入分母后两个参数' +#13#10
                + '将 0.233767173977171227594595848131575621665 填入增益系数1中'+#13#10
              );
end;

function fft_cal_dis_ext(data : PDouble; datalen : Integer):Boolean;
begin
    Form_fft.init;

    setlength(input,N);
    setlength(output,N);

    Form_fft.get_pra();
    Form_fft.Series1.Clear;
    Form_fft.Series2.Clear;
    Form_fft.Series3.Clear;
    Form_fft.Series4.Clear;
    if datalen > N then
        datalen := N;
    Form_fft.fft_cal_dis(data, datalen);

    setlength(input,0);
    setlength(output,0);
end;
procedure TForm_fft.Button5Click(Sender: TObject);
var
    s : string;
begin
    s := '数据格式： flag1  flag2 datalen_h  datalen_l data0 data1 data2 data3 ... datan xor_data;' + #13#10
      +  'flag1 = 0xAA;' + #13#10
      +  'flag2 = 0xBB;' + #13#10
      +  'datalen_h和datalen_l组成一个16位数据，表示实际数据长度，len = (flag1 << 8) | flag2;' + #13#10
      +  '每4个数据组成一个32位浮点数;' + #13#10
      +  'xor_data 为data0至datan的异或校验' + #13#10 + #13#10
      +  '数据长度必需为4的整数倍' + #13#10
      +  '采样频率等参数由"FFT参数"一栏中设置' + #13#10;
    ShowMessage(s);

    //Memo1.Lines.Add(s);
end;

procedure TForm_fft.ComboBox3Change(Sender: TObject);
begin
    PageControl2.TabIndex := ComboBox3.ItemIndex;
end;

procedure TForm_fft.ComboBox5Change(Sender: TObject);
begin
    if ComboBox5.ItemIndex > 1 then begin
        Label33.Visible := True;
        Edit29.Visible := True;
    end
    else begin
        Label33.Visible := False;
        Edit29.Visible := False;
    end;
    fir1_type := ComboBox5.ItemIndex;
    fir1_changed := True;
end;

function bessel0(x : Double):Double;
var
    i : Integer;
    d, y, d2, sum : Double;
begin
    y := x / 2.0;
    d := 1.0;
    sum := 1.0;
    for i := 1 to 25 do begin
        d := d * y / i;
        d2 := d * d;
        sum := sum + d2;
        if (d2 < sum * (1.0e-8)) then
            break;
    end;
    result := (sum);
end;

function kaiser(i, n : Integer;  beta :double):double;
var
    a, w, a2, b1, b2, beta1 : Double;
begin
    b1 := bessel0(beta);
    a := 2.0 * i / (n-1.0) - 1.0;
    a2 := a * a;
    beta1 := beta * sqrt(1.0 - a2);
    b2 := bessel0(beta1);
    w := b2 / b1;
    Result := w;
end;

//n：窗口长度 type：选择窗函数的类型 beta：生成凯塞窗的系数
function window(mode, n, i : Integer;  beta : double):double ;
var
    k : Integer;
    w : double;
begin
    //pi := Math.pi;
    w := 1.0;
    case (mode) of
        1: w := 1.0;  //矩形窗
        2: begin
            k := (n - 2) div 10;
            if i <= k then
                w := 0.5 * (1.0 - cos(i * pi / (k + 1)));  //图基窗
            if (i > n - k - 2) then
                w := 0.5 * (1.0 - cos((n - i - 1) * pi / (k + 1)));
        end;
        3: begin
            if (1.0 - 2 * i / (n - 1.0)) > 0 then
                w := 1.0 - (1.0 - 2 * i / (n - 1.0))//三角窗
            else
                w := 1.0 + (1.0 - 2 * i / (n - 1.0));//三角窗
        end;
        4: w := 0.5 * (1.0 - cos(2 * i * pi / (n - 1)));//汉宁窗
        5: w := 0.54 - 0.46 * cos(2 * i * pi / (n - 1));//海明窗
        6: w := 0.42 - 0.5 * cos(2 * i * pi / (n - 1)) + 0.08 * cos(4 * i * pi / (n - 1));//布莱克曼窗
        7: w := kaiser(i, n, beta);//凯塞窗
    end;
    result := (w);
end;

{
n：滤波器的阶数
band：滤波器的类型,取值1-4,分别为低通、高通,带通、带阻滤波器
wn：窗函数的类型，取值1-7，分别对应矩形窗、图基窗、三角窗、汉宁窗、海明窗、布拉克曼窗和凯塞窗
fs：采样频率
h：存放滤波器的系数
kaiser：beta值
fln：带通下边界频率
fhn：带通上边界频率
}
procedure firwin(h : PDouble; n, band, wn, fs : Integer; fln, fhn, kaiser: Double);
var
    i, n2, mid : Integer;
    s, wc1, wc2, beta, delay : Double;
    fir_h : array of Double;
begin
    beta := kaiser;
    //pi =  4.0 * atan(1.0);   //pi=PI;
    //*如果阶数n是偶数*/
    if ((n mod 2) = 0) then begin
        n2 := (n div 2) - 1;
        mid := 1;//
    end
    else begin
        n2 := n div 2;//n是奇数,则窗口长度为偶数
        mid := 0;
    end;

    delay := n / 2.0;
    wc1 := 2 * pi * fln;
    wc2 := 2 * pi * fhn;
    SetLength(fir_h, n+2);

    dbg_msg('n='+inttostr(n)+ ' band='+ IntToStr(band)+ ' wn='+IntToStr(wn) +' fs='+IntToStr(fs) + ' fln='+floattostr(fln)+' fhn='+floattostr(fhn));
    case (band) of
        1:  begin
            for i := 0 to n2 do begin
                s := i - delay;
                fir_h[i] := (sin(wc1 * s / fs) / (pi * s)) * window(wn, n + 1, i, beta);//低通,窗口长度=阶数+1，故为n+1
                fir_h[n - i] := fir_h[i];
                dbg_msg('h['+ IntToStr(i)+']=' + FloatToStr(fir_h[i]));
            end;
            if (mid = 1) then begin
                fir_h[n div 2] := wc1 / pi / fs;//n为偶数时，修正中间值系数
                dbg_msg('h['+ IntToStr(n div 2)+']=' + FloatToStr(fir_h[n div 2]));
            end;
        end;
        3:  begin
            for i := 0 to n2 do begin
                s := i - delay;
                fir_h[i] := (sin(wc2 * s / fs) - sin(wc1 * s / fs)) / (pi * s);//带通
                fir_h[i] := fir_h[i] * window(wn, n + 1, i, beta);
                fir_h[n - i] := fir_h[i];
                dbg_msg('h['+ IntToStr(i)+']=' + FloatToStr(fir_h[i]));
            end;
            if (mid = 1) then begin
                fir_h[n div 2] := (wc2 - wc1) / pi/fs;
                dbg_msg('h['+ IntToStr(n div 2)+']=' + FloatToStr(fir_h[n div 2]));
            end;
        end;
        4:  begin
            for i := 0 to n2 do begin
                s := i - delay;
                fir_h[i] := (sin(wc1 * s / fs) + sin(pi * s) - sin(wc2 * s / fs)) / (pi * s);//带阻
                fir_h[i] := fir_h[i] * window(wn, n + 1, i, beta);
                fir_h[n - i] := fir_h[i];
                dbg_msg('h['+ IntToStr(i)+']=' + FloatToStr(fir_h[i]));
            end;
            if (mid = 1) then begin
                fir_h[n div 2] := (wc1/fs + pi - wc2/fs) / pi;
                dbg_msg('h['+ IntToStr(n div 2)+']=' + FloatToStr(fir_h[n div 2]));
            end;
        end;
        2:  begin
            for i := 0 to n2 do begin
                s := i - delay;
                fir_h[i] := (sin(pi * s) - sin(wc1 * s / fs)) / (pi * s);//高通
                fir_h[i] := fir_h[i] * window(wn, n + 1, i, beta);
                fir_h[n - i] := fir_h[i];
                dbg_msg('h['+ IntToStr(i)+']=' + FloatToStr(fir_h[i]));
            end;
            if (mid = 1) then begin
                fir_h[n div 2] := 1.0 - wc1 / pi / fs;
                dbg_msg('h['+ IntToStr(n div 2)+']=' + FloatToStr(fir_h[n div 2]));
            end;
        end;
    end;

    for i := 0 to n -1 do begin
        h^ := fir_h[i];
        inc(pdouble(h));
    end;
end;

procedure fir1_conv(data : pDouble; h_len, data_len: Integer; target: pDouble);
var
    temp : Double;
    i, j : Integer;
begin
    for i := 0 to h_len-1 do begin
        fir1_state[i] := 0;
    end;
    dbg_msg(format('------%.06f', [data^])+format(' h0=%.06f', [fir1_h[0]]));
    for i := 0 to data_len - 1 do begin
        fir1_state[0] := data^;
        temp := 0;
        for j := 0 to h_len - 1 do begin
            temp := temp + fir1_h[j] * fir1_state[j];
        end;

        for j := h_len - 1 downto 0 do begin
            fir1_state[j + 1] := fir1_state[j];
        end;
        dbg_msg(format('%03d:', [i]) + format('%.06f', [data^]) + format('; %.06f', [temp]));
        target^ := temp;
        inc(pdouble(data));
        inc(pdouble(target));
    end;
end;

procedure fir2_conv(data : pDouble; h_len, data_len: Integer; target: pDouble);
var
    temp : Double;
    i, j : Integer;
begin
    for i := 0 to h_len-1 do begin
        fir2_state[i] := 0;
    end;
    dbg_msg(format('------%.06f', [data^])+format(' h0=%.06f', [fir2_h[0]]));
    for i := 0 to data_len - 1 do begin
        fir2_state[0] := data^;
        temp := 0;
        for j := 0 to h_len - 1 do begin
            temp := temp + fir2_h[j] * fir2_state[j];
        end;

        for j := h_len - 1 downto 0 do begin
            fir2_state[j + 1] := fir2_state[j];
        end;
        dbg_msg(format('%03d:', [i]) + format('%.06f', [data^]) + format('; %.06f', [temp]));
        target^ := temp;
        inc(pdouble(data));
        inc(pdouble(target));
    end;
end;
procedure fir3_conv(data : pDouble; h_len, data_len: Integer; target: pDouble);
var
    temp : Double;
    i, j : Integer;
begin
    for i := 0 to h_len-1 do begin
        fir3_state[i] := 0;
    end;
    dbg_msg(format('------%.06f', [data^])+format(' h0=%.06f', [fir3_h[0]]));
    for i := 0 to data_len - 1 do begin
        fir3_state[0] := data^;
        temp := 0;
        for j := 0 to h_len - 1 do begin
            temp := temp + fir3_h[j] * fir3_state[j];
        end;

        for j := h_len - 1 downto 0 do begin
            fir3_state[j + 1] := fir3_state[j];
        end;
        dbg_msg(format('%03d:', [i]) + format('%.06f', [data^]) + format('; %.06f', [temp]));
        target^ := temp;
        inc(pdouble(data));
        inc(pdouble(target));
    end;
end;
procedure TForm_fft.Button6Click(Sender: TObject);
var
    temp : Integer;
    temp1: Double;
begin
    if (TryStrToInt(Edit15.Text, temp) <> true) then
        raise Exception.Create('指定阶数需为大于0的整数');
    if (TryStrToInt(Edit1.Text, temp) <> true) then
        raise Exception.Create('采样率参数出错');
    if (TryStrToFloat(Edit17.Text, temp1) <> true) then
        raise Exception.Create('截止频率fc1参数出错');
    if ComboBox5.ItemIndex > 1 then begin
        if (TryStrToFloat(Edit29.Text, temp1) <> true) then
            raise Exception.Create('截止频率fc2参数出错');
    end;
    if ComboBox4.ItemIndex = 6 then begin
        if (TryStrToFloat(Edit16.Text, temp1) <> true) then
            raise Exception.Create('kaiser参数出错');
    end;
    fir1_fs := StrToInt(Edit1.Text);
    fir1_order := StrToInt(Edit15.Text);
    fir1_fc1 := StrToFloat(Edit17.Text);
    fir1_fc2 := StrToFloat(Edit29.Text);
    fir1_type := ComboBox5.ItemIndex+1;
    fir1_win_type := ComboBox4.ItemIndex+1;
    fir1_kaise := StrToFloat(Edit16.Text);
    SetLength(fir1_state, 0);
    SetLength(fir1_h, 0);
    dbg_msg('fir1_order='+inttostr(fir1_order));
    fir1_order :=  ((fir1_order + 1) div 2 ) * 2;
    Edit15.Text := IntToStr(fir1_order);
    SetLength(fir1_state, fir1_order+2);
    SetLength(fir1_h, fir1_order+2);

    firwin(@fir1_h[0], fir1_order, fir1_type, fir1_win_type, fir1_fs, fir1_fc1, fir1_fc2, fir1_kaise);
    fir1_changed := False;
end;

procedure TForm_fft.ge(Sender: TObject);
begin
    if ComboBox4.ItemIndex = 6 then  begin
        Label36.Visible := True;
        Label37.Visible := True;
        Edit16.Visible := True;
    end
    else begin
        Label36.Visible := False;
        Label37.Visible := False;
        Edit16.Visible := False;
    end;
    fir1_changed := True;
end;

procedure TForm_fft.Edit15Change(Sender: TObject);
begin
    fir1_changed := True;
end;

procedure TForm_fft.Edit17Change(Sender: TObject);
begin
    fir1_changed := True;
end;

procedure TForm_fft.Edit29Change(Sender: TObject);
begin
    fir1_changed := True;
end;

procedure TForm_fft.Edit16Change(Sender: TObject);
begin
    fir1_changed := True;
end;

procedure TForm_fft.Button7Click(Sender: TObject);
var
    bfile : TextFile;
    i ,j, k: Integer;
    s ,s1: string;
    p : PChar;
    temp : Double;
begin
    OpenDialog1.Title := '打开fcf文件';
    OpenDialog1.DefaultExt := '.fcf';
    OpenDialog1.Filter := 'fcf files (.fcf)|*.fcf';
    if OpenDialog1.Execute = true then begin
        if FileExists(OpenDialog1.FileName) = False then
            Exit;
        iir1_input_status := 0;
        Shape1.Brush.Color := clGray;
        Assignfile(bfile, OpenDialog1.FileName);
        reset(bfile);
        i := 0;
        while (not eof(bfile)) do begin
            readln(bfile, s);
            s:= UTF8ToAnsi(String(s));
            s:= Trim(s);

            //Memo1.Lines.Add(s);
            if (s = '') then begin
                Continue;
            end
            else if (s[1] = '%') then begin
                 if (Pos('节数', s) > 0) or (Pos('of Sections', s) > 0) then begin
                     j := Pos(':', s);
                     p := @s[j+1];
                     while True do begin
                        if ((p^ >= '1') and (p^ <= '9')) or (p^ = #13) or (p^ = #10) then begin
                            break;
                        end;
                        inc(p);
                     end;
                     if trystrtoint(p, iir1_order) <> True then begin
                        ShowMessage('获取节数出错');
                        Exit;
                     end;
                     if (iir1_order > 1000) then begin
                        ShowMessage('节数不能大于1000');
                        CloseFile(bfile);
                        Exit;
                     end;
                     SetLength(iir1_Coeffs, iir1_order+1);
                     SetLength(iir1_Statex, iir1_order+1);
                     SetLength(iir1_Statey, iir1_order+1);
                     SetLength(iir1_gain, iir1_order+1);
                 end;
            end
            else begin
                if (Pos('SOS', s) > 0) then begin
                    i := 0;
                    while (i < iir1_order) and (not eof(bfile)) do begin
                        readln(bfile, s);
                        s := Trim(s);
                        if s <> '' then begin
                            j := 0;
                            p := @s[1];
                            while j < 6 do begin
                                if (p^ = '-') or ((p^ >= '0') and (p^ <= '9')) then begin
                                    k := Pos(' ', p);
                                    if k > 0 then
                                        s1 := Copy(p, 0, k)
                                    else
                                        s1 := p;
                                    if TryStrToFloat(string(s1), temp) <> true then begin
                                        ShowMessage('获取系数出错，请检查文件格式');
                                        CloseFile(bfile);
                                        Exit;
                                    end;
                                    iir1_Coeffs[i][j] := temp;
                                    //ShowMessage('iir1_Coeffs['+ IntToStr(i)+']['+IntToStr(j)+ ']: ' + FloatToStr(temp));
                                    inc(j);
                                    if j = 6 then
                                        break;
                                    k := Pos(' ', p);
                                    p := @p[k];
                                end
                                else if (p^ = #10) or (p^ = #13) then begin
                                    break;
                                end;
                                Inc(p);
                            end;
                            if j = 6 then
                                Inc(i)
                            else if (Pos('定标', s) > 0) or (Pos('Scale', s) > 0)then begin
                                ShowMessage('获取系数出错，请检查文件格式');
                                CloseFile(bfile);
                                Exit;
                            end;
                        end;
                    end;
                end;
                if (Pos('定标', s) > 0) or (Pos('Scale', s) > 0)then begin
                    if i <> iir1_order then begin
                        ShowMessage('获取系数出错,系数过少，请检查文件格式');
                        CloseFile(bfile);
                        Exit;
                    end;

                    i := 0;
                    while (i < iir1_order) and (not eof(bfile)) do begin
                        readln(bfile, s);
                        s := Trim(s);
                        if s <> '' then begin
                            j := 0;
                            p := @s[1];
                            while j < Length(s)-2 do begin
                                if (p^ = '-') or ((p^ >= '0') and (p^ <= '9')) then begin
                                    if TryStrToFloat(p, temp) <> true then begin
                                        ShowMessage('获取增益系数出错，请检查文件格式');
                                        CloseFile(bfile);
                                        Exit;
                                    end;
                                    iir1_Gain[i] := temp;
                                    //ShowMessage('iir1_Gain['+IntToStr(i)+ ']: ' + FloatToStr(temp));
                                    inc(i);
                                    break;
                                end
                                else if (p^ = #10) or (p^ = #13) then begin
                                    break;
                                end;
                                Inc(p);
                                inc(j);
                            end;
                        end;
                    end;
                    if i <> iir1_order then begin
                        ShowMessage('获取系数出错,系数过少，请检查文件格式');
                        CloseFile(bfile);
                        Exit;
                    end;

                    iir1_input_status := 1;
                    shape1.Brush.Color := clGreen;
                    Label13.Caption := '文件参数导入成功, 节数: '+ IntToStr(iir1_order);
                    Edit37.Text := OpenDialog1.FileName;
                end;
            end;
        end;
        CloseFile(bfile);
    end;
end;

procedure TForm_fft.Button8Click(Sender: TObject);
var
    bfile : TextFile;
    i ,j, k: Integer;
    s ,s1: string;
    p : PChar;
    temp : Double;
begin
    OpenDialog1.Title := '打开fcf文件';
    OpenDialog1.DefaultExt := '.fcf';
    OpenDialog1.Filter := 'fcf files (.fcf)|*.fcf';
    if OpenDialog1.Execute = true then begin
        if FileExists(OpenDialog1.FileName) = False then
            Exit;
        iir2_input_status := 0;
        Shape2.Brush.Color := clGray;
        Assignfile(bfile, OpenDialog1.FileName);
        reset(bfile);
        i := 0;
        while (not eof(bfile)) do begin
            readln(bfile, s);
            s:= UTF8ToAnsi(String(s));
            s:= Trim(s);

            //Memo1.Lines.Add(s);
            if (s = '') then begin
                Continue;
            end
            else if (s[1] = '%') then begin
                 if (Pos('节数', s) > 0) or (Pos('of Sections', s) > 0) then begin
                     j := Pos(':', s);
                     p := @s[j+1];
                     while True do begin
                        if ((p^ >= '1') and (p^ <= '9')) or (p^ = #13) or (p^ = #10) then begin
                            break;
                        end;
                        inc(p);
                     end;
                     if trystrtoint(p, iir2_order) <> True then begin
                        ShowMessage('获取节数出错');
                        Exit;
                     end;
                     if (iir2_order > 1000) then begin
                        ShowMessage('节数不能大于1000');
                        CloseFile(bfile);
                        Exit;
                     end;
                     SetLength(iir2_Coeffs, iir2_order+1);
                     SetLength(iir2_Statex, iir2_order+1);
                     SetLength(iir2_Statey, iir2_order+1);
                     SetLength(iir2_gain, iir2_order+1);
                 end;
            end
            else begin
                if (Pos('SOS', s) > 0) then begin
                    i := 0;
                    while (i < iir2_order) and (not eof(bfile)) do begin
                        readln(bfile, s);
                        s := Trim(s);
                        if s <> '' then begin
                            j := 0;
                            p := @s[1];
                            while j < 6 do begin
                                if (p^ = '-') or ((p^ >= '0') and (p^ <= '9')) then begin
                                    k := Pos(' ', p);
                                    if k > 0 then
                                        s1 := Copy(p, 0, k)
                                    else
                                        s1 := p;
                                    if TryStrToFloat(string(s1), temp) <> true then begin
                                        ShowMessage('获取系数出错，请检查文件格式');
                                        CloseFile(bfile);
                                        Exit;
                                    end;
                                    iir2_Coeffs[i][j] := temp;
                                    //ShowMessage('iir1_Coeffs['+ IntToStr(i)+']['+IntToStr(j)+ ']: ' + FloatToStr(temp));
                                    inc(j);
                                    if j = 6 then
                                        break;
                                    k := Pos(' ', p);
                                    p := @p[k];
                                end
                                else if (p^ = #10) or (p^ = #13) then begin
                                    break;
                                end;
                                Inc(p);
                            end;
                            if j = 6 then
                                Inc(i)
                            else if (Pos('定标', s) > 0) or (Pos('Scale', s) > 0)then begin
                                ShowMessage('获取系数出错，请检查文件格式');
                                CloseFile(bfile);
                                Exit;
                            end;
                        end;
                    end;
                end;
                if (Pos('定标', s) > 0) or (Pos('Scale', s) > 0)then begin
                    if i <> iir2_order then begin
                        ShowMessage('获取系数出错,系数过少，请检查文件格式');
                        CloseFile(bfile);
                        Exit;
                    end;

                    i := 0;
                    while (i < iir2_order) and (not eof(bfile)) do begin
                        readln(bfile, s);
                        s := Trim(s);
                        if s <> '' then begin
                            j := 0;
                            p := @s[1];
                            while j < Length(s)-2 do begin
                                if (p^ = '-') or ((p^ >= '0') and (p^ <= '9')) then begin
                                    if TryStrToFloat(p, temp) <> true then begin
                                        ShowMessage('获取增益系数出错，请检查文件格式');
                                        CloseFile(bfile);
                                        Exit;
                                    end;
                                    iir2_Gain[i] := temp;
                                    //ShowMessage('iir1_Gain['+IntToStr(i)+ ']: ' + FloatToStr(temp));
                                    inc(i);
                                    break;
                                end
                                else if (p^ = #10) or (p^ = #13) then begin
                                    break;
                                end;
                                Inc(p);
                                inc(j);
                            end;
                        end;
                    end;
                    if i <> iir2_order then begin
                        ShowMessage('获取系数出错,系数过少，请检查文件格式');
                        CloseFile(bfile);
                        Exit;
                    end;

                    iir2_input_status := 1;
                    shape2.Brush.Color := clGreen;
                    Label44.Caption := '文件参数导入成功, 节数: '+ IntToStr(iir2_order);
                    Edit38.Text := OpenDialog1.FileName;
                end;
            end;
        end;
        CloseFile(bfile);
    end;
end;

procedure TForm_fft.filter2_BoxChange(Sender: TObject);
begin
    PageControl1.TabIndex := 0;
    shape2.Brush.Color := clGray;
    get_pra();
    if filter2_Box.ItemIndex = 1 then begin
        PageControl1.TabIndex := 5;
        shape2.Brush.Color := clGreen;
    end
    else if filter2_Box.ItemIndex = 2 then begin  // low pass
        PageControl1.TabIndex := 6;
        shape2.Brush.Color := clGreen;
    end
    else if filter2_Box.ItemIndex = 3 then begin  // iir
        PageControl1.TabIndex := 7;
        if iir2_input_status = 0 then
            Label44.Caption := '还未导入参数'
        else if iir2_input_status = 1 then
            shape2.Brush.Color := clGreen;
    end
    else if filter2_Box.ItemIndex = 4 then begin  // fir
        PageControl1.TabIndex := 8;
        shape2.Brush.Color := clGreen;
    end;
end;

procedure TForm_fft.filter3_BoxChange(Sender: TObject);
begin
    PageControl1.TabIndex := 0;
    shape3.Brush.Color := clGray;
    get_pra();
    if filter3_Box.ItemIndex = 1 then begin
        PageControl1.TabIndex := 9;
        shape3.Brush.Color := clGreen;
    end
    else if filter3_Box.ItemIndex = 2 then begin  // low pass
        PageControl1.TabIndex := 10;
        shape3.Brush.Color := clGreen;
    end
    else if filter3_Box.ItemIndex = 3 then begin  // iir
        PageControl1.TabIndex := 11;
        if iir3_input_status = 0 then
            Label63.Caption := '还未导入参数'
        else if iir3_input_status = 1 then
            shape3.Brush.Color := clGreen;
    end
    else if filter3_Box.ItemIndex = 4 then begin  // fir
        PageControl1.TabIndex := 12;
        shape3.Brush.Color := clGreen;
    end;
end;

procedure TForm_fft.Edit21Change(Sender: TObject);
begin
    fir2_changed := True;
end;

procedure TForm_fft.ComboBox2Change(Sender: TObject);
begin
    if ComboBox2.ItemIndex > 1 then begin
        Label51.Visible := True;
        Edit22.Visible := True;
    end
    else begin
        Label51.Visible := False;
        Edit22.Visible := False;
    end;
    fir2_type := ComboBox2.ItemIndex;
    fir2_changed := True;
end;

procedure TForm_fft.ComboBox8Change(Sender: TObject);
begin
    if ComboBox8.ItemIndex = 6 then  begin
        Label46.Visible := True;
        Label47.Visible := True;
        Edit21.Visible := True;
    end
    else begin
        Label46.Visible := False;
        Label47.Visible := False;
        Edit21.Visible := False;
    end;
    fir2_changed := True;
end;

procedure TForm_fft.Edit24Change(Sender: TObject);
begin
    fir2_changed := True;
end;

procedure TForm_fft.Edit23Change(Sender: TObject);
begin
    fir2_changed := True;
end;

procedure TForm_fft.Edit22Change(Sender: TObject);
begin
    fir2_changed := True;
end;

procedure TForm_fft.Button9Click(Sender: TObject);
var
    temp : Integer;
    temp1: Double;
begin
    if (TryStrToInt(Edit24.Text, temp) <> true) then
        raise Exception.Create('指定阶数需为大于0的整数');
    if (TryStrToInt(Edit1.Text, temp) <> true) then
        raise Exception.Create('采样率参数出错');
    if (TryStrToFloat(Edit23.Text, temp1) <> true) then
        raise Exception.Create('截止频率fc1参数出错');
    if ComboBox2.ItemIndex > 1 then begin
        if (TryStrToFloat(Edit22.Text, temp1) <> true) then
            raise Exception.Create('截止频率fc2参数出错');
    end;
    if ComboBox8.ItemIndex = 6 then begin
        if (TryStrToFloat(Edit21.Text, temp1) <> true) then
            raise Exception.Create('kaiser参数出错');
    end;
    fir2_fs := StrToInt(Edit1.Text);
    fir2_order := StrToInt(Edit24.Text);
    fir2_fc1 := StrToFloat(Edit23.Text);
    fir2_fc2 := StrToFloat(Edit22.Text);
    fir2_type := ComboBox2.ItemIndex+1;
    fir2_win_type := ComboBox8.ItemIndex+1;
    fir2_kaise := StrToFloat(Edit21.Text);
    SetLength(fir2_state, 0);
    SetLength(fir2_h, 0);

    fir2_order :=  ((fir2_order + 1) div 2 ) * 2;
    Edit24.Text := IntToStr(fir2_order);
    SetLength(fir2_state, fir2_order+2);
    SetLength(fir2_h, fir2_order+2);

    firwin(@fir2_h[0], fir2_order, fir2_type, fir2_win_type, fir2_fs, fir2_fc1, fir2_fc2, fir2_kaise);
    fir2_changed := False;
end;

procedure TForm_fft.Button11Click(Sender: TObject);
var
    temp : Integer;
    temp1: Double;
begin
    if (TryStrToInt(Edit35.Text, temp) <> true) then
        raise Exception.Create('指定阶数需为大于0的整数');
    if (TryStrToInt(Edit1.Text, temp) <> true) then
        raise Exception.Create('采样率参数出错');
    if (TryStrToFloat(Edit34.Text, temp1) <> true) then
        raise Exception.Create('截止频率fc1参数出错');
    if ComboBox9.ItemIndex > 1 then begin
        if (TryStrToFloat(Edit33.Text, temp1) <> true) then
            raise Exception.Create('截止频率fc2参数出错');
    end;
    if ComboBox10.ItemIndex = 6 then begin
        if (TryStrToFloat(Edit16.Text, temp1) <> true) then
            raise Exception.Create('kaiser参数出错');
    end;
    fir3_fs := StrToInt(Edit1.Text);
    fir3_order := StrToInt(Edit35.Text);
    fir3_fc1 := StrToFloat(Edit34.Text);
    fir3_fc2 := StrToFloat(Edit33.Text);
    fir3_type := ComboBox9.ItemIndex+1;
    fir3_win_type := ComboBox10.ItemIndex+1;
    fir3_kaise := StrToFloat(Edit32.Text);
    SetLength(fir3_state, 0);
    SetLength(fir3_h, 0);

    fir3_order :=  ((fir3_order + 1) div 2 ) * 2;
    Edit15.Text := IntToStr(fir3_order);
    SetLength(fir3_state, fir3_order+2);
    SetLength(fir3_h, fir3_order+2);

    firwin(@fir3_h[0], fir3_order, fir3_type, fir3_win_type, fir3_fs, fir3_fc1, fir3_fc2, fir3_kaise);
    fir3_changed := False;
end;

procedure TForm_fft.ComboBox9Change(Sender: TObject);
begin
    if ComboBox9.ItemIndex > 1 then begin
        Label70.Visible := True;
        Edit33.Visible := True;
    end
    else begin
        Label70.Visible := False;
        Edit33.Visible := False;
    end;
    fir3_type := ComboBox9.ItemIndex;
    fir3_changed := True;
end;

procedure TForm_fft.Edit35Change(Sender: TObject);
begin
    fir3_changed := True;
end;

procedure TForm_fft.Edit34Change(Sender: TObject);
begin
    fir3_changed := True;
end;

procedure TForm_fft.Edit33Change(Sender: TObject);
begin
    fir3_changed := True;
end;

procedure TForm_fft.Edit32Change(Sender: TObject);
begin
    fir3_changed := True;
end;

procedure TForm_fft.ComboBox10Change(Sender: TObject);
begin
    if ComboBox10.ItemIndex = 6 then  begin
        Label65.Visible := True;
        Label66.Visible := True;
        Edit32.Visible := True;
    end
    else begin
        Label65.Visible := False;
        Label66.Visible := False;
        Edit32.Visible := False;
    end;
    fir3_changed := True;
end;

procedure TForm_fft.ComboBox4Change(Sender: TObject);
begin
    if ComboBox4.ItemIndex = 6 then  begin
        Label36.Visible := True;
        Label37.Visible := True;
        Edit16.Visible := True;
    end
    else begin
        Label36.Visible := False;
        Label37.Visible := False;
        Edit16.Visible := False;
    end;
    fir1_changed := True;
end;

procedure TForm_fft.Button10Click(Sender: TObject);
var
    bfile : TextFile;
    i ,j, k: Integer;
    s ,s1: string;
    p : PChar;
    temp : Double;
begin
    OpenDialog1.Title := '打开fcf文件';
    OpenDialog1.DefaultExt := '.fcf';
    OpenDialog1.Filter := 'fcf files (.fcf)|*.fcf';
    if OpenDialog1.Execute = true then begin
        if FileExists(OpenDialog1.FileName) = False then
            Exit;
        iir3_input_status := 0;
        Shape3.Brush.Color := clGray;
        Assignfile(bfile, OpenDialog1.FileName);
        reset(bfile);
        i := 0;
        while (not eof(bfile)) do begin
            readln(bfile, s);
            s:= UTF8ToAnsi(String(s));
            s:= Trim(s);

            //Memo1.Lines.Add(s);
            if (s = '') then begin
                Continue;
            end
            else if (s[1] = '%') then begin
                 if (Pos('节数', s) > 0) or (Pos('of Sections', s) > 0) then begin
                     j := Pos(':', s);
                     p := @s[j+1];
                     while True do begin
                        if ((p^ >= '1') and (p^ <= '9')) or (p^ = #13) or (p^ = #10) then begin
                            break;
                        end;
                        inc(p);
                     end;
                     if trystrtoint(p, iir3_order) <> True then begin
                        ShowMessage('获取节数出错');
                        Exit;
                     end;
                     if (iir3_order > 1000) then begin
                        ShowMessage('节数不能大于1000');
                        CloseFile(bfile);
                        Exit;
                     end;
                     SetLength(iir3_Coeffs, iir3_order+1);
                     SetLength(iir3_Statex, iir3_order+1);
                     SetLength(iir3_Statey, iir3_order+1);
                     SetLength(iir3_gain, iir3_order+1);
                 end;
            end
            else begin
                if (Pos('SOS', s) > 0) then begin
                    i := 0;
                    while (i < iir3_order) and (not eof(bfile)) do begin
                        readln(bfile, s);
                        s := Trim(s);
                        if s <> '' then begin
                            j := 0;
                            p := @s[1];
                            while j < 6 do begin
                                if (p^ = '-') or ((p^ >= '0') and (p^ <= '9')) then begin
                                    k := Pos(' ', p);
                                    if k > 0 then
                                        s1 := Copy(p, 0, k)
                                    else
                                        s1 := p;
                                    if TryStrToFloat(string(s1), temp) <> true then begin
                                        ShowMessage('获取系数出错，请检查文件格式');
                                        CloseFile(bfile);
                                        Exit;
                                    end;
                                    iir3_Coeffs[i][j] := temp;
                                    //ShowMessage('iir1_Coeffs['+ IntToStr(i)+']['+IntToStr(j)+ ']: ' + FloatToStr(temp));
                                    inc(j);
                                    if j = 6 then
                                        break;
                                    k := Pos(' ', p);
                                    p := @p[k];
                                end
                                else if (p^ = #10) or (p^ = #13) then begin
                                    break;
                                end;
                                Inc(p);
                            end;
                            if j = 6 then
                                Inc(i)
                            else if (Pos('定标', s) > 0) or (Pos('Scale', s) > 0)then begin
                                ShowMessage('获取系数出错，请检查文件格式');
                                CloseFile(bfile);
                                Exit;
                            end;
                        end;
                    end;
                end;
                if (Pos('定标', s) > 0) or (Pos('Scale', s) > 0)then begin
                    if i <> iir3_order then begin
                        ShowMessage('获取系数出错,系数过少，请检查文件格式');
                        CloseFile(bfile);
                        Exit;
                    end;

                    i := 0;
                    while (i < iir3_order) and (not eof(bfile)) do begin
                        readln(bfile, s);
                        s := Trim(s);
                        if s <> '' then begin
                            j := 0;
                            p := @s[1];
                            while j < Length(s)-2 do begin
                                if (p^ = '-') or ((p^ >= '0') and (p^ <= '9')) then begin
                                    if TryStrToFloat(p, temp) <> true then begin
                                        ShowMessage('获取增益系数出错，请检查文件格式');
                                        CloseFile(bfile);
                                        Exit;
                                    end;
                                    iir3_Gain[i] := temp;
                                    //ShowMessage('iir1_Gain['+IntToStr(i)+ ']: ' + FloatToStr(temp));
                                    inc(i);
                                    break;
                                end
                                else if (p^ = #10) or (p^ = #13) then begin
                                    break;
                                end;
                                Inc(p);
                                inc(j);
                            end;
                        end;
                    end;
                    if i <> iir3_order then begin
                        ShowMessage('获取系数出错,系数过少，请检查文件格式');
                        CloseFile(bfile);
                        Exit;
                    end;

                    iir3_input_status := 1;
                    shape3.Brush.Color := clGreen;
                    Label63.Caption := '文件参数导入成功, 节数: '+ IntToStr(iir3_order);
                    Edit39.Text := OpenDialog1.FileName;
                end;
            end;
        end;
        CloseFile(bfile);
    end;
end;

procedure TForm_fft.shape1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    filter1_BoxChange(Self);
end;

procedure TForm_fft.Shape2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    filter2_BoxChange(Self);
end;

procedure TForm_fft.Shape3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    filter3_BoxChange(Self);
end;

procedure TForm_fft.Chart2MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
    i : Integer;
    r : Double;
begin
    {i := Series2.GetCursorValueIndex;
    r := Series2.MaxXValue;
    r := Series2.MaxYValue;
    Memo1.Lines.Add(IntToStr(i) + ' - ' + FloatToStr(r))}
end;

procedure TForm_fft.FormCreate(Sender: TObject);
var
    FileName : string;
    MyIniFile : TIniFile;
begin
    fft_have_open_flag := True;
    FileName := ExtractFilePath(ParamStr(0)) + 'System.ini';
    MyIniFile := TIniFile.Create(FileName);
    Edit1.Text := MyIniFile.readstring('FFT', 'fft_fs', '250');
    Edit2.Text := MyIniFile.readstring('FFT', 'fft_n', '256');
    Edit8.Text := MyIniFile.readstring('FFT', 'fft_dbn', '24');
    Edit40.Text := MyIniFile.readstring('FFT', 'fft_dbd', '20');

    csvFile := TStringList.Create;


    MyIniFile.Destroy;
end;

procedure TForm_fft.FormClose(Sender: TObject; var Action: TCloseAction);
var
    FileName : string;
    MyIniFile : TIniFile;
begin
    fft_have_open_flag := False;
    FileName := ExtractFilePath(ParamStr(0)) + 'System.ini';
    MyIniFile := TIniFile.Create(FileName);
    MyIniFile.writestring('FFT', 'fft_fs', Edit1.Text);
    MyIniFile.writestring('FFT', 'fft_n', Edit2.Text);
    MyIniFile.writestring('FFT', 'fft_dbn', Edit8.Text);
    MyIniFile.writestring('FFT', 'fft_dbd', Edit40.Text);

    MyIniFile.Destroy;

    If not varIsEmpty(MsExcel) then begin
        MsExcel.DisplayAlerts:=false;
        MsExcel.WorkBooks.close;
        MsExcel.quit;
        MsExcel:= Unassigned;//退出
        FindVarData(MsExcel)^.VType := varEmpty;
    end;
    csvFile.Free;
end;

procedure TForm_fft.Button12Click(Sender: TObject);
begin
    if OpenDialog1.Execute then begin
        if FileExists(OpenDialog1.FileName) = False then
            Exit;
        Edit36.Text := OpenDialog1.FileName;
    end;
end;

procedure TForm_fft.CheckBox1Click(Sender: TObject);
begin
    CheckBox4.Visible := CheckBox1.Checked;
end;

procedure TForm_fft.FormShow(Sender: TObject);
begin
    fft_have_open_flag := True;
end;

procedure TForm_fft.Button15Click(Sender: TObject);
const
  BeginRow= 1;
  BeginCol= 1;
var
  strdata , strdata2: string;
  iRE, I , k,maxrow, maxcol:Integer;
  inum,iRow,iCol,read_row :Integer; //列、行
  pdata : PChar;
begin
    iRow := StrToInt(Edit44.Text);
    iCol := StrToInt(Edit43.Text);
    inum := StrToInt(Edit42.Text);
    read_row := StrToInt(Edit45.Text);

    if csv_file_flag <> True then begin
        If varIsEmpty(MsExcel) then begin
           MessageBox(self.Handle,'文件未打开！','警告！',0);
           FindVarData(MsExcel)^.VType := varEmpty;
           exit;
        end;
        maxrow := (msExcel.WorkSheets[1].UsedRange.Rows.Count);
        //ShowMessage('max row='+ IntToStr(maxrow));
        //maxcol := (msExcel.WorkSheets[1].UsedRange.cols.Count);
        //开始从EXCEL文件读取相关信息 ,并导入到数据库中的表
        try
            if (maxrow - iRow + 1) < read_row then begin
                read_row := (maxrow - iRow + 1);
            end;
            I := 0;
            Memo1.Clear;
            strdata2 := '';
            while (trim(msExcel.WorkSheets[inum].Cells[iCol,iRow].value) <> '') and (i < read_row) do begin
                strdata := msExcel.WorkSheets[inum].Cells[iCol,iRow].value;
                //ShowMessage(strdata);
                 strdata2 := strdata2 + #13#10 + strdata;
                iRow:=iRow + 1;
                I := I + 1;
                Application.ProcessMessages;//防止进程阻塞
            end;

            //If not varIsEmpty(MsExcel) then
            //    MsExcel.quit;
        except
            MessageBox(self.Handle,'数据导入失败！','警告！',0);;
            //If not varIsEmpty(MsExcel) then
            //    MsExcel.quit;
            Exit;
        end;//end try
    end
    else begin

        //ShowMessage('csvFile.Count=' + IntToStr(csvFile.Count));
        if (iCol > csvFile.Count) then
            Exit;
        strdata2 := csvFile[iCol-1];
        i := 1;
        Pdata := @strdata2[1];
        while (i < iRow) do begin
            k := Pos(',', Pdata);
            
            if (k > 0) then
              inc(pdata, k);
            //ShowMessage('k='+inttostr(k)+' ' + pdata);
            inc(i);
        end;
        //strdata2 := '';
        strdata2 := pdata;
        //ShowMessage('k='+inttostr(k)+'-----------' + pdata);
        Memo1.Clear;
    end;

    Memo1.Lines.Add(strdata2);

    Button2.OnClick(self);
end;

procedure TForm_fft.Button16Click(Sender: TObject);
var
  strdata : string;
  iRE, I , maxrow, maxcol:Integer;
  iRow,iCol :Integer; //列、行
  MsExcel_temp: Variant;
begin

    try
        //OpenDialog1.Title := '打开fcf文件';
        //OpenDialog1.DefaultExt := '.xls';
        OpenDialog1.Filter := 'xls files (*.xlsx;*.xls;*.csv)|*.xlsx;*.xls;*.csv';
        if not OpenDialog1.Execute then begin
            Exit;
        end;
        if (ExtractFileExt(OpenDialog1.FileName) <> '.xls') and (ExtractFileExt(OpenDialog1.FileName) <> '.xlsx')
              and (ExtractFileExt(OpenDialog1.FileName) <> '.csv')then begin
            MessageBox(0, '请选择正确的文件',PChar('警告！'),MB_OK or MB_ICONWARNING);
            Exit;
        end;
        if Edit41.Text = OpenDialog1.FileName then begin
            Exit;
        end;
        if (ExtractFileExt(OpenDialog1.FileName) = '.csv') then begin
            Edit41.Text := OpenDialog1.FileName;
            Exit;
        end
        else begin
            try
                MsExcel_temp := CreateOleObject('Excel.Application');
                If not varIsEmpty(MsExcel_temp) then begin
                    MsExcel_temp.DisplayAlerts:=false;
                    MsExcel_temp.WorkBooks.close;
                    MsExcel_temp.quit;
                    MsExcel_temp:= Unassigned;//退出
                    FindVarData(MsExcel_temp)^.VType := varEmpty;
                end;
                If not varIsEmpty(MsExcel) then begin
                    MsExcel.DisplayAlerts:=false;
                    MsExcel.WorkBooks.close;
                    MsExcel.quit;
                    MsExcel:= Unassigned;//退出
                    FindVarData(MsExcel)^.VType := varEmpty;
                end;

                Shape4.Brush.Color := clGray;
            except
                MessageBox(self.Handle,'本机未安装Excel, 请先安装Excel再执行本操作！      ','警告！',0);;
                Exit;
            end;
        end;
        Edit41.Text := OpenDialog1.FileName;
        //MsExcel.visible := False;
    except
        If not varIsEmpty(MsExcel_temp) then begin
            MsExcel_temp.DisplayAlerts:=false;
            MsExcel_temp.WorkBooks.close;
            MsExcel_temp.quit;
            MsExcel_temp:= Unassigned;//退出
            FindVarData(MsExcel_temp)^.VType := varEmpty;
        end;
        Exit;
    end;

end;

procedure TForm_fft.CheckBox5Click(Sender: TObject);
begin
    if CheckBox5.Checked then begin
        Timer1.Interval := StrToInt(Edit47.Text)*1000;
        Timer1.Enabled := True;
    end
    else begin
        Timer1.Enabled := False;
    end;
end;

procedure TForm_fft.Timer1Timer(Sender: TObject);
begin
    Button15Click(Self);
    //ShowMessage('1');
    Edit43.Text := IntToStr(StrToInt(Edit43.Text) + 1);
end;

procedure TForm_fft.Button17Click(Sender: TObject);
begin
    if Button17.Caption = '打开文件' then begin
        if (ExtractFileExt(Edit41.Text) <> '.xls') and (ExtractFileExt(Edit41.Text) <> '.xlsx')
            and (ExtractFileExt(Edit41.Text) <> '.csv')then begin
            MessageBox(0, '请选择正确的文件',PChar('警告!'),MB_OK or MB_ICONWARNING);
            Exit;
        end;
        if (ExtractFileExt(Edit41.Text) = '.csv')then begin
            csv_file_flag := True;
            //ShowMessage(Edit41.Text);
            csvFile.Clear;
            csvFile.Free;
            csvFile := TStringList.Create;

            csvFile.LoadFromFile(Edit41.Text);

            Shape4.Brush.Color := clGreen;
            Button17.Caption := '关闭文件';

        end
        else begin
            csv_file_flag := False;

            If not varIsEmpty(MsExcel) then begin
                MsExcel.DisplayAlerts:=false;
                MsExcel.WorkBooks.close;
                MsExcel.quit;
                MsExcel:= Unassigned;//退出
                FindVarData(MsExcel)^.VType := varEmpty;
                Shape4.Brush.Color := clGray;
            end;
            try
                MsExcel := CreateOleObject('Excel.Application');
                If not varIsEmpty(MsExcel) then begin
                    Shape4.Brush.Color := clGreen;
                end
                else begin
                    MsExcel.DisplayAlerts:=false;
                    MsExcel.WorkBooks.close;
                    MsExcel.quit;
                    MsExcel:= Unassigned;//退出
                    FindVarData(MsExcel)^.VType := varEmpty;
                    Shape4.Brush.Color := clGray;
                end;
            except
                MessageBox(self.Handle,'本机未安装Excel, 请先安装Excel再执行本操作!','警告!',0);;
                Exit;
            end;
            MsExcel.WorkBooks.Open(Edit41.Text);
            if not varIsEmpty(MsExcel) then begin
                Shape4.Brush.Color := clGreen;
                Button17.Caption := '关闭文件';
            end
            else begin
                Shape4.Brush.Color := clGray;
                Button17.Caption := '打开文件';
            end;
        end;
    end
    else begin
        if csv_file_flag <> True then begin
            If not varIsEmpty(MsExcel) then begin
                MsExcel.DisplayAlerts:=false;
                MsExcel.WorkBooks.close;
                MsExcel.quit;
                MsExcel:= Unassigned;//退出
                FindVarData(MsExcel)^.VType := varEmpty;
                Shape4.Brush.Color := clGray;
                //winexec('cmd.exe /c taskkill /f /t /im EXCEL.EXE',sw_hide);
            end;
        end;
        
        Button17.Caption := '打开文件';
    end;

end;

procedure TForm_fft.Button18Click(Sender: TObject);
begin
    Button4.OnClick(Self);
end;

end.
