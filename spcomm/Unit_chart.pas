unit Unit_chart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeEngine, Series, ExtCtrls, TeeProcs, Chart, StdCtrls, unit_fft;

type
  TForm9 = class(TForm)
    Chart1: TChart;
    Series1: TLineSeries;
    OpenDialog1: TOpenDialog;
    GroupBox1: TGroupBox;
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Series2: TLineSeries;
    Series3: TLineSeries;
    Label1: TLabel;
    series_cbx: TComboBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    Label2: TLabel;
    CheckBox4: TCheckBox;
    Edit1: TEdit;
    Label3: TLabel;
    Edit2: TEdit;
    Label4: TLabel;
    Edit3: TEdit;
    Label5: TLabel;
    Edit4: TEdit;
    Label6: TLabel;
    Edit5: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure series_cbxChange(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure Label8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form9: TForm9;

implementation

{$R *.dfm}

var
    series_id:Integer;
    kal_lastp, kal_nowp, kal_out, kal_kg, kal_q, kal_r : double;

function kalman_filter(fdata : Double):Double;
begin
    kal_nowp := kal_lastp + kal_q;
    kal_kg := kal_nowp / (kal_nowp + kal_r);
    kal_out := kal_out + kal_kg * (fdata - kal_out);
    kal_lastp := (1 - kal_kg) * kal_nowp;

    Result := kal_out;
end;

procedure TForm9.Button1Click(Sender: TObject);
var
    txtFile:TextFile;
    s:String;
    pMyFile:file;
    i, flag : Integer;
    result : double;
    x_min,y_min, x_max, y_max : double;
begin
    if OpenDialog1.Execute then begin
        if FileExists(OpenDialog1.FileName) = False then
            Exit;
        Assignfile(txtFile, OpenDialog1.FileName);
        Reset(txtFile);
        i := 0;
        flag := 0;
        if CheckBox4.Checked = True then begin
            if (TryStrToFloat(Edit1.Text, Result) <> true) then begin
                CloseFile(txtFile);
                raise Exception.Create('LastP 参数有误');
            end;
            kal_lastp := Result;
            if (TryStrToFloat(Edit2.Text, Result) <> true) then begin
                CloseFile(txtFile);
                raise Exception.Create('NowP 参数有误');
            end;
            kal_nowp := Result;
            kal_out := 0;
            if (TryStrToFloat(Edit3.Text, Result) <> true) then begin
                CloseFile(txtFile);
                raise Exception.Create('Kg 参数有误');
            end;
            kal_kg := Result;
            if (TryStrToFloat(Edit4.Text, Result) <> true) then begin
                CloseFile(txtFile);
                raise Exception.Create('Q 参数有误');
            end;
            kal_q := Result;
            if (TryStrToFloat(Edit5.Text, Result) <> true) then begin
                CloseFile(txtFile);
                raise Exception.Create('R 参数有误');
            end;
            kal_r := Result;
        end;
        Chart1.Series[series_id].Clear;
        while not eof(txtFile) do begin
            readln(txtFile, s);//读取一行保存到字符串s中
            s := Trim(s);
            if s <> '' then begin
                if TryStrToFloat(s, Result) = True then begin
                    if CheckBox4.Checked = True then begin
                        Result := kalman_filter(Result);
                    end;
                    Chart1.Series[series_id].AddXY(i, Result);
                    inc(i);
                end
                else begin
                    if (flag = 0) then begin
                        ShowMessage('第'+inttostr(i)+'行数据有误');
                        flag := 1;
                    end;
                end;
            end;
        end;
        CloseFile(txtFile);
    end;
    Chart1.BottomAxis.Automatic := True;
    Chart1.LeftAxis.Automatic := True;
    Chart1.Refresh;
    y_min := Chart1.LeftAxis.Minimum;
    y_max := Chart1.LeftAxis.Maximum;
    Chart1.LeftAxis.Automatic := false;
    Chart1.LeftAxis.Minimum := y_min - (y_max-y_min)/30;
    Chart1.LeftAxis.Maximum := y_max + (y_max-y_min)/30;
end;

procedure TForm9.Button2Click(Sender: TObject);
var
    txtFile:TextFile;
    s:String;
    pMyFile:file;
    i, j, flag : Integer;
    Result:Double;
    x_min,y_min, x_max, y_max : double;
begin
    if Memo1.Lines.Count > 1 then begin
        if CheckBox4.Checked = True then begin
            if (TryStrToFloat(Edit1.Text, Result) <> true) then
                raise Exception.Create('LastP 参数有误');
            kal_lastp := Result;
            if (TryStrToFloat(Edit2.Text, Result) <> true) then
                raise Exception.Create('NowP 参数有误');
            kal_nowp := Result;
            kal_out := 0;
            if (TryStrToFloat(Edit3.Text, Result) <> true) then
                raise Exception.Create('Kg 参数有误');
            kal_kg := Result;
            if (TryStrToFloat(Edit4.Text, Result) <> true) then
                raise Exception.Create('Q 参数有误');
            kal_q := Result;
            if (TryStrToFloat(Edit5.Text, Result) <> true) then
                raise Exception.Create('R 参数有误');
            kal_r := Result;
        end;
        i := 0;
        flag := 0;
        j := 0;
        Chart1.Series[series_id].Clear;
        while (i < Memo1.Lines.Count) do
        begin
            if (Memo1.Lines[i] <> '') then begin
                s := Trim(Memo1.Lines[i]);
                if (TryStrToFloat(s, Result) = True) then begin
                    if CheckBox4.Checked = True then begin
                        Result := kalman_filter(Result);
                    end;
                    Chart1.Series[series_id].AddXY(j, Result);
                    inc(j);
                end
                else begin
                    if (flag = 0) then begin
                        ShowMessage('第'+inttostr(i)+'行数据有误');
                        flag := 1;
                    end;
                end;
            end;
            inc(i);
        end;
    end;
    Chart1.BottomAxis.Automatic := True;
    Chart1.LeftAxis.Automatic := True;
    Chart1.Refresh;
    y_min := Chart1.LeftAxis.Minimum;
    y_max := Chart1.LeftAxis.Maximum;
    Chart1.LeftAxis.Automatic := false;
    Chart1.LeftAxis.Minimum := y_min - (y_max-y_min)/30;
    Chart1.LeftAxis.Maximum := y_max + (y_max-y_min)/30;
end;

procedure TForm9.Button3Click(Sender: TObject);
begin
    Chart1.Series[series_id].Clear;
end;

procedure TForm9.Button4Click(Sender: TObject);
begin
    Memo1.Clear;
end;

procedure TForm9.FormCreate(Sender: TObject);
begin
    series_id := series_cbx.ItemIndex;
end;

procedure TForm9.series_cbxChange(Sender: TObject);
begin
    series_id := series_cbx.ItemIndex;
end;

procedure TForm9.CheckBox1Click(Sender: TObject);
var
    x_min,y_min, x_max, y_max : double;
begin
    Chart1.Series[0].Active := CheckBox1.Checked;
    Chart1.BottomAxis.Automatic := True;
    Chart1.LeftAxis.Automatic := True;
    Chart1.Refresh;
    y_min := Chart1.LeftAxis.Minimum;
    y_max := Chart1.LeftAxis.Maximum;
    Chart1.LeftAxis.Automatic := false;
    Chart1.LeftAxis.Minimum := y_min - (y_max-y_min)/30;
    Chart1.LeftAxis.Maximum := y_max + (y_max-y_min)/30;
end;

procedure TForm9.CheckBox2Click(Sender: TObject);
var
    x_min,y_min, x_max, y_max : double;
begin
    Chart1.Series[1].Active := CheckBox2.Checked;
    Chart1.BottomAxis.Automatic := True;
    Chart1.LeftAxis.Automatic := True;
    Chart1.Refresh;
    y_min := Chart1.LeftAxis.Minimum;
    y_max := Chart1.LeftAxis.Maximum;
    Chart1.LeftAxis.Automatic := false;
    Chart1.LeftAxis.Minimum := y_min - (y_max-y_min)/30;
    Chart1.LeftAxis.Maximum := y_max + (y_max-y_min)/30;
end;

procedure TForm9.CheckBox3Click(Sender: TObject);
var
    x_min,y_min, x_max, y_max : double;
begin
    Chart1.Series[2].Active := CheckBox3.Checked;
    Chart1.BottomAxis.Automatic := True;
    Chart1.LeftAxis.Automatic := True;
    Chart1.Refresh;
    y_min := Chart1.LeftAxis.Minimum;
    y_max := Chart1.LeftAxis.Maximum;
    Chart1.LeftAxis.Automatic := false;
    Chart1.LeftAxis.Minimum := y_min - (y_max-y_min)/30;
    Chart1.LeftAxis.Maximum := y_max + (y_max-y_min)/30;
end;

procedure TForm9.Label8Click(Sender: TObject);
begin
    ShowMessage('LastP:     上次估算协方差'+ #10#13
              +'Now_P;      当前估算协方差'+ #10#13
              +'Kg;         卡尔曼增益'+ #10#13
              +'Q;          过程噪声协方差'+ #10#13
              +'R;          观测噪声协方差'+ #10#13
              );
end;

end.
