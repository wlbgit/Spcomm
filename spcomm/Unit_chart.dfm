object Form9: TForm9
  Left = 201
  Top = 162
  Width = 1208
  Height = 510
  Caption = 'chart'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Chart1: TChart
    Left = 0
    Top = 0
    Width = 1200
    Height = 412
    AnimatedZoom = True
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    MarginBottom = 0
    MarginLeft = 0
    MarginRight = 0
    MarginTop = 0
    Title.Alignment = taLeftJustify
    Title.Text.Strings = (
      '')
    Title.Visible = False
    BottomAxis.AxisValuesFormat = '#,##0.######'
    BottomAxis.LabelsSeparation = 1
    DepthAxis.AxisValuesFormat = '#,##0.######'
    DepthAxis.LabelsSeparation = 1
    LeftAxis.AxisValuesFormat = '#,##0.######'
    LeftAxis.LabelsSeparation = 1
    Legend.Alignment = laTop
    Legend.ColorWidth = 50
    Legend.DividingLines.Style = psDot
    Legend.LegendStyle = lsSeries
    Legend.ShadowSize = 0
    Legend.TextStyle = ltsPlain
    Legend.TopPos = 0
    Legend.VertMargin = 1
    RightAxis.AxisValuesFormat = '#,##0.######'
    RightAxis.LabelsSeparation = 1
    RightAxis.Visible = False
    TopAxis.AxisValuesFormat = '#,##0.######'
    TopAxis.LabelsSeparation = 1
    TopAxis.Visible = False
    View3D = False
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Series1: TLineSeries
      HorizAxis = aBothHorizAxis
      Marks.ArrowLength = 8
      Marks.Style = smsXValue
      Marks.Visible = False
      SeriesColor = clRed
      VertAxis = aBothVertAxis
      Dark3D = False
      Pointer.HorizSize = 1
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.VertSize = 1
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
    object Series2: TLineSeries
      HorizAxis = aBothHorizAxis
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      VertAxis = aBothVertAxis
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
    object Series3: TLineSeries
      HorizAxis = aBothHorizAxis
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlue
      VertAxis = aBothVertAxis
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 412
    Width = 1200
    Height = 64
    Align = alBottom
    TabOrder = 1
    object Label1: TLabel
      Left = 193
      Top = 26
      Width = 61
      Height = 13
      AutoSize = False
      Caption = #26354#32447#24207#21495
    end
    object Label2: TLabel
      Left = 1011
      Top = 25
      Width = 69
      Height = 23
      AutoSize = False
      Caption = #26174#31034#26354#32447
    end
    object Label3: TLabel
      Left = 654
      Top = 16
      Width = 44
      Height = 13
      AutoSize = False
      Caption = 'LastP'
      Visible = False
    end
    object Label4: TLabel
      Left = 753
      Top = 16
      Width = 35
      Height = 13
      Caption = 'Now_P'
      Visible = False
    end
    object Label5: TLabel
      Left = 653
      Top = 43
      Width = 34
      Height = 13
      AutoSize = False
      Caption = 'Kg'
      Visible = False
    end
    object Label6: TLabel
      Left = 753
      Top = 43
      Width = 20
      Height = 13
      AutoSize = False
      Caption = 'Q'
      Visible = False
    end
    object Label7: TLabel
      Left = 849
      Top = 43
      Width = 23
      Height = 13
      AutoSize = False
      Caption = 'R'
      Visible = False
    end
    object Label8: TLabel
      Left = 878
      Top = 43
      Width = 48
      Height = 13
      AutoSize = False
      Caption = 'help  ?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
      OnClick = Label8Click
    end
    object Button1: TButton
      Left = 340
      Top = 35
      Width = 75
      Height = 25
      Caption = #25991#20214#36755#20837
      TabOrder = 0
      OnClick = Button1Click
    end
    object Memo1: TMemo
      Left = 2
      Top = 15
      Width = 185
      Height = 47
      Align = alLeft
      ScrollBars = ssVertical
      TabOrder = 1
    end
    object Button2: TButton
      Left = 340
      Top = 9
      Width = 75
      Height = 25
      Caption = #25163#21160#36755#20837
      TabOrder = 2
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 420
      Top = 9
      Width = 75
      Height = 25
      Caption = #28165#38500#26354#32447
      TabOrder = 3
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 420
      Top = 35
      Width = 75
      Height = 25
      Caption = #28165#38500#25968#25454
      TabOrder = 4
      OnClick = Button4Click
    end
    object series_cbx: TComboBox
      Left = 259
      Top = 22
      Width = 66
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 5
      Text = '1'
      OnChange = series_cbxChange
      Items.Strings = (
        '1'
        '2'
        '3')
    end
    object CheckBox1: TCheckBox
      Left = 1094
      Top = 7
      Width = 97
      Height = 17
      Caption = 'Series1'
      Checked = True
      State = cbChecked
      TabOrder = 6
      OnClick = CheckBox1Click
    end
    object CheckBox2: TCheckBox
      Left = 1094
      Top = 24
      Width = 97
      Height = 17
      Caption = 'Series2'
      Checked = True
      State = cbChecked
      TabOrder = 7
      OnClick = CheckBox2Click
    end
    object CheckBox3: TCheckBox
      Left = 1094
      Top = 41
      Width = 97
      Height = 17
      Caption = 'Series3'
      Checked = True
      State = cbChecked
      TabOrder = 8
      OnClick = CheckBox3Click
    end
    object CheckBox4: TCheckBox
      Left = 507
      Top = 14
      Width = 97
      Height = 17
      Caption = #21345#32819#26364#28388#27874
      TabOrder = 9
      Visible = False
    end
    object Edit1: TEdit
      Left = 599
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 10
      Text = '0.02'
      Visible = False
    end
    object Edit2: TEdit
      Left = 697
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 11
      Text = '0'
      Visible = False
    end
    object Edit3: TEdit
      Left = 599
      Top = 39
      Width = 50
      Height = 21
      TabOrder = 12
      Text = '0'
      Visible = False
    end
    object Edit4: TEdit
      Left = 697
      Top = 39
      Width = 50
      Height = 21
      TabOrder = 13
      Text = '0.001'
      Visible = False
    end
    object Edit5: TEdit
      Left = 792
      Top = 39
      Width = 50
      Height = 21
      TabOrder = 14
      Text = '0.543'
      Visible = False
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 446
    Top = 293
  end
end
